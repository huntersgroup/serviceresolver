**ServiceResolver.Ioc.SimpleInjector** is the implementation of **ServiceResolver.Ioc** framework, which uses SimpleInjector as underlying injection engine.

for details, see the [documentation](https://bitbucket.org/huntersgroup/serviceresolver/wiki/SimpleInjector).
