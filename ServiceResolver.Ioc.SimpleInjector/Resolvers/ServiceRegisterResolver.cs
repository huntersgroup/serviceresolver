using System;
using System.Diagnostics;
using ServiceResolver.Ioc.Exceptions;
using ServiceResolver.Ioc.Extensions;
using ServiceResolver.Ioc.Resolvers;

namespace ServiceResolver.Ioc.SimpleInjector.Resolvers;

/// <summary>
/// Represents a generic way to registering a component into isolated / internal container.
/// </summary>
/// <typeparam name="TCService">The type of the service.</typeparam>
[DebuggerDisplay("serviceType: {this.ServiceType}")]
public class ServiceRegisterResolver<TCService>
    : ServiceRegisterImpl, IServiceResolver<TCService>
    where TCService : class
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ServiceRegisterResolver{TCService}"/> class.
    /// </summary>
    /// <param name="serviceType"></param>
    /// <param name="descriptor"></param>
    internal ServiceRegisterResolver(Type serviceType, ServiceRegisterDescriptor descriptor)
        : base(descriptor)
    {
        this.ServiceType = serviceType ?? typeof(TCService);
    }

    /// <inheritdoc />
    public TCService Resolve()
    {
        try
        {
            return this.Container.GetInstance(this.ServiceType) as dynamic;
        }
        catch (Exception ex)
        {
            throw new ServiceUnavailableException(this.ServiceType, ex.Message, ex);
        }
    }

    /// <inheritdoc />
    public Type ServiceType { get; }

    /// <inheritdoc />
    object IServiceResolver.GetService(Type compatibleType)
    {
        try
        {
            if (compatibleType == null)
            {
                return this.Container.GetInstance(this.ServiceType);
            }

            if (!this.ServiceType.IsCompatible(compatibleType))
            {
                throw new InvalidOperationException("The current service type is open generic, so the service type argument must be compatible.");
            }

            return this.ServiceType.IsGenericTypeDefinition ? this.Container.GetInstance(compatibleType) : this.Container.GetInstance(this.ServiceType);
        }
        catch (Exception ex)
        {
            throw new ServiceUnavailableException(this.ServiceType, ex.Message, ex);
        }
    }
}
