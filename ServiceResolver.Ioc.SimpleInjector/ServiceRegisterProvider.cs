using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ServiceResolver.Ioc.Exceptions;
using ServiceResolver.Ioc.Extensions;
using ServiceResolver.Ioc.Modules;
using ServiceResolver.Ioc.SimpleInjector.Extensions;
using SimpleInjector;

namespace ServiceResolver.Ioc.SimpleInjector;

/// <summary>
/// 
/// </summary>
public class ServiceRegisterProvider : ServiceRegisterImpl, IServiceRegisterProvider
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ServiceRegisterProvider"/> class, using a custom descriptor.
    /// </summary>
    /// <param name="descriptor"></param>
    public ServiceRegisterProvider(ServiceRegisterDescriptor descriptor)
        : base(descriptor)
    {
    }

    /// <inheritdoc />
    /// <exception cref="ArgumentNullException">module;The given module to install into this container cannot be null.</exception>
    public void Register(IServiceModule module)
    {
        if (module == null)
        {
            throw new ArgumentNullException(nameof(module), "The given module to install into this container cannot be null.");
        }

        module.Initialize(this);
    }

    /// <inheritdoc />
    /// <exception cref="T:System.ArgumentNullException">assembly;The given assembly to use for loading service is null.</exception>
    public void RegisterModulesFrom(Assembly assembly, bool includeObsoleted = false)
    {
        var typeModules = assembly.GetDerivedTypesFrom(typeof(IServiceModule),
            new TypeResolutionOptions {IncludeGenericTypeDefinitions = false, IncludeObsoleted = includeObsoleted});
            
        foreach (var moduleType in typeModules)
        {
            try
            {
                if (Activator.CreateInstance(moduleType) is IServiceModule module)
                {
                    this.Register(module);
                }
            }
            catch (Exception ex)
            {
                throw new ModuleRegistrationException(moduleType, "Error on registering the given service module, see inner exception for details.", ex);
            }
        }
    }

    /// <inheritdoc />
    public IServiceRegisterProvider BuildChildServiceProvider()
    {
        return new ServiceRegisterProvider(new ServiceRegisterDescriptor
        {
            Initializer = container => this.InitializeContainer(container, this.Container)
        });
    }
    
    /// <inheritdoc />
    /// <exception cref="DiagnosticContainerException">The internal container has some errors.</exception>
    public void Verify()
    {
        try
        {
            this.Container.Verify(VerificationOption.VerifyOnly);
        }
        catch (Exception ex)
        {
            throw new DiagnosticContainerException("The internal container has some errors, see inner exception for details.", ex);
        }

        foreach (var (key, impl) in this.GetAllRegisterContexts())
        {
            try
            {
                impl.Container.Verify(VerificationOption.VerifyOnly);
            }
            catch (Exception ex)
            {
                throw new DiagnosticContainerException($"The internal container for this context (value: {key}) has some errors, see inner exception for details.", ex);
            }
        }

        var exceptions = new List<Exception>();

        IScope currentScope = null;

        if (this.Container.MustExistsAnScope() && this.Container.GetCurrentScope() == null)
        {
            currentScope = this.BeginScope();
        }

        using (currentScope)
        {
            IServiceProvider provider = this;

            foreach (var serviceResolver in this.GetResolvers())
            {
                try
                {
                    // todo: this is a temporary workaround..
                    if (!serviceResolver.ServiceType.IsGenericTypeDefinition)
                    {
                        provider.GetService(serviceResolver.ServiceType);
                    }
                }
                catch (Exception ex)
                {
                    exceptions.Add(ex);
                }
            }
        }

        if (exceptions.Any())
        {
            throw new DiagnosticContainerException("Some services cannot be resolved, see inner exception for details.", exceptions);
        }
    }
}
