using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using SimpleInjector;
using SimpleInjector.Advanced;

namespace ServiceResolver.Ioc.SimpleInjector.Advanced;

/// <summary>
/// Represents a custom implementation of <see cref="IConstructorResolutionBehavior"/> contract to find a suitable constructor with the most resolvable parameters.
/// <para>
/// This implementation is not intended to use as a default, but It's useful whenever it's needed to integrate with legacy services which exposes many public constructors.
/// </para>
/// </summary>
public class BestConstructorResolutionBehavior
    : IConstructorResolutionBehavior
{
    private readonly Container container;

    private const string MsgNoPublicConstructor = ", no public constructors are available.";
    private const string MsgAmbiguousConstructors = ", there are some ambiguous constructors, see the following details:";
    private const string MsgDependenciesNotRegistered = ", there are some constructors with some dependencies not registered, see details:";

    /// <summary>
    /// 
    /// </summary>
    /// <param name="container"></param>
    public BestConstructorResolutionBehavior(Container container)
    {
        this.container = container;
    }

    /// <summary>
    /// 
    /// </summary>
    private bool IsRegistrationPhase => !this.container.IsLocked;

    //[DebuggerStepThrough]
    /// <summary>
    /// 
    /// </summary>
    /// <param name="type"></param>
    /// <param name="errorMessage"></param>
    /// <returns></returns>
    public ConstructorInfo TryGetConstructor(Type type, out string errorMessage)
    {
        VerifyConcreteType(type);

        errorMessage = null;
        var typeName = type.FullName;

        var constructors = this.GetConstructors(type).ToList();
        var msgBuffer = new StringBuilder()
            .Append($"The configuration is invalid for the type '{typeName}'");

        if (!constructors.Any())
        {
            errorMessage = msgBuffer.AppendLine(MsgNoPublicConstructor).ToString();
            return null;
        }

        var grouping =
            GetBestConstructors(constructors, ctorDesc => ctorDesc.Parameters.All(arg1 => arg1.CanBeResolved))
            ?? GetBestConstructors(constructors, ctorDesc => ctorDesc.Parameters.All(arg1 => arg1.CanBeResolved || arg1.Value.IsOptional))
            ?? [];

        if (grouping.Any())
        {
            var bestCtorGroup = grouping.First();

            // constructors with the same number of parameters, and resolvable, but ambiguous. 
            if (bestCtorGroup.Count() > 1)
            {
                msgBuffer.AppendLine(MsgAmbiguousConstructors)
                    .BuildCtorMessages(bestCtorGroup);

                errorMessage = msgBuffer.ToString();
                return null;
            }

            return bestCtorGroup.First().Value;
        }

        if (this.IsRegistrationPhase)
        {
            return constructors.First().Value;
        }

        msgBuffer.AppendLine(MsgDependenciesNotRegistered)
            .BuildCtorMessagesWithDetails(constructors);

        errorMessage = msgBuffer.ToString();

        return null;
    }

    private static List<IGrouping<int, ConstructorDescriptor>> GetBestConstructors(IEnumerable<ConstructorDescriptor> constructors, Func<ConstructorDescriptor, bool> predicate)
    {
        var ret = constructors
            .Where(ctorDesc => ctorDesc.Parameters.Count == 0 || predicate(ctorDesc))
            .GroupBy(ctorDesc => ctorDesc.Parameters.Count)
            .OrderByDescending(grouping => grouping.Key)
            .ToList();

        return ret.Any() ? ret : null;
    }

    private IEnumerable<ConstructorDescriptor> GetConstructors(Type implementation)
    {
        var constructorsInfo = implementation.GetConstructors()
            .Select(info =>
            {
                var parameters = info.GetParameters();
                return new ConstructorDescriptor
                {
                    Value = info,
                    Parameters = parameters.Select(parameterInfo => new ParameterDescriptor
                    {
                        Value = parameterInfo,
                        CanBeResolved = this.CanBeResolved(parameterInfo)
                    }).ToList()
                };
            })
            .OrderByDescending(grouping => grouping.Parameters.Count);

        return constructorsInfo;
    }
    
    private bool CanBeResolved(ParameterInfo parameter)
    {
        if (ContainsGenericParameter(parameter.ParameterType))
        {
            return true;
        }

        return this.GetInstanceProducerFor(new InjectionConsumerInfo(parameter)) != null;
    }

    private InstanceProducer GetInstanceProducerFor(InjectionConsumerInfo injectionInfo)
    {
        return this.container.Options.DependencyInjectionBehavior.GetInstanceProducer(injectionInfo, false);
    }

    private static bool ContainsGenericParameter(Type type)
    {
        return type.IsGenericTypeParameter || (type.IsGenericType && type.GetGenericArguments().Any(ContainsGenericParameter));
    }

    private static void VerifyConcreteType(Type type)
    {
        if (!type.IsConcreteType())
        {
            throw new ActivationException($"The given type should be concrete, other types aren't allowed in this context, type: {type.FullName}");
        }
    }
}
