using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace ServiceResolver.Ioc.SimpleInjector.Advanced;

internal static class CommonExtensions
{
    internal static StringBuilder BuildCtorMessagesWithDetails(this StringBuilder msgBuffer, IEnumerable<ConstructorDescriptor> ctorDescs)
    {
        foreach (var ctorDesc in ctorDescs)
        {
            msgBuffer.BuildCtorMessageWithDetails(ctorDesc);
        }

        return msgBuffer;
    }

    internal static StringBuilder BuildCtorMessageWithDetails(this StringBuilder msgBuffer, ConstructorDescriptor ctorDesc)
    {
        msgBuffer.BuildCtorMessage(ctorDesc);

        ctorDesc.Parameters
            .ForEach(desc =>
            {
                msgBuffer.AppendLine($"  - parameter: [name= {desc.Value.Name}, registered={desc.CanBeResolved}, type={desc.Value.ParameterType}]");
            });

        return msgBuffer;
    }

    internal static StringBuilder BuildCtorMessages(this StringBuilder msgBuffer, IEnumerable<ConstructorDescriptor> ctorDescs)
    {
        foreach (var ctorDesc in ctorDescs)
        {
            msgBuffer.BuildCtorMessage(ctorDesc);
        }

        return msgBuffer;
    }

    internal static StringBuilder BuildCtorMessage(this StringBuilder msgBuffer, ConstructorDescriptor ctorDesc)
    {
        msgBuffer.AppendLine($"- constructor: {ctorDesc.Value.ToString()}");

        return msgBuffer;
    }

    internal static bool IsConcreteType(this Type serviceType)
    {
        return !serviceType.GetTypeInfo().IsAbstract && !serviceType.IsArray && serviceType != typeof(object) && !typeof(Delegate).IsAssignableFrom(serviceType);
    }
}
