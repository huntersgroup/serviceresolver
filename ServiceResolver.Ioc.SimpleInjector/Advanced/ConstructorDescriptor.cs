using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;

namespace ServiceResolver.Ioc.SimpleInjector.Advanced;

/// <summary>
/// 
/// </summary>
[DebuggerDisplay("ctor: {Value}, parameters.count: {Parameters.Count}")]
public class ConstructorDescriptor
{
    /// <summary>
    /// 
    /// </summary>
    public ConstructorInfo Value { get; init; }

    /// <summary>
    /// 
    /// </summary>
    public List<ParameterDescriptor> Parameters { get; init; } = new();
}

/// <summary>
/// 
/// </summary>
[DebuggerDisplay("name: {Value.Name}, type: {Value.ParameterType}, optional: {Value.IsOptional}, canBeResolved: {CanBeResolved}")]
public class ParameterDescriptor
{
    /// <summary>
    /// 
    /// </summary>
    public ParameterInfo Value { get; init; }

    /// <summary>
    /// 
    /// </summary>
    public bool CanBeResolved { get; init; }
}
