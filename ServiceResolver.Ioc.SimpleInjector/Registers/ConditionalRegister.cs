using System;
using ServiceResolver.Ioc.Exceptions;
using ServiceResolver.Ioc.Registers;
using SimpleInjector;

namespace ServiceResolver.Ioc.SimpleInjector.Registers;

/// <summary>
/// A custom implementation of <see cref="IConditionalRegister{TService}"/> for SimpleInjector
/// </summary>
/// <typeparam name="TService"></typeparam>
public class ConditionalRegister<TService> : IConditionalRegister<TService>, IConditionalRegister
    where TService : class
{
    private readonly IGenericServiceProvider serviceProvider;
    private readonly Container container;
    private bool locked;

    /// <summary>
    /// Creates a new instance of <see cref="ConditionalRegister{TService}"/>
    /// </summary>
    /// <param name="serviceProvider"></param>
    /// <param name="container"></param>
    /// <param name="serviceType"></param>
    public ConditionalRegister(IGenericServiceProvider serviceProvider, Container container, Type serviceType = null)
    {
        this.ServiceType = serviceType ?? typeof(TService);
        this.serviceProvider = serviceProvider;
        this.container = container;
    }

    /// <inheritdoc />
    public Type ServiceType { get; }

    /// <inheritdoc />
    public IConditionalRegister<TService> Register<TImplementor>(Func<ConsumerService, bool> condition, LifetimeScope? scope = null)
        where TImplementor : class, TService
    {
        this.Register(typeof(TImplementor), condition, scope);

        return this;
    }

    /// <inheritdoc />
    public IConditionalRegister Register(Type implementorType, Func<ConsumerService, bool> condition, LifetimeScope? scope = null)
    {
        if (scope.HasValue)
        {
            this.container.RegisterConditional(this.ServiceType, implementorType, scope.Value.Translate(), context =>
                context.HasConsumer && condition(new ConsumerService(context.Consumer.ImplementationType)));
        }
        else
        {
            this.container.RegisterConditional(this.ServiceType, implementorType, context =>
                context.HasConsumer && condition(new ConsumerService(context.Consumer.ImplementationType)));
        }

        return this;
    }

    /// <inheritdoc />
    [Obsolete("Use the other overload which these args: [serviceFactory, condition and scope]. This method will be removed on next major version.")]
    public IConditionalRegister<TService> Register(Func<ConsumerService, bool> condition, Func<IGenericServiceProvider, TService> serviceFactory, LifetimeScope? scope = null)
    {
        IConditionalRegister register = this;

        register.Register(serviceFactory, condition, scope);

        return this;
    }

    /// <inheritdoc />
    public IConditionalRegister<TService> Register(Func<IGenericServiceProvider, TService> serviceFactory, Func<ConsumerService, bool> condition, LifetimeScope? scope = null)
    {
        IConditionalRegister register = this;

        register.Register(serviceFactory, condition, scope);

        return this;
    }

    /// <inheritdoc />
    public IConditionalRegister Register(Func<IGenericServiceProvider, object> serviceFactory, Func<ConsumerService, bool> condition, LifetimeScope? scope = null)
    {
        var lifetime = scope.HasValue ? scope.Value.Translate() : this.container.Options.DefaultLifestyle;

        var registration = lifetime.CreateRegistration(this.ServiceType, () => serviceFactory(this.serviceProvider), this.container);

        this.container.RegisterConditional(this.ServiceType, registration,
            context => context.HasConsumer && condition(new ConsumerService(context.Consumer.ImplementationType)));

        return this;
    }

    /// <inheritdoc />
    public void Default<TImplementor>(LifetimeScope? scope = null)
        where TImplementor : class, TService
    {
        this.Default(typeof(TImplementor), scope);
    }

    /// <inheritdoc />
    public void Default(Type implementorType, LifetimeScope? scope = null)
    {
        this.ThrowIfLocked();

        if (scope.HasValue)
        {
            this.container.RegisterConditional(this.ServiceType, implementorType, scope.Value.Translate(), context => !context.Handled);
        }
        else
        {
            this.container.RegisterConditional(this.ServiceType, implementorType, context => !context.Handled);
        }

        this.locked = true;
    }

    /// <inheritdoc />
    public void Default(Func<IGenericServiceProvider, TService> serviceFactory, LifetimeScope? scope = null)
    {
        IConditionalRegister register = this;

        register.Default(serviceFactory, scope);
    }

    /// <inheritdoc />
    public void Default(Func<IGenericServiceProvider, object> serviceFactory, LifetimeScope? scope = null)
    {
        this.ThrowIfLocked();

        var lifetime = scope.HasValue ? scope.Value.Translate() : this.container.Options.DefaultLifestyle;

        var registration = lifetime.CreateRegistration(this.ServiceType, () => serviceFactory(this.serviceProvider), this.container);

        this.container.RegisterConditional(this.ServiceType, registration, context => !context.Handled);

        this.locked = true;
    }

    private void ThrowIfLocked()
    {
        if (this.locked)
        {
            throw new ServiceRegistrationException(this.ServiceType, $"The conditional registration for '{this.ServiceType.FullName}' was locked, due to a previous default registration.");
        }
    }
}
