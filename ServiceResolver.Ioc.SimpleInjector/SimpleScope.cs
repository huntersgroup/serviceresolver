using System;
using SimpleInjector;

namespace ServiceResolver.Ioc.SimpleInjector;

/// <summary>
/// 
/// </summary>
/// <param name="scope"></param>
internal class SimpleScope(Scope scope) : IScope
{
    ///<inheritdoc />
    public string Id { get; } = Guid.NewGuid().ToString("N");

    ///<inheritdoc />
    public bool IsDisposed { get; private set; }

    ///<inheritdoc />
    public void Dispose()
    {
        if (this.IsDisposed)
        {
            return;
        }

        this.IsDisposed = true;
        scope.Dispose();
    }
}
