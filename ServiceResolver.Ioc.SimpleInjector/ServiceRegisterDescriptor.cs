using System;
using ServiceResolver.Ioc.Resolvers;
using SimpleInjector;

namespace ServiceResolver.Ioc.SimpleInjector;

/// <summary>
/// Represents a set of definitions for each service providers.
/// </summary>
public class ServiceRegisterDescriptor
{
    /// <summary>
    /// 
    /// </summary>
    public ServiceRegisterDescriptor()
    {
        this.DeduceScopeFactory = true;
    }

    /// <summary>
    /// A custom <see cref="Container"/> initializer.
    /// </summary>
    public Action<Container> Initializer { get; set; }

    /// <summary>
    /// Gets true if the Scope factory function could be deduced (built) by the current <see cref="ContainerOptions.DefaultScopedLifestyle"/> value, on current <see cref="Container"/> instance.
    /// <para>
    /// When this value is true, the property <see cref="ScopeFactory"/> is computed if its value is null.
    /// This value is checked on validation, and It must be set in order to work with <see cref="IServiceResolver{TCService}"/> registrations.
    /// </para>
    /// </summary>
    public bool DeduceScopeFactory { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="Scope"/> delegates used to build scopes for <see cref="IServiceResolver"/> instances, whenever services are registered into as <see cref="LifetimeScope.Scoped"/>
    /// </summary>
    public Func<Container, Scope> ScopeFactory { get; set; }
}
