using System;
using System.Collections.Generic;
using SimpleInjector;
using SimpleInjector.Diagnostics;

namespace ServiceResolver.Ioc.SimpleInjector.Extensions;

/// <summary>
/// 
/// </summary>
public static class ContainerExtensions
{
    private static readonly IDictionary<Type, Func<Container, Scope>> ScopeFactories;

    static ContainerExtensions()
    {
        ScopeFactories = new Dictionary<Type, Func<Container, Scope>>();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="container"></param>
    /// <param name="rootContainer"></param>
    public static void InheritLifeStylesFrom(this Container container, Container rootContainer)
    {
        var rootOptions = rootContainer.Options;
        var options = container.Options;
        
        if (rootOptions.DefaultScopedLifestyle != null)
        {
            options.DefaultScopedLifestyle = rootOptions.DefaultScopedLifestyle;
        }

        options.DefaultLifestyle = rootOptions.DefaultLifestyle;

        // other options to inherent from root
        options.AllowOverridingRegistrations = rootOptions.AllowOverridingRegistrations;
        options.SuppressLifestyleMismatchVerification = rootOptions.SuppressLifestyleMismatchVerification;
        options.UseStrictLifestyleMismatchBehavior = rootOptions.UseStrictLifestyleMismatchBehavior;
        options.UseFullyQualifiedTypeNames = rootOptions.UseFullyQualifiedTypeNames;
        options.ResolveUnregisteredConcreteTypes = rootOptions.ResolveUnregisteredConcreteTypes;
        
        // these dependencies cannot be assigned directly due to its dependencies, it requires a factory / delegate.
        //options.ConstructorResolutionBehavior = rootOptions.ConstructorResolutionBehavior;
        //options.DependencyInjectionBehavior = rootOptions.DependencyInjectionBehavior;
        //options.PropertySelectionBehavior = rootOptions.PropertySelectionBehavior;
        //options.LifestyleSelectionBehavior = rootOptions.LifestyleSelectionBehavior;
        //options.ExpressionCompilationBehavior = rootOptions.ExpressionCompilationBehavior;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="container"></param>
    /// <returns></returns>
    public static bool MustExistsAnScope(this Container container)
    {
        return container.Options.DefaultScopedLifestyle != null;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="container"></param>
    /// <returns></returns>
    public static Scope GetCurrentScope(this Container container)
    {
        return container.Options.DefaultScopedLifestyle?.GetCurrentScope(container);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serviceRegister"></param>
    /// <param name="serviceType"></param>
    /// <param name="registrationInfo"></param>
    /// <returns></returns>
    public static Registration CreateRegistration(this ServiceRegisterImpl serviceRegister, Type serviceType, IRegistrationInfo registrationInfo)
    {
        Registration registration;

        switch (registrationInfo.Scope)
        {
            case LifetimeScope.Singleton:
            {
                registration = registrationInfo.Factory is not null 
                    ? Lifestyle.Singleton.CreateRegistration(serviceType, () => registrationInfo.Factory(serviceRegister), serviceRegister.Container)
                    : Lifestyle.Singleton.CreateRegistration(registrationInfo.ConcreteType, serviceRegister.Container);

                break;
            }
            case LifetimeScope.Scoped:
            {
                registration = registrationInfo.Factory is not null
                    ? Lifestyle.Scoped.CreateRegistration(serviceType, () => registrationInfo.Factory(serviceRegister), serviceRegister.Container)
                    : Lifestyle.Scoped.CreateRegistration(registrationInfo.ConcreteType, serviceRegister.Container);

                break;
            }
            default:
            {
                registration = registrationInfo.Factory is not null
                    ? Lifestyle.Transient.CreateRegistration(serviceType, () => registrationInfo.Factory(serviceRegister), serviceRegister.Container)
                    : Lifestyle.Transient.CreateRegistration(registrationInfo.ConcreteType, serviceRegister.Container);

                break;
            }
        }

        var diagnostic = registrationInfo.SuppressDiagnostic;

        if (diagnostic is not null)
        {
            if (diagnostic.DisposableTransientComponent)
            {
                registration.SuppressDiagnosticWarning(DiagnosticType.DisposableTransientComponent, $"{nameof(diagnostic.DisposableTransientComponent)} changed by consumer.");
            }

            if (diagnostic.LifestyleMismatch)
            {
                registration.SuppressDiagnosticWarning(DiagnosticType.LifestyleMismatch, $"{nameof(diagnostic.LifestyleMismatch)} changed by consumer.");
            }
        }

        return registration;
    }

    /// <summary>
    /// Register the given <see cref="IRegistrationInfo"/> instance into the underlying container.
    /// </summary>
    /// <param name="serviceRegister"></param>
    /// <param name="serviceType"></param>
    /// <param name="registrationInfo"></param>
    public static void AddRegistration(this ServiceRegisterImpl serviceRegister, Type serviceType, IRegistrationInfo registrationInfo)
    {
        var container = serviceRegister.Container;

        if (serviceType.IsGenericTypeDefinition)
        {
            // The value of registrationInfo.Factory property must be null, impossible having a Func<IGenericServiceProvider, object> for open-generics.
            container.Register(serviceType, registrationInfo.ConcreteType, registrationInfo.Scope.Translate());
        }
        else
        {
            var diagnostic = registrationInfo.SuppressDiagnostic;
            var withDiagnostic = diagnostic != null
                                   && (diagnostic.LifestyleMismatch || diagnostic.DisposableTransientComponent);

            if (withDiagnostic)
            {
                // It doesn't throw an exception if the constructor is ambiguous !!!
                var registration = serviceRegister.CreateRegistration(serviceType, registrationInfo);
                container.AddRegistration(serviceType, registration);
            }
            else
            {
                var scope = registrationInfo.Scope.Translate();

                if (registrationInfo.Factory is not null)
                {
                    container.Register(serviceType, () => registrationInfo.Factory(serviceRegister), scope);
                }
                else
                {
                    container.Register(serviceType, registrationInfo.ConcreteType, scope);
                }
            }

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="lifestyle"></param>
    /// <returns></returns>
    internal static Func<Container, Scope> GetFactoryScope(this ScopedLifestyle lifestyle)
    {
        if (lifestyle == null)
        {
            return null;
        }

        var type = lifestyle.GetType();

        if (ScopeFactories.TryGetValue(type, out var factoryScope))
        {
            return factoryScope;
        }

        var method = type.GetMethod("BeginScope", [typeof(Container)]);

        if (method == null)
        {
            return null;
        }

        var func = (Func<Container, Scope>)Delegate.CreateDelegate(typeof(Func<Container, Scope>), method);
        ScopeFactories.Add(type, func);

        return func;

    }
}
