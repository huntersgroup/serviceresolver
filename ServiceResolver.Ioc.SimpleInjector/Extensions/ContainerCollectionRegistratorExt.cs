using System;
using System.Collections.Generic;
using System.Reflection;
using SimpleInjector;

namespace ServiceResolver.Ioc.SimpleInjector.Extensions;

/// <summary>
/// 
/// </summary>
public static class ContainerCollectionRegistratorExt
{
    private static readonly MethodInfo CreateRegistrationForRegistrationsOpenMethodInfo;
    private static readonly IDictionary<Type, MethodInfo> CreateRegistrationForRegistrationsClosedMethods;

    private static readonly MethodInfo CreateRegistrationForTypesOpenMethodInfo;
    private static readonly IDictionary<Type, MethodInfo> CreateRegistrationForTypesClosedMethods;

    /// <summary>
    /// 
    /// </summary>
    static ContainerCollectionRegistratorExt()
    {
        var containerCollectionType = typeof(ContainerCollectionRegistrator);

        CreateRegistrationForRegistrationsOpenMethodInfo = containerCollectionType
            .GetMethod(nameof(ContainerCollectionRegistrator.CreateRegistration), [typeof(IEnumerable<Registration>)]);

        CreateRegistrationForRegistrationsClosedMethods = new Dictionary<Type, MethodInfo>();

        CreateRegistrationForTypesOpenMethodInfo = containerCollectionType
            .GetMethod(nameof(ContainerCollectionRegistrator.CreateRegistration), [typeof(IEnumerable<Type>)]);

        CreateRegistrationForTypesClosedMethods = new Dictionary<Type, MethodInfo>();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="register"></param>
    /// <param name="serviceType"></param>
    /// <param name="registrations"></param>
    /// <returns></returns>
    public static Registration CreateRegistration(this ContainerCollectionRegistrator register, Type serviceType, IEnumerable<Registration> registrations)
    {
        if (!CreateRegistrationForRegistrationsClosedMethods.TryGetValue(serviceType, out var method))
        {
            method = CreateRegistrationForRegistrationsOpenMethodInfo.MakeGenericMethod([serviceType]);
            CreateRegistrationForRegistrationsClosedMethods.Add(serviceType, method);
        }

        dynamic ret = method.Invoke(register, [registrations]);

        return ret;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="register"></param>
    /// <param name="serviceType"></param>
    /// <param name="serviceTypes"></param>
    /// <returns></returns>
    public static Registration CreateRegistration(this ContainerCollectionRegistrator register, Type serviceType, IEnumerable<Type> serviceTypes)
    {
        if (!CreateRegistrationForTypesClosedMethods.TryGetValue(serviceType, out var method))
        {
            method = CreateRegistrationForTypesOpenMethodInfo.MakeGenericMethod([serviceType]);
            CreateRegistrationForTypesClosedMethods.Add(serviceType, method);
        }

        dynamic ret = method.Invoke(register, [serviceTypes]);

        return ret;
    }
}
