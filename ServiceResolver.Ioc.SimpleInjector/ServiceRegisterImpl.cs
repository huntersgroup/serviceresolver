using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ServiceResolver.Ioc.Caching;
using ServiceResolver.Ioc.Exceptions;
using ServiceResolver.Ioc.Extensions;
using ServiceResolver.Ioc.Registers;
using ServiceResolver.Ioc.Resolvers;
using ServiceResolver.Ioc.SimpleInjector.Extensions;
using ServiceResolver.Ioc.SimpleInjector.Registers;
using ServiceResolver.Ioc.SimpleInjector.Resolvers;
using SimpleInjector;

namespace ServiceResolver.Ioc.SimpleInjector;

/// <summary>
/// 
/// </summary>
/// <seealso cref="IServiceRegister" />
/// <seealso cref="IGenericServiceProvider" />
public class ServiceRegisterImpl : IServiceRegister, IGenericServiceProvider, IDisposable
{
    private readonly ServiceCache cache;
    private readonly Func<Container, Scope> scopeFactory;
    private readonly ICollection<IServiceResolver> resolvers;
    private readonly HashSet<SingleRegistration> singleLazyRegistrations;
    private readonly HashSet<ServiceKey> singleRegistrations;
    private readonly HashSet<ServiceKey> manyRegistrations;
    private readonly HashSet<ManyRegistration> manyLazyRegistrations;
    private readonly Dictionary<object, ServiceRegisterImpl> contextRegisters;
    private readonly LifetimeScope lifetimeDefault;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="descriptor"></param>
    protected ServiceRegisterImpl(ServiceRegisterDescriptor descriptor)
    {
        this.Container = new Container();
        this.cache = new ServiceCache();
        this.resolvers = [];
        this.singleLazyRegistrations = [];
        this.singleRegistrations = [];
        this.manyRegistrations = [];
        this.manyLazyRegistrations = [];
        this.contextRegisters = [];

        this.Container.ResolveUnregisteredType += this.OnResolveUnregisteredTypeOnResolvers;
        this.Container.ResolveUnregisteredType += this.OnResolveUnregisteredLazyTypes;

        if (descriptor == null)
        {
            return;
        }

        descriptor.Initializer?.Invoke(this.Container);

        if (descriptor.ScopeFactory == null)
        {
            if (descriptor.DeduceScopeFactory)
            {
                this.scopeFactory = this.Container.Options.DefaultScopedLifestyle.GetFactoryScope();
            }
        }
        else
        {
            this.scopeFactory = descriptor.ScopeFactory;
        }

        this.lifetimeDefault = this.Container.Options.DefaultLifestyle.Translate();
    }

    /// <inheritdoc />
    public IServiceCache Cache { get => this.cache; }

    /// <summary>
    /// Gets the container.
    /// </summary>
    /// <value>
    /// The container.
    /// </value>
    protected internal Container Container { get; }

    /// <inheritdoc />
    public IScope BeginScope()
    {
        if (this.scopeFactory == null)
        {
            throw new InvalidOperationException("No scope factory available, in order to use Scopes you need to specify a Scope factory on ServiceRegisterDescriptor instance.");
        }

        return new SimpleScope(this.scopeFactory.Invoke(this.Container));
    }

    /// <inheritdoc />
    public IServiceRegister Register(ServiceKey svcKey, IRegistrationInfo registrationInfo)
    {
        this.ThrowExceptionIfRegistered(svcKey);
        var svcRegister = this.GetOrCreateServiceRegister(svcKey.Key);
        
        registrationInfo.Validate(svcKey.Type);

        try
        {
            svcRegister.AddRegistration(svcKey.Type, registrationInfo);
            svcRegister.singleRegistrations.Add(svcKey);

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(svcKey.Type, "The given service cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    public IServiceRegister Register(Type serviceType, IRegistrationInfo registrationInfo)
    {
        return this.Register(new ServiceKey(serviceType), registrationInfo);
    }

    /// <inheritdoc />
    public IServiceRegister Register<TService>(IRegistrationInfo<TService> registrationInfo)
        where TService : class
    {
        return this.Register(typeof(TService), registrationInfo);
    }

    /// <inheritdoc />
    public IServiceRegister Register<TService, TImplementor>(IRegistrationInfo<TImplementor> registrationInfo)
        where TService : class where TImplementor : class, TService
    {
        return this.Register(typeof(TService), registrationInfo);
    }

    /// <inheritdoc />
    public IServiceRegister RegisterLazy(ServiceKey svcKey, IRegistrationInfo registrationInfo)
    {
        this.ThrowExceptionIfRegistered(svcKey);
        var svcRegister = this.GetOrCreateServiceRegister(svcKey.Key);
        
        registrationInfo.Validate(svcKey.Type);

        try
        {
            svcRegister.singleLazyRegistrations.Add(new SingleRegistration
            {
                ServiceKey = svcKey,
                Registration = registrationInfo
            });

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(svcKey.Type, "The given service cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException"></exception>
    public IServiceRegister RegisterLazy(Type serviceType, IRegistrationInfo registrationInfo)
    {
        return this.RegisterLazy(new ServiceKey(serviceType), registrationInfo);
    }

    /// <inheritdoc />
    public IServiceRegister RegisterLazy<TService>(IRegistrationInfo<TService> registrationInfo)
        where TService : class
    {
        return this.RegisterLazy(typeof(TService), registrationInfo);
    }

    /// <inheritdoc />
    public IServiceRegister RegisterLazy<TService, TImplementor>(IRegistrationInfo<TImplementor> registrationInfo)
        where TService : class where TImplementor : class, TService
    {
        return this.RegisterLazy(typeof(TService), registrationInfo);
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">The given service cannot be registered, see inner exception for details.</exception>
    public IServiceRegister Register(Type serviceType, Type implementorType, LifetimeScope? scope = null)
    {
        var regInfo = (scope ?? this.lifetimeDefault).CreateRegistration(implementorType);
        return this.Register(serviceType, regInfo);
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">The given service cannot be registered, see inner exception for details.</exception>
    public IServiceRegister Register(Type serviceType, Func<object> serviceFactory, LifetimeScope? scope = null)
    {
        return this.Register(serviceType, _ => serviceFactory.Invoke(), scope);
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">The given service cannot be registered, see inner exception for details.</exception>
    public IServiceRegister Register(Type serviceType, Func<IGenericServiceProvider, object> serviceCreator, LifetimeScope? scope = null)
    {
        var regInfo = (scope ?? this.lifetimeDefault).CreateRegistration(serviceCreator);
        
        return this.Register(serviceType, regInfo);
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">The given service cannot be registered, see inner exception for details.</exception>
    public IServiceRegister Register<TService, TImplementor>(LifetimeScope? scope = null)
        where TService : class where TImplementor : class, TService
    {
        return this.Register(typeof(TService), typeof(TImplementor), scope);
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">The given service cannot be registered, see inner exception for details.</exception>
    public IServiceRegister Register<TConcreteService>(LifetimeScope? scope = null)
        where TConcreteService : class
    {
        return this.Register(typeof(TConcreteService), scope);
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">The given service cannot be registered, see inner exception for details.</exception>
    public IServiceRegister Register(Type implementorType, LifetimeScope? scope = null)
    {
        return this.Register(implementorType, implementorType, scope);
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">The given service cannot be registered, see inner exception for details.</exception>
    public IServiceRegister Register<TService>(Func<TService> serviceCreator, LifetimeScope? scope = null)
        where TService : class
    {
        return this.Register(_ => serviceCreator.Invoke(), scope);
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">The given service cannot be registered, see inner exception for details.</exception>
    public IServiceRegister Register<TService>(Func<IGenericServiceProvider, TService> serviceCreator, LifetimeScope? scope = null)
        where TService : class
    {
        var serviceType = typeof(TService);
        var regInfo = (scope ?? this.lifetimeDefault).CreateRegistration(serviceCreator);
        
        return this.Register(serviceType, regInfo);
    }

    /// <inheritdoc />
    public IServiceRegister RegisterDecorator<TService, TDecorator>(LifetimeScope? scope = null)
        where TService : class
        where TDecorator : class, TService
    {
        return this.RegisterDecorator(typeof(TService), typeof(TDecorator), scope);
    }

    /// <inheritdoc />
    public IServiceRegister RegisterDecorator(Type serviceType, Type decoratorType, LifetimeScope? scope = null)
    {
        try
        {
            if (scope.HasValue)
            {
                this.Container.RegisterDecorator(serviceType, decoratorType, scope.Value.Translate());
            }
            else
            {
                this.Container.RegisterDecorator(serviceType, decoratorType);
            }

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(serviceType, "The given service cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">The given service cannot register initializer, see inner exception for details.</exception>
    public IServiceRegister RegisterConditional<TService>(Action<IConditionalRegister<TService>> conditionalRegister)
        where TService : class
    {
        var serviceType = typeof(TService);
        this.ThrowExceptionIfRegistered(new ServiceKey(serviceType));

        try
        {
            conditionalRegister(new ConditionalRegister<TService>(this, this.Container));

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(serviceType,
                "The given service cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException"></exception>
    public IServiceRegister RegisterConditional(Type serviceType, Action<IConditionalRegister> conditionalRegister)
    {
        this.ThrowExceptionIfRegistered(new ServiceKey(serviceType));

        try
        {
            conditionalRegister(new ConditionalRegister<object>(this, this.Container, serviceType));

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(serviceType,
                "The given service cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">The given service cannot register initializer, see inner exception for details.</exception>
    public IServiceRegister RegisterInitializer<TService>(
        Action<IGenericServiceProvider, TService> serviceInitialize) where TService : class
    {
        try
        {
            this.Container.RegisterInitializer<TService>(data => { serviceInitialize.Invoke(this, data); });
            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(typeof(TService),
                "The given service cannot register initializer, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">The given service cannot register initializer, see inner exception for details.</exception>
    public IServiceRegister RegisterInitializer<TService>(Action<TService> serviceInitialize) where TService : class
    {
        try
        {
            this.Container.RegisterInitializer<TService>(serviceInitialize.Invoke);
            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(typeof(TService),
                "The given service cannot register initializer, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">The given service cannot register initializer, see inner exception for details.</exception>
    public IServiceRegister RegisterDerivedServices<TServiceBase>(Assembly assembly, bool includeObsoleted = false, LifetimeScope? scope = null)
        where TServiceBase : class
    {
        return this.RegisterDerivedServices(typeof(TServiceBase), assembly, includeObsoleted, scope);
    }

    /// <inheritdoc />
    /// /// <exception cref="ServiceRegistrationException">The given service cannot register initializer, see inner exception for details.</exception>
    public IServiceRegister RegisterDerivedServices(Type serviceBase, Assembly assembly, bool includeObsoleted = false, LifetimeScope? scope = null)
    {
        try
        {
            var derivedTypes = assembly.GetDerivedTypesFrom(serviceBase,
                new TypeResolutionOptions
                    {IncludeGenericTypeDefinitions = true, IncludeObsoleted = includeObsoleted});

            foreach (var derivedType in derivedTypes)
            {
                this.Register(derivedType, derivedType, scope);
                this.singleRegistrations.Add(new ServiceKey(serviceBase));
            }

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(serviceBase, "The given services cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">The given service collection cannot be registered, see inner exception for details.</exception>
    public IServiceRegister RegisterMany<TService>(params Type[] implementorTypes) where TService : class
    {
        return this.RegisterMany(typeof(TService), implementorTypes.ToList());
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException"></exception>
    public IServiceRegister RegisterMany<TService>(IEnumerable<Type> implementorTypes, LifetimeScope? scope = null)
        where TService : class
    {
        return this.RegisterMany(typeof(TService), implementorTypes, scope);
    }

    /// <inheritdoc />
    public IServiceRegister RegisterMany(Type serviceType, IEnumerable<Type> implementorTypes, LifetimeScope? scope = null)
    {
        var svcReference = new ServiceKey(serviceType);
        this.ThrowExceptionIfRegistered(svcReference, true);

        try
        {
            if (scope.HasValue)
            {
                this.Container.Collection.Register(serviceType, implementorTypes, scope.Value.Translate());
            }
            else
            {
                this.Container.Collection.Register(serviceType, implementorTypes);
            }

            this.manyRegistrations.Add(svcReference);

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(typeof(IEnumerable<>).MakeGenericType(serviceType), "The given service collection cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">The given service collection cannot be registered, see inner exception for details.</exception>
    public IServiceRegister RegisterMany<TService>(params TService[] singletons) where TService : class
    {
        var svcReference = new ServiceKey(typeof(TService));
        this.ThrowExceptionIfRegistered(svcReference, true);

        try
        {
            this.Container.Collection.Register(singletons);

            this.manyRegistrations.Add(svcReference);

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(typeof(IEnumerable<TService>), "The given service collection cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">The given service collection cannot be registered, see inner exception for details.</exception>
    public IServiceRegister RegisterMany<TService>(params Assembly[] assemblies) where TService : class
    {
        return this.RegisterMany(typeof(TService), assemblies.ToList());
    }

    /// <inheritdoc />
    /// /// <exception cref="ServiceRegistrationException">The given service collection cannot be registered, see inner exception for details.</exception>
    public IServiceRegister RegisterMany<TService>(IEnumerable<Assembly> assemblies, LifetimeScope? scope = null)
        where TService : class
    {
        return this.RegisterMany(typeof(TService), assemblies, scope);
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">The given service collection cannot be registered, see inner exception for details.</exception>
    public IServiceRegister RegisterMany(Type serviceType, params Assembly[] assemblies)
    {
        return this.RegisterMany(serviceType, assemblies.ToList());
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">The given service collection cannot be registered, see inner exception for details.</exception>
    public IServiceRegister RegisterMany(Type serviceType, IEnumerable<Assembly> assemblies, LifetimeScope? scope = null)
    {
        return this.RegisterMany(new ServiceKey(serviceType), assemblies, scope);
    }

    /// <inheritdoc />
    public IServiceRegister RegisterMany(ServiceKey svcKey, IEnumerable<Assembly> assemblies, LifetimeScope? scope = null)
    {
        this.ThrowExceptionIfRegistered(svcKey, true);
        var svcRegister = this.GetOrCreateServiceRegister(svcKey.Key);

        var serviceType = svcKey.Type;
        var container = svcRegister.Container;

        try
        {
            if (scope.HasValue)
            {
                container.Collection.Register(serviceType, assemblies.ToList(), scope.Value.Translate());
            }
            else
            {
                container.Collection.Register(serviceType, assemblies.ToList());
            }

            svcRegister.manyRegistrations.Add(svcKey);

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(serviceType, "The given service collection cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException"></exception>
    public IServiceRegister RegisterMany<TService>(IEnumerable<IRegistrationInfo> registrationInfos)
        where TService : class
    {
        return this.RegisterMany(typeof(TService), registrationInfos);
    }

    /// <inheritdoc />
    public IServiceRegister RegisterMany<TService>(IEnumerable<IRegistrationInfo<TService>> registrationInfos)
        where TService : class
    {
        return this.RegisterMany(typeof(TService), registrationInfos);
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException"></exception>
    public IServiceRegister RegisterMany(Type serviceType, IEnumerable<IRegistrationInfo> registrationInfos)
    {
        return this.RegisterMany(new ServiceKey(serviceType), registrationInfos);
    }

    /// <inheritdoc />
    public IServiceRegister RegisterMany(ServiceKey svcKey, IEnumerable<IRegistrationInfo> registrationInfos)
    {
        this.ThrowExceptionIfRegistered(svcKey, true);
        var svcRegister = this.GetOrCreateServiceRegister(svcKey.Key);

        var serviceType = svcKey.Type;
        var container = svcRegister.Container;

        try
        {
            var regs = (registrationInfos ?? [])
                .ToList();

            if (!regs.Any())
            {
                throw new ArgumentException("The collection of RegistrationInfo cannot be null or empty.");
            }

            registrationInfos.Validate(serviceType);

            if (serviceType.IsGenericTypeDefinition)
            {
                var scope = regs.First().Scope;

                if (regs.Any(info => info.Scope != scope))
                {
                    throw new InvalidOperationException("All open generic types registrations must have the same LifeStyle");
                }
                
                container.Collection.Register(serviceType, regs.Select(info => info.ConcreteType).ToList(), scope.Translate());
            }
            else
            {
                var registrations = regs.Select(registrationInfo =>
                        svcRegister.CreateRegistration(serviceType, registrationInfo))
                    .ToList();

                container.Collection.Register(serviceType, registrations);
            }

            svcRegister.manyRegistrations.Add(svcKey);

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(serviceType, "The given service collection cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    public IServiceRegister RegisterManyLazy(ServiceKey svcKey, IEnumerable<IRegistrationInfo> registrationInfos)
    {
        this.ThrowExceptionIfRegistered(svcKey, true);
        var svcRegister = this.GetOrCreateServiceRegister(svcKey.Key);

        try
        {
            var regs = registrationInfos.ToList();

            regs.Validate(svcKey.Type);

            svcRegister.manyLazyRegistrations.Add(new ManyRegistration
            {
                ServiceKey = svcKey,
                Registrations = regs.ToList()
            });

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(svcKey.Type, "The given service collection cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    public IServiceRegister RegisterManyLazy(Type serviceType, IEnumerable<IRegistrationInfo> registrationInfos)
    {
        return this.RegisterManyLazy(new ServiceKey(serviceType), registrationInfos);
    }

    /// <inheritdoc />
    public IServiceRegister RegisterManyLazy<TService>(IEnumerable<IRegistrationInfo<TService>> registrationInfos)
        where TService : class
    {
        return this.RegisterManyLazy(typeof(TService), registrationInfos);
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">The given service type cannot be registered more than once.</exception>
    public IServiceRegister RegisterResolver(Type serviceType, Type implementorType, LifetimeScope? scope = null)
    {
        this.ThrowExceptionIfRegistered(new ServiceKey(serviceType));
            
        var serviceResolver = this.MakeResolver(serviceType, implementorType, scope);
        this.resolvers.Add(serviceResolver);

        return serviceResolver;
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">The given service type cannot be registered more than once.</exception>
    public IServiceRegister RegisterResolver<TService, TImplementor>(LifetimeScope? scope = null)
        where TService : class
        where TImplementor : class, TService
    {
        var serviceType = typeof(TService);

        this.ThrowExceptionIfRegistered(new ServiceKey(serviceType));

        var serviceResolver = this.MakeResolver<TService, TImplementor>(scope);
        this.resolvers.Add(serviceResolver);

        return serviceResolver;
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">The given service type cannot be registered more than once.</exception>
    public IServiceRegister RegisterResolver<TService>(LifetimeScope? scope = null)
        where TService : class
    {
        var serviceType = typeof(TService);

        this.ThrowExceptionIfRegistered(new ServiceKey(serviceType));

        var serviceResolver = this.MakeResolver<TService>(scope);
        this.resolvers.Add(serviceResolver);

        return serviceResolver;
    }

    /// <inheritdoc />
    public IServiceRegister RegisterResolver(Type concreteType, LifetimeScope? scope = null)
    {
        this.ThrowExceptionIfRegistered(new ServiceKey(concreteType));

        var serviceResolver = this.MakeResolver(concreteType, concreteType, scope);
        this.resolvers.Add(serviceResolver);

        return serviceResolver;
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">The given service type cannot be registered more than once.</exception>
    public IServiceRegister RegisterResolver<TService>(Func<TService> serviceCreator, LifetimeScope? scope = null)
        where TService : class
    {
        var serviceType = typeof(TService);

        this.ThrowExceptionIfRegistered(new ServiceKey(serviceType));

        var serviceResolver = this.MakeResolver(_ => serviceCreator(), scope);
        this.resolvers.Add(serviceResolver);

        return serviceResolver;
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">The given service type cannot be registered more than once.</exception>
    public IServiceRegister RegisterResolver<TService>(Func<IGenericServiceProvider, TService> serviceCreator, LifetimeScope? scope = null)
        where TService : class
    {
        var serviceType = typeof(TService);

        this.ThrowExceptionIfRegistered(new ServiceKey(serviceType));

        var serviceResolver = this.MakeResolver(serviceCreator, scope);
        this.resolvers.Add(serviceResolver);

        return serviceResolver;
    }

    /// <inheritdoc />
    public IServiceRegister RegisterGeneric(Type genericServiceType, IEnumerable<Assembly> assemblies, LifetimeScope? scope = null)
    {
        try
        {
            // TODO: to understand how it works !!!
            if (scope.HasValue)
            {
                this.Container.Register(genericServiceType, assemblies, scope.Value.Translate());
            }
            else
            {
                this.Container.Register(genericServiceType, assemblies);
            }
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(genericServiceType, "The given generic service type cannot be registered, see inner exception for details.", ex);
        }

        return this;
    }

    /// <inheritdoc />
    public bool IsRegistered(Type serviceType, bool many = false)
    {
        return this.IsRegistered(new ServiceKey(serviceType), many);
    }

    /// <inheritdoc />
    public bool IsRegistered<TService>(bool many = false) where TService : class
    {
        return this.IsRegistered(typeof(TService), many);
    }

    /// <inheritdoc />
    public bool IsRegistered(ServiceKey svcKey, bool many = false)
    {
        if (!this.TryGetServiceRegister(svcKey.Key, out var svcRegister))
        {
            return false;
        }

        var serviceType = svcKey.Type;

        if (many)
        {
            return svcRegister.manyRegistrations.Contains(svcKey)
                   || svcRegister.manyLazyRegistrations.Any(registration => registration.ServiceKey == svcKey);
        }

        return svcRegister.resolvers.Any(resolver => resolver.ServiceType == serviceType)
               || svcRegister.singleLazyRegistrations.Any(registration => registration.ServiceKey == svcKey)
               || svcRegister.singleRegistrations.Contains(svcKey)
               || svcRegister.Container.GetCurrentRegistrations().Any(producer => producer.ServiceType == serviceType)
            ;
    }

    /// <inheritdoc />
    public void Dispose()
    {
        this.Dispose(true);

        GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources.
    /// </summary>
    /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
    protected virtual void Dispose(bool disposing)
    {
        if (!disposing)
        {
            return;
        }

        try
        {
            this.cache.Dispose();
        }
        catch
        {
            // nothing to do here !
        }

        try
        {
            this.Container.Dispose();
        }
        finally
        {
            this.resolvers.Clear();
            this.singleLazyRegistrations.Clear();
            this.manyLazyRegistrations.Clear();
            this.singleRegistrations.Clear();
            this.manyRegistrations.Clear();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    protected IEnumerable<IServiceResolver> GetResolvers()
    {
        return this.resolvers;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    protected IDictionary<object, ServiceRegisterImpl> GetAllRegisterContexts()
    {
        return this.contextRegisters;
    }

    /// <summary>
    /// Called when a service is not registered, and It's called on a root related context (on parent).
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="args">The <see cref="UnregisteredTypeEventArgs" /> instance containing the event data.</param>
    protected void OnResolveUnregisteredTypeOnRootContainer(object sender, UnregisteredTypeEventArgs args)
    {
        if (args.Handled)
        {
            return;
        }

        var producer = this.Container.GetRegistration(args.UnregisteredServiceType, false);

        if (producer is not null)
        {
            args.Register(producer!.Registration);
        }
    }

    /// <summary>
    /// Called when a service is not registered, and It's called on the current context (itself).
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="args">The <see cref="UnregisteredTypeEventArgs"/> instance containing the event data.</param>
    protected void OnResolveUnregisteredTypeOnResolvers(object sender, UnregisteredTypeEventArgs args)
    {
        if (args.Handled)
        {
            return;
        }

        var unregisteredType = args.UnregisteredServiceType;

        var resolver = this.resolvers.FirstOrDefault(serviceResolver => serviceResolver.ServiceType == unregisteredType)
                       ?? this.resolvers.FirstOrDefault(serviceResolver =>
                           serviceResolver.ServiceType.IsGenericTypeDefinition &&
                           serviceResolver.ServiceType.IsCompatible(unregisteredType));

        if (resolver != null)
        {
            if (resolver is not ServiceRegisterImpl serviceResolver)
            {
                return;
            }

            var producer = serviceResolver.Container.GetRegistration(unregisteredType, true);
            if (producer!.Lifestyle is ScopedLifestyle)
            {
                var registration = producer.Lifestyle.CreateRegistration(unregisteredType,
                    () =>
                    {
                        serviceResolver.BeginScopeFromParent(this);

                        var service = resolver.ServiceType.IsGenericTypeDefinition
                            ? resolver.GetService(unregisteredType)
                            : resolver.GetService();
                            
                        return service;
                    }, this.Container);

                registration.SuppressDisposal = true;

                args.Register(registration);
            }
            else
            {
                args.Register(producer.Registration);
            }
        }
    }

    /// <summary>
    /// A handler for resolving unregistered types, for single and many lazy registrations.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    protected void OnResolveUnregisteredLazyTypes(object sender, UnregisteredTypeEventArgs args)
    {
        if (args.Handled)
        {
            return;
        }

        // if the unregistered type is for many registrations.
        if (args.UnregisteredServiceType.IsGenEnumerable())
        {
            this.OnResolveUnregisteredTypesFromManyLazy(args);
        }
        else
        {
            this.OnResolveUnregisteredTypesFromSingleLazy(args);
        }
    }

    /// <inheritdoc />
    public object GetService(Type serviceType, object key = null)
    {
        try
        {
            if (!this.TryGetServiceRegister(key, out var svcRegister))
            {
                throw new InvalidOperationException($"No container exists associated to the given service key, value: {key}");
            }

            svcRegister.BeginScopeFromParent(this);

            return svcRegister.Container.GetInstance(serviceType);
        }
        catch (Exception ex)
        {
            throw new ServiceUnavailableException(serviceType, ex.Message, ex);
        }
    }

    /// <inheritdoc />
    /// <exception cref="ServiceUnavailableException"></exception>
    object IServiceProvider.GetService(Type serviceType)
    {
        return this.GetService(serviceType);
    }
    
    /// <inheritdoc />
    /// <exception cref="ServiceUnavailableException"></exception>
    public TService GetService<TService>(object key = null)
        where TService : class
    {
        return (TService)this.GetService(typeof(TService), key);
    }

    /// <inheritdoc />
    /// <exception cref="ServiceUnavailableException"></exception>
    public IEnumerable<TService> GetServices<TService>(object key = null)
        where TService : class
    {
        return this.GetService<IEnumerable<TService>>(key);
    }

    /// <summary>
    /// Makes the resolver.
    /// </summary>
    /// <param name="scope">The scope.</param>
    /// <returns></returns>
    protected ServiceRegisterResolver<TCService> MakeResolver<TCService>(LifetimeScope? scope = null)
        where TCService : class
    {
        try
        {
            var resolver = this.BuildResolver<TCService>();
            resolver.Register<TCService>(scope);

            return resolver;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(typeof(TCService), "The given service cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <summary>
    /// Makes the resolver.
    /// </summary>
    /// <typeparam name="TCService">The type of the c service.</typeparam>
    /// <param name="serviceCreator">The service creator.</param>
    /// <param name="scope">The scope.</param>
    /// <returns></returns>
    protected ServiceRegisterResolver<TCService> MakeResolver<TCService>(Func<IGenericServiceProvider, TCService> serviceCreator, LifetimeScope? scope = null)
        where TCService : class
    {
        try
        {
            var resolver = this.BuildResolver<TCService>();
            resolver.Register(() => serviceCreator(resolver), scope);

            return resolver;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(typeof(TCService), "The given service cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <summary>
    /// Makes the resolver.
    /// </summary>
    /// <typeparam name="TImplementor">The type of the implementor.</typeparam>
    /// <typeparam name="TCService"></typeparam>
    /// <param name="scope">The scope.</param>
    /// <returns></returns>
    protected ServiceRegisterResolver<TCService> MakeResolver<TCService, TImplementor>(LifetimeScope? scope = null)
        where TCService : class
        where TImplementor : class, TCService
    {
        try
        {
            var resolver = this.BuildResolver<TCService>();
            resolver.Register<TCService, TImplementor>(scope);

            return resolver;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(typeof(TCService), "The given service cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <summary>
    /// Makes the resolver.
    /// </summary>
    /// <param name="serviceType">Type of the service.</param>
    /// <param name="implementorType">Type of the implementor.</param>
    /// <param name="scope">The scope.</param>
    /// <returns></returns>
    protected ServiceRegisterResolver<object> MakeResolver(Type serviceType, Type implementorType, LifetimeScope? scope = null)
    {
        try
        {
            var resolver = this.BuildResolver<object>(serviceType);
            resolver.Register(serviceType, implementorType, scope);

            return resolver;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(serviceType, "The given service cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="container"></param>
    /// <param name="rootContainer"></param>
    protected void InitializeContainer(Container container, Container rootContainer)
    {
        container.ResolveUnregisteredType += this.OnResolveUnregisteredTypeOnRootContainer;
        container.InheritLifeStylesFrom(rootContainer);

        rootContainer.ContainerScope.RegisterForDisposal(container);
    }

    /// <summary>
    /// Builds a service resolver for the given type or for the given generic definition type.
    /// </summary>
    /// <typeparam name="TCService"></typeparam>
    /// <param name="serviceType"></param>
    /// <returns></returns>
    private ServiceRegisterResolver<TCService> BuildResolver<TCService>(Type serviceType = null)
        where TCService : class
    {
        return new ServiceRegisterResolver<TCService>(serviceType,
            new ServiceRegisterDescriptor
            {
                Initializer = container => this.InitializeContainer(container, this.Container),
                ScopeFactory = this.scopeFactory,
                DeduceScopeFactory = true
            });
    }

    /// <summary>
    /// Tries to begin scope if the <see cref="parent"/> has a scope opened.
    /// </summary>
    /// <param name="parent"></param>
    private void BeginScopeFromParent(ServiceRegisterImpl parent)
    {
        var svcRegister = this;

        if (ReferenceEquals(svcRegister, parent))
        {
            return;
        }

        var parentScope = parent.Container.GetCurrentScope();

        if (parentScope == null)
        {
            return;
        }
        
        var childScope = svcRegister.Container.GetCurrentScope();

        if (childScope != null)
        {
            return;
        }

        childScope = svcRegister.scopeFactory(svcRegister.Container);
        parentScope.RegisterForDisposal(childScope);
    }

    private void OnResolveUnregisteredTypesFromManyLazy(UnregisteredTypeEventArgs args)
    {
        var unregisteredGenericArg = args.UnregisteredServiceType.GetGenericArguments()[0];

        var lazyRegistration = this.manyLazyRegistrations.FirstOrDefault(registration => registration.ServiceKey.Type == unregisteredGenericArg)
                               ?? this.manyLazyRegistrations.FirstOrDefault(registration =>
                                   registration.ServiceKey.Type.IsGenericTypeDefinition &&
                                   registration.ServiceKey.Type.IsCompatible(unregisteredGenericArg));

        if (lazyRegistration != null)
        {
            IEnumerable<IRegistrationInfo> lazyRegInfos = null;

            if (lazyRegistration.ServiceKey.Type == unregisteredGenericArg)
            {
                lazyRegInfos = lazyRegistration.Registrations;
            }
            else if (lazyRegistration.ServiceKey.Type.IsGenericTypeDefinition && unregisteredGenericArg.IsGenericType)
            {
                var genArgUnregisteredGenArg = unregisteredGenericArg.GetGenericArguments();

                lazyRegInfos = lazyRegistration.Registrations
                    .Select(info =>
                        info.Scope.CreateRegistration(info.ConcreteType.MakeGenericType(genArgUnregisteredGenArg),
                            info.SuppressDiagnostic));
            }

            if (lazyRegInfos == null)
            {
                return;
            }

            var registrations = lazyRegInfos.Select(info => this.CreateRegistration(unregisteredGenericArg, info));
            var registration = this.Container.Collection.CreateRegistration(unregisteredGenericArg, registrations);

            args.Register(registration);
        }
    }

    private void OnResolveUnregisteredTypesFromSingleLazy(UnregisteredTypeEventArgs args)
    {
        var unregisteredType = args.UnregisteredServiceType;

        var lazyRegistration = this.singleLazyRegistrations.FirstOrDefault(registration => registration.ServiceKey.Type == unregisteredType)
                               ?? this.singleLazyRegistrations.FirstOrDefault(registration =>
                                   registration.ServiceKey.Type.IsGenericTypeDefinition &&
                                   registration.ServiceKey.Type.IsCompatible(unregisteredType));

        if (lazyRegistration != null)
        {
            var lazyRegInfo = lazyRegistration.Registration;

            IRegistrationInfo regInfo;

            if (lazyRegistration.ServiceKey.Type.IsGenericTypeDefinition)
            {
                var concreteType = lazyRegInfo.ConcreteType.MakeGenericType(unregisteredType.GetGenericArguments());
                regInfo = lazyRegInfo.Scope.CreateRegistration(concreteType, lazyRegInfo.SuppressDiagnostic);
            }
            else
            {
                regInfo = lazyRegInfo;
            }

            var registration = this.CreateRegistration(unregisteredType, regInfo);

            args.Register(registration);
        }
    }

    /// <summary>
    /// Gets the current <see cref="ServiceRegisterImpl"/> instance related to the key context.
    /// </summary>
    /// <param name="keyCtx"></param>
    /// <returns></returns>
    private ServiceRegisterImpl GetOrCreateServiceRegister(object keyCtx)
    {
        if (keyCtx is null)
        {
            return this;
        }

        if (this.contextRegisters.TryGetValue(keyCtx, out var svcRegister))
        {
            return svcRegister;
        }

        svcRegister = new ServiceRegisterImpl(new ServiceRegisterDescriptor
        {
            Initializer = container => this.InitializeContainer(container, this.Container),
            ScopeFactory = this.scopeFactory,
            DeduceScopeFactory = true
        });

        this.contextRegisters.TryAdd(keyCtx, svcRegister);

        return svcRegister;
    }

    /// <summary>
    /// Try to get the current <see cref="ServiceRegisterImpl"/> instance related to the given context key.
    /// </summary>
    /// <param name="keyCtx"></param>
    /// <param name="svcRegister"></param>
    /// <returns></returns>
    private bool TryGetServiceRegister(object keyCtx, out ServiceRegisterImpl svcRegister)
    {
        if (keyCtx is null)
        {
            svcRegister = this;
            return true;
        }

        if (this.contextRegisters.TryGetValue(keyCtx, out var svcImpl))
        {
            svcRegister = svcImpl;
            return true;
        }

        svcRegister = null;
        return false;
    }
}
