using SimpleInjector;

namespace ServiceResolver.Ioc.SimpleInjector;

/// <summary>
/// 
/// </summary>
public static class ScopeTranslator
{
    /// <summary>
    /// Translates the specified scope.
    /// </summary>
    /// <param name="scope">The scope.</param>
    /// <returns></returns>
    public static Lifestyle Translate(this LifetimeScope scope)
    {
        return scope switch
        {
            LifetimeScope.Scoped => Lifestyle.Scoped,
            LifetimeScope.Singleton => Lifestyle.Singleton,
            LifetimeScope.Transient => Lifestyle.Transient,
            _ => Lifestyle.Transient
        };
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="scope"></param>
    /// <returns></returns>
    public static LifetimeScope Translate(this Lifestyle scope)
    {
        if (scope == null)
        {
            return LifetimeScope.Transient;
        }

        if (scope.Equals(Lifestyle.Scoped))
        {
            return LifetimeScope.Scoped;
        }

        if (scope.Equals(Lifestyle.Singleton))
        {
            return LifetimeScope.Singleton;
        }

        return LifetimeScope.Transient;
    }
}
