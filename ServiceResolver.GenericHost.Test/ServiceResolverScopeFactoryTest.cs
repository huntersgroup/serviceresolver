using System.ComponentModel;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using ServiceResolver.GenericHost.Test.Services;
using ServiceResolver.Ioc;
using ServiceResolver.Ioc.Extensions;
using ServiceResolver.Ioc.Options;
using ServiceResolver.Ioc.Providers;
using ServiceResolver.Ioc.SimpleInjector;
using SimpleInjector;
using SimpleInjector.Lifestyles;
using Xunit.Abstractions;

namespace ServiceResolver.GenericHost.Test;

public class ServiceResolverScopeFactoryTest
{
    private readonly ITestOutputHelper output;

    public ServiceResolverScopeFactoryTest(ITestOutputHelper output)
    {
        this.output = output;
    }
    
    [Fact]
    public void Test1()
    {
        var serviceResolver = this.GetServiceResolver();
        var serviceCollection = this.GetServiceCollection();

        serviceResolver.Register<IMyDisposable, MyDisposable>(LifetimeScope.Scoped)
            .Register(() => this.output, LifetimeScope.Singleton);

        var serviceProvider = serviceResolver.BuildServiceProvider(serviceCollection);

        IMyDisposable svc1;

        using (var sc1 = serviceProvider.CreateScope())
        {
            svc1 = sc1.ServiceProvider.GetService<IMyDisposable>();
        }

        Assert.Equal(1, svc1.DisposeCounter);
    }

    [Fact]
    public void Test2()
    {
        var serviceResolver = this.GetServiceResolver();
        var serviceCollection = this.GetServiceCollection();

        serviceCollection.AddScoped(_ => this.output);
        serviceResolver.Register<IMyDisposable, MyDisposable>(LifetimeScope.Scoped);

        var serviceProvider = serviceResolver.BuildServiceProvider(serviceCollection);

        IMyDisposable svc1;

        using (var sc1 = serviceProvider.CreateScope())
        {
            svc1 = sc1.ServiceProvider.GetService<IMyDisposable>();
        }

        Assert.Equal(1, svc1.DisposeCounter);
    }

    [Fact]
    public void Test3()
    {
        var serviceResolver = this.GetServiceResolver();
        var serviceCollection = this.GetServiceCollection();

        serviceCollection.AddScoped(_ => this.output);
        serviceCollection.AddScoped<IMyDisposable, MyDisposable>();

        var serviceProvider = serviceResolver.BuildServiceProvider(serviceCollection);
        
        IMyDisposable svc1;

        using (var sc1 = serviceProvider.CreateScope())
        {
            svc1 = sc1.ServiceProvider.GetService<IMyDisposable>();
        }

        Assert.Equal(1, svc1.DisposeCounter);
    }

    [Fact]
    public void Test4()
    {
        var serviceResolver = this.GetServiceResolver();
        var serviceCollection = this.GetServiceCollection();

        serviceResolver.Register(_ => this.output, LifetimeScope.Singleton)
            .Register<IMyDisposable, MyDisposable>(LifetimeScope.Scoped)
            .Register<ISvcDisposable, SvcDisposable>(LifetimeScope.Scoped)
            ;

        var serviceProvider = serviceResolver.BuildServiceProvider(serviceCollection);

        ISvcDisposable svc1, svc2;

        using (var sc1 = serviceProvider.CreateScope())
        {
            svc1 = sc1.ServiceProvider.GetService<ISvcDisposable>();
            svc2 = sc1.ServiceProvider.GetService<ISvcDisposable>();

            Assert.Equal(svc1, svc2);
            Assert.Same(svc1, svc2);
        }

        Assert.Equal(1, svc1.DisposeCounter);
        Assert.Equal(1, svc1.MyDisposable.DisposeCounter);
    }

    [Fact]
    public void Test5()
    {
        var serviceResolver = this.GetServiceResolver();
        var serviceCollection = this.GetServiceCollection();

        serviceCollection.AddSingleton(_ => this.output)
            .AddScoped<IMyDisposable, MyDisposable>()
            .AddScoped<ISvcDisposable, SvcDisposable>();

        var serviceProvider = serviceResolver.BuildServiceProvider(serviceCollection);

        ISvcDisposable svc1, svc2;

        using (var sc1 = serviceProvider.CreateScope())
        {
            svc1 = sc1.ServiceProvider.GetService<ISvcDisposable>();
            svc2 = sc1.ServiceProvider.GetService<ISvcDisposable>();

            Assert.Equal(svc1, svc2);
            Assert.Same(svc1, svc2);
        }

        Assert.Equal(1, svc1.DisposeCounter);
        Assert.Equal(1, svc1.MyDisposable.DisposeCounter);
    }

    [Fact]
    public void Test5_1()
    {
        var serviceResolver = this.GetServiceResolver();
        var serviceCollection = this.GetServiceCollection();

        serviceCollection.AddSingleton(_ => this.output)
            .AddSingleton<IMyDisposable, MyDisposable>()
            .AddSingleton<ISvcDisposable, SvcDisposable>();

        var serviceProvider = serviceResolver.BuildServiceProvider(serviceCollection);

        ISvcDisposable svc1, svc2;

        using (var sc1 = serviceProvider.CreateScope())
        {
            svc1 = sc1.ServiceProvider.GetService<ISvcDisposable>();
            svc2 = sc1.ServiceProvider.GetService<ISvcDisposable>();

            Assert.Equal(svc1, svc2);
            Assert.Same(svc1, svc2);
        }

        // asserts: no disposing due to their singletons
        Assert.Equal(0, svc1.DisposeCounter);
        Assert.Equal(0, svc1.MyDisposable.DisposeCounter);

        // acts
        serviceProvider.TryToDispose();

        // assert: after disposing service provider
        Assert.Equal(1, svc1.DisposeCounter);
        Assert.Equal(1, svc1.MyDisposable.DisposeCounter);
    }

    [Fact]
    public void GetService_Key()
    {
        var serviceResolver = this.GetServiceResolver();
        var serviceCollection = this.GetServiceCollection();
        const string key = "ctx";

        serviceCollection.AddSingleton(_ => this.output)
            .AddScoped<IMyDisposable>(provider =>
            {
                var svc = new MyDisposable(provider.GetRequiredService<ITestOutputHelper>());
                return svc;
            })
            .AddScoped<ISvcDisposable, SvcDisposable>()
            .AddKeyedScoped<ISvcDisposable, SvcDisposable>(key);

        var serviceProvider =
            serviceResolver.BuildServiceProvider(serviceCollection, new ServiceRegisterOptions { AutoRegistration = true });

        using (serviceProvider.CreateScope())
        {
            var svc01 = serviceProvider.GetRequiredService<ISvcDisposable>();
            var svc02 = serviceProvider.GetRequiredKeyedService<ISvcDisposable>(key);

            Assert.NotEqual(svc01, svc02);
            Assert.NotSame(svc01, svc02);
        }
    }

    [Theory]
    [InlineData(true)]
    [InlineData(false)]
    public void VerifyProvider_OnServiceCollectionTest(bool autoReg)
    {
        var serviceResolver = this.GetServiceResolver();
        var serviceCollection = this.GetServiceCollection();

        serviceCollection.AddSingleton<IMyDisposable>(provider =>
        {
            if (autoReg)
            {
                Assert.IsAssignableFrom<IServiceProviderAdapter>(provider);
            }
            else
            {
                Assert.IsAssignableFrom<IGenericServiceProvider>(provider);
            }

            var instance = new MyDisposable(this.output);
            return instance;
        });

        var serviceProvider =
            serviceResolver.BuildServiceProvider(serviceCollection, new ServiceRegisterOptions { AutoRegistration = autoReg });

        var svc01 = serviceProvider.GetService<IMyDisposable>();
        var svc02 = serviceProvider.GetService<IMyDisposable>();

        Assert.Equal(svc01, svc02);
        Assert.Same(svc01, svc02);
    }

    [Theory]
    [InlineData(true)]
    [InlineData(false)]
    public void VerifyProvider_OnServiceResolverTest(bool autoReg)
    {
        var serviceResolver = this.GetServiceResolver();
        var serviceCollection = this.GetServiceCollection();

        serviceResolver.Register<IMyDisposable>(provider =>
        {
            // here the provider type remains the same always.
            Assert.IsAssignableFrom<IGenericServiceProvider>(provider);

            var instance = new MyDisposable(this.output);
            return instance;
        }, LifetimeScope.Singleton);

        var serviceProvider =
            serviceResolver.BuildServiceProvider(serviceCollection, new ServiceRegisterOptions { AutoRegistration = autoReg });

        var svc01 = serviceProvider.GetService<IMyDisposable>();
        var svc02 = serviceProvider.GetService<IMyDisposable>();

        Assert.Equal(svc01, svc02);
        Assert.Same(svc01, svc02);
    }

    [Theory]
    [InlineData(true)]
    [InlineData(false)]
    public void VerifyProvider_OnServiceResolverV2Test(bool autoReg)
    {
        var serviceResolver = this.GetServiceResolver();
        var serviceCollection = this.GetServiceCollection();

        serviceResolver.Register(() => this.output, LifetimeScope.Singleton)
            .Register<IMyDisposable, MyDisposableV5>(LifetimeScope.Singleton);

        var serviceProvider =
            serviceResolver.BuildServiceProvider(serviceCollection, new ServiceRegisterOptions { AutoRegistration = autoReg });

        var svc01 = (MyDisposableV5)serviceProvider.GetService<IMyDisposable>();

        if (autoReg)
        {
            Assert.NotNull(svc01);
            Assert.IsAssignableFrom<IServiceProviderAdapter>(svc01.Provider);
        }
        else
        {
            Assert.Null(svc01);
        }
    }

    [Fact]
    public void Test5_2()
    {
        var serviceResolver = this.GetServiceResolver();
        var serviceCollection = this.GetServiceCollection();

        serviceCollection.AddSingleton(_ => this.output)
            .AddScoped<IMyDisposable, MyDisposable>()
            .AddScoped<ISvcDisposable, SvcDisposable>();

        var serviceProvider = serviceResolver.BuildServiceProvider(serviceCollection);

        ISvcDisposable svc1, svc2;

        using (var sc1 = serviceProvider.CreateScope())
        {
            // services are resolved by service provider root !!!
            svc1 = serviceProvider.GetService<ISvcDisposable>();
            svc2 = serviceProvider.GetService<ISvcDisposable>();

            Assert.Equal(svc1, svc2);
            Assert.Same(svc1, svc2);
        }

        // asserts
        Assert.Equal(1, svc1.DisposeCounter);
        Assert.Equal(1, svc1.MyDisposable.DisposeCounter);

        // acts
        serviceProvider.TryToDispose();

        // asserts
        Assert.Equal(1, svc1.DisposeCounter);
        Assert.Equal(1, svc1.MyDisposable.DisposeCounter);
    }

    [Fact]
    public void CreateScopeNestedTest()
    {
        var serviceResolver = this.GetServiceResolver();
        var serviceCollection = this.GetServiceCollection();

        serviceCollection.AddSingleton(_ => this.output)
            .AddScoped<IMyDisposable, MyDisposable>()
            .AddScoped<ISvcDisposable, SvcDisposable>();

        var serviceProvider = serviceResolver.BuildServiceProvider(serviceCollection);

        ISvcDisposable svc1, svc2, svc_child1, svc_child2;

        using (var sc1 = serviceProvider.CreateScope())
        {
            // services are resolved by service provider root !!!
            svc1 = serviceProvider.GetService<ISvcDisposable>();
            svc2 = serviceProvider.GetService<ISvcDisposable>();

            Assert.Equal(svc1, svc2);
            Assert.Same(svc1, svc2);

            using (var sc1_1 = serviceProvider.CreateScope())
            {
                svc_child1 = serviceProvider.GetService<ISvcDisposable>();
                svc_child2 = serviceProvider.GetService<ISvcDisposable>();

                Assert.Equal(svc_child1, svc_child2);
                Assert.Same(svc_child1, svc_child2);

                Assert.NotEqual(svc_child1, svc1);
                Assert.NotSame(svc_child1, svc1);
            }

            // asserts
            Assert.Equal(0, svc1.DisposeCounter);
            Assert.Equal(0, svc1.MyDisposable.DisposeCounter);
            Assert.Equal(1, svc_child1.DisposeCounter);
            Assert.Equal(1, svc_child1.MyDisposable.DisposeCounter);
        }

        // asserts
        Assert.Equal(1, svc1.DisposeCounter);
        Assert.Equal(1, svc1.MyDisposable.DisposeCounter);

        // acts
        serviceProvider.TryToDispose();

        // asserts
        Assert.Equal(1, svc1.DisposeCounter);
        Assert.Equal(1, svc1.MyDisposable.DisposeCounter);
        Assert.Equal(1, svc_child1.DisposeCounter);
        Assert.Equal(1, svc_child1.MyDisposable.DisposeCounter);
    }

    [Fact]
    [Description("Open a scope with manually disposing")]
    public void CreateScopeWithRootProviderDisposeTest()
    {
        var serviceResolver = this.GetServiceResolver();
        var serviceCollection = this.GetServiceCollection();

        serviceCollection.AddSingleton(_ => this.output)
            .AddScoped<IMyDisposable, MyDisposable>()
            .AddScoped<ISvcDisposable, SvcDisposable>();

        var serviceProvider = serviceResolver.BuildServiceProvider(serviceCollection);

        var sc1 = serviceProvider.CreateScope();

        // services are resolved by service provider root !!!
        var svc1 = serviceProvider.GetService<ISvcDisposable>();
        
        // asserts
        Assert.Equal(0, svc1.DisposeCounter);
        Assert.Equal(0, svc1.MyDisposable.DisposeCounter);

        // acts
        serviceProvider.TryToDispose();

        // asserts: services was resolved by scope, and not disposed by service provider after calling serviceProvider.dispose()
        Assert.Equal(0, svc1.DisposeCounter);
        Assert.Equal(0, svc1.MyDisposable.DisposeCounter);

        // acts
        sc1.Dispose();

        // asserts
        Assert.Equal(1, svc1.DisposeCounter);
        Assert.Equal(1, svc1.MyDisposable.DisposeCounter);
    }

    [Fact]
    [Description("Open a scope with manually disposing")]
    public void CreateScopeWithRootProviderDispose2Test()
    {
        var serviceResolver = this.GetServiceResolver();
        var serviceCollection = this.GetServiceCollection();

        serviceCollection.AddSingleton(_ => this.output)
            .AddScoped<IMyDisposable, MyDisposable>()
            .AddScoped<ISvcDisposable, SvcDisposable>();

        var serviceProvider = serviceResolver.BuildServiceProvider(serviceCollection);

        var sc1 = serviceProvider.CreateScope();
        // services are resolved by service provider root !!!
        var svc1 = serviceProvider.GetService<ISvcDisposable>();

        var sc2 = serviceProvider.CreateScope();
        // services are resolved by service provider root !!!
        var svc2 = serviceProvider.GetService<ISvcDisposable>();

        // acts
        sc1.Dispose();

        // asserts
        Assert.Equal(1, svc1.DisposeCounter);
        Assert.Equal(1, svc1.MyDisposable.DisposeCounter);
        Assert.Equal(0, svc2.DisposeCounter);
        Assert.Equal(0, svc2.MyDisposable.DisposeCounter);

        sc2.Dispose();
        // asserts
        Assert.Equal(1, svc1.DisposeCounter);
        Assert.Equal(1, svc1.MyDisposable.DisposeCounter);
        Assert.Equal(1, svc2.DisposeCounter);
        Assert.Equal(1, svc2.MyDisposable.DisposeCounter);
    }

    [Fact]
    public void Test6()
    {
        var serviceResolver = this.GetServiceResolver();
        var serviceCollection = this.GetServiceCollection();

        serviceResolver.Register(_ => this.output, LifetimeScope.Singleton)
            .Register<IMyDisposable, MyDisposable>(LifetimeScope.Scoped)
            .Register<ISvcDisposable, SvcDisposable>(LifetimeScope.Scoped)
            ;

        var serviceProvider = serviceResolver.BuildServiceProvider(serviceCollection);

        ISvcDisposable svc1, svc2;

        using (var sc1 = serviceProvider.CreateScope())
        {
            svc1 = sc1.ServiceProvider.GetService<ISvcDisposable>();
            svc2 = sc1.ServiceProvider.GetService<ISvcDisposable>();
            var svcx1 = sc1.ServiceProvider.GetService<IMyDisposable>();

            Assert.Equal(svc1, svc2);
            Assert.Same(svc1, svc2);

            Assert.Equal(svc1.MyDisposable, svcx1);
            Assert.Same(svc1.MyDisposable, svcx1);
        }

        Assert.Equal(1, svc1.DisposeCounter);
        Assert.Equal(1, svc1.MyDisposable.DisposeCounter);
    }

    [Fact]
    public void Test7()
    {
        var serviceResolver = this.GetServiceResolver();
        var serviceCollection = this.GetServiceCollection();

        serviceCollection.AddSingleton(_ => this.output)
            .AddScoped<IMyDisposable, MyDisposable>()
            .AddScoped<ISvcDisposable, SvcDisposable>();

        var serviceProvider = serviceResolver.BuildServiceProvider(serviceCollection);

        ISvcDisposable svc1, svc2;

        using (var sc1 = serviceProvider.CreateScope())
        {
            svc1 = sc1.ServiceProvider.GetService<ISvcDisposable>();
            svc2 = sc1.ServiceProvider.GetService<ISvcDisposable>();
            var svcx1 = sc1.ServiceProvider.GetService<IMyDisposable>();

            Assert.Equal(svc1, svc2);
            Assert.Same(svc1, svc2);

            Assert.Equal(svc1.MyDisposable, svcx1);
            Assert.Same(svc1.MyDisposable, svcx1);
        }

        Assert.Equal(1, svc1.DisposeCounter);
        Assert.Equal(1, svc1.MyDisposable.DisposeCounter);
    }

    [Fact]
    public void Test7_v1()
    {
        var serviceResolver = this.GetServiceResolver();
        var serviceCollection = this.GetServiceCollection();

        serviceCollection.AddSingleton(_ => this.output)
            //.AddScoped<IMyDisposable>(provider =>
            //{
            //    var dep = provider.GetRequiredService<ITestOutputHelper>();
            //    return new MyDisposable(dep);
            //})
            .AddScoped<IMyDisposable, MyDisposable>()
            .AddScoped<ISvcDisposable>(provider =>
            {
                var ret = new SvcDisposable(provider.GetRequiredService<IMyDisposable>());
                return ret;
            })
            //.AddScoped<ISvcDisposable, SvcDisposable>()
            ;

        var serviceProvider = serviceResolver.BuildServiceProvider(serviceCollection);

        ISvcDisposable svc1, svc2;

        using (var sc1 = serviceProvider.CreateScope())
        {
            svc1 = sc1.ServiceProvider.GetService<ISvcDisposable>();
            svc2 = sc1.ServiceProvider.GetService<ISvcDisposable>();
            var svcx1 = sc1.ServiceProvider.GetService<IMyDisposable>();

            Assert.Equal(svc1, svc2);
            Assert.Same(svc1, svc2);

            Assert.Equal(svc1.MyDisposable, svcx1);
            Assert.Same(svc1.MyDisposable, svcx1);
        }

        Assert.Equal(1, svc1.DisposeCounter);
        Assert.Equal(1, svc1.MyDisposable.DisposeCounter);
    }

    [Fact]
    public void Test8()
    {
        var serviceResolver = this.GetServiceResolver();
        var serviceCollection = this.GetServiceCollection();

        serviceResolver
            .Register(() => new StringBuilder("yeah.."))
            .Register(() => this.output);

        serviceCollection
            .AddScoped<IMyDisposable>(provider =>
            {
                var tmp1 = provider.GetRequiredService<ITestOutputHelper>();
                var tmp2 = provider.GetRequiredService<StringBuilder>();
                var ret = new MyDisposableV2(tmp1, tmp2);

                return ret;
            })
            ;

        var serviceProvider = serviceResolver.BuildServiceProvider(serviceCollection);

        IMyDisposable svc1;

        using (var sc1 = serviceProvider.CreateScope())
        {
            svc1 = sc1.ServiceProvider.GetService<IMyDisposable>();

            Assert.NotNull(svc1);
        }

        Assert.Equal(1, svc1.DisposeCounter);
    }

    [Fact]
    public void VerifySelfRegistrationTest()
    {
        var serviceResolver = this.GetServiceResolver();
        var serviceCollection = this.GetServiceCollection();

        var serviceProvider = serviceResolver.BuildServiceProvider(serviceCollection, new ServiceRegisterOptions());

        Assert.NotNull(serviceProvider.GetService<IServiceProvider>());
        Assert.NotNull(serviceProvider.GetService<IServiceProviderIsService>());
    }

    [Fact]
    public void VerifyNoSelfRegistrationTest()
    {
        var serviceResolver = this.GetServiceResolver();
        var serviceCollection = this.GetServiceCollection();

        var serviceProvider = serviceResolver.BuildServiceProvider(serviceCollection);

        Assert.Null(serviceProvider.GetService<IServiceProvider>());
        Assert.Null(serviceProvider.GetService<IServiceProviderIsService>());
    }

    [Fact]
    public void VerifyExceptionsOnMissingSingleRegistrations()
    {
        var serviceResolver = this.GetServiceResolver();

        var serviceCollection = this.GetServiceCollection();

        var serviceProvider = serviceResolver.BuildServiceProvider(serviceCollection);

        Assert.Null(serviceProvider.GetService<StringBuilder>());
        Assert.Null(serviceProvider.GetService<ISvcDisposable>());
        Assert.NotNull(serviceProvider.GetServices<ISvcDisposable>());
        Assert.Empty(serviceProvider.GetServices<ISvcDisposable>());
    }

    private ServiceRegisterProvider GetServiceResolver()
    {
        return new ServiceRegisterProvider(new ServiceRegisterDescriptor
        {
            ScopeFactory = AsyncScopedLifestyle.BeginScope,
            Initializer = container =>
            {
                container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
                container.Options.DefaultLifestyle = Lifestyle.Singleton;
            }
        });
    }

    private ServiceCollection GetServiceCollection()
    {
        return new ServiceCollection();
    }
}
