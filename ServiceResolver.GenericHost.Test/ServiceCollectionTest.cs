using System.Reflection;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using ServiceResolver.GenericHost.Test.Services;
using ServiceResolver.GenericHost.Test.Services.Ambiguous;
using ServiceResolver.Ioc.Extensions;
using Xunit.Abstractions;

namespace ServiceResolver.GenericHost.Test;

public class ServiceCollectionTest
{
    private readonly ITestOutputHelper output;

    public ServiceCollectionTest(ITestOutputHelper output)
    {
        this.output = output;
    }

    [Fact]
    public void TestKeyed()
    {
        const string key = "ctx";

        IServiceCollection serviceCollection = new ServiceCollection();
        serviceCollection.AddSingleton(this.output);
        serviceCollection.AddSingleton<IMyDisposable, MyDisposable>();
        serviceCollection.AddKeyedSingleton<IMyDisposable, MyDisposableV2>(key);

        var provider = serviceCollection.BuildServiceProvider();

        Assert.IsType<MyDisposable>(provider.GetService<IMyDisposable>());
        Assert.IsType<MyDisposableV2>(provider.GetKeyedService<IMyDisposable>(key));
    }

    [Fact]
    public void TestKeyed_WithFactory()
    {
        const string key = "ctx";

        IServiceCollection serviceCollection = new ServiceCollection();
        serviceCollection.AddSingleton(this.output);
        serviceCollection.AddSingleton<IMyDisposable, MyDisposable>();
        serviceCollection.AddKeyedSingleton<IMyDisposable>(key, (serviceProvider, keyArg) =>
        {
            var svc = new MyDisposableV2(this.output);
            return svc;
        });

        var provider = serviceCollection.BuildServiceProvider();

        Assert.NotNull(provider.GetRequiredKeyedService<IMyDisposable>(key));
        Assert.NotNull(provider.GetRequiredService<IMyDisposable>());
    }


    [Fact]
    public void TestKeyed_WithFactoryV1()
    {
        const string key = "ctx";

        IServiceCollection serviceCollection = new ServiceCollection();
        serviceCollection.AddSingleton<IMyDisposable, MyDisposable>();
        serviceCollection.AddKeyedSingleton<IMyDisposable>(key, (serviceProvider, keyArg) =>
        {
            var svc = new MyDisposableV2(this.output);
            return svc;
        });

        var provider = serviceCollection.BuildServiceProvider();

        var instance = (MyDisposableV4)ActivatorUtilities.CreateInstance(provider, typeof(MyDisposableV4), this.output, key);
        Assert.NotNull(instance);
        Assert.NotNull(instance.Key);
        Assert.Equal(key, instance.Key);

        Assert.True(typeof(MyDisposableV4).HasCtorWithAnyParamsDecorated([typeof(ServiceKeyAttribute)]));
    }

    [Fact]
    public void TestKeyed_WithFactoryV2()
    {
        const string key = "ctx";

        IServiceCollection serviceCollection = new ServiceCollection();
        serviceCollection.AddSingleton(sp =>
        {
            var a = this.output;
            return a;
        });

        serviceCollection.AddKeyedSingleton(key, (sp, keyArg) =>
        {
            var a = this.output;
            return a;
        });

        serviceCollection.AddKeyedSingleton<IMyDisposable, MyDisposableV2>(key);

        var provider = serviceCollection.BuildServiceProvider();

        Assert.NotNull(provider.GetRequiredKeyedService<IMyDisposable>(key));

        Assert.True(true);
    }

    [Fact]
    public void Test1()
    {
        //var serviceCollection = new Service
        IServiceCollection serviceCollection = new ServiceCollection();

        serviceCollection.AddScoped<IMyDisposable, MyDisposable>();
        serviceCollection.AddSingleton(this.output);

        var serviceProvider = serviceCollection.BuildServiceProvider();

        var svc1 = serviceProvider.GetService<IMyDisposable>();
        var svc2 = serviceProvider.GetService<IMyDisposable>();

        Assert.Equal(svc1, svc2);
        Assert.Same(svc1, svc2);

        serviceProvider.Dispose();
    }

    [Fact]
    public void Test2()
    {
        IServiceCollection serviceCollection = new ServiceCollection();

        serviceCollection.AddScoped<IMyDisposable, MyDisposable>();
        serviceCollection.AddSingleton(this.output);

        var serviceProvider = serviceCollection.BuildServiceProvider();

        var svc0 = serviceProvider.GetService<IMyDisposable>();

        using (var sc1 = serviceProvider.CreateScope())
        {
            var svc1 = sc1.ServiceProvider.GetService<IMyDisposable>();

            Assert.NotSame(svc0, svc1);
        }

        serviceProvider.Dispose();
    }

    [Fact]
    public void Test3()
    {
        IServiceCollection serviceCollection = new ServiceCollection();

        serviceCollection.AddScoped<IMyDisposable, MyDisposable>();
        serviceCollection.AddSingleton(this.output);

        var serviceProvider = serviceCollection.BuildServiceProvider();

        // acts
        var svc1 = serviceProvider.GetService<IMyDisposable>();
        var svc2 = serviceProvider.GetService<IMyDisposable>();
        
        // asserts
        Assert.Equal(svc1, svc2);
        Assert.Same(svc1, svc2);

        // acts
        var svc3 = serviceProvider.GetService<IServiceProvider>();

        var svc01 = svc3.GetService<IMyDisposable>();

        Assert.Equal(svc1, svc01);
        Assert.Same(svc1, svc01);

        serviceProvider.Dispose();
    }

    [Fact]
    public void Test4()
    {
        IServiceCollection serviceCollection = new ServiceCollection();

        serviceCollection.AddScoped<IMyDisposable, MyDisposable>();
        serviceCollection.AddSingleton(this.output);

        var sp01 = serviceCollection.BuildServiceProvider();

        IMyDisposable svc1;

        using (var sc1 = sp01.CreateScope())
        {
            svc1 = sc1.ServiceProvider.GetService<IMyDisposable>();
        }

        Assert.Equal(1, svc1.DisposeCounter);
    }

    [Fact]
    public void Test5()
    {
        IServiceCollection serviceCollection = new ServiceCollection();

        serviceCollection.AddScoped<IMyDisposable, MyDisposable>();
        serviceCollection.AddSingleton(this.output);

        var sp01 = serviceCollection.BuildServiceProvider();

        IMyDisposable svc1;

        var sc1 = sp01.CreateScope();
        svc1 = sc1.ServiceProvider.GetService<IMyDisposable>();

        // asserts
        Assert.Equal(0, svc1.DisposeCounter);

        // acts
        sp01.Dispose();

        // asserts, no disposing of scopes by disposing service provider.
        Assert.Equal(0, svc1.DisposeCounter);
    }

    [Fact]
    public void Test6()
    {
        IServiceCollection serviceCollection = new ServiceCollection();

        serviceCollection.AddScoped<IMyDisposable, MyDisposable>();
        serviceCollection.AddSingleton(this.output);

        var sp01 = serviceCollection.BuildServiceProvider();
        var sp02 = sp01.GetService<IServiceProvider>();

        IMyDisposable svc1;

        using (var sc1 = sp02.CreateScope())
        {
            svc1 = sc1.ServiceProvider.GetService<IMyDisposable>();
        }

        Assert.Equal(1, svc1.DisposeCounter);

        sp02.TryToDispose();
        Assert.True(sp01 is not null);
    }

    [Fact]
    public void AmbiguousTest01()
    {
        IServiceCollection serviceCollection = new ServiceCollection();

        serviceCollection.AddScoped(_ => this.output)
            .AddScoped<IMyDisposable, MyDisposable>()
            .AddScoped(_ => new StringBuilder())
            .AddScoped<Amb2Svc>();

        var provider = serviceCollection.BuildServiceProvider();

        Amb2Svc svc1;

        using (var sc1 = provider.CreateScope())
        {
            Assert.ThrowsAny<InvalidOperationException>(() => svc1 = sc1.ServiceProvider.GetService<Amb2Svc>());
        }
    }

    [Fact]
    public void AmbiguousTest02()
    {
        IServiceCollection serviceCollection = new ServiceCollection();

        serviceCollection.AddScoped(_ => this.output)
            .AddScoped<IMyDisposable, MyDisposable>()
            .AddScoped<Amb2Svc>();

        var provider = serviceCollection.BuildServiceProvider();

        Amb2Svc svc1;

        using (var sc1 = provider.CreateScope())
        {
            svc1 = sc1.ServiceProvider.GetService<Amb2Svc>();
        }

        Assert.NotNull(svc1);
        Assert.Null(svc1.StrBuilder);
        Assert.NotNull(svc1.MyDisposable);
    }

    [Fact]
    public void AmbiguousTest03()
    {
        IServiceCollection serviceCollection = new ServiceCollection();

        serviceCollection.AddScoped(_ => this.output)
            .AddScoped(_ => new StringBuilder())
            .AddScoped<Amb2Svc>();

        var provider = serviceCollection.BuildServiceProvider();

        Amb2Svc svc1;

        using (var sc1 = provider.CreateScope())
        {
            svc1 = sc1.ServiceProvider.GetService<Amb2Svc>();
        }

        Assert.NotNull(svc1);
        Assert.NotNull(svc1.StrBuilder);
        Assert.Null(svc1.MyDisposable);
    }

    [Fact]
    public void AmbiguousTest04()
    {
        IServiceCollection serviceCollection = new ServiceCollection();

        serviceCollection.AddScoped(_ => this.output)
            //.AddScoped(_ => new StringBuilder())
            .AddScoped<Amb2Svc>();

        var provider = serviceCollection.BuildServiceProvider();

        Amb2Svc svc1;

        using (var sc1 = provider.CreateScope())
        {
            svc1 = sc1.ServiceProvider.GetService<Amb2Svc>();
        }

        Assert.NotNull(svc1);
        Assert.Null(svc1.StrBuilder);
        Assert.Null(svc1.MyDisposable);
    }

    [Fact]
    public void VerifyDisposingSingletonTest()
    {
        IServiceCollection serviceCollection = new ServiceCollection();

        var svc = new MyDisposable(this.output);

        serviceCollection.AddSingleton<IMyDisposable>(_ => svc);

        var serviceProvider = serviceCollection.BuildServiceProvider();

        var svc01 = serviceProvider.GetService<IMyDisposable>();

        // asserts
        Assert.NotNull(svc01);
        Assert.Same(svc, svc01);

        // acts
        serviceProvider.Dispose();

        Assert.Equal(1, svc.DisposeCounter);
    }

    [Fact]
    public void VerifyNoDisposingSingletonTest()
    {
        IServiceCollection serviceCollection = new ServiceCollection();

        var svc = new MyDisposable(this.output);

        serviceCollection.AddSingleton<IMyDisposable>(_ => svc);

        var serviceProvider = serviceCollection.BuildServiceProvider();

        // acts
        serviceProvider.Dispose();

        // asserts
        Assert.Equal(0, svc.DisposeCounter);
    }

    //[Fact]
    //public void VerifyDisposingSingletonTest02()
    //{
    //    IServiceCollection serviceCollection = new ServiceCollection();

    //    var svc = new MyDisposable(this.output);

    //    serviceCollection.AddSingleton<IMyDisposable>(_ => svc)
    //        .AddSingleton(_ => new StringBuilder("ciaone"));

    //    var serviceProvider = serviceCollection.BuildServiceProvider();

    //    // acts
    //    var str = serviceProvider.GetService<StringBuilder>();
    //    serviceProvider.Dispose();

    //    // asserts
    //    Assert.NotNull(str);
    //    Assert.Equal(1, svc.DisposeCounter);
    //}

    [Fact]
    public void VerifyEmptyCollectionOnNoRegistrationTest()
    {
        IServiceCollection serviceCollection = new ServiceCollection();

        var serviceProvider = serviceCollection.BuildServiceProvider();

        var list = serviceProvider.GetServices<IMyDisposable>();
        Assert.NotNull(list);
        Assert.Empty(list);

        serviceProvider.Dispose();
    }

    [Fact]
    public void VerifyDefaultResolutionForManyServicesTest()
    {
        IServiceCollection serviceCollection = new ServiceCollection();

        serviceCollection.AddSingleton<ISvcForDisposables, SvcForDisposables01>();

        var serviceProvider = serviceCollection.BuildServiceProvider();

        var svc01 = serviceProvider.GetServices<IMyDisposable>();
        var svc02 = serviceProvider.GetService<ISvcForDisposables>();

        // asserts
        Assert.NotNull(svc01);

        // dependency collection is empty if there's no registration.
        Assert.NotNull(svc02);
        Assert.Empty(svc02.Instances);

        serviceProvider.Dispose();
    }

    [Fact]
    public void VerifyOptionalParameterTest()
    {
        IServiceCollection serviceCollection = new ServiceCollection();

        serviceCollection.
            //AddSingleton<IMyDisposable, MyDisposableV2>()
            AddScoped<IMyDisposable, MyDisposableV2>()
            //.AddSingleton(_ => this.output)
            .AddTransient(_ => this.output)
            ;

        var serviceProvider = serviceCollection.BuildServiceProvider(new ServiceProviderOptions
        {
            ValidateOnBuild = true,
            ValidateScopes = true
        });

        using (var scope = serviceProvider.CreateScope())
        {
            var svc03 = scope.ServiceProvider.GetService<IMyDisposable>();

            Assert.NotNull(svc03);
        }

        Assert.Throws<InvalidOperationException>(() => serviceProvider.GetService<IMyDisposable>());
    }

    [Fact]
    public void VerifyManyRegistrationTest()
    {
        IServiceCollection serviceCollection = new ServiceCollection();

        var serviceProvider = serviceCollection.BuildServiceProvider();

        var demo = serviceProvider.GetServices<IMyDisposable>();

        Assert.NotNull(demo);
    }

    [Fact]
    public void TryGetSingletonTest()
    {
        IServiceCollection serviceCollection = new ServiceCollection();

        serviceCollection.AddSingleton(new MyDisposableV2(this.output, new StringBuilder()));
        serviceCollection.AddSingleton<MyDisposable>();

        Assert.True(serviceCollection.TryGetSingleton<MyDisposableV2>(out var service));
        Assert.NotNull(service);

        Assert.False(serviceCollection.TryGetSingleton<MyDisposable>(out var service2));
        Assert.Null(service2);
    }
}
