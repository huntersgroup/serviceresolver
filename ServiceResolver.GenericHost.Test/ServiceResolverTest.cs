using System.Text;
using Microsoft.Extensions.DependencyInjection;
using ServiceResolver.GenericHost.Test.Services;
using ServiceResolver.GenericHost.Test.Services.Ambiguous;
using ServiceResolver.Ioc;
using ServiceResolver.Ioc.Advanced;
using ServiceResolver.Ioc.Exceptions;
using ServiceResolver.Ioc.Extensions;
using ServiceResolver.Ioc.Options;
using ServiceResolver.Ioc.SimpleInjector;
using ServiceResolver.Ioc.SimpleInjector.Advanced;
using SimpleInjector;
using SimpleInjector.Lifestyles;
using Xunit.Abstractions;

namespace ServiceResolver.GenericHost.Test;

public class ServiceResolverTest
{
    private readonly ITestOutputHelper output;

    public ServiceResolverTest(ITestOutputHelper output)
    {
        this.output = output;
    }

    [Fact]
    public void Test01()
    {
        var serviceResolver = this.GetServiceResolver();

        serviceResolver.Register(_ => this.output, LifetimeScope.Singleton)
            .Register<IMyDisposable, MyDisposable>(LifetimeScope.Scoped)
            .Register<ISvcDisposable, SvcDisposable>(LifetimeScope.Scoped)
            ;

        ISvcDisposable svc1;

        using (serviceResolver.BeginScope())
        {
            svc1 = serviceResolver.GetService<ISvcDisposable>();
        }

        Assert.NotNull(svc1);
        Assert.Equal(1, svc1.DisposeCounter);
        Assert.Equal(1, svc1.MyDisposable.DisposeCounter);
    }

    [Fact]
    public void Test02()
    {
        var serviceResolver = this.GetServiceResolver();

        serviceResolver.Register(_ => this.output, LifetimeScope.Singleton)
            .Register<IMyDisposable, MyDisposable>(LifetimeScope.Scoped)
            .Register<Amb1Svc>(LifetimeScope.Scoped)
            ;

        Amb1Svc svc1;

        using (serviceResolver.BeginScope())
        {
            svc1 = serviceResolver.GetService<Amb1Svc>();
        }

        Assert.NotNull(svc1);
        Assert.Equal(1, svc1.MyDisposable.DisposeCounter);
    }

    [Fact]
    public void GetServiceWithAmbiguousTest01()
    {
        var serviceResolver = this.GetServiceResolver();

        // acts
        serviceResolver.Register(_ => this.output, LifetimeScope.Singleton)
            .Register<IMyDisposable, MyDisposable>(LifetimeScope.Scoped)
            .Register<ISvcDisposable, SvcDisposable>(LifetimeScope.Scoped);

        // asserts
        Assert.ThrowsAny<ServiceRegistrationException>(() =>
            serviceResolver.Register<Amb1Svc>(LifetimeScope.Scoped));
    }

    [Fact]
    public void GetServiceWithAmbiguousTest01_Contract()
    {
        var serviceResolver = this.GetServiceResolver();

        // acts
        serviceResolver.Register(_ => this.output, LifetimeScope.Singleton)
            .Register<IMyDisposable, MyDisposable>(LifetimeScope.Scoped)
            .Register<ISvcDisposable, SvcDisposable>(LifetimeScope.Scoped);

        // asserts
        Assert.ThrowsAny<ServiceRegistrationException>(() =>
            serviceResolver.Register<IAmbSvc, Amb1Svc>(LifetimeScope.Scoped));
    }

    [Fact]
    public void GetServiceWithAmbiguousTest02()
    {
        var serviceResolver = this.GetServiceResolver();

        // acts
        serviceResolver.Register(_ => this.output, LifetimeScope.Singleton)
            .Register<IMyDisposable, MyDisposable>(LifetimeScope.Scoped)
            .Register<StringBuilder>(LifetimeScope.Scoped);

        // asserts
        Assert.ThrowsAny<ServiceRegistrationException>(() =>
            serviceResolver.Register<Amb2Svc>(LifetimeScope.Scoped));
    }

    [Fact]
    public void GetServiceWithAmbiguousTest02_Contract()
    {
        var serviceResolver = this.GetServiceResolver();

        // acts
        serviceResolver.Register(_ => this.output, LifetimeScope.Singleton)
            .Register<IMyDisposable, MyDisposable>(LifetimeScope.Scoped)
            .Register<StringBuilder>(LifetimeScope.Scoped);

        // asserts
        Assert.ThrowsAny<ServiceRegistrationException>(() =>
            serviceResolver.Register<IAmbSvc, Amb2Svc>(LifetimeScope.Scoped));
    }

    [Fact]
    public void GetServiceWithAmbiguousTest03()
    {
        var serviceResolver = this.GetServiceResolver();

        // acts
        serviceResolver.Register(_ => this.output, LifetimeScope.Singleton)
            .Register<ISvcDisposable, SvcDisposable>(LifetimeScope.Scoped)
            .Register<IMyDisposable, MyDisposable>(LifetimeScope.Scoped)
            .Register<StringBuilder>(LifetimeScope.Scoped);

        // asserts
        Assert.ThrowsAny<ServiceRegistrationException>(() =>
            serviceResolver.Register<Amb3Svc>(LifetimeScope.Scoped));
    }

    [Fact]
    public void GetServiceWithAmbiguousTest03_Contract()
    {
        var serviceResolver = this.GetServiceResolver();

        // acts
        serviceResolver.Register(_ => this.output, LifetimeScope.Singleton)
            .Register<ISvcDisposable, SvcDisposable>(LifetimeScope.Scoped)
            .Register<IMyDisposable, MyDisposable>(LifetimeScope.Scoped)
            .Register<StringBuilder>(LifetimeScope.Scoped);

        // asserts
        Assert.ThrowsAny<ServiceRegistrationException>(() =>
            serviceResolver.Register<IAmbSvc, Amb3Svc>(LifetimeScope.Scoped));
    }

    [Fact]
    public void GetServiceWithOnlyOneDependencyTest01()
    {
        var serviceResolver = this.GetServiceResolver();

        serviceResolver.Register(_ => this.output, LifetimeScope.Singleton)
            .Register<IMyDisposable, MyDisposable>(LifetimeScope.Scoped)
            .Register<Amb2Svc>(LifetimeScope.Scoped);

        Amb2Svc svc1;

        using (serviceResolver.BeginScope())
        {
            svc1 = serviceResolver.GetService<Amb2Svc>();
        }

        Assert.NotNull(svc1);
        Assert.NotNull(svc1.MyDisposable);
        Assert.Null(svc1.StrBuilder);
    }

    [Fact]
    public void GetServiceWithOnlyOneDependencyTest01_Contract()
    {
        var serviceResolver = this.GetServiceResolver();

        serviceResolver.Register(_ => this.output, LifetimeScope.Singleton)
            .Register<IMyDisposable, MyDisposable>(LifetimeScope.Scoped)
            .Register<IAmbSvc, Amb2Svc>(LifetimeScope.Scoped);

        Amb2Svc svc1;

        using (serviceResolver.BeginScope())
        {
            svc1 = (Amb2Svc)serviceResolver.GetService<IAmbSvc>();
        }

        Assert.NotNull(svc1);
        Assert.NotNull(svc1.MyDisposable);
        Assert.Null(svc1.StrBuilder);
    }

    [Fact]
    public void GetServiceWithOnlyOneDependencyTest02()
    {
        var serviceResolver = this.GetServiceResolver();

        serviceResolver.Register(_ => this.output, LifetimeScope.Singleton)
            .Register<StringBuilder>(LifetimeScope.Scoped)
            .Register<Amb2Svc>(LifetimeScope.Scoped);

        Amb2Svc svc1;

        using (serviceResolver.BeginScope())
        {
            svc1 = serviceResolver.GetService<Amb2Svc>();
        }

        Assert.NotNull(svc1);
        Assert.Null(svc1.MyDisposable);
        Assert.NotNull(svc1.StrBuilder);
    }

    [Fact]
    public void GetServiceWithOnlyOneDependencyTest02_Contract()
    {
        var serviceResolver = this.GetServiceResolver();

        serviceResolver.Register(_ => this.output, LifetimeScope.Singleton)
            .Register<StringBuilder>(LifetimeScope.Scoped)
            .Register<IAmbSvc, Amb2Svc>(LifetimeScope.Scoped);

        Amb2Svc svc1;

        using (serviceResolver.BeginScope())
        {
            svc1 = (Amb2Svc)serviceResolver.GetService<IAmbSvc>();
        }

        Assert.NotNull(svc1);
        Assert.Null(svc1.MyDisposable);
        Assert.NotNull(svc1.StrBuilder);
    }

    [Fact]
    public void GetServiceUsingDefCtorTest01()
    {
        var serviceResolver = this.GetServiceResolver();

        serviceResolver.Register(_ => this.output, LifetimeScope.Singleton)
            .Register<Amb1Svc>(LifetimeScope.Scoped)
            ;

        Amb1Svc svc1, svc2;

        using (serviceResolver.BeginScope())
        {
            svc1 = serviceResolver.GetService<Amb1Svc>();
            svc2 = serviceResolver.GetService<Amb1Svc>();
        }

        Assert.NotNull(svc1);
        Assert.NotNull(svc2);
        Assert.Same(svc1, svc2);
        Assert.Null(svc1.MyDisposable);
        Assert.Null(svc1.SvcDisposable);
    }

    [Fact]
    public void GetServiceUsingDefCtorTest01_Contract()
    {
        var serviceResolver = this.GetServiceResolver();

        serviceResolver.Register(_ => this.output, LifetimeScope.Singleton)
            .Register<IAmbSvc, Amb1Svc>(LifetimeScope.Scoped)
            ;

        Amb1Svc svc1, svc2;

        using (serviceResolver.BeginScope())
        {
            svc1 = (Amb1Svc)serviceResolver.GetService<IAmbSvc>();
            svc2 = (Amb1Svc)serviceResolver.GetService<IAmbSvc>();
        }

        Assert.NotNull(svc1);
        Assert.NotNull(svc2);
        Assert.Same(svc1, svc2);
        Assert.Null(svc1.MyDisposable);
        Assert.Null(svc1.SvcDisposable);
    }

    [Fact]
    public void GetServiceUsingDefCtorTest02()
    {
        var serviceResolver = this.GetServiceResolver();

        serviceResolver
            .Register<Amb2Svc>(LifetimeScope.Scoped);

        Amb2Svc svc1, svc2;

        using (serviceResolver.BeginScope())
        {
            svc1 = serviceResolver.GetService<Amb2Svc>();
            svc2 = serviceResolver.GetService<Amb2Svc>();
        }

        Assert.NotNull(svc1);
        Assert.NotNull(svc2);
        Assert.Same(svc1, svc2);
        Assert.Null(svc1.MyDisposable);
        Assert.Null(svc1.StrBuilder);
    }

    [Fact]
    public void GetServiceWithNoDependenciesTest01()
    {
        var serviceResolver = this.GetServiceResolver();

        // never should be thrown an exception
        serviceResolver.Register<Amb3Svc>(LifetimeScope.Transient);

        // assert
        Assert.Throws<ServiceUnavailableException>(() => serviceResolver.GetService<Amb3Svc>());
    }

    [Fact]
    public void GetServiceWithNoDependenciesTest02()
    {
        var serviceResolver = this.GetServiceResolver();

        // never should be thrown an exception
        serviceResolver.Register<Amb3Svc>(LifetimeScope.Transient)
            .Register<ISvcDisposable, SvcDisposable>()
            .Register(_ => new StringBuilder("ciaone"));

        // assert
        Assert.Throws<ServiceUnavailableException>(() => serviceResolver.GetService<Amb3Svc>());
    }

    [Fact]
    public void GetServiceWithBestCtorTest01()
    {
        var serviceResolver = this.GetServiceResolver();

        serviceResolver
            .Register<Amb3Svc>(LifetimeScope.Singleton)
            .Register(_ => new StringBuilder("ciaone"));

        serviceResolver.GetService<Amb3Svc>();
    }

    [Fact]
    public void GetServiceGenericTest01()
    {
        var serviceResolver = this.GetServiceResolver();

        serviceResolver
            .Register(_ => this.output)
            .Register(_ => new StringBuilder("ciaone"))
            .Register(typeof(IGenericChildSvc<>), typeof(GenericChildSvc<>));

        var svc01 = serviceResolver.GetService<IGenericChildSvc<StringBuilder>>();

        Assert.NotNull(svc01);
    }

    [Fact]
    public void GetServiceGenericTest02()
    {
        var serviceResolver = this.GetServiceResolver();

        serviceResolver
            .Register(_ => this.output)
            .Register(_ => new StringBuilder("ciaone"))
            .Register(typeof(IGenericChildSvc<>), typeof(GenericChildSvc<>))
            .Register(typeof(IGenericSvc<>), typeof(GenericSvc<>));

        var svc01 = serviceResolver.GetService<IGenericSvc<StringBuilder>>();

        Assert.NotNull(svc01);
    }

    [Fact]
    public void VerifyDisposingSingletonTest()
    {
        var serviceResolver = this.GetServiceResolver();

        var svc = new MyDisposable(this.output);

        serviceResolver.Register<IMyDisposable>(() => svc);

        var svc01 = serviceResolver.GetService<IMyDisposable>();

        // asserts
        Assert.NotNull(svc01);
        Assert.Same(svc, svc01);

        // acts
        serviceResolver.Dispose();

        Assert.Equal(1, svc.DisposeCounter);
    }

    [Fact]
    public void VerifyNoDisposingSingletonTest()
    {
        var serviceResolver = this.GetServiceResolver();

        var svc = new MyDisposable(this.output);

        serviceResolver.Register<IMyDisposable>(() => svc);

        // acts
        serviceResolver.Dispose();

        // asserts
        Assert.Equal(0, svc.DisposeCounter);
    }

    [Fact]
    public void VerifyDisposingSingletonTest02()
    {
        var serviceResolver = this.GetServiceResolver();

        var svc = new MyDisposable(this.output);

        serviceResolver.Register<IMyDisposable>(() => svc)
            .Register(_ => new StringBuilder("ciaone"));

        // acts
        var str = serviceResolver.GetService<StringBuilder>();
        serviceResolver.Dispose();

        // asserts
        Assert.NotNull(str);
        Assert.Equal(1, svc.DisposeCounter);
    }

    [Fact]
    public void VerifyOptionalParameterTest()
    {
        var serviceResolver = this.GetServiceResolverV2();

        serviceResolver.Register<IMyDisposable, MyDisposableV2>(LifetimeScope.Singleton)
            .Register(_ => this.output, LifetimeScope.Singleton)
            //.Register(_ => new StringBuilder("ciaone"), LifetimeScope.Singleton)
            ;

        var svc01 = serviceResolver.GetService<IMyDisposable>();

        var svc02 = serviceResolver.GetService<IMyDisposable>();

        Assert.NotNull(svc01);
        Assert.Null(((MyDisposableV2)svc01).Builder);

        Assert.NotNull(svc02);
    }

    [Fact]
    public void GetServiceWithAmbiguousAndOptParameterTest01()
    {
        var serviceResolver = this.GetServiceResolverV2();

        serviceResolver.Register<Amb4Svc>(LifetimeScope.Transient)
            .Register<ISvcDisposable, SvcDisposable>()
            .Register(_ => new StringBuilder("ciaone"));

        Assert.Throws<ServiceUnavailableException>(() => serviceResolver.GetService<Amb4Svc>());
    }

    [Fact]
    public void GetServiceWithAmbiguousAndOptParameterTest02()
    {
        var serviceResolver = this.GetServiceResolverV2();

        serviceResolver
            .Register<ISvcDisposable, SvcDisposable>()
            .Register(_ => new StringBuilder("ciaone"));

        Assert.Throws<ServiceRegistrationException>(() => serviceResolver.Register<Amb4Svc>(LifetimeScope.Transient));
    }

    [Fact]
    public void GetServiceWithAmbiguousAndOptParameterTest03()
    {
        var serviceResolver = this.GetServiceResolverV2();

        serviceResolver.Register<Amb4Svc>(LifetimeScope.Transient)
            .Register<IMyDisposable, MyDisposable>()
            .Register(_ => this.output, LifetimeScope.Singleton)
            .Register<ISvcDisposable, SvcDisposable>();

        Assert.ThrowsAny<ServiceUnavailableException>(() => serviceResolver.GetService<Amb4Svc>());
    }

    [Fact]
    public void GetServiceWithAmbiguousAndOptParameterTest04()
    {
        var serviceResolver = this.GetServiceResolverV2();

        serviceResolver
            .Register<IMyDisposable, MyDisposable>()
            .Register(_ => this.output, LifetimeScope.Singleton)
            .Register<ISvcDisposable, SvcDisposable>();

        Assert.ThrowsAny<ServiceRegistrationException>(() => serviceResolver.Register<Amb4Svc>(LifetimeScope.Transient));
    }

    [Fact]
    public void VerifyOptionalParameterTest02()
    {
        var serviceResolver = this.GetServiceResolverV2();

        serviceResolver.Register<Amb4Svc>(LifetimeScope.Transient)
            .Register<IMyDisposable, MyDisposable>()
            .Register(_ => this.output, LifetimeScope.Singleton)
            .Register<ISvcDisposable, SvcDisposable>()
            .Register(_ => new StringBuilder("ciaone"));

        var svc01 = serviceResolver.GetService<Amb4Svc>();
        var svc02 = serviceResolver.GetService<Amb4Svc>();

        Assert.NotNull(svc01);
        Assert.NotNull(svc01.StrBuilder);
        Assert.NotNull(svc01.MyDisposable);
        Assert.Null(svc02.SvcDisposable);

        Assert.NotNull(svc02);
        Assert.NotNull(svc02.StrBuilder);
        Assert.NotNull(svc02.MyDisposable);
        Assert.Null(svc02.SvcDisposable);
    }

    [Fact]
    public void VerifyOptionalParameterTest03()
    {
        var serviceResolver = this.GetServiceResolverV3();

        serviceResolver.Register<Amb5Svc>(LifetimeScope.Transient)
            .Register<IMyDisposable, MyDisposable>()
            .Register(_ => this.output, LifetimeScope.Singleton)
            .Register(_ => new StringBuilder("ciaone"));

        var svc01 = serviceResolver.GetService<Amb5Svc>();
        var svc02 = serviceResolver.GetService<Amb5Svc>();

        Assert.NotNull(svc01);
        Assert.NotNull(svc01.StrBuilder);
        Assert.NotNull(svc01.MyDisposable);

        Assert.NotNull(svc02);
        Assert.NotNull(svc02.StrBuilder);
        Assert.NotNull(svc02.MyDisposable);
    }

    [Fact]
    public void VerifyOptionalParameterTest04()
    {
        var serviceResolver = this.GetServiceResolverV3();

        serviceResolver.Register<Amb5Svc>(LifetimeScope.Transient)
            .Register<IMyDisposable, MyDisposable>()
            .Register(_ => this.output, LifetimeScope.Singleton);

        var svc01 = serviceResolver.GetService<Amb5Svc>();
        var svc02 = serviceResolver.GetService<Amb5Svc>();

        Assert.NotNull(svc01);
        Assert.Null(svc01.StrBuilder);
        Assert.NotNull(svc01.MyDisposable);

        Assert.NotNull(svc02);
        Assert.Null(svc02.StrBuilder);
        Assert.NotNull(svc02.MyDisposable);
    }

    [Fact]
    public void VerifyOptionalParameterTest05()
    {
        var serviceResolver = this.GetServiceResolverV3();

        serviceResolver
            .Register<IMyDisposable, MyDisposable>()
            .Register(_ => this.output, LifetimeScope.Singleton)
            .Register(_ => new StringBuilder("ciaone"));

        Assert.Throws<ServiceRegistrationException>(() => serviceResolver.Register<Amb4Svc>(LifetimeScope.Transient));
    }

    [Fact]
    public void VerifySvcWithEnumerableDependencyTest()
    {
        var serviceResolver = this.GetServiceResolverV2();

        serviceResolver.Register<SvcForDisposables01>();
        //serviceResolver.RegisterMany<IMyDisposable>(typeof(MyDisposable));
        //serviceResolver.Register(_ => this.output, LifetimeScope.Singleton);

        var svc = serviceResolver.GetService<SvcForDisposables01>();

        // asserts
        Assert.NotNull(svc);
        Assert.Empty(svc.Instances);
    }

    [Fact]
    public void VerifySvcWithEnumerableDependencyTest02()
    {
        var serviceResolver = this.GetServiceResolverV2();

        serviceResolver.RegisterLazy(typeof(ISvcForDisposables), LifetimeScope.Singleton.CreateRegistration<SvcForDisposables01>());
        //serviceResolver.RegisterMany<IMyDisposable>(typeof(MyDisposable));
        //serviceResolver.Register(_ => this.output, LifetimeScope.Singleton);

        var svc = serviceResolver.GetService<ISvcForDisposables>();

        // asserts
        Assert.NotNull(svc);
        Assert.Empty(svc.Instances);
    }

    [Fact]
    public void VerifySvcWithEnumerableDependencyTest03()
    {
        var serviceResolver = this.GetServiceResolverV2();

        serviceResolver.RegisterLazy(typeof(ISvcForDisposables), LifetimeScope.Singleton.CreateRegistration<SvcForDisposables01>());

        serviceResolver.RegisterLazy(typeof(ITestOutputHelper), LifetimeScope.Singleton.CreateRegistration(_ => this.output));

        serviceResolver.RegisterManyLazy(typeof(IMyDisposable),
            new List<IRegistrationInfo> { LifetimeScope.Singleton.CreateRegistration<MyDisposable>() });

        var svc = serviceResolver.GetService<ISvcForDisposables>();

        // asserts
        Assert.NotNull(svc);
        Assert.NotEmpty(svc.Instances);
    }

    private ServiceRegisterProvider GetServiceResolver()
    {
        return new ServiceRegisterProvider(new ServiceRegisterDescriptor
        {
            ScopeFactory = AsyncScopedLifestyle.BeginScope,
            Initializer = container =>
            {
                container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
                container.Options.DefaultLifestyle = Lifestyle.Singleton;
                container.Options.ConstructorResolutionBehavior = new BestConstructorResolutionBehavior(container);
            }
        });
    }

    private ServiceRegisterProvider GetServiceResolverV2()
    {
        return new ServiceRegisterProvider(new ServiceRegisterDescriptor
        {
            ScopeFactory = AsyncScopedLifestyle.BeginScope,
            Initializer = container =>
            {
                var options = container.Options;
                options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
                options.DefaultLifestyle = Lifestyle.Singleton;
                options.ConstructorResolutionBehavior = new BestConstructorResolutionBehavior(container);
                options.DependencyInjectionBehavior = new HybridDependencyInjectionBehavior(container, options.DependencyInjectionBehavior,
                    new InjectionBehaviorOptions
                    {
                        MissingManyRegistrations = ResolutionType.LessRestrictive,
                        OptionalParameters = ResolutionType.LessRestrictive
                    });
            }
        });
    }

    private ServiceRegisterProvider GetServiceResolverV3()
    {
        return new ServiceRegisterProvider(new ServiceRegisterDescriptor
        {
            ScopeFactory = AsyncScopedLifestyle.BeginScope,
            Initializer = container =>
            {
                var options = container.Options;
                options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
                options.DefaultLifestyle = Lifestyle.Singleton;
                options.DependencyInjectionBehavior = new OptionalDependencyInjectionBehavior(container, options.DependencyInjectionBehavior);
            }
        });
    }
}
