using System.Text;

namespace ServiceResolver.GenericHost.Test.Services.Ambiguous;

public interface IAmbSvc
{
    IMyDisposable MyDisposable { get; }
}

public class Amb1Svc : IAmbSvc
{
    public Amb1Svc()
    {

    }

    public Amb1Svc(IMyDisposable myDisposable)
    {
        this.MyDisposable = myDisposable;
    }

    public Amb1Svc(ISvcDisposable svcDisposable)
    {
        this.SvcDisposable = svcDisposable;
    }

    public IMyDisposable MyDisposable { get; }

    public ISvcDisposable SvcDisposable { get; }
}

public class Amb2Svc : IAmbSvc
{
    public Amb2Svc()
    {

    }

    public Amb2Svc(IMyDisposable myDisposable)
    {
        this.MyDisposable = myDisposable;
    }

    public Amb2Svc(StringBuilder strBuilder)
    {
        this.StrBuilder = strBuilder;
    }

    public StringBuilder StrBuilder { get; }

    public IMyDisposable MyDisposable { get; }
}

public class Amb3Svc : IAmbSvc
{
    public Amb3Svc(StringBuilder strBuilder)
    {
        this.StrBuilder = strBuilder;
    }

    public Amb3Svc(IMyDisposable myDisposable, StringBuilder strBuilder)
    {
        this.MyDisposable = myDisposable;
        this.StrBuilder = strBuilder;
    }

    public Amb3Svc(ISvcDisposable svcDisposable, StringBuilder strBuilder)
    {
        this.SvcDisposable = svcDisposable;
        this.StrBuilder = strBuilder;
    }

    public StringBuilder StrBuilder { get; }

    public IMyDisposable MyDisposable { get; }

    public ISvcDisposable SvcDisposable { get; }
}

public class Amb4Svc : IAmbSvc
{
    public Amb4Svc(IMyDisposable myDisposable, StringBuilder strBuilder = null)
    {
        this.MyDisposable = myDisposable;
        this.StrBuilder = strBuilder;
    }

    public Amb4Svc(StringBuilder strBuilder)
    {
        this.StrBuilder = strBuilder;
    }

    public Amb4Svc(IMyDisposable myDisposable)
    {
        this.MyDisposable = myDisposable;
    }

    public Amb4Svc(ISvcDisposable svcDisposable)
    {
        this.SvcDisposable = svcDisposable;
    }

    public StringBuilder StrBuilder { get; }

    public IMyDisposable MyDisposable { get; }

    public ISvcDisposable SvcDisposable { get; }
}

public class Amb5Svc : IAmbSvc
{
    public Amb5Svc(IMyDisposable myDisposable, StringBuilder strBuilder = null)
    {
        this.MyDisposable = myDisposable;
        this.StrBuilder = strBuilder;
    }

    public StringBuilder StrBuilder { get; }

    public IMyDisposable MyDisposable { get; }

}
