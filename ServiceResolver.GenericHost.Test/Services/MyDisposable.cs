using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Xunit.Abstractions;

namespace ServiceResolver.GenericHost.Test.Services;

public class MyDisposable : IMyDisposable, IDisposable
{
    private readonly ITestOutputHelper output;
    private readonly string id;
    private byte counter;

    public MyDisposable(ITestOutputHelper output)
    {
        this.output = output;
        this.id = Guid.NewGuid().ToString("D");
    }

    public byte DisposeCounter => this.counter;

    public void Dispose()
    {
        this.counter++;
        this.output.WriteLine($"dispose counter: {this.counter}, class: {nameof(MyDisposable)}, id: {this.id}");
    }

    public void DoWork(string message)
    {
        this.output.WriteLine($"message: {message}");
    }
}

public class MyDisposableV2 : MyDisposable
{
    public MyDisposableV2(ITestOutputHelper output, StringBuilder builder = null)
        :base(output)
    {
        this.Builder = builder;
    }

    public StringBuilder Builder { get; }
}

public class MyDisposableV3 : MyDisposable
{
    public MyDisposableV3([ServiceKey] string key, ITestOutputHelper output, StringBuilder builder = null)
        : base(output)
    {
        this.Builder = builder;
        this.Key = key;
    }

    public string Key { get; }

    public StringBuilder Builder { get; }
}

public class MyDisposableV4 : MyDisposable
{
    public MyDisposableV4([ServiceKey] string key, ITestOutputHelper output, StringBuilder builder = null)
        : base(output)
    {
        this.Builder = builder;
        this.Key = key;
    }

    public string Key { get; }

    public StringBuilder Builder { get; }
}

public class MyDisposableV5 : MyDisposable
{
    public MyDisposableV5(ITestOutputHelper output, IServiceProvider serviceProvider)
        : base(output)
    {
        this.Provider = serviceProvider;
    }

    public string Key { get; }

    public IServiceProvider Provider { get; }
}

public interface IMyDisposable
{
    void DoWork(string message);

    byte DisposeCounter { get; }
}
