namespace ServiceResolver.GenericHost.Test.Services;

/// <summary>
/// 
/// </summary>
/// <typeparam name="TData"></typeparam>
public interface ISvcForGenerics<out TData>
{
    /// <summary>
    /// 
    /// </summary>
    IEnumerable<TData> Instances { get; }
}

/// <summary>
/// 
/// </summary>
/// <typeparam name="TData"></typeparam>
public class SvcForGenerics<TData> : ISvcForGenerics<TData>
{
    ///<inheritdoc />
    public IEnumerable<TData> Instances { get; init; }
}
