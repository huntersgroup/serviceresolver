namespace ServiceResolver.GenericHost.Test.Services;

public interface ISvcForDisposables : ISvcForGenerics<IMyDisposable>
{
}

public class SvcForDisposables01 : ISvcForDisposables
{
    public SvcForDisposables01(IEnumerable<IMyDisposable> disposables)
    {
        this.Instances = disposables;
    }

    public IEnumerable<IMyDisposable> Instances { get; }
}
