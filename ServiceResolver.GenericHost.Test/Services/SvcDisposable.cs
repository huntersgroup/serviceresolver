namespace ServiceResolver.GenericHost.Test.Services;

public class SvcDisposable : ISvcDisposable, IDisposable
{
    private byte counter;

    public SvcDisposable(IMyDisposable myDisposable)
    {
        this.MyDisposable = myDisposable;
    }

    public byte DisposeCounter => this.counter;

    public void DoWork(string message)
    {
        this.MyDisposable.DoWork(message);
    }

    public IMyDisposable MyDisposable { get; }

    public void Dispose()
    {
        this.counter++;
    }
}

public interface ISvcDisposable : IMyDisposable
{
    IMyDisposable MyDisposable { get; }
}
