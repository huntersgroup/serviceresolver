using Xunit.Abstractions;

namespace ServiceResolver.GenericHost.Test.Services;

public interface IGenericSvc<out TOptions>
    where TOptions : class
{
    IGenericChildSvc<TOptions> Child { get; }
}

public interface IGenericChildSvc<out TOptions>
    where TOptions : class
{
    TOptions Options { get; }

    ITestOutputHelper Output { get; }
}


public class GenericSvc<TOptions> : IGenericSvc<TOptions>
    where TOptions : class
{
    public GenericSvc(IGenericChildSvc<TOptions> child)
    {
        this.Child = child;
    }

    public IGenericChildSvc<TOptions> Child { get; }
}

public class GenericChildSvc<TOptions> : IGenericChildSvc<TOptions>
    where TOptions : class
{
    public GenericChildSvc(TOptions options, ITestOutputHelper output)
    {
        this.Options = options;
        this.Output = output;
    }

    public TOptions Options { get; }

    public ITestOutputHelper Output { get; }
}
