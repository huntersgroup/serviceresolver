using System;
using System.Diagnostics;
using DryIoc;
using ServiceResolver.Ioc.Exceptions;
using ServiceResolver.Ioc.Extensions;
using ServiceResolver.Ioc.Resolvers;

namespace ServiceResolver.Ioc.DryIoc.Resolvers;

/// <summary>
/// 
/// </summary>
/// <typeparam name="TCService"></typeparam>
[DebuggerDisplay("serviceType: {this.ServiceType}")]
public class ServiceRegisterResolver<TCService>
    : ServiceRegisterImpl, IServiceResolver<TCService>
    where TCService : class
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ServiceRegisterResolver{TCService}"/> class.
    /// </summary>
    internal ServiceRegisterResolver(Func<IContainer, IContainer> containerAction = null)
        : this(typeof(TCService), containerAction)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ServiceRegisterResolver{TCService}"/> class.
    /// </summary>
    /// <param name="serviceType">Type of the service.</param>
    /// <param name="containerAction">The container action.</param>
    internal ServiceRegisterResolver(Type serviceType, Func<IContainer, IContainer> containerAction = null)
        : base(containerAction)
    {
        this.ServiceType = serviceType ?? typeof(TCService);
    }

    /// <inheritdoc />
    public TCService Resolve()
    {
        try
        {
            //return this.Container.Resolve<TCService>();
            return this.GetService<TCService>();
        }
        catch (Exception ex)
        {
            throw new ServiceUnavailableException(this.ServiceType, ex.Message, ex);
        }
    }

    /// <inheritdoc />
    public Type ServiceType { get; }

    /// <inheritdoc />
    object IServiceResolver.GetService(Type compatibleType)
    {
        try
        {
            if (compatibleType == null)
            {
                //return this.Container.Resolve(this.Type);
                return this.GetService(this.ServiceType);
            }

            if (!this.ServiceType.IsCompatible(compatibleType))
            {
                throw new InvalidOperationException("The current service type is open generic, so the service type argument must be compatible.");
            }

            //return this.Type.IsGenericTypeDefinition ? this.Container.Resolve(compatibleType) : this.Container.Resolve(this.Type);
            return this.ServiceType.IsGenericTypeDefinition
                ? this.GetService(compatibleType)
                : this.GetService(this.ServiceType);
        }
        catch (Exception ex)
        {
            throw new ServiceUnavailableException(this.ServiceType, ex.Message, ex);
        }
    }
}
