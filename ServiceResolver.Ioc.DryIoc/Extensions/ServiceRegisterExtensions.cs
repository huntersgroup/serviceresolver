using System;
using DryIoc;

namespace ServiceResolver.Ioc.DryIoc.Extensions;

internal static class ServiceRegisterExtensions
{
    internal static ServiceRegisterImpl RegisterInfo(this ServiceRegisterImpl serviceRegister, Type serviceType, IRegistrationInfo registrationInfo)
    {
        //Register<YourService>(setup: Setup.With(allowDisposableTransient: true))

        var reuse = registrationInfo.Scope.Translate();
        var setup = registrationInfo.SuppressDiagnostic.CreateSetup();

        if (registrationInfo.Factory is not null)
        {
            serviceRegister.Container.RegisterDelegate(serviceType, _ => registrationInfo.Factory.Invoke(serviceRegister), reuse, setup);
        }
        else
        {
            serviceRegister.Container.Register(serviceType, registrationInfo.ConcreteType, reuse, setup: setup);
        }

        return serviceRegister;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serviceRegister"></param>
    /// <param name="registrationInfo"></param>
    /// <returns></returns>
    internal static Factory CreateFactory(this ServiceRegisterImpl serviceRegister, IRegistrationInfo registrationInfo)
    {
        Factory ret;
        var reuse = registrationInfo.Scope.Translate();
        var setup = registrationInfo.SuppressDiagnostic.CreateSetup();

        if (registrationInfo.ConcreteType is not null)
        {
            ret = ReflectionFactory.Of(registrationInfo.ConcreteType, reuse: reuse, setup: setup);
        }
        else
        {
            ret = DelegateFactory.Of(_ => registrationInfo.Factory(serviceRegister), reuse: reuse, setup: setup);
        }

        return ret;
    }

    private static Setup CreateSetup(this SuppressDiagnostic diagnostic)
    {
        return Setup.With(
            allowDisposableTransient: diagnostic.DisposableTransientComponent
            );
    }
}
