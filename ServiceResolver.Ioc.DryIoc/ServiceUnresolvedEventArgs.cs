using System;

namespace ServiceResolver.Ioc.DryIoc;

/// <summary>
/// 
/// </summary>
/// <seealso cref="System.EventArgs" />
public class ServiceUnresolvedEventArgs : EventArgs
{
    /// <summary>
    /// Gets the type of the service.
    /// </summary>
    /// <value>
    /// The type of the service.
    /// </value>
    public Type ServiceType { get; }

    /// <summary>
    /// Gets a value indicating whether this <see cref="ServiceUnresolvedEventArgs"/> is handled.
    /// </summary>
    /// <value>
    ///   <c>true</c> if handled; otherwise, <c>false</c>.
    /// </value>
    public bool Handled { get; private set; }

    /// <summary>
    /// Gets the service resolved.
    /// </summary>
    /// <value>
    /// The service resolved.
    /// </value>
    public object ServiceResolved { get; private set; }

    /// <summary>
    /// Initializes a new instance of the <see cref="ServiceUnresolvedEventArgs"/> class.
    /// </summary>
    /// <param name="serviceType">Type of the service.</param>
    public ServiceUnresolvedEventArgs(Type serviceType)
    {
        this.ServiceType = serviceType;
        this.Handled = false;
    }

    /// <summary>
    /// set resolved service
    /// </summary>
    /// <param name="service">service resolved</param>
    /// <exception cref="ArgumentException">service not implement requested type</exception>
    public void SetServiceResolved(object service)
    {
        if(!this.ServiceType.IsInstanceOfType(service))
        {
            throw  new ArgumentException($"service must implement type: {this.ServiceType} ", nameof(service));
        }

        this.Handled = true;
        this.ServiceResolved = service;
    }
}
