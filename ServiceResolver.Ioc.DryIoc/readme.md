**ServiceResolver.Ioc.DryIoc** is the implementation of **ServiceResolver.Ioc** framework, which uses DryIoc as underlying injection engine.

for details, see the [documentation](https://bitbucket.org/huntersgroup/serviceresolver/wiki/DryIoc).
