using System;

namespace ServiceResolver.Ioc.DryIoc;

/// <summary>
/// 
/// </summary>
public class ResolutionContext<TRegistration, TResolver>
    where TResolver : class
    where TRegistration : class
{
    /// <summary>
    /// 
    /// </summary>
    public Type ServiceType { get; init; }

    /// <summary>
    /// 
    /// </summary>
    public TRegistration Registration { get; init; }

    /// <summary>
    /// 
    /// </summary>
    public TResolver Resolver { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public bool Handled { get => this.Resolver != null; }

    ///<inheritdoc />
    public override int GetHashCode()
    {
        return this.ServiceType.GetHashCode();
    }

    ///<inheritdoc />
    public override bool Equals(object obj)
    {
        if (obj is ResolutionContext<TRegistration, TResolver> ctx)
        {
            return this.ServiceType == ctx.ServiceType;
        }

        return false;
    }
}
