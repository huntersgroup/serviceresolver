using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DryIoc;
using ServiceResolver.Ioc.Caching;
using ServiceResolver.Ioc.DryIoc.Extensions;
using ServiceResolver.Ioc.DryIoc.Registers;
using ServiceResolver.Ioc.DryIoc.Resolvers;
using ServiceResolver.Ioc.Exceptions;
using ServiceResolver.Ioc.Extensions;
using ServiceResolver.Ioc.Registers;
using ServiceResolver.Ioc.Resolvers;

namespace ServiceResolver.Ioc.DryIoc;

/// <summary>
/// 
/// </summary>
/// <seealso cref="IServiceRegister" />
/// <seealso cref="IGenericServiceProvider" />
public class ServiceRegisterImpl : IServiceRegister, IGenericServiceProvider, IDisposable
{
    private readonly ServiceCache cache;
    private readonly ScopeManager scopeManager;
    private readonly ICollection<IServiceResolver> resolvers;
    private readonly HashSet<SingleRegistration> lazyRegistrations;
    private readonly HashSet<Type> manyRegistrations;
    private readonly HashSet<ManyRegistration> manyLazyRegistrations;

    private readonly ResolutionContextManager<IServiceResolver, Factory> singleResolverResolutionContext = new();
    private readonly ResolutionContextManager<SingleRegistration, Factory> singleLazyResolutionContext = new();
    private readonly ResolutionContextManager<ManyRegistration, IEnumerable<DynamicRegistration>> manyLazyResolutionContext = new();

    /// <summary>
    /// Initializes a new instance of the <see cref="ServiceRegisterImpl"/> class.
    /// </summary>
    /// <param name="containerAction">The container action.</param>
    protected ServiceRegisterImpl(Func<IContainer, IContainer> containerAction = null)
    {
        this.cache = new ServiceCache();
        this.ContainerLocked = false;

        this.resolvers = [];
        this.lazyRegistrations = [];
        this.manyRegistrations = [];
        this.manyLazyRegistrations = [];

        var container = new Container()
                .With(rules =>
                    rules
                        .WithUnknownServiceResolvers(this.CreateFactoryOnSingleLazy)    // third (works only for single)
                        .WithUnknownServiceResolvers(this.CreateFactoryOnResolvers)     // second
                        .WithDynamicRegistrations(this.GetDynamicRegistrationOnLazy)    //first (works for single and many)
                    )
                //.With(rules => rules.WithUnknownServiceResolvers(this.CreateFactoryOnLazy))
                //.WithNoMoreRegistrationAllowed()
                //.With(rules => rules.WithUnknownServiceResolvers(global::DryIoc.Rules.AutoResolveConcreteTypeRule()))
            ;

        this.Container = containerAction?.Invoke(container) ?? container;
        
        this.scopeManager = new ScopeManager(this.Container);
    }

    /// <summary>
    /// 
    /// </summary>
    public event EventHandler<ServiceUnresolvedEventArgs> Unresolved;

    /// <inheritdoc />
    public IServiceCache Cache { get => this.cache; }

    /// <summary>
    /// Gets the container.
    /// </summary>
    /// <value>
    /// The container.
    /// </value>
    protected internal IContainer Container { get; }

    /// <summary>
    /// Gets or sets a value indicating whether this <see cref="ServiceRegisterImpl"/> is verified.
    /// </summary>
    /// <value>
    ///   <c>true</c> if verified; otherwise, <c>false</c>.
    /// </value>
    protected bool ContainerLocked { get; set; }

    /// <inheritdoc />
    public IScope BeginScope()
    {
        return this.scopeManager.OpenScope();
    }

    /// <inheritdoc />
    public IServiceRegister Register(ServiceKey svcKey, IRegistrationInfo registrationInfo)
    {
        this.ThrowExceptionIfRegistered(svcKey);
        registrationInfo.Validate(svcKey.Type);

        try
        {
            return this.RegisterInfo(svcKey.Type, registrationInfo);
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(svcKey.Type, "The given service cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    public IServiceRegister Register(Type serviceType, IRegistrationInfo registrationInfo)
    {
        return this.Register(new ServiceKey(serviceType), registrationInfo);
    }

    /// <inheritdoc />
    public IServiceRegister Register<TService>(IRegistrationInfo<TService> registrationInfo)
        where TService : class
    {
        return this.Register(typeof(TService), registrationInfo);
    }

    /// <inheritdoc />
    public IServiceRegister Register<TService, TImplementor>(IRegistrationInfo<TImplementor> registrationInfo)
        where TService : class where TImplementor : class, TService
    {
        return this.Register(typeof(TService), registrationInfo);
    }

    public IServiceRegister RegisterLazy(ServiceKey svcKey, IRegistrationInfo registrationInfo)
    {
        this.ThrowExceptionIfRegistered(svcKey);
        registrationInfo.Validate(svcKey.Type);

        try
        {
            this.lazyRegistrations.Add(new SingleRegistration
            {
                ServiceKey = svcKey,
                Registration = registrationInfo
            });

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(svcKey.Type, "The given service cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    public IServiceRegister RegisterLazy(Type serviceType, IRegistrationInfo registrationInfo)
    {
        return this.RegisterLazy(new ServiceKey(serviceType), registrationInfo);
    }

    /// <inheritdoc />
    public IServiceRegister RegisterLazy<TService>(IRegistrationInfo<TService> registrationInfo)
        where TService : class
    {
        return this.RegisterLazy(typeof(TService), registrationInfo);
    }

    /// <inheritdoc />
    public IServiceRegister RegisterLazy<TService, TImplementor>(IRegistrationInfo<TImplementor> registrationInfo)
        where TService : class where TImplementor : class, TService
    {
        return this.RegisterLazy(typeof(TService), registrationInfo);
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">Service registration exception</exception>
    public IServiceRegister Register(Type serviceType, Type implementorType, LifetimeScope? scope = null)
    {
        this.CheckContainerLocked(serviceType);
        this.ThrowExceptionIfRegistered(new ServiceKey(serviceType));

        try
        {
            if (scope.HasValue)
            {
                this.Container.Register(serviceType, implementorType, reuse: scope.Value.Translate(), ifAlreadyRegistered: IfAlreadyRegistered.Throw);
            }
            else
            {
                this.Container.Register(serviceType, implementorType, ifAlreadyRegistered: IfAlreadyRegistered.Throw);
            }

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(serviceType,
                "The given service cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    public IServiceRegister Register(Type serviceType, Func<object> serviceFactory, LifetimeScope? scope = null)
    {
        this.CheckContainerLocked(serviceType);
        this.ThrowExceptionIfRegistered(new ServiceKey(serviceType));

        try
        {
            if (scope.HasValue)
            {
                this.Container.RegisterDelegate(serviceType, _ => serviceFactory.Invoke(), scope.Value.Translate(), ifAlreadyRegistered: IfAlreadyRegistered.Throw);
            }
            else
            {
                this.Container.RegisterDelegate(serviceType, _ => serviceFactory.Invoke(), ifAlreadyRegistered: IfAlreadyRegistered.Throw);
            }

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(serviceType,
                "The given service cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    public IServiceRegister Register(Type serviceType, Func<IGenericServiceProvider, object> serviceCreator, LifetimeScope? scope = null)
    {
        this.CheckContainerLocked(serviceType);
        this.ThrowExceptionIfRegistered(new ServiceKey(serviceType));

        try
        {
            if (scope.HasValue)
            {
                this.Container.RegisterDelegate(serviceType, _ => serviceCreator.Invoke(this), scope.Value.Translate(), ifAlreadyRegistered: IfAlreadyRegistered.Throw);
            }
            else
            {
                this.Container.RegisterDelegate(serviceType, _ => serviceCreator.Invoke(this), ifAlreadyRegistered: IfAlreadyRegistered.Throw);
            }

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(serviceType,
                "The given service cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">Service registration exception</exception>
    public IServiceRegister Register<TService, TImplementor>(LifetimeScope? scope = null)
        where TService : class where TImplementor : class, TService
    {
        var serviceType = typeof(TService);

        this.CheckContainerLocked(serviceType);
        this.ThrowExceptionIfRegistered(new ServiceKey(serviceType));

        try
        {
            if (scope.HasValue)
            {
                this.Container.Register<TService, TImplementor>(scope.Value.Translate(), ifAlreadyRegistered: IfAlreadyRegistered.Throw);
            }
            else
            {
                this.Container.Register<TService, TImplementor>(ifAlreadyRegistered: IfAlreadyRegistered.Throw);
            }

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(typeof(TService),
                "The given service cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">Service registration exception</exception>
    public IServiceRegister Register<TConcreteService>(LifetimeScope? scope = null) where TConcreteService : class
    {
        var serviceType = typeof(TConcreteService);

        this.CheckContainerLocked(serviceType);
        this.ThrowExceptionIfRegistered(new ServiceKey(serviceType));

        try
        {
            if (scope.HasValue)
            {
                this.Container.Register(typeof(TConcreteService), scope.Value.Translate(), ifAlreadyRegistered: IfAlreadyRegistered.Throw);
            }
            else
            {
                this.Container.Register(typeof(TConcreteService), ifAlreadyRegistered: IfAlreadyRegistered.Throw);
            }

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(typeof(LifetimeScope),
                "The given service cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">Service registration exception</exception>
    public IServiceRegister Register(Type implementorType, LifetimeScope? scope = null)
    {
        var serviceType = implementorType;

        this.CheckContainerLocked(serviceType);
        this.ThrowExceptionIfRegistered(new ServiceKey(serviceType));

        try
        {
            if (scope.HasValue)
            {
                this.Container.Register(implementorType, scope.Value.Translate(), ifAlreadyRegistered: IfAlreadyRegistered.Throw);
            }
            else
            {
                this.Container.Register(implementorType, ifAlreadyRegistered: IfAlreadyRegistered.Throw);
            }

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(typeof(LifetimeScope),
                "The given service cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">Service registration exception</exception>
    public IServiceRegister Register<TService>(Func<TService> serviceCreator, LifetimeScope? scope = null)
        where TService : class
    {
        var serviceType = typeof(TService);
        this.CheckContainerLocked(serviceType);
        this.ThrowExceptionIfRegistered(new ServiceKey(serviceType));

        try
        {
            if (scope.HasValue)
            {
                this.Container.RegisterDelegate(_ => serviceCreator.Invoke(), scope.Value.Translate(), ifAlreadyRegistered: IfAlreadyRegistered.Throw);
            }
            else
            {
                this.Container.RegisterDelegate(_ => serviceCreator.Invoke(), ifAlreadyRegistered: IfAlreadyRegistered.Throw);
            }

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(typeof(TService),
                "The given service cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">Service registration exception</exception>
    public IServiceRegister Register<TService>(Func<IGenericServiceProvider, TService> serviceCreator,
        LifetimeScope? scope = null) where TService : class
    {
        var serviceType = typeof(TService);
        this.CheckContainerLocked(serviceType);
        this.ThrowExceptionIfRegistered(new ServiceKey(serviceType));

        try
        {
            if (scope.HasValue)
            {
                this.Container.RegisterDelegate(_ => serviceCreator.Invoke(this), scope.Value.Translate(), ifAlreadyRegistered: IfAlreadyRegistered.Throw);
            }
            else
            {
                this.Container.RegisterDelegate(_ => serviceCreator.Invoke(this), ifAlreadyRegistered: IfAlreadyRegistered.Throw);
            }

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(typeof(TService),
                "The given service cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    public IServiceRegister RegisterDecorator<TService, TDecorator>(LifetimeScope? scope = null)
        where TService : class
        where TDecorator : class, TService
    {
        return this.RegisterDecorator(typeof(TService), typeof(TDecorator), scope);
    }

    /// <inheritdoc />
    public IServiceRegister RegisterDecorator(Type serviceType, Type decoratorType, LifetimeScope? scope = null)
    {
        this.CheckContainerLocked(serviceType);

        try
        {
            if (scope.HasValue)
            {
                this.Container.Register(serviceType, decoratorType, setup: Setup.Decorator, reuse: scope.Value.Translate(), ifAlreadyRegistered: IfAlreadyRegistered.Throw);
            }
            else
            {
                this.Container.Register(serviceType, decoratorType, setup: Setup.Decorator, ifAlreadyRegistered: IfAlreadyRegistered.Throw);
            }

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(serviceType, "The given service cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">The given service cannot register initializer, see inner exception for details.</exception>
    public IServiceRegister RegisterConditional<TService>(Action<IConditionalRegister<TService>> conditionalRegister)
        where TService : class
    {
        var serviceType = typeof(TService);
        this.CheckContainerLocked(serviceType);
        this.ThrowExceptionIfRegistered(new ServiceKey(serviceType));

        try
        {
            conditionalRegister(new ConditionalRegister<TService>(this, this.Container));

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(typeof(TService),
                "The given service cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    public IServiceRegister RegisterConditional(Type serviceType, Action<IConditionalRegister> conditionalRegister)
    {
        this.CheckContainerLocked(serviceType);
        this.ThrowExceptionIfRegistered(new ServiceKey(serviceType));

        try
        {
            conditionalRegister(new ConditionalRegister<object>(this, this.Container, serviceType));

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(serviceType,
                "The given service cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">Error in register initializer</exception>
    public IServiceRegister RegisterInitializer<TService>(
        Action<IGenericServiceProvider, TService> serviceInitialize) where TService : class
    {
        this.CheckContainerLocked(typeof(TService));

        try
        {
            this.Container.RegisterInitializer<TService>((service, _) =>
                serviceInitialize.Invoke(this, service));

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(typeof(TService),
                "The given service cannot register initializer, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">Error in register initializer</exception>
    public IServiceRegister RegisterInitializer<TService>(Action<TService> serviceInitialize) where TService : class
    {
        this.CheckContainerLocked(typeof(TService));

        try
        {
            this.Container.RegisterInitializer<TService>((service, _) =>
                serviceInitialize.Invoke(service));

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(typeof(TService),
                "The given service cannot register initializer, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">Error in register service</exception>
    public IServiceRegister RegisterMany<TService>(params Type[] implementorTypes) where TService : class
    {
        return this.RegisterMany(typeof(TService), implementorTypes.ToList());
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException"></exception>
    public IServiceRegister RegisterMany<TService>(IEnumerable<Type> implementorTypes, LifetimeScope? scope = null)
        where TService : class
    {
        return this.RegisterMany(typeof(TService), implementorTypes, scope);
    }

    /// <inheritdoc />
    public IServiceRegister RegisterMany(Type serviceType, IEnumerable<Type> implementorTypes, LifetimeScope? scope = null)
    {
        this.CheckContainerLocked(serviceType);

        try
        {
            if (scope.HasValue)
            {
                this.Container.RegisterMany(implementorTypes, serviceTypeCondition: type => type == serviceType, reuse: scope.Value.Translate());
            }
            else
            {
                this.Container.RegisterMany(implementorTypes, serviceTypeCondition: type => type == serviceType);
            }

            this.manyRegistrations.Add(serviceType);

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(typeof(IEnumerable<>).MakeGenericType(serviceType),
                "The given service collection cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">Error in register service</exception>
    public IServiceRegister RegisterMany<TService>(params TService[] singletons) where TService : class
    {
        var serviceType = typeof(TService);
        this.CheckContainerLocked(serviceType);

        try
        {
            this.Container.RegisterMany(singletons.Select(t => t.GetType()),
                serviceTypeCondition: type => type == serviceType);

            this.manyRegistrations.Add(typeof(TService));

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(typeof(IEnumerable<TService>),
                "The given service collection cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">Error in register service</exception>
    public IServiceRegister RegisterMany<TService>(params Assembly[] assemblies) where TService : class
    {
        return this.RegisterMany(typeof(TService), assemblies.ToList());
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">Error in register service</exception>
    public IServiceRegister RegisterMany<TService>(IEnumerable<Assembly> assemblies, LifetimeScope? scope = null)
        where TService : class
    {
        return this.RegisterMany(typeof(TService), assemblies, scope);
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">Error in register service</exception>
    public IServiceRegister RegisterMany(Type serviceType, params Assembly[] assemblies)
    {
        return this.RegisterMany(serviceType, assemblies.ToList());
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">Error in register service</exception>
    public IServiceRegister RegisterMany(Type serviceType, IEnumerable<Assembly> assemblies, LifetimeScope? scope = null)
    {
        return this.RegisterMany(new ServiceKey(serviceType), assemblies, scope);
    }

    /// <inheritdoc />
    public IServiceRegister RegisterMany(ServiceKey svcKey, IEnumerable<Assembly> assemblies, LifetimeScope? scope = null)
    {
        var serviceType = svcKey.Type;
        this.CheckContainerLocked(serviceType);
        this.ThrowExceptionIfRegistered(svcKey, true);

        try
        {
            if (scope.HasValue)
            {
                this.Container.RegisterMany(assemblies, type => type == serviceType, reuse: scope.Value.Translate());
            }
            else
            {
                this.Container.RegisterMany(assemblies, type => type == serviceType);
            }

            this.manyRegistrations.Add(serviceType);

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(serviceType,
                "The given service collection cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException"></exception>
    public IServiceRegister RegisterMany<TService>(IEnumerable<IRegistrationInfo> registrationInfos)
        where TService : class
    {
        return this.RegisterMany(typeof(TService), registrationInfos);
    }

    /// <inheritdoc />
    public IServiceRegister RegisterMany<TService>(IEnumerable<IRegistrationInfo<TService>> registrationInfos)
        where TService : class
    {
        return this.RegisterMany(typeof(TService), registrationInfos);
    }

    /// <inheritdoc />
    public IServiceRegister RegisterMany(Type serviceType, IEnumerable<IRegistrationInfo> registrationInfos)
    {
        return this.RegisterMany(new ServiceKey(serviceType), registrationInfos);
    }

    /// <inheritdoc />
    public IServiceRegister RegisterMany(ServiceKey svcKey, IEnumerable<IRegistrationInfo> registrationInfos)
    {
        var serviceType = svcKey.Type;
        this.ThrowExceptionIfRegistered(new ServiceKey(serviceType), true);

        try
        {
            var regs = (registrationInfos ?? [])
                .ToList();

            if (!regs.Any())
            {
                throw new ArgumentException("The collection of RegistrationInfo cannot be null or empty.");
            }

            registrationInfos.Validate(serviceType);

            var scope = regs.First().Scope;

            if (serviceType.IsGenericTypeDefinition)
            {
                if (regs.Any(info => info.Scope != scope))
                {
                    throw new InvalidOperationException("All open generic types registrations must have the same LifeStyle");
                }

                this.Container.RegisterMany(regs.Select(info => info.ConcreteType).ToList(),
                    serviceTypeCondition: type => type.IsCompatible(serviceType), reuse: scope.Translate());
            }
            else
            {
                var typesImpl = regs.Where(info => info.ConcreteType is not null).ToList();
                this.Container.RegisterMany(implTypes: typesImpl.Select(info => info.ConcreteType)
                    , serviceTypeCondition: type => type.IsCompatible(serviceType), reuse: scope.Translate());


                var factories = regs.Where(info => info.Factory is not null).ToList();

                // TODO: missing implementation for this use-case
                //this.Container.RegisterMany();

            }

            this.manyRegistrations.Add(serviceType);

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(serviceType, "The given service collection cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    public IServiceRegister RegisterManyLazy(ServiceKey svcKey, IEnumerable<IRegistrationInfo> registrationInfos)
    {
        this.ThrowExceptionIfRegistered(svcKey, true);

        try
        {
            var regs = registrationInfos.ToList();

            regs.Validate(svcKey.Type);

            this.manyLazyRegistrations.Add(new ManyRegistration
            {
                ServiceKey = svcKey,
                Registrations = regs.ToList()
            });

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(svcKey.Type, "The given service collection cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    public IServiceRegister RegisterManyLazy(Type serviceType, IEnumerable<IRegistrationInfo> registrationInfos)
    {
        return this.RegisterManyLazy(new ServiceKey(serviceType), registrationInfos);
    }

    /// <inheritdoc />
    public IServiceRegister RegisterManyLazy<TService>(IEnumerable<IRegistrationInfo<TService>> registrationInfos)
        where TService : class
    {
        return this.RegisterManyLazy(typeof(TService), registrationInfos);
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">Error in register service</exception>
    public IServiceRegister RegisterResolver(Type serviceType, Type implementorType, LifetimeScope? scope = null)
    {
        var serviceResolver = this.BuildResolver<object>(serviceType);

        serviceResolver.Register(serviceType, implementorType, scope);

        this.resolvers.Add(serviceResolver);

        return serviceResolver;
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">Error in register service</exception>
    public IServiceRegister RegisterResolver<TService, TImplementor>(LifetimeScope? scope = null) where TService : class where TImplementor : class, TService
    {
        var serviceResolver = this.BuildResolver<TService>();

        serviceResolver.Register<TService, TImplementor>(scope);

        this.resolvers.Add(serviceResolver);

        return serviceResolver;
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">Error in register service</exception>
    public IServiceRegister RegisterResolver<TService>(LifetimeScope? scope = null) where TService : class
    {
        var serviceResolver = this.BuildResolver<TService>();

        serviceResolver.Register<TService>(scope);

        this.resolvers.Add(serviceResolver);

        return serviceResolver;
    }

    /// <inheritdoc />
    public IServiceRegister RegisterResolver(Type concreteType, LifetimeScope? scope = null)
    {
        var serviceResolver = this.BuildResolver<object>(concreteType);

        serviceResolver.Register(concreteType, scope);

        this.resolvers.Add(serviceResolver);

        return serviceResolver;
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">Error in register service</exception>
    public IServiceRegister RegisterResolver<TService>(Func<TService> serviceCreator, LifetimeScope? scope = null)
        where TService : class
    {
        var serviceResolver = this.BuildResolver<TService>();

        serviceResolver.Register(_ => serviceCreator.Invoke(), scope);

        this.resolvers.Add(serviceResolver);
            
        return serviceResolver;
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">Error in register service</exception>
    public IServiceRegister RegisterResolver<TService>(Func<IGenericServiceProvider, TService> serviceCreator,
        LifetimeScope? scope = null) where TService : class
    {
        var serviceResolver = this.BuildResolver<TService>();

        serviceResolver.Register(() => serviceCreator(serviceResolver), scope);

        this.resolvers.Add(serviceResolver);

        return serviceResolver;
    }

    private ServiceRegisterResolver<TCService> BuildResolver<TCService>(Type serviceType = null)
        where TCService : class
    {
        this.CheckContainerLocked(serviceType);
        this.ThrowExceptionIfRegistered(new ServiceKey(serviceType ?? typeof(TCService)));

        var serviceResolver = new ServiceRegisterResolver<TCService>(serviceType, container =>
        {
            var rootContainer = this.Container;
                
            // default lifetime scope inherited from parent container.
            return container.With(rules => rules.WithDefaultReuse(rootContainer.Rules.DefaultReuse));
        });

        serviceResolver.Unresolved += this.UnresolvedServiceResolver;

        return serviceResolver;
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">Error in register service</exception>
    public IServiceRegister RegisterGeneric(Type genericServiceType, IEnumerable<Assembly> assemblies, LifetimeScope? scope = null)
    {
        try
        {
            foreach (var assembly in assemblies)
            {
                var concreteTypes= assembly.GetResolutionFromGenericImplementations(genericServiceType);

                foreach (var concreteType in concreteTypes)
                {
                    this.Container.Register(concreteType.Service, concreteType.Implementation, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
                }
            }
                
            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(genericServiceType,
                "The given generic service type cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    public bool IsRegistered(Type serviceType, bool many = false)
    {
        return this.IsRegistered(new ServiceKey(serviceType), many);
    }

    /// <inheritdoc />
    public bool IsRegistered<TService>(bool many = false) where TService : class
    {
        return this.IsRegistered(typeof(TService), many);
    }

    /// <inheritdoc />
    public bool IsRegistered(ServiceKey svcKey, bool many = false)
    {
        var serviceType = svcKey.Type;

        if (many)
        {
            return this.manyRegistrations.Contains(serviceType)
                   || this.manyLazyRegistrations.Any(registration => registration.ServiceKey.Type == serviceType);
        }

        return this.resolvers.Any(resolver => resolver.ServiceType == serviceType)
               || this.lazyRegistrations.Any(registration => registration.ServiceKey.Type == serviceType)
               || this.Container.IsRegistered(serviceType);
    }

    /// <inheritdoc />
    public object GetService(Type serviceType, object key = null)
    {
        try
        {
            this.ContainerLocked = true;

            var ret = this.scopeManager.GetResolver().Resolve(serviceType);

            return ret;
        }
        catch (Exception ex)
        {
            throw new ServiceUnavailableException(serviceType, ex.Message, ex);
        }
    }

    /// <inheritdoc />
    object IServiceProvider.GetService(Type serviceType)
    {
        return this.GetService(serviceType);
    }

    /// <inheritdoc />
    public TService GetService<TService>(object key = null) where TService : class
    {
        return this.GetService(typeof(TService)) as TService;
    }

    /// <inheritdoc />
    /// <exception cref="ServiceUnavailableException">Service unavailable exception</exception>
    public IEnumerable<TService> GetServices<TService>(object key = null) where TService : class
    {
        try
        {
            this.ContainerLocked = true;

            var ret = this.scopeManager.GetResolver().ResolveMany<TService>(behavior: ResolveManyBehavior.AsFixedArray);

            return ret;
        }
        catch (Exception ex)
        {
            throw new ServiceUnavailableException(typeof(IEnumerable<TService>), ex.Message, ex);
        }
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">Service registration exception</exception>
    public IServiceRegister RegisterDerivedServices<TServiceBase>(Assembly assembly, bool includeObsoleted = false, LifetimeScope? scope = null)
        where TServiceBase : class
    {
        try
        {
            var derivedTypes = assembly.GetDerivedTypesFrom(typeof(TServiceBase),
                new TypeResolutionOptions
                    {IncludeGenericTypeDefinitions = true, IncludeObsoleted = includeObsoleted});

            foreach (var derivedType in derivedTypes)
            {
                this.Register(derivedType, derivedType, scope);
            }

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(typeof(TServiceBase), "The given services cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <inheritdoc />
    /// /// <exception cref="ServiceRegistrationException">Service registration exception</exception>
    public IServiceRegister RegisterDerivedServices(Type serviceBase, Assembly assembly, bool includeObsoleted = false,
        LifetimeScope? scope = null)
    {
        try
        {
            var derivedTypes = assembly.GetDerivedTypesFrom(serviceBase,
                new TypeResolutionOptions
                    {IncludeGenericTypeDefinitions = true, IncludeObsoleted = includeObsoleted});
                
            foreach (var derivedType in derivedTypes)
            {
                this.Register(derivedType, derivedType, scope);
            }

            return this;
        }
        catch (Exception ex)
        {
            throw new ServiceRegistrationException(serviceBase, "The given services cannot be registered, see inner exception for details.", ex);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    protected Factory CreateFactoryOnResolvers(Request request)
    {
        var unregisteredType = request.ServiceType;
        
        if (!this.singleResolverResolutionContext.TryGet(unregisteredType, out var ctx))
        {
            var resolver = this.resolvers.FirstOrDefault(serviceResolver => serviceResolver.ServiceType == unregisteredType)
                       ?? this.resolvers.FirstOrDefault(serviceResolver =>
                           serviceResolver.ServiceType.IsGenericTypeDefinition &&
                           serviceResolver.ServiceType.IsCompatible(unregisteredType));

            if (resolver != null)
            {
                ctx = new ResolutionContext<IServiceResolver, Factory>
                {
                    ServiceType = unregisteredType,
                    Registration = resolver,
                    Resolver = DelegateFactory.Of(_ =>
                        resolver.ServiceType.IsGenericTypeDefinition ? resolver.GetService(unregisteredType) : resolver.GetService())
                };

                this.singleResolverResolutionContext.TryAdd(ctx);
            }
        }
        
        if (ctx is not null)
        {
            var resolver = ctx.Registration;

            if (this.scopeManager.CurrentScope is not null && resolver is ServiceRegisterImpl res && res.scopeManager.CurrentScope is null)
            {
                this.scopeManager.CurrentScope.AddDisposable(res.BeginScope());
            }

            return ctx.Resolver;
        }

        var unresolvedArgs = new ServiceUnresolvedEventArgs(unregisteredType);
        this.Unresolved?.Invoke(this, unresolvedArgs);

        // note: at this point, the resolution is not done by any resolver, so who does it open a scope if the workflow is Scoped?
        return unresolvedArgs.Handled ? new DelegateFactory(_ => unresolvedArgs.ServiceResolved) : null;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    protected Factory CreateFactoryOnSingleLazy(Request request)
    {
        var unregisteredType = request.ServiceType;

        if (this.singleLazyResolutionContext.TryGet(unregisteredType, out var ctx))
        {
            return ctx.Resolver;
        }

        var lazyRegistration = this.lazyRegistrations.FirstOrDefault(registration => registration.ServiceKey.Type == unregisteredType)
                               ?? this.lazyRegistrations.FirstOrDefault(registration =>
                                   registration.ServiceKey.Type.IsGenericTypeDefinition &&
                                   registration.ServiceKey.Type.IsCompatible(unregisteredType));

        if (lazyRegistration != null)
        {
            var lazyRegInfo = lazyRegistration.Registration;

            IRegistrationInfo regInfo;

            if (lazyRegistration.ServiceKey.Type.IsGenericTypeDefinition)
            {
                var concreteType = lazyRegInfo.ConcreteType.MakeGenericType(unregisteredType.GetGenericArguments());
                regInfo = lazyRegInfo.Scope.CreateRegistration(concreteType, lazyRegInfo.SuppressDiagnostic);
            }
            else
            {
                regInfo = lazyRegInfo;
            }

            ctx = new ResolutionContext<SingleRegistration, Factory>
            {
                ServiceType = unregisteredType,
                Registration = lazyRegistration,
                Resolver = this.CreateFactory(regInfo)
            };

            this.singleLazyResolutionContext.TryAdd(ctx);

            return ctx.Resolver;
        }

        return null;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serviceType"></param>
    /// <param name="key"></param>
    /// <returns></returns>
    protected IEnumerable<DynamicRegistration> GetDynamicRegistrationOnLazy(Type serviceType, object key)
    {
        var unregisteredType = serviceType;

        if (this.manyLazyResolutionContext.TryGet(unregisteredType, out var ctx))
        {
            return ctx.Resolver;
        }

        var lazyRegistration = this.manyLazyRegistrations.FirstOrDefault(registration => registration.ServiceKey.Type == unregisteredType)
                               ?? this.manyLazyRegistrations.FirstOrDefault(registration =>
                                   registration.ServiceKey.Type.IsGenericTypeDefinition &&
                                   registration.ServiceKey.Type.IsCompatible(unregisteredType))
            ;
        if (lazyRegistration != null)
        {
            ctx = new ResolutionContext<ManyRegistration, IEnumerable<DynamicRegistration>>
            {
                ServiceType = unregisteredType,
                Registration = lazyRegistration,
                Resolver = lazyRegistration.Registrations
                    .Select(info => new DynamicRegistration(this.CreateFactory(info)))
                    .ToList()
            };
            
            this.manyLazyResolutionContext.TryAdd(ctx);

            return ctx.Resolver;
        }

        return null;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serviceType"></param>
    protected void CheckContainerLocked(Type serviceType)
    {
        if (this.ContainerLocked)
        {
            throw new ServiceRegistrationException(serviceType, "The given service cannot be registered after verify");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void UnresolvedServiceResolver(object sender, ServiceUnresolvedEventArgs e)
    {
        try
        {
            var service = this.GetService(e.ServiceType);
            e.SetServiceResolved(service);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error get service {ex}");
        }
    }

    /// <inheritdoc />
    public void Dispose()
    {
        this.Dispose(true);

        GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources.
    /// </summary>
    /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
    protected virtual void Dispose(bool disposing)
    {
        if (!disposing)
        {
            return;
        }

        try
        {
            this.cache.Dispose();
        }
        catch
        {
            // nothing to do here !
        }

        try
        {
            this.Container.Dispose();
        }
        finally
        {
            foreach (var resolver in this.resolvers)
            {
                (resolver as IDisposable)?.Dispose();
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    protected IEnumerable<IServiceResolver> GetResolvers()
    {
        return this.resolvers;
    }
}
