using System.Collections.Generic;
using System;
using DryIoc;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceResolver.Ioc.DryIoc;

/// <summary>
/// 
/// </summary>
internal class SimpleScope(string id, IResolverContext resolver, IDryScope rootScope = null) : IDryScope
{
    private readonly List<Action<IDryScope>> disposableActions = [];
    private readonly List<IDisposable> disposables = [];

    ///<inheritdoc />
    public string Id { get => id; }

    ///<inheritdoc />
    public IResolver Resolver { get => resolver; }

    ///<inheritdoc />
    public IDryScope RootScope { get => rootScope; }

    ///<inheritdoc />
    public bool IsDisposed { get; private set; }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public IDryScope AddDisposableAction(Action<IDryScope> action)
    {
        this.disposableActions.Add(action);
        return this;
    }

    ///<inheritdoc />
    public IDryScope AddDisposable(IDisposable disposable)
    {
        if (!this.disposables.Contains(disposable))
        {
            this.disposables.Add(disposable);
        }

        return this;
    }

    ///<inheritdoc />
    public void Dispose()
    {
        if (this.IsDisposed)
        {
            return;
        }
        
        this.IsDisposed = true;
        resolver.Dispose();

        foreach (var disposable in this.disposables)
        {
            try
            {
                disposable.Dispose();
            }
            catch
            {
                // ignore exceptions here
            }
        }

        foreach (var actionReturn in this.disposableActions.Select(disposableAction => disposableAction.DynamicInvoke(this)))
        {
            if (actionReturn is Task task)
            {
                task.GetAwaiter()
                    .GetResult();
            }
        }

        rootScope = null;
    }
}


/// <summary>
/// 
/// </summary>
public interface IDryScope : IScope
{
    /// <summary>
    /// Gets the current resolver instance.
    /// </summary>
    IResolver Resolver { get; }

    /// <summary>
    /// Gets the current root scope, it could be null.
    /// </summary>
    IDryScope RootScope { get; }

    /// <summary>
    /// Add a disposable instance, which will be disposed when the current scope will be disposed.
    /// </summary>
    /// <param name="disposable"></param>
    /// <returns></returns>
    IDryScope AddDisposable(IDisposable disposable);
}
