using DryIoc;

namespace ServiceResolver.Ioc.DryIoc;

/// <summary>
/// Extension class for translate <see cref="LifetimeScope"/> enum into <see cref="IReuse"/> instance.
/// </summary>
public static class ScopeTranslator
{
    /// <summary>
    /// Translates the <see cref="LifetimeScope"/> instance into <see cref="IReuse"/>.
    /// </summary>
    /// <param name="scope"></param>
    /// <returns></returns>
    public static IReuse Translate(this LifetimeScope scope)
    {
        return scope switch
        {
            LifetimeScope.Scoped => Reuse.Scoped,
            LifetimeScope.Singleton => Reuse.Singleton,
            LifetimeScope.Transient => Reuse.Transient,
            _ => Reuse.Transient
        };
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="reuse"></param>
    /// <returns></returns>
    public static LifetimeScope Translate(this IReuse reuse)
    {
        if (reuse == Reuse.Singleton)
        {
            return LifetimeScope.Singleton;
        }

        return reuse == Reuse.Scoped ? LifetimeScope.Scoped : LifetimeScope.Transient;
    }
}
