using System;
using System.Threading;
using DryIoc;

namespace ServiceResolver.Ioc.DryIoc;

/// <summary>
/// 
/// </summary>
/// <param name="rootResolver"></param>
public class ScopeManager(IResolverContext rootResolver)
{
    private readonly AsyncLocal<IDryScope> asyncLocalScope = new();

    /// <summary>
    /// 
    /// </summary>
    public IDryScope CurrentScope { get => this.asyncLocalScope.Value; }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public IDryScope OpenScope(string id = null)
    {
        id ??= Guid.NewGuid().ToString("D");
        var scope = new SimpleScope(id, rootResolver.OpenScope(id), this.asyncLocalScope.Value);

        if (scope.RootScope is null)
        {
            // on the root context, the scope is removed.
            scope.AddDisposableAction(sc =>
            {
                if (this.asyncLocalScope.Value == sc)
                {
                    this.asyncLocalScope.Value = null;
                }
            });
        }
        else
        {
            scope.AddDisposableAction(sc =>
            {
                var root = sc.RootScope;
                this.asyncLocalScope.Value = root;
            });
        }

        this.asyncLocalScope.Value = scope;

        return scope;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public IResolver GetResolver()
    {
        return this.asyncLocalScope.Value?.Resolver ?? rootResolver;
    }
}
