using System;
using DryIoc;
using ServiceResolver.Ioc.Exceptions;
using ServiceResolver.Ioc.Registers;

namespace ServiceResolver.Ioc.DryIoc.Registers;

/// <summary>
/// A custom implementation of <see cref="IConditionalRegister{TService}"/> for DryIoc.
/// </summary>
/// <typeparam name="TService"></typeparam>
public class ConditionalRegister<TService> : IConditionalRegister<TService>, IConditionalRegister
    where TService : class
{
    private readonly IGenericServiceProvider serviceProvider;
    private readonly IContainer container;
    private bool locked;

    /// <summary>
    /// Creates a new instance of <see cref="ConditionalRegister{TService}"/>
    /// </summary>
    /// <param name="serviceProvider"></param>
    /// <param name="container"></param>
    /// <param name="serviceType"></param>
    public ConditionalRegister(IGenericServiceProvider serviceProvider, IContainer container, Type serviceType = null)
    {
        this.ServiceType = serviceType ?? typeof(TService);
        this.serviceProvider = serviceProvider;
        this.container = container;
    }

    /// <inheritdoc />
    public Type ServiceType { get; }

    /// <inheritdoc />
    public IConditionalRegister<TService> Register<TImplementor>(Func<ConsumerService, bool> condition, LifetimeScope? scope = null) where TImplementor : class, TService
    {
        this.Register(typeof(TImplementor), condition, scope);

        return this;
    }

    /// <inheritdoc />
    public IConditionalRegister Register(Type implementorType, Func<ConsumerService, bool> condition, LifetimeScope? scope = null)
    {
        if (scope.HasValue)
        {
            this.container.Register(this.ServiceType, implementorType,
                reuse: scope.Value.Translate(),
                setup: Setup.With(condition: request => condition(new ConsumerService(request.DirectParent.ImplementationType)))
                //,ifAlreadyRegistered: IfAlreadyRegistered.Throw
            );
        }
        else
        {
            this.container.Register(this.ServiceType, implementorType,
                setup: Setup.With(condition: request => condition(new ConsumerService(request.DirectParent.ImplementationType)))
                //,ifAlreadyRegistered: IfAlreadyRegistered.Throw
            );
        }

        return this;
    }

    /// <inheritdoc />
    [Obsolete("Use the other overload which these args: [serviceFactory, condition and scope]. This method will be removed on next major version.")]
    public IConditionalRegister<TService> Register(Func<ConsumerService, bool> condition, Func<IGenericServiceProvider, TService> serviceFactory, LifetimeScope? scope = null)
    {
        IConditionalRegister register = this;

        register.Register(serviceFactory, condition, scope);

        return this;
    }

    /// <inheritdoc />
    public IConditionalRegister<TService> Register(Func<IGenericServiceProvider, TService> serviceFactory, Func<ConsumerService, bool> condition, LifetimeScope? scope = null)
    {
        IConditionalRegister register = this;

        register.Register(serviceFactory, condition, scope);

        return this;
    }

    /// <inheritdoc />
    public IConditionalRegister Register(
        Func<IGenericServiceProvider, object> serviceFactory,
        Func<ConsumerService, bool> condition,
        LifetimeScope? scope = null)
    {
        if (scope.HasValue)
        {
            this.container.RegisterDelegate(this.ServiceType, factoryDelegate: resolver => serviceFactory.Invoke(this.serviceProvider),
                reuse: scope.Value.Translate(),
                setup: Setup.With(condition: request => condition(new ConsumerService(request.DirectParent.ImplementationType)))
                //,ifAlreadyRegistered: IfAlreadyRegistered.Throw
            );
        }
        else
        {
            this.container.RegisterDelegate(this.ServiceType, factoryDelegate: resolver => serviceFactory.Invoke(this.serviceProvider),
                setup: Setup.With(condition: request => condition(new ConsumerService(request.DirectParent.ImplementationType)))
                //,ifAlreadyRegistered: IfAlreadyRegistered.Throw
            );
        }

        return this;
    }

    /// <inheritdoc />
    public void Default<TImplementor>(LifetimeScope? scope = null)
        where TImplementor : class, TService
    {
        this.Default(typeof(TImplementor), scope);
    }

    /// <inheritdoc />
    public void Default(Type implementorType, LifetimeScope? scope = null)
    {
        this.ThrowIfLocked();

        if (scope.HasValue)
        {
            this.container.Register(this.ServiceType, implementorType,
                reuse: scope.Value.Translate()
                //,setup: Setup.With(condition: request => request.Type == this.Type)
                //, setup: Setup.With(condition: Condition)
            );

            //bool Condition(Request request)
            //{
            //    var eq = request.DirectParent.Type == typeof(object);
            //    return eq;
            //}
        }
        else
        {
            this.container.Register(this.ServiceType, implementorType);
        }

        this.locked = true;
    }

    /// <inheritdoc />
    public void Default(Func<IGenericServiceProvider, TService> serviceFactory, LifetimeScope? scope = null)
    {
        IConditionalRegister register = this;

        register.Default(serviceFactory, scope);
    }

    /// <inheritdoc />
    public void Default(Func<IGenericServiceProvider, object> serviceFactory, LifetimeScope? scope = null)
    {
        this.ThrowIfLocked();

        if (scope.HasValue)
        {
            this.container.RegisterDelegate(this.ServiceType,
                factoryDelegate: resolver => serviceFactory.Invoke(this.serviceProvider),
                reuse: scope.Value.Translate()
            );
        }
        else
        {
            this.container.RegisterDelegate(this.ServiceType, factoryDelegate: resolver => serviceFactory.Invoke(this.serviceProvider));
        }

        this.locked = true;
    }

    private void ThrowIfLocked()
    {
        var serviceType = typeof(TService);

        if (this.locked)
        {
            throw new ServiceRegistrationException(serviceType, $"The conditional registration for '{serviceType.FullName}' was locked, due to a previous default registration.");
        }
    }
}
