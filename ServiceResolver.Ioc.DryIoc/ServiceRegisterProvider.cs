using System;
using System.Linq;
using System.Reflection;
using DryIoc;
using ServiceResolver.Ioc.Exceptions;
using ServiceResolver.Ioc.Extensions;
using ServiceResolver.Ioc.Modules;

namespace ServiceResolver.Ioc.DryIoc;

/// <summary>
/// 
/// </summary>
/// <seealso cref="ServiceRegisterImpl" />
/// <seealso cref="IServiceRegisterProvider" />
/// <seealso cref="IDisposable" />
public class ServiceRegisterProvider
    : ServiceRegisterImpl, IServiceRegisterProvider
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ServiceRegisterProvider"/> class.
    /// </summary>
    /// <param name="containerAction">The container action.</param>
    public ServiceRegisterProvider(Func<IContainer, IContainer> containerAction = null)
        : base(containerAction)
    {
    }

    /// <inheritdoc />
    /// <exception cref="ServiceRegistrationException">Error in register service</exception>
    public void Register(IServiceModule module)
    {
        if (module == null)
        {
            throw new ArgumentNullException(nameof(module), "The given module to install into this container cannot be null.");
        }

        module.Initialize(this);
    }
        
    /// <inheritdoc />
    /// <exception cref="ArgumentNullException">assembly;The given assembly to use for loading service is null.</exception>
    /// <exception cref="ModuleRegistrationException">Module registration exception</exception>
    public void RegisterModulesFrom(Assembly assembly, bool includeObsoleted)
    {
        if (assembly == null)
        {
            throw new ArgumentNullException(nameof(assembly), "The given assembly to use for loading service is null.");
        }

        var typeModules = assembly.GetDerivedTypesFrom(typeof(IServiceModule),
            new TypeResolutionOptions {IncludeGenericTypeDefinitions = false, IncludeObsoleted = includeObsoleted});
            
        foreach (var moduleType in typeModules)
        {
            try
            {
                if (Activator.CreateInstance(moduleType) is IServiceModule module)
                {
                    this.Register(module);
                }
            }
            catch (Exception ex)
            {
                throw new ModuleRegistrationException(moduleType, "Error on registering the given service module, see inner exception for details.", ex);
            }
        }
    }

    /// <inheritdoc />
    public IServiceRegisterProvider BuildChildServiceProvider()
    {
        var child= new ServiceRegisterProvider();
        child.Unresolved += this.UnresolvedServiceResolver;
        return child;
    }

    /// <inheritdoc />
    public void Verify()
    {
        try
        {
            this.ContainerLocked = true;
                
            var resolutions = this.Container.Validate().ToList();

            foreach (var serviceResolver in this.GetResolvers())
            {
                serviceResolver.GetService();
            }

            if (resolutions.Any())
            {
                throw new DiagnosticContainerException("There are errors in your configuration.",
                    resolutions.Select(pair => pair.Value).ToList());
            }
        }
        catch (Exception ex)
        {
            throw new DiagnosticContainerException("The internal container has some errors, see inner exception for details.", ex);
        }
    }
}
