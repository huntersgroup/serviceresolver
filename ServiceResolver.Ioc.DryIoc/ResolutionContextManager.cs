using System;
using System.Collections.Generic;
using System.Linq;

namespace ServiceResolver.Ioc.DryIoc;

/// <summary>
/// 
/// </summary>
/// <typeparam name="TRegistration"></typeparam>
/// <typeparam name="TResolver"></typeparam>
public class ResolutionContextManager<TRegistration, TResolver>
    where TResolver : class
    where TRegistration : class
{
    private readonly HashSet<ResolutionContext<TRegistration, TResolver>> currentContext = new();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="resolutionContext"></param>
    /// <returns></returns>
    public bool TryAdd(ResolutionContext<TRegistration, TResolver> resolutionContext)
    {
        return this.currentContext.Add(resolutionContext);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serviceType"></param>
    /// <returns></returns>
    public ResolutionContext<TRegistration, TResolver> Get(Type serviceType)
    {
        return this.currentContext.FirstOrDefault(context => context.ServiceType == serviceType);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serviceType"></param>
    /// <param name="context"></param>
    /// <returns></returns>
    public bool TryGet(Type serviceType, out ResolutionContext<TRegistration, TResolver> context)
    {
        context = this.currentContext.FirstOrDefault(context => context.ServiceType == serviceType);

        return context != null;
    }

    /// <summary>
    /// 
    /// </summary>
    public void Clear()
    {
        this.currentContext.Clear();
    }
}
