using Hunter.Host.Services;
using Hunter.Host.Workers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ServiceResolver.Ioc;
using ServiceResolver.Ioc.Extensions;
using ServiceResolver.Ioc.Hosting;
using ServiceResolver.Ioc.Options;
using Host = Microsoft.Extensions.Hosting.Host;

var env = Environment.GetEnvironmentVariable("APP_ENV") ?? "dev";

var hostBuilder =
        Host.CreateDefaultBuilder(args)
            .ConfigureHostConfiguration(builder =>
            {
            })
            .ConfigureAppConfiguration((context, builder) =>
            {
            })
            .ConfigureServices((context, services) =>
            {
                //services.RegisterServices(context);

                //services
                //    .AddHost()
                //    .AddScoped<DemoSvc>()
                //    .AddHostedService<DemoBackgroundService02>();

                services.AddSingleton(context.Configuration);
            })
            .UseServiceResolver(new SimpleInjectorOptions
            {
                HostSettings = new HostSettings
                {
                    UseScope = true
                },
                BeforeRegistering = collection =>
                {
                    // other custom registrations here !!
                },
                OnRegistering = serviceRegister =>
                {
                    serviceRegister
                        .Register<DemoSvc>(LifetimeScope.Scoped)
                        .RegisterMany<IHostedService>([typeof(DemoBackgroundService02)],
                            LifetimeScope.Scoped);
                },
                AfterBuildingProvider = provider =>
                {
                    provider.Verify();
                }
            })
            .UseEnvironment(env)
    ;

var host = hostBuilder.Build();

await host.RunAsync();
