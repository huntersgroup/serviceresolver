using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Hunter.Host.Workers;

public class DemoBackgroundService01 : BackgroundService, IAsyncDisposable
{
    private readonly ILogger logger;
    private readonly IHostApplicationLifetime appLifetime;
    private readonly string runnableTarget;

    public DemoBackgroundService01(IHostApplicationLifetime appLifetime, ILogger<DemoBackgroundService01> logger)
    {
        this.appLifetime = appLifetime;
        this.logger = logger;
        this.runnableTarget = $"{nameof(DemoBackgroundService01)} app, id: {Guid.NewGuid():D}";
    }

    public override void Dispose()
    {
        this.DisposeAsync().AsTask().GetAwaiter().GetResult();
    }

    public ValueTask DisposeAsync()
    {
        base.Dispose();
        this.logger.LogInformation($"### {this.runnableTarget} was disposed (async) !!! ###");

        return ValueTask.CompletedTask;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        this.appLifetime.ApplicationStarted.Register(() => this.OnStarted().GetAwaiter().GetResult());
        this.appLifetime.ApplicationStopping.Register(() => this.OnStopping().GetAwaiter().GetResult());
        this.appLifetime.ApplicationStopped.Register(() => this.OnStopped().GetAwaiter().GetResult());

        await ValueTask.CompletedTask;
    }

    private async ValueTask OnStarted()
    {
        this.logger.LogInformation($"### {this.runnableTarget} - {nameof(this.OnStarted)} starts at: {DateTime.UtcNow:u} ###");

        try
        {
            //await this.runnable.Start();
            await Task.Delay(TimeSpan.FromSeconds(5));

        }
        catch (Exception)
        {
            this.appLifetime.StopApplication();
        }

        this.logger.LogInformation($"### {this.runnableTarget} - {nameof(this.OnStarted)} ends at: {DateTime.UtcNow:u} ###");

        await ValueTask.CompletedTask;
    }

    private async ValueTask OnStopping()
    {
        this.logger.LogInformation($"### {this.runnableTarget} on stopping at: {DateTime.UtcNow:u} ###");

        await ValueTask.CompletedTask;
    }

    private async ValueTask OnStopped()
    {
        this.logger.LogInformation($"### {this.runnableTarget} - {nameof(this.OnStopped)} starts at: {DateTime.UtcNow:u} ###");

        await Task.Delay(TimeSpan.FromSeconds(5));

        this.logger.LogInformation($"### {this.runnableTarget} - {nameof(this.OnStopped)} ends at: {DateTime.UtcNow:u} ###");
    }
}

