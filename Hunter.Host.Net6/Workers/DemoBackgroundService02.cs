using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Hunter.Host.Workers;

public class DemoBackgroundService02 : BackgroundService, IAsyncDisposable
{
    private readonly ILogger logger;
    private readonly IHostApplicationLifetime appLifetime;
    private readonly string runnableTarget;
#pragma warning disable CS0414 // Field is assigned but its value is never used
    private bool disposed;
#pragma warning restore CS0414 // Field is assigned but its value is never used

    public DemoBackgroundService02(IHostApplicationLifetime appLifetime, ILogger<DemoBackgroundService02> logger)
    {
        this.appLifetime = appLifetime;
        this.logger = logger;
        this.runnableTarget = $"{nameof(DemoBackgroundService02)} app, id: {Guid.NewGuid():D}";
    }

    public override void Dispose()
    {
        this.DisposeAsync().AsTask().GetAwaiter().GetResult();
    }

    public ValueTask DisposeAsync()
    {
        this.disposed = true;

        base.Dispose();
        this.logger.LogInformation($"<<< {this.runnableTarget} was disposed (async) !!! >>>");

        return ValueTask.CompletedTask;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        this.appLifetime.ApplicationStarted.Register(() => this.OnStarted().AsTask().GetAwaiter().GetResult());
        this.appLifetime.ApplicationStopping.Register(() => this.OnStopping().AsTask().GetAwaiter().GetResult());
        this.appLifetime.ApplicationStopped.Register(() => this.OnStopped().AsTask().GetAwaiter().GetResult());

        await ValueTask.CompletedTask;
    }

    private async ValueTask OnStarted()
    {
        this.logger.LogInformation($"<<< {this.runnableTarget} - {nameof(this.OnStarted)} starts at: {DateTime.UtcNow:u} >>>");

        try
        {
            //await this.runnable.Start();
            await Task.Delay(TimeSpan.FromSeconds(5));
        }
        catch (Exception)
        {
            this.appLifetime.StopApplication();
        }

        this.logger.LogInformation($"<<< {this.runnableTarget} - {nameof(this.OnStarted)} ends at: {DateTime.UtcNow:u} >>>");

        await ValueTask.CompletedTask;
    }

    private async ValueTask OnStopping()
    {
        this.logger.LogInformation($"<<< {this.runnableTarget} on stopping at: {DateTime.UtcNow:u} >>>");

        await ValueTask.CompletedTask;
    }

    private async ValueTask OnStopped()
    {
        this.logger.LogInformation($"<<< {this.runnableTarget} on stopped at:  {DateTime.UtcNow:u} >>>");

        //await this.runnable.Stop();
        await Task.Delay(TimeSpan.FromSeconds(7));

        this.logger.LogInformation($"<<< {this.runnableTarget} stopped at: {DateTime.UtcNow:u} >>>");
    }
}
