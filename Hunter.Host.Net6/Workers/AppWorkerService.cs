using System.Text;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Hunter.Host.Workers;

public class AppWorkerService : BackgroundService
{
    private readonly IHostApplicationLifetime appLifetime;
    //private readonly IAsyncRunnable runnable;
    private readonly ILogger logger;
    private readonly string runnableTarget;

    public AppWorkerService(IHostApplicationLifetime appLifetime, ILogger<AppWorkerService> logger, StringBuilder builder)
    {
        this.appLifetime = appLifetime;
        this.logger = logger;
        this.runnableTarget = $"{nameof(AppWorkerService)} app";
    }

    public override void Dispose()
    {
        base.Dispose();
        this.logger.LogInformation("### Disposed method was called !!! ###");
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        this.appLifetime.ApplicationStarted.Register(async () => await this.OnStarted());
        this.appLifetime.ApplicationStopping.Register(async () => await this.OnStopping());
        this.appLifetime.ApplicationStopped.Register(async () => await this.OnStopped());

        await Task.CompletedTask;
    }

    private async ValueTask OnStarted()
    {
        this.logger.LogInformation($"######## on started at: {DateTime.UtcNow:u}");

        try
        {
            //await this.runnable.Start();
            await Task.Delay(TimeSpan.FromMinutes(5));
        }
        catch (Exception)
        {
            this.appLifetime.StopApplication();
        }

        this.logger.LogInformation($"######## {this.runnableTarget} started ########");

        await ValueTask.CompletedTask;
    }

    private async ValueTask OnStopping()
    {
        this.logger.LogInformation($"######## on stopping at: {DateTime.UtcNow:u}");

        await ValueTask.CompletedTask;
    }

    private async ValueTask OnStopped()
    {
        this.logger.LogInformation($"######## on stopped at: {DateTime.UtcNow:u}");

        //await this.runnable.Stop();
        await Task.Delay(TimeSpan.FromSeconds(10));

        this.logger.LogInformation($"######## {this.runnableTarget} stopped ########");

        await ValueTask.CompletedTask;
    }
}
