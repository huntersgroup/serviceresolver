using Microsoft.Extensions.Logging;

namespace Hunter.Host.Services;

public class DemoSvc : IDisposable
{
    private readonly string id;
    private readonly ILogger logger;

    public DemoSvc(ILogger<DemoSvc> logger)
    {
        this.id = Guid.NewGuid().ToString();
        this.logger = logger;
    }

    public void Dispose()
    {
        this.logger.LogWarning($"{nameof(DemoSvc)} instance was disposed, id: {this.id}");
    }
}
