using System;

namespace ServiceResolver.Ioc;

/// <summary>
/// Represents a utility to validate parameters, and on failure this throws exceptions.
/// </summary>
internal static class Require
{
    private const string ArgOrParamNullMessage = "The given argument or parameter cannot be null.";
    private const string ArgNullMessage = "The '{0}' cannot be null.";
    private const string InvalidCastMessage = "The given argument or parameter cannot be casted into '{0}' type.";

    /// <summary>
    /// Throws an exception if the given parameter is null.
    /// </summary>
    /// <param name="instance"></param>
    /// <param name="name"></param>
    /// <exception cref="ArgumentNullException"></exception>
    internal static void NotNull(this object instance, string name)
    {
        if (instance is null)
        {
            throw new ArgumentNullException(name, ArgOrParamNullMessage);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="instance"></param>
    /// <param name="name"></param>
    /// <exception cref="ArgumentNullException"></exception>
    internal static void InvalidOperationIfNull(this object instance, string name)
    {
        if (instance is null)
        {
            throw new InvalidOperationException(string.Format(ArgNullMessage, name));
        }
    }

    /// <summary>
    /// Throws an exception if the given instance cannot be applied a cast into TService type.
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    /// <param name="instance"></param>
    /// <param name="name"></param>
    /// <returns></returns>
    /// <exception cref="ArgumentException"></exception>
    internal static TService CastOrThrow<TService>(this object instance, string name)
        where TService : class
    {
        return instance as TService ?? throw new ArgumentException(name, string.Format(InvalidCastMessage, typeof(TService).FullName));
    }
}
