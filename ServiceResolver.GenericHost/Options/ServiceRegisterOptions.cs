using System;
using Microsoft.Extensions.DependencyInjection;
using ServiceResolver.Ioc.Registers;

namespace ServiceResolver.Ioc.Options;

/// <summary>
/// 
/// </summary>
/// <typeparam name="TServiceRegister"></typeparam>
public class ServiceRegisterOptions<TServiceRegister> : ServiceRegisterOptions, IServiceRegisterBuilder<TServiceRegister>
    where TServiceRegister : class, IServiceRegister
{
    ///<inheritdoc />
    public Func<IServiceCollection, TServiceRegister> OnCreating { get; init; }

    ///<inheritdoc />
    public Action<IServiceCollection> BeforeRegistering { get; init; }

    /// <summary>
    /// Gets or sets the custom action used to register all services into TServiceRegister instance.
    /// <para>
    /// On this step, the <see cref="IServiceCollection"/> parameter is read-only, if it's needed to add / remove / change service registrations
    /// it can be done using the <see cref="BeforeRegistering"/> action.
    /// </para>
    /// </summary>
    public Action<TServiceRegister> OnRegistering { get; set; }

    ///<inheritdoc />
    public Action<TServiceRegister> AfterBuildingProvider { get; init; }
}

/// <summary>
/// 
/// </summary>
public class ServiceRegisterOptions : IServiceRegisterOptions
{
    ///<inheritdoc />
    public bool AutoRegistration { get; init; } = true;

    ///<inheritdoc />
    public bool IgnoreMissingRegistrations { get; init; } = true;

    ///<inheritdoc />
    public ResolutionType SuppressDiagnosticType { get; init; } = ResolutionType.Hybrid;
}
