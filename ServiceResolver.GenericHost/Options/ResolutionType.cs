using Microsoft.Extensions.DependencyInjection;

namespace ServiceResolver.Ioc.Options;

/// <summary>
/// Represents the way of different kind of behaviors that service resolver can assume when:
/// <code>
/// It's needed to adopt a new constructor resolution (many constructors on services types).
/// Optional parameter on services.
/// </code>
/// </summary>
public enum ResolutionType : byte
{
    /// <summary>
    /// Specifies the default behavior of underlying provider.
    /// </summary>
    Standard = 0,

    /// <summary>
    /// Chooses the strategy less restrictive.
    /// </summary>
    LessRestrictive = 1,

    /// <summary>
    /// Similar to <see cref="LessRestrictive"/> but only for <see cref="IServiceCollection"/> registrations.
    /// </summary>
    Hybrid = 2
}
