using Microsoft.Extensions.DependencyInjection;
using ServiceResolver.Ioc.Registers;
using System;

namespace ServiceResolver.Ioc.Options;

/// <summary>
/// 
/// </summary>
public interface IServiceProviderOptions
{
    /// <summary>
    /// Gets or sets a boolean value indicating if no exceptions will be thrown whenever a service is not registered.
    ///
    /// <para>
    /// Missing registrations produces default resolutions, so: <b>null</b> for single registrations, and <b>empty collection</b> for many ones.
    /// </para>
    /// </summary>
    bool IgnoreMissingRegistrations { get; init; }
}

/// <summary>
/// 
/// </summary>
public interface IServiceRegisterOptions : IServiceProviderOptions
{
    /// <summary>
    /// Gets or sets a boolean value indicating if the custom service provider is auto-registered.
    /// </summary>
    bool AutoRegistration { get; init; }

    /// <summary>
    /// Gets or sets the strategy which diagnostics are suppressed.
    /// <code>
    /// examples:
    ///     <see cref="ResolutionType.Standard"/> uses default behavior (all diagnostics are enabled).
    ///     <see cref="ResolutionType.LessRestrictive"/> disables all diagnostics.
    ///     <see cref="ResolutionType.Hybrid"/> is like <see cref="ResolutionType.LessRestrictive"/> strategy, but only for <see cref="IServiceCollection"/> registrations.
    /// </code>
    /// <para>
    /// The default resolution is <see cref="ResolutionType.Hybrid"/>.
    /// </para>
    /// </summary>
    ResolutionType SuppressDiagnosticType { get; init; }
}

/// <summary>
/// 
/// </summary>
/// <typeparam name="TServiceRegister"></typeparam>
public interface IServiceRegisterOptions<TServiceRegister> : IServiceRegisterOptions
    where TServiceRegister : class, IServiceRegister
{
    /// <summary>
    /// A custom action used to prepare or change <see cref="IServiceCollection"/> instance before registering action.
    /// <para>
    /// After execution of this action, the <see cref="IServiceCollection"/> instance becomes read-only.
    /// </para>
    /// </summary>
    Action<IServiceCollection> BeforeRegistering { get; init; }

    /// <summary>
    /// Gets or sets the custom action used to execute at the end of building the <see cref="IServiceProvider"/> instance.
    /// </summary>
    Action<TServiceRegister> AfterBuildingProvider { get; init; }
}

/// <summary>
/// 
/// </summary>
/// <typeparam name="TServiceRegister"></typeparam>
public interface IServiceRegisterBuilder<TServiceRegister> : IServiceRegisterOptions<TServiceRegister>
    where TServiceRegister : class, IServiceRegister
{
    /// <summary>
    /// Gets or sets the custom factory, used to create the TServiceRegister instance.
    /// </summary>
    Func<IServiceCollection, TServiceRegister> OnCreating { get; init; }
}
