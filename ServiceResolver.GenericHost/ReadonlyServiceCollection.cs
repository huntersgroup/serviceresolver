using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;

namespace ServiceResolver.Ioc;

/// <summary>
/// 
/// </summary>
/// <param name="decoree"></param>
internal class ReadonlyServiceCollection(IServiceCollection decoree) : IServiceCollection
{
    private const string ErrorMessage = "The current service descriptor collection is readonly.";

    ///<inheritdoc />
    public IEnumerator<ServiceDescriptor> GetEnumerator()
    {
        return decoree.GetEnumerator();
    }

    ///<inheritdoc />
    IEnumerator IEnumerable.GetEnumerator()
    {
        return this.GetEnumerator();
    }

    ///<inheritdoc />
    public void Add(ServiceDescriptor item)
    {
        throw new InvalidOperationException(ErrorMessage);
    }

    ///<inheritdoc />
    public void Clear()
    {
        throw new InvalidOperationException(ErrorMessage);
    }

    ///<inheritdoc />
    public bool Contains(ServiceDescriptor item)
    {
        return decoree.Contains(item);
    }

    ///<inheritdoc />
    public void CopyTo(ServiceDescriptor[] array, int arrayIndex)
    {
        decoree.CopyTo(array, arrayIndex);
    }

    ///<inheritdoc />
    public bool Remove(ServiceDescriptor item)
    {
        throw new InvalidOperationException(ErrorMessage);
    }

    ///<inheritdoc />
    public int Count { get; } = decoree.Count;

    ///<inheritdoc />
    public bool IsReadOnly { get => true; }

    ///<inheritdoc />
    public int IndexOf(ServiceDescriptor item)
    {
        return decoree.IndexOf(item);
    }

    ///<inheritdoc />
    public void Insert(int index, ServiceDescriptor item)
    {
        throw new InvalidOperationException(ErrorMessage);
    }

    ///<inheritdoc />
    public void RemoveAt(int index)
    {
        throw new InvalidOperationException(ErrorMessage);
    }

    ///<inheritdoc />
    public ServiceDescriptor this[int index]
    {
        get => decoree[index];
        set => throw new InvalidOperationException(ErrorMessage);
    }
}
