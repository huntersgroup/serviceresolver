using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ServiceResolver.Ioc.Hosting;

namespace ServiceResolver.Ioc.Extensions;

/// <summary>
/// 
/// </summary>
public static class ServiceCollectionExtensions
{
    /// <summary>
    /// Makes this collection read-only.
    /// </summary>
    /// <remarks>
    /// After the collection is marked as read-only, any further attempt to modify it throws an <see cref="InvalidOperationException" />.
    /// </remarks>
    public static IServiceCollection AsReadonly(this IServiceCollection serviceCollection)
    {
        if (serviceCollection is not ServiceCollection col)
        {
            return new ReadonlyServiceCollection(serviceCollection);
        }

        col.MakeReadOnly();
        return serviceCollection;
    }

    /// <summary>
    /// Gets the <see cref="ServiceDescriptor"/> instances grouped by service type.
    /// </summary>
    /// <param name="serviceCollection"></param>
    /// <returns></returns>
    public static IDictionary<ServiceKey, ICollection<ServiceDescriptor>> GetRegistrationsInternal(this IServiceCollection serviceCollection)
    {
        var ret = new Dictionary<ServiceKey, ICollection<ServiceDescriptor>>();

        foreach (var descriptor in serviceCollection)
        {
            var svcKey = descriptor.ServiceType.AsServiceKey(descriptor.ServiceKey);

            if (ret.TryGetValue(svcKey, out var col))
            {
                col.Add(descriptor);
            }
            else
            {
                ret.Add(svcKey, new List<ServiceDescriptor> { descriptor });
            }
        }

        return ret;
    }

    /// <summary>
    /// Tries to get the <see cref="ServiceDescriptor"/> instance if there's a registration with that TService type.
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    /// <param name="serviceCollection"></param>
    /// <param name="descriptor"></param>
    /// <returns></returns>
    public static bool TryGetDescriptor<TService>(this IServiceCollection serviceCollection, out ServiceDescriptor descriptor)
    {
        return serviceCollection.TryGetDescriptor(typeof(TService), out descriptor);
    }

    /// <summary>
    /// Tries to get the <see cref="ServiceDescriptor"/> instance if there's a registration with the given service type parameter.
    /// </summary>
    /// <param name="serviceCollection"></param>
    /// <param name="serviceType"></param>
    /// <param name="descriptor"></param>
    /// <returns></returns>
    public static bool TryGetDescriptor(this IServiceCollection serviceCollection, Type serviceType, out ServiceDescriptor descriptor)
    {
        descriptor = serviceCollection.LastOrDefault(descriptor => descriptor.ServiceType == serviceType);

        return descriptor != null;
    }

    /// <summary>
    /// Tries to get the <see cref="IEnumerable{ServiceDescriptor}"/> instance if there's a registration with that TService type.
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    /// <param name="serviceCollection"></param>
    /// <param name="descriptor"></param>
    /// <returns></returns>
    public static bool TryGetDescriptors<TService>(this IServiceCollection serviceCollection, out IEnumerable<ServiceDescriptor> descriptor)
    {
        return serviceCollection.TryGetDescriptors(typeof(TService), out descriptor);
    }

    /// <summary>
    /// Tries to get the <see cref="IEnumerable{ServiceDescriptor}"/> instance if there's a registration with the given service type parameter.
    /// </summary>
    /// <param name="serviceCollection"></param>
    /// <param name="serviceType"></param>
    /// <param name="descriptor"></param>
    /// <returns></returns>
    public static bool TryGetDescriptors(this IServiceCollection serviceCollection, Type serviceType, out IEnumerable<ServiceDescriptor> descriptor)
    {
        descriptor = serviceCollection.Where(descriptor => descriptor.ServiceType == serviceType);

        return descriptor.Any();
    }

    /// <summary>
    /// Tries to get a singleton instance registered as a constant into registration.
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    /// <param name="serviceCollection"></param>
    /// <param name="service"></param>
    /// <returns>returns true if a registration exists for the given TService type.</returns>
    public static bool TryGetSingleton<TService>(this IServiceCollection serviceCollection, out TService service)
        where TService : class
    {
        var ret = serviceCollection.TryGetSingleton(typeof(TService), out var svc);

        service = ret ? (TService)svc : null;

        return ret;
    }

    /// <summary>
    /// Tries to get a singleton instance registered as a constant into registration.
    /// </summary>
    /// <param name="serviceCollection"></param>
    /// <param name="serviceType"></param>
    /// <param name="service"></param>
    /// <returns></returns>
    public static bool TryGetSingleton(this IServiceCollection serviceCollection, Type serviceType, out object service)
    {
        var descriptor = serviceCollection.LastOrDefault(descriptor =>
            descriptor.ServiceType == serviceType && descriptor.ImplementationInstance != null);

        if (descriptor != null)
        {
            service = descriptor.ImplementationInstance;

            return true;
        }

        service = null;

        return false;
    }

    /// <summary>
    /// Adds the new <see cref="Hosting.Host"/> with some extra customizations.
    /// <para>
    /// The registration of this host implies to register further services like <see cref="Microsoft.Extensions.FileProviders.IFileProvider"/>
    /// </para>
    /// </summary>
    /// <param name="serviceCollection"></param>
    /// <param name="settings"></param>
    /// <returns></returns>
    public static IServiceCollection AddHost(this IServiceCollection serviceCollection, IHostSettings settings)
    {
        // as well as adding a new Host, register the IFileProvider
        // the reason of that is for disposing at the end of lifetime of ServiceProvider
        // That service needs to be registered and resolved, in order to call Dispose on that service.

        return serviceCollection.AddSingleton<IHost, Hosting.Host>()
            .AddSingleton(settings)
            .AddSingleton(provider => provider.GetService<IHostEnvironment>().ContentRootFileProvider);
    }

    /// <summary>
    /// Changes a registration which was registered with the given TService type, with the TImplementation type.
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    /// <typeparam name="TImplementation"></typeparam>
    /// <param name="serviceCollection"></param>
    /// <returns></returns>
    public static IServiceCollection ChangeRegistration<TService, TImplementation>(this IServiceCollection serviceCollection)
        where TService : class, TImplementation
    {
        return serviceCollection.ChangeRegistration(typeof(TService), typeof(TImplementation));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    /// <param name="serviceCollection"></param>
    /// <param name="factory"></param>
    /// <returns></returns>
    public static IServiceCollection ChangeRegistration<TService>(this IServiceCollection serviceCollection, Func<IServiceProvider, TService> factory)
        where TService : class
    {
        return serviceCollection.ChangeRegistration(typeof(TService), factory);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    /// <param name="serviceCollection"></param>
    /// <param name="singleton"></param>
    /// <returns></returns>
    public static IServiceCollection ChangeRegistration<TService>(this IServiceCollection serviceCollection, TService singleton)
        where TService : class
    {
        return serviceCollection.ChangeRegistration(typeof(TService), singleton);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serviceCollection"></param>
    /// <param name="serviceType"></param>
    /// <param name="serviceImplementation"></param>
    /// <returns></returns>
    /// <exception cref="InvalidOperationException"></exception>
    public static IServiceCollection ChangeRegistration(this IServiceCollection serviceCollection, Type serviceType, Type serviceImplementation)
    {
        // check compatibility of types
        if (serviceType.IsAssignableFrom(serviceImplementation))
        {
            throw new InvalidOperationException($"The given implementation type is not compatible with the service type, serviceType: {serviceType.Name}, implementationType: {serviceImplementation?.Name}");
        }

        return serviceCollection.ForeachOnMatch(serviceType,
            descriptor => new ServiceDescriptor(descriptor.ServiceType, serviceImplementation, descriptor.Lifetime));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serviceCollection"></param>
    /// <param name="serviceType"></param>
    /// <param name="factory"></param>
    /// <returns></returns>
    public static IServiceCollection ChangeRegistration(this IServiceCollection serviceCollection, Type serviceType, Func<IServiceProvider, object> factory)
    {
        return serviceCollection.ForeachOnMatch(serviceType,
            descriptor => new ServiceDescriptor(descriptor.ServiceType, factory, descriptor.Lifetime));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serviceCollection"></param>
    /// <param name="serviceType"></param>
    /// <param name="singleton"></param>
    /// <returns></returns>
    public static IServiceCollection ChangeRegistration(this IServiceCollection serviceCollection, Type serviceType, object singleton)
    {
        return serviceCollection.ForeachOnMatch(serviceType,
            descriptor => new ServiceDescriptor(descriptor.ServiceType, singleton));
    }

    /// <summary>
    /// Changes lifetime of the given service type if this was registered previously.
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    /// <param name="serviceCollection"></param>
    /// <param name="lifetime"></param>
    /// <returns></returns>
    public static IServiceCollection ChangeLifetime<TService>(this IServiceCollection serviceCollection, ServiceLifetime lifetime)
    {
        return serviceCollection.ChangeLifetime(typeof(TService), lifetime);
    }

    /// <summary>
    /// Changes lifetime of the given service type if this was registered previously.
    /// </summary>
    /// <param name="serviceCollection"></param>
    /// <param name="serviceType"></param>
    /// <param name="lifetime"></param>
    /// <returns></returns>
    public static IServiceCollection ChangeLifetime(this IServiceCollection serviceCollection, Type serviceType, ServiceLifetime lifetime)
    {
        return serviceCollection.ForeachOnMatch(serviceType, descriptor => descriptor.ChangeLifetime(lifetime));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serviceCollection"></param>
    /// <param name="serviceTypeName"></param>
    /// <param name="lifetime"></param>
    /// <returns></returns>
    public static IServiceCollection ChangeLifetime(this IServiceCollection serviceCollection, string serviceTypeName, ServiceLifetime lifetime)
    {
        for (var i = 0; i < serviceCollection.Count; i++)
        {
            var current = serviceCollection[i];

            if (current.ServiceType.Name.Equals(serviceTypeName) || (current.ServiceType.FullName?.Equals(serviceTypeName)).GetValueOrDefault())
            {
                serviceCollection[i] = current.ChangeLifetime(lifetime);
            }
        }

        return serviceCollection;
    }

    /// <summary>
    /// Creates a new copy of <see cref="ServiceDescriptor"/> instance using the given lifetime if their lifetimes are different.
    /// </summary>
    /// <param name="descriptor"></param>
    /// <param name="lifetime"></param>
    /// <returns></returns>
    public static ServiceDescriptor ChangeLifetime(this ServiceDescriptor descriptor, ServiceLifetime lifetime)
    {
        var ret = descriptor;

        if (descriptor.Lifetime == lifetime)
        {
            return ret;
        }

        if (descriptor.ImplementationType is not null)
        {
            ret = new ServiceDescriptor(descriptor.ServiceType, descriptor.ImplementationType, lifetime);
        }
        else if (descriptor.ImplementationFactory is not null)
        {
            ret = new ServiceDescriptor(descriptor.ServiceType, descriptor.ImplementationFactory, lifetime);
        }
        else if (descriptor.ImplementationInstance is not null)
        {
            ret = new ServiceDescriptor(descriptor.ServiceType, descriptor.ImplementationInstance);
        }

        return ret;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    /// <param name="serviceCollection"></param>
    /// <returns></returns>
    public static IServiceCollection Remove<TService>(this IServiceCollection serviceCollection)
    {
        return serviceCollection.Remove(typeof(TService));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serviceCollection"></param>
    /// <param name="func"></param>
    /// <returns></returns>
    public static IServiceCollection RemoveWhere(this IServiceCollection serviceCollection, Func<ServiceDescriptor, bool> func)
    {
        var indexes = new List<int>();

        for (var index = 0; index < serviceCollection.Count; index++)
        {
            if (func(serviceCollection[index]))
            {
                indexes.Add(index);
            }
        }

        indexes.ForEach(serviceCollection.RemoveAt);

        return serviceCollection;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serviceCollection"></param>
    /// <param name="serviceType"></param>
    /// <returns></returns>
    public static IServiceCollection Remove(this IServiceCollection serviceCollection, Type serviceType)
    {
        var indexes = new List<int>();

        for (var index = 0; index < serviceCollection.Count; index++)
        {
            if (serviceCollection[index].ServiceType == serviceType)
            {
                indexes.Add(index);
            }
        }

        indexes.ForEach(serviceCollection.RemoveAt);

        return serviceCollection;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serviceCollection"></param>
    /// <param name="serviceType"></param>
    /// <param name="factory"></param>
    /// <returns></returns>
    private static IServiceCollection ForeachOnMatch(this IServiceCollection serviceCollection, Type serviceType, Func<ServiceDescriptor, ServiceDescriptor> factory)
    {
        for (var index = 0; index < serviceCollection.Count; index++)
        {
            var current = serviceCollection[index];

            if (current.ServiceType == serviceType)
            {
                serviceCollection[index] = factory(current);
            }
        }

        return serviceCollection;
    }
}
