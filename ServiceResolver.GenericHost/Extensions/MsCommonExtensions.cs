using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace ServiceResolver.Ioc.Extensions;

/// <summary>
/// 
/// </summary>
public static class MsCommonExtensions
{
    private static readonly MethodInfo EnumerableEmptyMethod;
    private static readonly ConcurrentDictionary<Type, object> EmptyCollections;

    static MsCommonExtensions()
    {
        EnumerableEmptyMethod = typeof(Enumerable).GetMethod(nameof(Enumerable.Empty));
        EmptyCollections = [];
    }

    /// <summary>
    /// Builds a mew <see cref="IEnumerable{T}"/> instance for the given type.
    /// </summary>
    /// <param name="serviceType"></param>
    /// <returns></returns>
    public static object AsEmptyCollection(this Type serviceType)
    {
        if (EmptyCollections.TryGetValue(serviceType, out var obj))
        {
            return obj;
        }

        var method = EnumerableEmptyMethod.MakeGenericMethod(serviceType);

        obj = method.Invoke(obj: null, []);

        EmptyCollections.TryAdd(serviceType, obj);

        return obj;
    }

    /// <summary>
    /// Creates a new <see cref="RegistrationInfo"/> instance using the given <see cref="ServiceDescriptor"/> instance.
    /// </summary>
    /// <param name="serviceDescriptor"></param>
    /// <param name="diagnosticFactory"></param>
    /// <returns></returns>
    public static IRegistrationInfo MakeRegistration(this ServiceDescriptor serviceDescriptor, Func<SuppressDiagnostic> diagnosticFactory)
    {
        var isKeyedService = serviceDescriptor.IsKeyedService;
        var serviceKey = serviceDescriptor.ServiceKey;
        
        object implementationInstance;
        Func<IGenericServiceProvider, object> implementationFactory = null;
        Type implementationType;

        if (isKeyedService)
        {
            implementationInstance = serviceDescriptor.KeyedImplementationInstance;

            if (serviceDescriptor.KeyedImplementationFactory is not null)
            {
                implementationFactory = serviceProvider => serviceDescriptor.KeyedImplementationFactory(serviceProvider.GetCurrentProvider(), serviceKey);
            }

            implementationType = serviceDescriptor.KeyedImplementationType;
        }
        else
        {
            implementationInstance = serviceDescriptor.ImplementationInstance;

            if (serviceDescriptor.ImplementationFactory is not null)
            {
                implementationFactory = serviceProvider => serviceDescriptor.ImplementationFactory(serviceProvider.GetCurrentProvider());
            }

            implementationType = serviceDescriptor.ImplementationType;
        }

        var lifetime = serviceDescriptor.Lifetime.Translate();

        var diagnostic = diagnosticFactory.Invoke();

        IRegistrationInfo registration = null;

        if (implementationInstance != null)
        {
            registration = lifetime.CreateRegistration(_ => serviceDescriptor.ImplementationInstance, diagnostic);
        }
        else if (implementationFactory != null)
        {
            registration = lifetime.CreateRegistration(implementationFactory, diagnostic);
        }
        else if (implementationType != null)
        {
            if (serviceKey != null && implementationType.HasCtorWithAnyParamsDecorated([typeof(ServiceKeyAttribute)]))
            {
                registration = lifetime.CreateRegistration(serviceProvider =>
                    ActivatorUtilities.CreateInstance(serviceProvider.GetCurrentProvider(), implementationType, serviceKey), diagnostic);
            }
            else
            {
                registration = lifetime.CreateRegistration(implementationType, diagnostic);
            }
        }

        return registration;
    }

    /// <summary>
    /// Translates the given <see cref="ServiceLifetime"/> reference into <see cref="LifetimeScope"/> reference.
    /// </summary>
    /// <param name="lifetime"></param>
    /// <returns></returns>
    public static LifetimeScope Translate(this ServiceLifetime lifetime)
    {
        return lifetime switch
        {
            ServiceLifetime.Scoped => LifetimeScope.Scoped,
            ServiceLifetime.Singleton => LifetimeScope.Singleton,
            _ => LifetimeScope.Transient
        };
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="instance"></param>
    /// <returns></returns>
    internal static async ValueTask TryDisposeAsync(this object instance)
    {
        switch (instance)
        {
            case IAsyncDisposable asyncDisposable:
                await asyncDisposable.DisposeAsync().ConfigureAwait(false);
                break;
            case IDisposable disposable:
                disposable.Dispose();
                break;
        }
    }
}
