using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ServiceResolver.Ioc.Hosting;
using ServiceResolver.Ioc.Options;
using ServiceResolver.Ioc.Providers;
using ServiceResolver.Ioc.Registers;

namespace ServiceResolver.Ioc.Extensions;

/// <summary>
/// 
/// </summary>
public static class ServiceResolverExtensions
{
    /// <summary>
    /// Adds the new <see cref="Hosting.Host"/> with some extra customizations.
    /// <para>
    /// The registration of this host implies to register further services like <see cref="Microsoft.Extensions.FileProviders.IFileProvider"/>
    /// </para>
    /// </summary>
    /// <param name="serviceRegister"></param>
    /// <param name="settings"></param>
    /// <returns></returns>
    public static IServiceRegister RegisterHost(this IServiceRegister serviceRegister, IHostSettings settings)
    {
        // as well as adding a new Host, register the IFileProvider
        // the reason of that is for disposing at the end of lifetime of ServiceProvider
        // That service needs to be registered and resolved, in order to call Dispose on that service.

        return serviceRegister.Register<IHost, Hosting.Host>(LifetimeScope.Singleton)
            .Register(_ => settings, LifetimeScope.Singleton)
            // todo: use a decorator registration in order to dispose IFileProvider
            .Register(provider => provider.GetService<IHostEnvironment>().ContentRootFileProvider, LifetimeScope.Singleton);
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TServiceRegister"></typeparam>
    /// <param name="serviceRegister"></param>
    /// <param name="serviceCollection"></param>
    /// <param name="options"></param>
    /// <returns></returns>
    public static IServiceProvider BuildServiceProvider<TServiceRegister>(
        this TServiceRegister serviceRegister,
        IServiceCollection serviceCollection,
        IServiceRegisterOptions options = null)
        where TServiceRegister : class, IServiceRegisterProvider
    {
        var regGroups = serviceCollection.GetRegistrationsInternal();
        
        options ??= new ServiceRegisterOptions
        {
            AutoRegistration = false,
            IgnoreMissingRegistrations = false,
            SuppressDiagnosticType = ResolutionType.Standard
        };

        Func<SuppressDiagnostic> diagnosticFactory = options.SuppressDiagnosticType == ResolutionType.Standard
            ? () => null
            : () => new SuppressDiagnostic
            {
                LifestyleMismatch = true,
                DisposableTransientComponent = true
            };

        foreach (var (svcKey, svcDescriptors) in regGroups)
        {
            try
            {
                var manyRegistrations = svcDescriptors
                    .Select(serviceDescriptor => serviceDescriptor.MakeRegistration(diagnosticFactory))
                    .ToList();

                var singleRegistration = manyRegistrations.Last();

                if (serviceRegister.IsRegistered(svcKey) || serviceRegister.IsRegistered(svcKey, true))
                {
                    continue;
                }

                serviceRegister.RegisterLazy(svcKey, singleRegistration);
                serviceRegister.RegisterManyLazy(svcKey, manyRegistrations);
            }
            catch (Exception ex)
            {
                // nothing to do !!!
                Console.WriteLine(ex);
            }
        }

        var serviceProvider =  new ServiceResolverScopeFactory(serviceRegister);

        serviceRegister.Register<IServiceScopeFactory>(() => serviceProvider, LifetimeScope.Singleton);

        if (options.AutoRegistration)
        {
            //serviceRegister.Register<IServiceProvider>(() => serviceProvider, LifetimeScope.Singleton);

            //if (!serviceRegister.IsRegistered<IServiceProviderIsService>())
            //{
            //    serviceRegister.Register<IServiceProviderIsService>(() => serviceProvider, LifetimeScope.Singleton);
            //}

            // another services related to service providers (even with keyed version).
            // NOTE: these services don't have to be registered before by microsoft DI, in that case, you need to remove them.
            serviceRegister.Register<IServiceProvider>(() => serviceProvider, LifetimeScope.Singleton)
                .Register<IKeyedServiceProvider>(() => serviceProvider, LifetimeScope.Singleton)
                .Register<IServiceProviderIsService>(() => serviceProvider, LifetimeScope.Singleton)
                .Register<IServiceProviderIsKeyedService>(() => serviceProvider, LifetimeScope.Singleton);
        }

        return serviceProvider;
    }

    /// <summary>
    /// Gets the current <see cref="IServiceProvider"/> if It's registered, otherwise it returns itself.
    /// </summary>
    /// <param name="current"></param>
    /// <returns></returns>
    public static IServiceProvider GetCurrentProvider(this IGenericServiceProvider current)
    {
        return current.IsRegistered<IServiceProvider>()
            ? current.GetService<IServiceProvider>()
            : current;
    }
}
