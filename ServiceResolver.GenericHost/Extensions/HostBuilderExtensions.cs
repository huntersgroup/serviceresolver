using System;
using Microsoft.Extensions.Hosting;
using ServiceResolver.Ioc.Caching;
using ServiceResolver.Ioc.Options;
using ServiceResolver.Ioc.Providers;

namespace ServiceResolver.Ioc.Extensions;

/// <summary>
/// 
/// </summary>
public static class HostBuilderExtensions
{
    /// <summary>
    /// Represents a basic extension method to use for building a custom <see cref="IServiceProvider"/> instance on the top of <see cref="IServiceRegisterProvider"/> contract, replacing the default one.
    /// </summary>
    /// <typeparam name="TServiceRegister"></typeparam>
    /// <param name="builder">The <see cref="IHostBuilder"/> instance.</param>
    /// <param name="options">The <see cref="ServiceRegisterOptions&lt;TServiceRegister&gt;"/> instance.</param>
    /// <returns></returns>
    public static IHostBuilder UseServiceResolver<TServiceRegister>(this IHostBuilder builder,
        ServiceRegisterOptions<TServiceRegister> options)
        where TServiceRegister : class, IServiceRegisterProvider
    {
        builder.UseServiceProviderFactory(_ => new ServiceResolverBuilder<TServiceRegister>(options))
            .ConfigureServiceResolver<TServiceRegister>();

        return builder;
    }

    /// <summary>
    /// Represents a basic extension method to use for building a custom <see cref="IServiceProvider"/> instance on the top of <see cref="IServiceRegisterProvider"/> contract, replacing the default one.
    /// </summary>
    /// <typeparam name="TServiceRegister"></typeparam>
    /// <param name="builder"></param>
    /// <param name="optionsFactory"></param>
    /// <returns></returns>
    public static IHostBuilder UseServiceResolver<TServiceRegister>(
        this IHostBuilder builder,
        Func<HostBuilderContext, ServiceRegisterOptions<TServiceRegister>> optionsFactory)
        where TServiceRegister : class, IServiceRegisterProvider
    {
        builder.UseServiceProviderFactory(context => new ServiceResolverBuilder<TServiceRegister>(optionsFactory(context)))
            .ConfigureServiceResolver<TServiceRegister>();

        return builder;
    }

    /// <summary>
    /// Configure the current <see cref="TServiceRegister"/> instance.
    /// </summary>
    /// <typeparam name="TServiceRegister"></typeparam>
    /// <param name="builder"></param>
    /// <returns></returns>
    private static IHostBuilder ConfigureServiceResolver<TServiceRegister>(this IHostBuilder builder)
        where TServiceRegister : class, IServiceRegisterProvider
    {
        return builder.ConfigureContainer<TServiceRegister>(provider =>
        {
            var options = provider.Cache.Get<ServiceRegisterOptions<TServiceRegister>>();

            // It's removed it, It doesn't necessary on registration phase.
            provider.Cache.Remove(new CacheKey{ Type = options.GetType()});

            options.OnRegistering?.Invoke(provider);
        });
    }
}
