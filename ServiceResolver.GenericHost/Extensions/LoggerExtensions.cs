using System;
using Microsoft.Extensions.Logging;

namespace ServiceResolver.Ioc.Extensions;

internal static class LoggerExtensions
{
    internal static void Starting<TService>(this ILogger<TService> logger)
    {
        if (logger.IsEnabled(LogLevel.Debug))
        {
            logger.LogDebug($"service: {typeof(TService)}, starting at: {DateTime.UtcNow:s}");
        }
    }

    internal static void Started<TService>(this ILogger<TService> logger)
    {
        if (logger.IsEnabled(LogLevel.Debug))
        {
            logger.LogDebug($"service: {typeof(TService)}, started at: {DateTime.UtcNow:s}");
        }
    }

    internal static void Stopping<TService>(this ILogger<TService> logger)
    {
        if (logger.IsEnabled(LogLevel.Debug))
        {
            logger.LogDebug($"service: {typeof(TService)}, Stopping at: {DateTime.UtcNow:s}");
        }
    }

    internal static void Stopped<TService>(this ILogger<TService> logger, Exception exception = null)
    {
        if (exception is null && logger.IsEnabled(LogLevel.Debug))
        {
            logger.LogDebug($"service: {typeof(TService)}, Stopped at: {DateTime.UtcNow:s}");
        }
        else if (exception is not null && logger.IsEnabled(LogLevel.Error))
        {
            logger.LogError(exception,$"service: {typeof(TService)}, Stopped at: {DateTime.UtcNow:s}");
        }
    }

    internal static void BackgroundServiceFaulted(this ILogger logger, Exception ex)
    {
        if (logger.IsEnabled(LogLevel.Error))
        {
            logger.LogError(
                exception: ex,
                message: $"BackgroundService failed at: {DateTime.UtcNow:s}");
        }
    }

    internal static void BackgroundServiceStoppingHost(this ILogger logger, Exception ex)
    {
        if (logger.IsEnabled(LogLevel.Critical))
        {
            logger.LogCritical(
                exception: ex,
                message: $"BackgroundService is stopping host at: {DateTime.UtcNow:s}");
        }
    }
}
