using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace ServiceResolver.Ioc.Hosting;

/// <summary>
/// Represents a custom configuration for the custom internal <see cref="IHost"/> implementation.
/// </summary>
public class HostSettings : IHostSettings
{
    /// <summary>
    /// Gets or sets if the custom Host is enabled (if it will be installed the new one).
    /// </summary>
    public bool Enabled { get; init; } = true;

    ///<inheritdoc />
    public bool UseScope { get; init; } = false;
}

/// <summary>
/// Represents a custom configuration for the custom internal <see cref="IHost"/> implementation.
/// </summary>
public interface IHostSettings
{
    /// <summary>
    /// Gets the boolean value indicating all <see cref="IHostedService"/> instance will be run under an <see cref="AsyncServiceScope"/> instance.
    /// </summary>
    bool UseScope { get; }
}
