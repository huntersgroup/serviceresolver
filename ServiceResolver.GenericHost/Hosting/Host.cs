using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Hosting.Internal;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ServiceResolver.Ioc.Extensions;

namespace ServiceResolver.Ioc.Hosting;

/// <summary>
/// Represents a custom implementation of <see cref="IHost"/>, which can create scopes, It depends on its <see cref="IHostSettings"/> dependency.
/// </summary>
[DebuggerDisplay("status: {this.Status}")]
public class Host : IHost, IAsyncDisposable
{
    private readonly ILogger<Host> logger;
    private readonly IHostLifetime hostLifetime;
    // ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
    private readonly IFileProvider fileProvider;
    private readonly ApplicationLifetime applicationLifetime;
    private readonly HostOptions options;
    private readonly IHostSettings hostSettings;
    private readonly Lazy<AsyncServiceScope> lazyScope;
    private IEnumerable<IHostedService> hostedServices;
    private bool disposed;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="services"></param>
    /// <param name="fileProvider"></param>
    /// <param name="applicationLifetime"></param>
    /// <param name="logger"></param>
    /// <param name="hostLifetime"></param>
    /// <param name="options"></param>
    /// <param name="hostSettings"></param>
    /// <exception cref="ArgumentException"></exception>
    /// <exception cref="ArgumentNullException"></exception>
    public Host(IServiceProvider services,
        IFileProvider fileProvider,
        IHostApplicationLifetime applicationLifetime,
        ILogger<Host> logger,
        IHostLifetime hostLifetime,
        IOptions<HostOptions> options,
        IHostSettings hostSettings)
    {
        this.Services = services;
        this.fileProvider = fileProvider;
        // todo: can be replaced by another custom service (which implements missing methods present in the ApplicationLifetime class).
        this.applicationLifetime = applicationLifetime.CastOrThrow<ApplicationLifetime>(nameof(applicationLifetime));

        this.logger = logger;
        this.hostLifetime = hostLifetime;
        // todo: tobe replaced by another compatible service (maybe extended)
        this.options = options?.Value ?? throw new ArgumentNullException(nameof(options));
        this.hostSettings = hostSettings;
        this.lazyScope = new Lazy<AsyncServiceScope>(() => this.Services.CreateAsyncScope(), true);

        this.logger.LogDebug("File provider was resolved, {0}", this.fileProvider.ToString());

        this.Status = HostStatus.WaitingFor;
    }

    ///<inheritdoc />
    public IServiceProvider Services { get; }

    /// <summary>
    /// 
    /// </summary>
    public HostStatus Status { get; private set; }

    /// <summary>
    /// 
    /// </summary>
    private IServiceProvider ServiceProvider
    {
        get
        {
            return this.lazyScope.IsValueCreated ? this.lazyScope.Value.ServiceProvider : this.Services;
        }
    }

    ///<inheritdoc />
    public async Task StartAsync(CancellationToken cancellationToken = new())
    {
        this.logger.Starting();

        using var combinedCancellationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken, this.applicationLifetime.ApplicationStopping);
        var combinedCancellationToken = combinedCancellationTokenSource.Token;

        await this.hostLifetime.WaitForStartAsync(combinedCancellationToken).ConfigureAwait(false);

        combinedCancellationToken.ThrowIfCancellationRequested();

        if (this.hostSettings.UseScope)
        {
            // this is used to initialize a new scope
            _ = this.lazyScope.Value;
        }

        this.hostedServices = this.ServiceProvider
            .GetRequiredService<IEnumerable<IHostedService>>()
            .ToList()
            ;

        foreach (var hostedService in this.hostedServices)
        {
            // Fire IHostedService.Start
            await hostedService.StartAsync(combinedCancellationToken).ConfigureAwait(false);

            if (hostedService is BackgroundService backgroundService)
            {
                _ = this.TryExecuteBackgroundServiceAsync(backgroundService);
            }
        }

        this.Status = HostStatus.Running;

        // Fire IHostApplicationLifetime.Started
        this.applicationLifetime.NotifyStarted();

        this.logger.Started();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="backgroundService"></param>
    /// <returns></returns>
    private async Task TryExecuteBackgroundServiceAsync(BackgroundService backgroundService)
    {
        // backgroundService.ExecuteTask may not be set (e.g. if the derived class doesn't call base.StartAsync)
        var backgroundTask = backgroundService.ExecuteTask;

        if (backgroundTask == null)
        {
            return;
        }

        try
        {
            await backgroundTask.ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            // When the host is being stopped, it cancels the background services.
            // This isn't an error condition, so don't log it as an error.
            if (this.Status == HostStatus.Stopped && backgroundTask.IsCanceled && ex is OperationCanceledException)
            {
                return;
            }

            this.logger.BackgroundServiceFaulted(ex);

            if (this.options.BackgroundServiceExceptionBehavior == BackgroundServiceExceptionBehavior.StopHost)
            {
                this.logger.BackgroundServiceStoppingHost(ex);
                this.applicationLifetime.StopApplication();
            }
        }
    }

    ///<inheritdoc />
    public async Task StopAsync(CancellationToken cancellationToken = new())
    {
        this.Status = HostStatus.Stopped;
        this.logger.Stopping();

        using (var cts = new CancellationTokenSource(this.options.ShutdownTimeout))
        using (var linkedCts = CancellationTokenSource.CreateLinkedTokenSource(cts.Token, cancellationToken))
        {
            var token = linkedCts.Token;
            // Trigger IHostApplicationLifetime.ApplicationStopping
            this.applicationLifetime.StopApplication();

            var exceptions = new List<Exception>();
            if (this.hostedServices != null) // Started?
            {
                foreach (var hostedService in this.hostedServices.Reverse())
                {
                    try
                    {
                        await hostedService.StopAsync(token).ConfigureAwait(false);
                    }
                    catch (Exception ex)
                    {
                        exceptions.Add(ex);
                    }
                }
            }

            // Fire IHostApplicationLifetime.Stopped
            this.applicationLifetime.NotifyStopped();

            try
            {
                await this.hostLifetime.StopAsync(token).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                exceptions.Add(ex);
            }

            if (exceptions.Count > 0)
            {
                var ex = new AggregateException("One or more hosted services failed to stop.", exceptions);
                this.logger.Stopped(ex);
                throw ex;
            }
        }

        this.logger.Stopped();
    }

    ///<inheritdoc />
    public void Dispose() => this.DisposeAsync().AsTask().GetAwaiter().GetResult();

    ///<inheritdoc />
    public async ValueTask DisposeAsync()
    {
        if (this.disposed)
        {
            return;
        }

        this.disposed = true;

        if (this.lazyScope.IsValueCreated)
        {
            await this.lazyScope.Value.DisposeAsync();
        }

        await this.Services.TryDisposeAsync().ConfigureAwait(false);
    }
}
