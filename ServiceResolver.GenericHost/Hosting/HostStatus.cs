using Microsoft.Extensions.Hosting;

namespace ServiceResolver.Ioc.Hosting;

/// <summary>
/// Represents the status of <see cref="IHost"/> instances.
/// </summary>
public enum HostStatus : byte
{
    /// <summary>
    /// An unknown value set.
    /// </summary>
    Unknown = 0,

    /// <summary>
    /// Indicates host is waiting for starting.
    /// </summary>
    WaitingFor = 1,

    /// <summary>
    /// Indicates host is running.
    /// </summary>
    Running = 2,

    /// <summary>
    /// Indicates host is stopped
    /// </summary>
    Stopped = 3
}
