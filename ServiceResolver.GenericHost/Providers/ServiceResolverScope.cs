using System;
using Microsoft.Extensions.DependencyInjection;

namespace ServiceResolver.Ioc.Providers;

/// <summary>
/// 
/// </summary>
/// <param name="serviceResolver"></param>
internal class ServiceResolverScope(IGenericServiceProvider serviceResolver)
    : IServiceScope
{
    private readonly IScope scope = serviceResolver.BeginScope();
    private bool disposed;

    ///<inheritdoc />
    public IServiceProvider ServiceProvider { get; } = serviceResolver;

    ///<inheritdoc />
    public void Dispose()
    {
        if (this.disposed)
        {
            return;
        }

        try
        {
            this.scope.Dispose();
            this.disposed = true;
        }
        catch
        {
            // nothing to do !!
        }
    }
}
