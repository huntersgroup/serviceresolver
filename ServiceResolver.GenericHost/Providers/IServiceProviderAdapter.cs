using Microsoft.Extensions.DependencyInjection;

namespace ServiceResolver.Ioc.Providers;

/// <summary>
/// Represents a custom implementation for custom service provider used for host integration.
/// </summary>
public interface IServiceProviderAdapter : IKeyedServiceProvider, IServiceProviderIsKeyedService;
