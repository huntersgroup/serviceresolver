using System;
using Microsoft.Extensions.DependencyInjection;
using ServiceResolver.Ioc.Caching;
using ServiceResolver.Ioc.Extensions;
using ServiceResolver.Ioc.Options;

namespace ServiceResolver.Ioc.Providers;

/// <summary>
/// Provides an extension point for creating a container specific builder (of <see cref="IServiceRegisterProvider"/> type) and an <see cref="IServiceProvider" /> instance to return to the calling code.
/// </summary>
/// <typeparam name="TServiceRegister"></typeparam>
/// <param name="options"></param>
public class ServiceResolverBuilder<TServiceRegister>(IServiceRegisterBuilder<TServiceRegister> options)
    : IServiceProviderFactory<TServiceRegister>
    where TServiceRegister : class, IServiceRegisterProvider
{
    ///<inheritdoc />
    public TServiceRegister CreateBuilder(IServiceCollection services)
    {
        /*
         * the sequence should be:
         * 1. create the service resolver
         * 2. execute the before-registering action
         * 3. IServiceCollection instance becomes readonly
         * 4. IServiceCollection is registered in the Service provider cache.
         * 5. Options is registered into the service provider cache.
         * 6. return the IServiceRegisterProvider instance.
         */

        var serviceRegister = options.OnCreating(services);

        options.BeforeRegistering?.Invoke(services);

        var readonlyServices = services.AsReadonly();

        serviceRegister.Cache.Upsert(readonlyServices);
        serviceRegister.Cache.Upsert(new CacheItem
        {
            Key = new CacheKey
            {
                Type = options.GetType()
            },
            Item = options
        });

        return serviceRegister;
    }

    ///<inheritdoc />
    public IServiceProvider CreateServiceProvider(TServiceRegister serviceRegister)
    {
        var serviceCollection = serviceRegister.Cache.Get<IServiceCollection>();

        var provider = serviceRegister.BuildServiceProvider(serviceCollection, options);

        options.AfterBuildingProvider?.Invoke(serviceRegister);

        return provider;
    }
}
