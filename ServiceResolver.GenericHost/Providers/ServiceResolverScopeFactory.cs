using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using ServiceResolver.Ioc.Exceptions;
using ServiceResolver.Ioc.Extensions;

namespace ServiceResolver.Ioc.Providers;

/// <summary>
/// 
/// </summary>
/// <param name="serviceResolver"></param>
internal class ServiceResolverScopeFactory(IGenericServiceProvider serviceResolver)
    : IServiceScopeFactory, IServiceProviderAdapter, IDisposable, IAsyncDisposable
{
    private bool disposed;
    private readonly Dictionary<ServiceKey, object> missingRegistrations = [];

    ///<inheritdoc />
    public object GetService(Type serviceType)
    {
        return this.GetServiceInternal(serviceType);
    }

    ///<inheritdoc />
    public object GetKeyedService(Type serviceType, object serviceKey)
    {
        return this.GetServiceInternal(serviceType, serviceKey);
    }

    ///<inheritdoc />
    public object GetRequiredKeyedService(Type serviceType, object serviceKey)
    {
        return this.GetServiceInternal(serviceType, serviceKey, false);
    }

    ///<inheritdoc />
    public IServiceScope CreateScope()
    {
        return new ServiceResolverScope(serviceResolver);
    }

    ///<inheritdoc />
    public bool IsService(Type serviceType)
    {
        return this.IsServiceInternal(serviceType);
    }

    ///<inheritdoc />
    public bool IsKeyedService(Type serviceType, object serviceKey)
    {
        return this.IsServiceInternal(serviceType, serviceKey);
    }

    ///<inheritdoc />
    public void Dispose() => this.DisposeAsync().AsTask().GetAwaiter().GetResult();

    ///<inheritdoc />
    public async ValueTask DisposeAsync()
    {
        if (this.disposed)
        {
            return;
        }

        this.disposed = true;

        try
        {
            serviceResolver.TryToDispose();
        }
        catch (Exception)
        {
            //nothing to do here
        }

        await Task.CompletedTask;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serviceType"></param>
    /// <param name="key"></param>
    /// <returns></returns>
    private bool IsServiceInternal(Type serviceType, object key = null)
    {
        // If serviceType is IEnumerable<TService> so it's important to indicate TService as Type, and the flag that it is a collection.
        return serviceType.TryGetEnumerableGenType(out var genericType)
            ? serviceResolver.IsRegistered(new ServiceKey(genericType, key), true)
            : serviceResolver.IsRegistered(new ServiceKey(serviceType, key));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serviceType"></param>
    /// <param name="key"></param>
    /// <param name="optional"></param>
    /// <returns></returns>
    /// <exception cref="ServiceUnavailableException"></exception>
    private object GetServiceInternal(Type serviceType, object key = null, bool optional = true)
    {
        var svcRef = new ServiceKey(serviceType, key);
        
        try
        {
            return !this.missingRegistrations.TryGetValue(svcRef, out var service)
                ? serviceResolver.GetService(serviceType, key)
                : service;
        }
        catch (Exception ex)
        {
            // TODO: It seems the property IgnoreMissingRegistrations doesn't serve anymore, It would be enough the 'optional' parameter.
            //if (options.IgnoreMissingRegistrations && optional)
            if (optional)
            {
                object val = null;

                if (serviceType.TryGetEnumerableGenType(out var genericType))
                {
                    val = genericType.AsEmptyCollection();
                }

                this.missingRegistrations.Add(svcRef, val);

                return val;
            }

            throw new ServiceUnavailableException(serviceType, "An exception was occurred on service resolution, see inner exception for details.", ex);
        }
    }
}


