# About this library

**ServiceResolver.GenericHost** is an abstract library for .NET generic host, which uses **ServiceResolver.Ioc** library.

Take this example defining a new **IServiceRegisterProvider** implementation (or other third party IServiceRegisterProvider implementation).

```csharp
class CustomServiceRegisterProvider : IServiceRegisterProvider
{
    public CustomServiceRegisterProvider(CustomServiceRegisterDescriptor descriptor)
    {
        // coding for setup.
    }

    // your custom implementation ..
}
```

## Configuration for console applications (Net Core, Net 6 or higher)

### IHostBuilder configuration

```csharp
var hostBuilder = Host.CreateDefaultBuilder(args)
    // custom extension method to use for integration.
    .UseServiceResolver(new ServiceRegisterOptions<IServiceRegisterProvider>
    {
        // a factory used to create your service register provider
        OnCreating = collection =>
        {
            return new CustomServiceRegisterProvider(new CustomServiceRegisterDescriptor());
        },
        // (optional)
        // an action used to prepare (if needed) your IServiceCollection.
        BeforeRegistering = collection =>
        {
            // your IServiceCollection registrations or integrations
        },
        // an action used to register your services.
        OnRegistering = provider =>
        {
            // example:
            provider.Register<StringBuilder>(() => new StringBuilder("hello world.."));
        },
        // (optional)
        // an action used to executes custom action at the end of all previous steps.
        AfterBuildingProvider = provider =>
        {
        }
    });

var host = hostBuilder.Build();

await host.RunAsync();
```

### IHostBuilder configuration with a function factory

The function factory has as input the **HostBuilderContext** instance. 

```csharp
var hostBuilder = Host.CreateDefaultBuilder(args)
    // custom extension method to use for integration.
    .UseServiceResolver(context => new ServiceRegisterOptions<IServiceRegisterProvider>
    {
        // a factory used to create your service register provider
        OnCreating = collection =>
        {
            return new CustomServiceRegisterProvider(new CustomServiceRegisterDescriptor());
        },
        // (optional)
        // an action used to prepare (if needed) your IServiceCollection.
        BeforeRegistering = collection =>
        {
            // your IServiceCollection registrations or integrations
        },
        // an action used to register your services.
        OnRegistering = provider =>
        {
            // example:
            provider.Register<StringBuilder>(() => new StringBuilder("hello world.."));
        },
        // (optional)
        // an action used to executes custom action at the end of all previous steps.
        AfterBuildingProvider = provider =>
        {
        }
    });

var host = hostBuilder.Build();

await host.RunAsync();
```
