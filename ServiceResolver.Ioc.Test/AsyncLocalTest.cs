using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace ServiceResolver.Ioc.Test;

public class AsyncLocalTest(ITestOutputHelper output)
{
    [Fact]
    public async Task TestOnContext01()
    {
        var instance = new ScopeManager();

        Assert.Null(instance.GetScope());

        var rootScopeName = "root-scope";

        using (instance.OpenScope(rootScopeName))
        {
            var childScopeName = "child-scope";

            Assert.NotNull(instance.GetScope());
            Assert.Equal(rootScopeName, instance.GetScope()?.Name);

            using (instance.OpenScope(childScopeName))
            {
                var task01 = this.OpenScopeAsync(instance, "demo-01");

                output.WriteLine($"after opening first child scope: {instance.GetScope()?.Name}");

                var task02 = this.OpenScopeAsync(instance, "demo-02");

                output.WriteLine($"after opening second child scope: {instance.GetScope()?.Name}");

                Assert.NotNull(instance.GetScope());
                Assert.Equal(childScopeName, instance.GetScope()?.Name);

                await task02;

                Assert.NotNull(instance.GetScope());
                Assert.Equal(childScopeName, instance.GetScope()?.Name);

                await task01;

                output.WriteLine($"before disposing child scope: {instance.GetScope()?.Name}");
                Assert.NotNull(instance.GetScope());
                Assert.Equal(childScopeName, instance.GetScope()?.Name);
            }

            output.WriteLine($"before disposing root scope: {instance.GetScope()?.Name}");
            Assert.NotNull(instance.GetScope());
            Assert.Equal(rootScopeName, instance.GetScope()?.Name);
        }

        output.WriteLine($"after disposing root scope: {instance.GetScope()?.Name}");
        Assert.Null(instance.GetScope());
    }

    private async Task OpenScopeAsync(ScopeManager instance, string scopeName)
    {
        output.WriteLine($"before opening on {nameof(this.OpenScopeAsync)}, arg: {scopeName}, from instance: {instance.GetScope()?.Name}");

        using (instance.OpenScope(scopeName))
        {
            output.WriteLine($"after opening (before delay) on {nameof(this.OpenScopeAsync)}, arg: {scopeName}, from instance: {instance.GetScope()?.Name}");
            await Task.Delay(4000);
            output.WriteLine($"after opening (after delay) on {nameof(this.OpenScopeAsync)}, arg: {scopeName}, from instance: {instance.GetScope()?.Name}");
        }

        output.WriteLine($"after disposing on {nameof(this.OpenScopeAsync)}, arg: {scopeName}, from instance: {instance.GetScope()?.Name}");
    }
}

public class ScopeManager
{
    private readonly AsyncLocal<ScopeContext> asyncLocal = new();
    
    public ScopeContext OpenScope(string name)
    {
        var ctx = new ScopeContext(name, this.GetScope());

        if (ctx.Root is null)
        {
            ctx.AddDisposableAction(this.TryToRemoveContext);
        }
        else
        {
            ctx.AddDisposableAction(context =>
            {
                var root = context.Root;
                this.asyncLocal.Value = root;
            });
        }
        
        this.asyncLocal.Value = ctx;

        return ctx;
    }

    public ScopeContext GetScope()
    {
        return this.asyncLocal.Value;
    }

    private void TryToRemoveContext(ScopeContext scope)
    {
        if (this.asyncLocal.Value == scope)
        {
            this.asyncLocal.Value = null;
        }
    }
}

public class ScopeContext(string name, ScopeContext rootContext = null)
    : IDisposable
{
    private readonly List<Action<ScopeContext>> disposableActions = [];

    public string Name { get => name; }

    public ScopeContext Root { get => rootContext; }

    public void AddDisposableAction(Action<ScopeContext> action)
    {
        this.disposableActions.Add(action);
    }

    public bool Disposed { get; private set; }

    public void Dispose()
    {
        if (this.Disposed)
        {
            return;
        }

        this.Disposed = true;

        foreach (var ret in this.disposableActions.Select(disposableAction => disposableAction.DynamicInvoke(this)))
        {
            if (ret is Task tk)
            {
                tk.GetAwaiter().GetResult();
            }
        }

        rootContext = null;
    }
}
