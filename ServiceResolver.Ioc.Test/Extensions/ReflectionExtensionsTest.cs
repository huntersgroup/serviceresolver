using ServiceResolver.Ioc.Test.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ServiceResolver.Ioc.Extensions;
using Xunit;

namespace ServiceResolver.Ioc.Test.Extensions;

public class ReflectionExtensionsTest
{
    [Theory]
    [InlineData(typeof(IGenericContract<>), typeof(GenericContractInt))]
    [InlineData(typeof(IGenericContractBase<>), typeof(GenericContractInt))]
    [InlineData(typeof(IGenericContract<>), typeof(GenericContractString))]
    [InlineData(typeof(IGenericContractBase<>), typeof(GenericContractString))]
    [InlineData(typeof(IGenericContract<>), typeof(GenericContract<>))]
    [InlineData(typeof(IGenericContractBase<>), typeof(GenericContract<>))]
    [InlineData(typeof(IGenericContractBase<>), typeof(IGenericContract<>))]
    [InlineData(typeof(IGenericContract<int>), typeof(GenericContract<int>))]
    [InlineData(typeof(IGenericContract), typeof(GenericContractV1))]
    [InlineData(typeof(IGenericContractBase), typeof(GenericContractV1))]
    [InlineData(typeof(IGenericContract), typeof(GenericContractV2))]
    [InlineData(typeof(IGenericContractBase), typeof(GenericContractV2))]
    [InlineData(typeof(IGenericContractBase), typeof(IGenericContract))]
    [InlineData(typeof(IGenericContract), typeof(GenericContractV3))]
    [InlineData(typeof(GenericContractV2), typeof(GenericContractV3))]
    public void IsAssignableByTest(Type baseType, Type concreteType)
    {
        Assert.True(baseType.IsAssignableBy(concreteType));
    }

    [Theory]
    [InlineData(typeof(IEnumerable<int>))]
    [InlineData(typeof(IEnumerable<string>))]
    [InlineData(typeof(ICollection<int>))]
    [InlineData(typeof(Collection<int>))]
    [InlineData(typeof(IList<int>))]
    [InlineData(typeof(List<int>))]
    [InlineData(typeof(int[]))]
    [InlineData(typeof(IEnumerable<>))]
    [InlineData(typeof(string))]
    public void IsEnumerableTest(Type type)
    {
        Assert.True(type.IsEnumerable());
    }

    [Theory]
    [InlineData(typeof(IEnumerable<int>))]
    [InlineData(typeof(IEnumerable<string>))]
    [InlineData(typeof(ICollection<int>))]
    [InlineData(typeof(Collection<int>))]
    [InlineData(typeof(IList<int>))]
    [InlineData(typeof(List<int>))]
    [InlineData(typeof(IEnumerable<>))]
    public void IsGenEnumerableTest(Type type)
    {
        Assert.True(type.IsGenEnumerable());
    }

    [Theory]
    [InlineData(typeof(string))]
    public void IsGenEnumerable_WrongTest(Type type)
    {
        Assert.False(type.IsGenEnumerable());
    }
}
