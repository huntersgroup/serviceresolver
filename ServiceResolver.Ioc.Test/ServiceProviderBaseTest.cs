using System;
using System.Linq;
using System.Reflection;
using System.Text;
using ServiceResolver.Ioc.Exceptions;
using ServiceResolver.Ioc.Extensions;
using ServiceResolver.Ioc.Test.Impl;
using ServiceResolver.Ioc.Test.Modules;
using ServiceResolver.Ioc.Test.Services;
using Xunit;
using Xunit.Abstractions;

namespace ServiceResolver.Ioc.Test;

public abstract class ServiceProviderBaseTest(ITestOutputHelper output)
{
    protected abstract IServiceRegisterProvider GetProvider(LifetimeScope? defaultLifeStyle = null);

    protected abstract ImplementorType Type { get; }

    [Fact]
    public void Register_InfoWithTransientTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register(typeof(IMyService), LifetimeScope.Transient.CreateRegistration<MyService>());

        // asserts
        Assert.NotNull(serviceRegister.GetService<IMyService>());
        Assert.NotSame(serviceRegister.GetService<IMyService>(), serviceRegister.GetService<IMyService>());
    }

    [Fact]
    public void Register_InfoGenWithTransientTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register<IMyService>(LifetimeScope.Transient.CreateRegistration<MyService>());

        // asserts
        Assert.NotNull(serviceRegister.GetService<IMyService>());
        Assert.NotSame(serviceRegister.GetService<IMyService>(), serviceRegister.GetService<IMyService>());
    }

    [Fact]
    public void Register_InfoGenConcreteTypeWithTransientTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register(LifetimeScope.Transient.CreateRegistration<MyService>());

        // asserts
        Assert.Throws<ServiceUnavailableException>(() => serviceRegister.GetService<IMyService>());
        Assert.NotNull(serviceRegister.GetService<MyService>());
        Assert.NotSame(serviceRegister.GetService<MyService>(), serviceRegister.GetService<MyService>());
    }

    [Fact]
    public void Register_InfoGenServiceConcreteTypeWithTransientTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register<IMyService, MyService>(LifetimeScope.Transient.CreateRegistration<MyService>());

        // asserts
        Assert.Throws<ServiceUnavailableException>(() => serviceRegister.GetService<MyService>());
        Assert.NotNull(serviceRegister.GetService<IMyService>());
        Assert.NotSame(serviceRegister.GetService<IMyService>(), serviceRegister.GetService<IMyService>());
    }

    [Fact]
    public void Register_InfoGenFactoryWithTransientTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register<IMyService>(LifetimeScope.Transient.CreateRegistration(_ => new MyService()));

        // asserts
        Assert.NotNull(serviceRegister.GetService<IMyService>());
        Assert.NotSame(serviceRegister.GetService<IMyService>(), serviceRegister.GetService<IMyService>());
    }

    [Fact]
    public void Register_InfoGenFactoryConcreteTypeWithTransientTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register(LifetimeScope.Transient.CreateRegistration(_ => new MyService()));

        // asserts
        Assert.Throws<ServiceUnavailableException>(() => serviceRegister.GetService<IMyService>());
        Assert.NotNull(serviceRegister.GetService<MyService>());
        Assert.NotSame(serviceRegister.GetService<MyService>(), serviceRegister.GetService<MyService>());
    }

    [Fact]
    public void Register_InfoWithScopeTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register(typeof(IMyService), LifetimeScope.Scoped.CreateRegistration<MyService>());

        using (serviceRegister.BeginScope())
        {
            // asserts
            Assert.NotNull(serviceRegister.GetService<IMyService>());
            Assert.Same(serviceRegister.GetService<IMyService>(), serviceRegister.GetService<IMyService>());
        }
    }

    [Fact]
    public void Register_InfoWithSingletonTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register(typeof(IMyService), LifetimeScope.Singleton.CreateRegistration<MyService>());

        // asserts
        Assert.NotNull(serviceRegister.GetService<IMyService>());
        Assert.Same(serviceRegister.GetService<IMyService>(), serviceRegister.GetService<IMyService>());
    }

    [Fact]
    public void RegisterLazy_InfoWithTransientTest()
    {
        var serviceRegister = this.GetProvider();

        var lifetime = LifetimeScope.Transient;

        // acts
        serviceRegister
            .RegisterLazy(typeof(IMyService), lifetime.CreateRegistration<MyService>())
            .RegisterLazy(typeof(IRootService), lifetime.CreateRegistration(provider => new RootService(provider.GetService<IMyService>())))
            .RegisterLazy(typeof(IMyServiceGen<>), lifetime.CreateRegistration(typeof(MyServiceGen<>)))
            ;

        // asserts
        Assert.NotNull(serviceRegister.GetService<IMyService>());
        Assert.NotSame(serviceRegister.GetService<IMyService>(), serviceRegister.GetService<IMyService>());

        Assert.NotNull(serviceRegister.GetService<IRootService>());
        Assert.NotSame(serviceRegister.GetService<IRootService>(), serviceRegister.GetService<IRootService>());

        Assert.NotNull(serviceRegister.GetService<IMyServiceGen<int>>());
        Assert.NotSame(serviceRegister.GetService<IMyServiceGen<int>>(), serviceRegister.GetService<IMyServiceGen<int>>());
    }

    [Fact]
    public void RegisterLazy_InfoGenWithTransientTest()
    {
        var serviceRegister = this.GetProvider();

        var lifetime = LifetimeScope.Transient;

        // acts
        serviceRegister
            .RegisterLazy<IMyService, MyService>(lifetime.CreateRegistration<MyService>())
            .RegisterLazy<IRootService>(lifetime.CreateRegistration(provider => new RootService(provider.GetService<IMyService>())))
            .RegisterLazy(typeof(IMyServiceGen<>), lifetime.CreateRegistration(typeof(MyServiceGen<>)))
            ;

        // asserts
        Assert.NotNull(serviceRegister.GetService<IMyService>());
        Assert.NotSame(serviceRegister.GetService<IMyService>(), serviceRegister.GetService<IMyService>());

        Assert.NotNull(serviceRegister.GetService<IRootService>());
        Assert.NotSame(serviceRegister.GetService<IRootService>(), serviceRegister.GetService<IRootService>());

        Assert.NotNull(serviceRegister.GetService<IMyServiceGen<int>>());
        Assert.NotSame(serviceRegister.GetService<IMyServiceGen<int>>(), serviceRegister.GetService<IMyServiceGen<int>>());
    }

    [Fact]
    public void RegisterLazy_InfoWithTransientV2Test()
    {
        var serviceRegister = this.GetProvider();

        var lifetime = LifetimeScope.Transient;

        // acts
        serviceRegister
            .RegisterLazy(typeof(IMyService), lifetime.CreateRegistration<MyService>())
            .RegisterLazy(typeof(IRootService), lifetime.CreateRegistration<RootService>())
            .RegisterLazy(typeof(IMyServiceGen<>), lifetime.CreateRegistration(typeof(MyServiceGen<>)))
            ;

        // asserts
        Assert.NotSame(serviceRegister.GetService<IMyService>(), serviceRegister.GetService<IMyService>());
        Assert.NotSame(serviceRegister.GetService<IRootService>(), serviceRegister.GetService<IRootService>());
        Assert.NotSame(serviceRegister.GetService<IMyServiceGen<int>>(), serviceRegister.GetService<IMyServiceGen<int>>());
        Assert.NotSame(serviceRegister.GetService<IMyServiceGen<double>>(), serviceRegister.GetService<IMyServiceGen<double>>());
    }

    [Fact]
    public void RegisterLazy_InfoWithSingletonTest()
    {
        var serviceRegister = this.GetProvider();

        var lifetime = LifetimeScope.Singleton;

        // acts
        serviceRegister
            .RegisterLazy(typeof(IMyService), lifetime.CreateRegistration<MyService>())
            .RegisterLazy(typeof(IRootService), lifetime.CreateRegistration<RootService>())
            .RegisterLazy(typeof(IMyServiceGen<>), lifetime.CreateRegistration(typeof(MyServiceGen<>)))
            ;

        // asserts
        Assert.Same(serviceRegister.GetService<IMyService>(), serviceRegister.GetService<IMyService>());
        Assert.Same(serviceRegister.GetService<IRootService>(), serviceRegister.GetService<IRootService>());
        Assert.Same(serviceRegister.GetService<IMyServiceGen<int>>(), serviceRegister.GetService<IMyServiceGen<int>>());
        Assert.Same(serviceRegister.GetService<IMyServiceGen<double>>(), serviceRegister.GetService<IMyServiceGen<double>>());
    }

    [Fact]
    public void RegisterLazy_InfoWithScopeTest()
    {
        var serviceRegister = this.GetProvider();

        var lifetime = LifetimeScope.Scoped;

        // acts
        serviceRegister
            .RegisterLazy(typeof(IMyService), lifetime.CreateRegistration<MyService>())
            .RegisterLazy(typeof(IRootService), lifetime.CreateRegistration<RootService>())
            .RegisterLazy(typeof(IMyServiceGen<>), lifetime.CreateRegistration(typeof(MyServiceGen<>)))
            ;

        using (serviceRegister.BeginScope())
        {
            // asserts
            Assert.Same(serviceRegister.GetService<IMyService>(), serviceRegister.GetService<IMyService>());
            Assert.Same(serviceRegister.GetService<IRootService>(), serviceRegister.GetService<IRootService>());
            Assert.Same(serviceRegister.GetService<IMyServiceGen<int>>(), serviceRegister.GetService<IMyServiceGen<int>>());
            Assert.Same(serviceRegister.GetService<IMyServiceGen<double>>(), serviceRegister.GetService<IMyServiceGen<double>>());
        }
    }

    [Fact]
    public void RegisterLazy_InfoWithScopeV2Test()
    {
        var serviceRegister = this.GetProvider();

        var lifetime = LifetimeScope.Scoped;

        // acts
        serviceRegister
            .RegisterLazy(typeof(IMyServiceGen<>), lifetime.CreateRegistration(typeof(MyServiceGen<>)))
            .RegisterLazy(typeof(IRootServiceGen<>), lifetime.CreateRegistration(typeof(RootServiceGen<>)))
            ;

        IRootServiceGen<string> svcRoot01, svcRoot02, svcRoot03, svcRoot04;

        using (serviceRegister.BeginScope())
        {
            svcRoot01 = serviceRegister.GetService<IRootServiceGen<string>>();
            svcRoot02 = serviceRegister.GetService<IRootServiceGen<string>>();

            // asserts
            Assert.Equal(svcRoot01, svcRoot02);
            Assert.Same(svcRoot01, svcRoot02);
        }

        using (serviceRegister.BeginScope())
        {
            svcRoot03 = serviceRegister.GetService<IRootServiceGen<string>>();
            svcRoot04 = serviceRegister.GetService<IRootServiceGen<string>>();

            // asserts
            Assert.Equal(svcRoot03, svcRoot04);
            Assert.Same(svcRoot03, svcRoot04);
        }

        Assert.NotSame(svcRoot01, svcRoot03);
        Assert.NotSame(svcRoot02, svcRoot04);
    }

    [Fact]
    public void RegisterLazy_InfoWithScopeV3Test()
    {
        var serviceRegister = this.GetProvider();

        var lifetime = LifetimeScope.Scoped;

        // acts
        serviceRegister
            .RegisterLazy(typeof(IMyServiceGen<>), lifetime.CreateRegistration(typeof(MyServiceGenDisposable<>)))
            .RegisterLazy(typeof(IRootServiceGen<>), lifetime.CreateRegistration(typeof(RootServiceGen<>)))
            ;

        IRootServiceGen<string> svcRoot01, svcRoot02, svcRoot03, svcRoot04;

        using (serviceRegister.BeginScope())
        {
            svcRoot01 = serviceRegister.GetService<IRootServiceGen<string>>();
            svcRoot02 = serviceRegister.GetService<IRootServiceGen<string>>();

            // asserts
            Assert.Equal(svcRoot01, svcRoot02);
            Assert.Same(svcRoot01, svcRoot02);
            Assert.Same(svcRoot01.Service, svcRoot02.Service);
        }

        Assert.Equal(1, ((IDisposableService)svcRoot01.Service).DisposeCounter);

        using (serviceRegister.BeginScope())
        {
            svcRoot03 = serviceRegister.GetService<IRootServiceGen<string>>();
            svcRoot04 = serviceRegister.GetService<IRootServiceGen<string>>();

            // asserts
            Assert.Equal(svcRoot03, svcRoot04);
            Assert.Same(svcRoot03, svcRoot04);
            Assert.Same(svcRoot03.Service, svcRoot04.Service);
        }

        Assert.Equal(1, ((IDisposableService)svcRoot03.Service).DisposeCounter);

        Assert.NotSame(svcRoot01, svcRoot03);
        Assert.NotSame(svcRoot02, svcRoot04);
    }

    [Fact]
    public void RegisterLazy_InfoAndResolverWithScopeTest()
    {
        var serviceRegister = this.GetProvider();

        var lifetime = LifetimeScope.Scoped;

        // acts
        serviceRegister
            .RegisterResolver(typeof(IRootServiceGen<>), typeof(RootServiceGen<>), lifetime)
            .RegisterLazy(typeof(IMyServiceGen<>), lifetime.CreateRegistration(typeof(MyServiceGenDisposable<>)))
            ;

        IRootServiceGen<string> svcRoot01, svcRoot02, svcRoot03, svcRoot04;

        using (serviceRegister.BeginScope())
        {
            svcRoot01 = serviceRegister.GetService<IRootServiceGen<string>>();
            svcRoot02 = serviceRegister.GetService<IRootServiceGen<string>>();

            // asserts
            Assert.Equal(svcRoot01, svcRoot02);
            Assert.Same(svcRoot01, svcRoot02);
            Assert.Same(svcRoot01.Service, svcRoot02.Service);
        }

        Assert.Equal(1, ((IDisposableService)svcRoot01.Service).DisposeCounter);

        using (serviceRegister.BeginScope())
        {
            svcRoot03 = serviceRegister.GetService<IRootServiceGen<string>>();
            svcRoot04 = serviceRegister.GetService<IRootServiceGen<string>>();

            // asserts
            Assert.Equal(svcRoot03, svcRoot04);
            Assert.Same(svcRoot03, svcRoot04);
            Assert.Same(svcRoot03.Service, svcRoot04.Service);
        }

        Assert.Equal(1, ((IDisposableService)svcRoot03.Service).DisposeCounter);

        Assert.NotSame(svcRoot01, svcRoot03);
        Assert.NotSame(svcRoot02, svcRoot04);
    }

    [Fact]
    public void Register_WithTransientTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register<IMyService, MyService>(LifetimeScope.Transient);
        
        // asserts
        Assert.NotNull(serviceRegister.GetService<IMyService>());
        Assert.NotSame(serviceRegister.GetService<IMyService>(), serviceRegister.GetService<IMyService>());
    }

    [Fact]
    public void Register_WithScopeTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register<IMyService, MyService>(LifetimeScope.Scoped);

        using (serviceRegister.BeginScope())
        {
            // asserts
            Assert.NotNull(serviceRegister.GetService<IMyService>());
            Assert.Same(serviceRegister.GetService<IMyService>(), serviceRegister.GetService<IMyService>());
        }
    }

    [Fact]
    public void Register_WithSingletonTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register<IMyService, MyService>(LifetimeScope.Singleton);

        // asserts
        Assert.NotNull(serviceRegister.GetService<IMyService>());
        Assert.Same(serviceRegister.GetService<IMyService>(), serviceRegister.GetService<IMyService>());
    }

    [Fact]
    public void Register_TypeWithTransientTest()
    {
        var serviceRegister = this.GetProvider();
        var serviceType = typeof(IMyService);

        // acts
        serviceRegister.Register(serviceType, typeof(MyService), LifetimeScope.Transient);

        // asserts
        Assert.NotNull(serviceRegister.GetService(serviceType));
        Assert.NotSame(serviceRegister.GetService(serviceType), serviceRegister.GetService(serviceType));
    }

    [Fact]
    public void Register_OpenGenericTypeWithTransientTest()
    {
        var serviceRegister = this.GetProvider();
        var serviceType = typeof(IMyServiceGen<long>);

        // acts
        serviceRegister.Register(typeof(IMyServiceGen<>), typeof(MyServiceGen<>), LifetimeScope.Transient);

        // asserts
        Assert.NotNull(serviceRegister.GetService(serviceType));
        Assert.NotSame(serviceRegister.GetService(serviceType), serviceRegister.GetService(serviceType));
    }

    [Fact]
    public void Register_OpenGenericTypeWithSingletonTest()
    {
        var serviceRegister = this.GetProvider();
        var serviceType = typeof(IMyServiceGen<long>);

        // acts
        serviceRegister.Register(typeof(IMyServiceGen<>), typeof(MyServiceGen<>), LifetimeScope.Singleton);

        // asserts
        Assert.NotNull(serviceRegister.GetService(serviceType));
        Assert.Same(serviceRegister.GetService(serviceType), serviceRegister.GetService(serviceType));
    }

    [Fact]
    public void Register_TypeFactory2WithTransientTest()
    {
        var serviceRegister = this.GetProvider();
        var serviceType = typeof(IMyService);

        // acts
        serviceRegister.Register(serviceType, () => new MyService(), LifetimeScope.Transient);

        // asserts
        Assert.NotNull(serviceRegister.GetService(serviceType));
        Assert.NotSame(serviceRegister.GetService(serviceType), serviceRegister.GetService(serviceType));
    }

    [Fact]
    public void Register_TypeFactory2WithSingletonTest()
    {
        var serviceRegister = this.GetProvider();
        var serviceType = typeof(IMyService);

        // acts
        serviceRegister.Register(serviceType, () => new MyService(), LifetimeScope.Singleton);

        // asserts
        Assert.NotNull(serviceRegister.GetService(serviceType));
        Assert.Same(serviceRegister.GetService(serviceType), serviceRegister.GetService(serviceType));
    }

    [Fact]
    public void Register_TypeFactoryWithTransientTest()
    {
        var serviceRegister = this.GetProvider();
        var serviceType = typeof(IMyService);
        
        // acts
        serviceRegister.Register(serviceType, _ => new MyService(), LifetimeScope.Transient);

        // asserts
        Assert.NotNull(serviceRegister.GetService(serviceType));
        Assert.NotSame(serviceRegister.GetService(serviceType), serviceRegister.GetService(serviceType));
    }

    [Fact]
    public void Register_GenericTypeFactoryWithTransientTest()
    {
        var serviceRegister = this.GetProvider();
        var serviceType = typeof(IMyServiceGen<long>);

        // acts
        serviceRegister.Register(serviceType, _ => new MyServiceGen<long>(), LifetimeScope.Transient);

        // asserts
        Assert.NotNull(serviceRegister.GetService(serviceType));
        Assert.NotSame(serviceRegister.GetService(serviceType), serviceRegister.GetService(serviceType));
    }

    [Fact]
    public void Register_TypeWithScopeTest()
    {
        var serviceRegister = this.GetProvider();
        var serviceType = typeof(IMyService);

        // acts
        serviceRegister.Register<IMyService, MyService>(LifetimeScope.Scoped);

        using (serviceRegister.BeginScope())
        {
            // asserts
            Assert.NotNull(serviceRegister.GetService(serviceType));
            Assert.Same(serviceRegister.GetService(serviceType), serviceRegister.GetService(serviceType));
        }
    }

    [Fact]
    public void Register_TypeWithSingletonTest()
    {
        var serviceRegister = this.GetProvider();
        var serviceType = typeof(IMyService);

        // acts
        serviceRegister.Register(serviceType, typeof(MyService), LifetimeScope.Singleton);

        // asserts
        Assert.NotNull(serviceRegister.GetService(serviceType));
        Assert.Same(serviceRegister.GetService(serviceType), serviceRegister.GetService(serviceType));
    }

    [Fact]
    public void Register_InfoFactoryWithTransientTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register(typeof(IMyService), LifetimeScope.Transient.CreateRegistration<IMyService>(_ => new MyService()));

        // asserts
        Assert.NotNull(serviceRegister.GetService<IMyService>());
        Assert.NotSame(serviceRegister.GetService<IMyService>(), serviceRegister.GetService<IMyService>());
    }

    [Fact]
    public void Register_InfoFactoryWithScopeTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register(typeof(IMyService), LifetimeScope.Scoped.CreateRegistration<IMyService>(_ => new MyService()));

        using (serviceRegister.BeginScope())
        {
            // asserts
            Assert.NotNull(serviceRegister.GetService<IMyService>());
            Assert.Same(serviceRegister.GetService<IMyService>(), serviceRegister.GetService<IMyService>());
        }
    }

    [Fact]
    public void Register_InfoFactoryWithSingletonTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register(typeof(IMyService), LifetimeScope.Singleton.CreateRegistration<IMyService>(_ => new MyService()));

        // asserts
        Assert.NotNull(serviceRegister.GetService<IMyService>());
        Assert.Same(serviceRegister.GetService<IMyService>(), serviceRegister.GetService<IMyService>());
    }

    [Fact]
    public void Register_InfoDisposableTransientTest()
    {
        var serviceRegister = this.GetProvider();

        // note: if DisposableTransientComponent is false, the behavior expected is wrong (for SimpleInjector), it would throw an exception.
        // instead for DryIoc the behavior is correct.

        // acts
        serviceRegister.Register(typeof(IMyServiceGen<>),
            LifetimeScope.Transient.CreateRegistration(typeof(MyServiceGenDisposable<>),
                new SuppressDiagnostic {DisposableTransientComponent = true}));

        // asserts
        Assert.NotNull(serviceRegister.GetService<IMyServiceGen<double>>());
    }

    [Fact]
    public void Register_InfoMismatchLifetimeTest()
    {
        var serviceRegister = this.GetProvider();

        // note: mismatch lifetime doesn't throw any exception in DryIoc
        // https://github.com/dadhi/DryIoc/issues/633

        // acts
        serviceRegister.Register<IRootService>(LifetimeScope.Singleton.CreateRegistration<RootServiceV2>())
            .Register(typeof(IMyService), LifetimeScope.Transient.CreateRegistration(typeof(MyService), new SuppressDiagnostic{ LifestyleMismatch = true }));

        // asserts
        Assert.NotNull(serviceRegister.GetService<IRootService>());
    }

    [Fact]
    public void Register_FactoryWithTransientTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register<IMyService>(() => new MyService(), LifetimeScope.Transient);

        // asserts
        Assert.NotNull(serviceRegister.GetService<IMyService>());
        Assert.NotSame(serviceRegister.GetService<IMyService>(), serviceRegister.GetService<IMyService>());
    }

    [Fact]
    public void Register_FactoryWithScopeTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register<IMyService>(() => new MyService(), LifetimeScope.Scoped);

        using (serviceRegister.BeginScope())
        {
            // asserts
            Assert.NotNull(serviceRegister.GetService<IMyService>());
            Assert.Same(serviceRegister.GetService<IMyService>(), serviceRegister.GetService<IMyService>());
        }
    }

    [Fact]
    public void Register_FactoryWithSingletonTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register<IMyService>(() => new MyService(), LifetimeScope.Singleton);

        // asserts
        Assert.NotNull(serviceRegister.GetService<IMyService>());
        Assert.Same(serviceRegister.GetService<IMyService>(), serviceRegister.GetService<IMyService>());
    }

    [Fact]
    public void Register_ConcreteTypeTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register(typeof(MyService));

        // asserts
        Assert.NotNull(serviceRegister.GetService<MyService>());
    }

    [Fact]
    public void Register_GenericConcreteTypeTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register<MyService>();

        // asserts
        Assert.NotNull(serviceRegister.GetService<MyService>());
    }

    [Fact]
    public void Register_FactoryConcreteTypeTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register(_ => new MyService());

        // asserts
        Assert.NotNull(serviceRegister.GetService<MyService>());
    }

    [Fact]
    public void Register_FactoryConcreteTypeAndServiceProviderTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister
            .Register<IMyService, MyServiceV2>()
            .Register(serviceProvider => new RootService(serviceProvider.GetService<IMyService>()));

        // asserts
        Assert.NotNull(serviceRegister.GetService<RootService>());
    }

    [Fact]
    public void Register_OpenGenericTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register(typeof(IMyServiceGen<>), typeof(MyServiceGen<>));

        // asserts
        Assert.NotNull(serviceRegister.GetService<IMyServiceGen<string>>());
        Assert.NotNull(serviceRegister.GetService<IMyServiceGen<long>>());
        Assert.NotNull(serviceRegister.GetService<IMyServiceGen<byte>>());
        Assert.NotNull(serviceRegister.GetService<IMyServiceGen<Exception>>());
    }

    [Fact]
    public void Register_InfoOpenGenericTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register(typeof(IMyServiceGen<>), LifetimeScope.Singleton.CreateRegistration(typeof(MyServiceGen<>)));

        // asserts
        Assert.NotNull(serviceRegister.GetService<IMyServiceGen<string>>());
        Assert.NotNull(serviceRegister.GetService<IMyServiceGen<long>>());
        Assert.NotNull(serviceRegister.GetService<IMyServiceGen<byte>>());
        Assert.NotNull(serviceRegister.GetService<IMyServiceGen<Exception>>());
    }

    [Fact]
    public void RegisterDecorator_Test()
    {
        var serviceRegister = this.GetProvider();

        serviceRegister.Register<ICustomSvc, CustomSvc>()
            .RegisterDecorator<ICustomSvc, CustomSvcDecoratorV1>()
            .RegisterDecorator<ICustomSvc, CustomSvcDecoratorV2>()
            .RegisterDecorator<ICustomSvc, CustomSvcDecoratorV3>();

        var svc03 = serviceRegister.GetService<ICustomSvc>() as ICustomSvcDecorator;
        var svc02 = svc03?.Decoratee as ICustomSvcDecorator;
        var svc01 = svc02?.Decoratee as ICustomSvcDecorator;
        var svc00 = svc01?.Decoratee;

        // asserts
        Assert.NotNull(svc03);
        Assert.NotNull(svc02);
        Assert.NotNull(svc01);
        Assert.NotNull(svc00);
        
        Assert.IsType<CustomSvcDecoratorV3>(svc03);
        Assert.IsType<CustomSvcDecoratorV2>(svc02);
        Assert.IsType<CustomSvcDecoratorV1>(svc01);
        Assert.IsType<CustomSvc>(svc00);
    }

    [Fact]
    public void RegisterDecorator_WithTypesTest()
    {
        var serviceRegister = this.GetProvider();

        serviceRegister.Register<ICustomSvc, CustomSvc>()
            .RegisterDecorator(typeof(ICustomSvc), typeof(CustomSvcDecoratorV1))
            .RegisterDecorator(typeof(ICustomSvc), typeof(CustomSvcDecoratorV2))
            .RegisterDecorator(typeof(ICustomSvc), typeof(CustomSvcDecoratorV3));

        var svc03 = serviceRegister.GetService<ICustomSvc>() as ICustomSvcDecorator;
        var svc02 = svc03?.Decoratee as ICustomSvcDecorator;
        var svc01 = svc02?.Decoratee as ICustomSvcDecorator;
        var svc00 = svc01?.Decoratee;

        // asserts
        Assert.NotNull(svc03);
        Assert.NotNull(svc02);
        Assert.NotNull(svc01);
        Assert.NotNull(svc00);

        Assert.IsType<CustomSvcDecoratorV3>(svc03);
        Assert.IsType<CustomSvcDecoratorV2>(svc02);
        Assert.IsType<CustomSvcDecoratorV1>(svc01);
        Assert.IsType<CustomSvc>(svc00);
    }

    [Fact]
    public void RegisterDecorator_WithoutBaseTest()
    {
        var serviceRegister = this.GetProvider();

        serviceRegister
            .RegisterDecorator<ICustomSvc, CustomSvcDecoratorV1>();

        Assert.Throws<ServiceUnavailableException>(() => serviceRegister.GetService<ICustomSvc>());
    }

    [Fact]
    public void RegisterDecorator_WithResolverTest()
    {
        var serviceRegister = this.GetProvider();

        serviceRegister.Register<ICustomSvc, CustomSvc>();

        serviceRegister
            .RegisterResolver<IRootServiceDec, RootServiceDec>()
            .RegisterDecorator<ICustomSvc, CustomSvcDecoratorV1>();

        var svc01 = serviceRegister.GetService<IRootServiceDec>();
    }

    [Fact]
    public void RegisterConditional_WithTransientTest()
    {
        var serviceRegister = this.GetProvider();
        var lifetime = LifetimeScope.Transient;

        // acts
        serviceRegister.Register<RootService>(lifetime)
            .Register<RootServiceV2>(lifetime);

        serviceRegister.RegisterConditional<IMyService>(register =>
            register
                .Register(
                    _ => new MyService(),
                    service => service.ConsumerType == typeof(RootService),
                    lifetime)
                .Register(
                    _ => new MyServiceV2(),
                    service => service.ConsumerType == typeof(RootServiceV2),
                    lifetime)
        );

        // asserts..
        Assert.IsType<MyService>(serviceRegister.GetService<RootService>().Service);
        Assert.IsType<MyServiceV2>(serviceRegister.GetService<RootServiceV2>().Service);
    }

    [Fact]
    public void RegisterConditional_WithScopeTest()
    {
        var serviceRegister = this.GetProvider();
        var lifetime = LifetimeScope.Scoped;

        // acts
        serviceRegister.Register<RootService>(lifetime)
            .Register<RootServiceV2>(lifetime);

        serviceRegister.RegisterConditional<IMyService>(register =>
            register
                .Register(
                    _ => new MyService(),
                    service => service.ConsumerType == typeof(RootService),
                    lifetime)
                .Register(
                    _ => new MyServiceV2(),
                    service => service.ConsumerType == typeof(RootServiceV2),
                    lifetime)
        );

        using (serviceRegister.BeginScope())
        {
            // asserts..
            Assert.IsType<MyService>(serviceRegister.GetService<RootService>().Service);
            Assert.IsType<MyServiceV2>(serviceRegister.GetService<RootServiceV2>().Service);
        }
    }

    [Fact]
    public void RegisterConditional_WithSingletonTest()
    {
        var serviceRegister = this.GetProvider();
        var lifetime = LifetimeScope.Singleton;

        // acts
        serviceRegister.Register<RootService>(lifetime)
            .Register<RootServiceV2>(lifetime);

        serviceRegister.RegisterConditional<IMyService>(register =>
            register
                .Register(
                    _ => new MyService(),
                    service => service.ConsumerType == typeof(RootService),
                    lifetime)
                .Register(
                    _ => new MyServiceV2(),
                    service => service.ConsumerType == typeof(RootServiceV2),
                    lifetime)
        );

        // asserts..
        Assert.IsType<MyService>(serviceRegister.GetService<RootService>().Service);
        Assert.IsType<MyServiceV2>(serviceRegister.GetService<RootServiceV2>().Service);
    }

    [Fact]
    public void RegisterConditional_WithSingletonTestV2()
    {
        var serviceRegister = this.GetProvider();
        var lifetime = LifetimeScope.Singleton;

        // acts
        serviceRegister.Register<IRootService, RootService>(lifetime)
            .Register<IOtherRootService, OtherRootService>(lifetime);

        serviceRegister.RegisterConditional<IMyService>(register =>
            register
                .Register(
                    _ => new MyService(),
                    service => service.ConsumerType == typeof(RootService),
                    lifetime)
                .Register(
                    _ => new MyServiceV2(),
                    service => service.ConsumerType == typeof(OtherRootService),
                    lifetime)
        );

        // asserts..
        Assert.IsType<MyService>(serviceRegister.GetService<IRootService>().Service);
        Assert.IsType<MyServiceV2>(serviceRegister.GetService<IOtherRootService>().Service);
    }

    [Fact]
    public void RegisterConditional_WithSingletonTestV3()
    {
        var serviceRegister = this.GetProvider();
        var lifetime = LifetimeScope.Singleton;

        // acts
        serviceRegister.Register<IRootService, RootService>(lifetime)
            .Register<IOtherRootService, OtherRootService>(lifetime);

        serviceRegister.RegisterConditional<IMyService>(register =>
            register
                .Register<MyService>(
                    service => service.ConsumerType == typeof(RootService),
                    lifetime)
                .Register<MyServiceV2>(
                    service => service.ConsumerType == typeof(OtherRootService),
                    lifetime)
        );

        // asserts..
        Assert.IsType<MyService>(serviceRegister.GetService<IRootService>().Service);
        Assert.IsType<MyServiceV2>(serviceRegister.GetService<IOtherRootService>().Service);
    }

    [Fact]
    public void RegisterConditional_WithMixLifetimesTest()
    {
        var serviceRegister = this.GetProvider();
        var lifetime = LifetimeScope.Transient;

        if (this.Type == ImplementorType.DryIoc)
        {
            // todo: an issue was opened, default registrations don't work, overrides the other with conditions (when default uses Singleton).
            // https://github.com/dadhi/DryIoc/discussions/630
            return;
        }

        // acts
        serviceRegister.Register<RootService>(lifetime)
        .Register<RootServiceV2>(lifetime)
        .Register<RootServiceV3>(LifetimeScope.Singleton);

        serviceRegister.RegisterConditional<IMyService>(register =>
            register
                .Register(
                    _ => new MyService(),
                    service => service.ConsumerType == typeof(RootService),
                    lifetime)
                .Register(
                    _ => new MyServiceV2(),
                    service => service.ConsumerType == typeof(RootServiceV2),
                    lifetime)
                .Default<MyServiceV3>(LifetimeScope.Singleton)
        );

        // asserts..
        Assert.IsType<MyService>(serviceRegister.GetService<RootService>().Service);
        Assert.IsType<MyServiceV2>(serviceRegister.GetService<RootServiceV2>().Service);
        Assert.IsType<MyServiceV3>(serviceRegister.GetService<RootServiceV3>().Service);
    }

    [Fact]
    public void RegisterConditional_TypeWithScopeTest()
    {
        var serviceRegister = this.GetProvider();
        var lifetime = LifetimeScope.Scoped;

        // acts
        serviceRegister.Register<RootService>(lifetime)
            .Register<RootServiceV2>(lifetime);

        serviceRegister.RegisterConditional(typeof(IMyService), register =>
            register
                .Register(
                    typeof(MyService),
                    service => service.ConsumerType == typeof(RootService),
                    lifetime)
                .Register(
                    typeof(MyServiceV2),
                    service => service.ConsumerType == typeof(RootServiceV2),
                     lifetime)
        );

        using (serviceRegister.BeginScope())
        {
            // asserts..
            Assert.IsType<MyService>(serviceRegister.GetService<RootService>().Service);
            Assert.IsType<MyServiceV2>(serviceRegister.GetService<RootServiceV2>().Service);
        }
    }

    [Fact]
    public void RegisterInitializer_Test()
    {
        var serviceRegister = this.GetProvider();

        serviceRegister.Register<IScheduler, Scheduler>()
            .Register(() => output, LifetimeScope.Singleton)
            .RegisterInitializer<IScheduler>((provider, sc) =>
            {
                sc.Scheduled = _ =>
                {
                    var writer = provider.GetService<ITestOutputHelper>();

                    writer.WriteLine("A message by event handler");
                };
            });

        var instance = serviceRegister.GetService<IScheduler>();

        if (instance is Scheduler temp)
        {
            Assert.NotNull(temp.Scheduled);
        }
    }

    [Fact]
    public void RegisterInitializer_DifferentServiceContractOnInitTest()
    {
        var serviceRegister = this.GetProvider();

        if (this.Type == ImplementorType.DryIoc)
        {
            //// It doesn't work with DryIoc
            //// https://github.com/dadhi/DryIoc/blob/master/docs/DryIoc.Docs/RegisterResolve.md#registerinitializer
            //// TODO: Initializer works with the same service contract which was registered with.
            return;
        }

        serviceRegister.Register<IRunnable, Scheduler>()
            .Register(() => output, LifetimeScope.Singleton)
            .RegisterInitializer<IScheduler>((provider, sc) =>
            {
                sc.Scheduled = _ =>
                {
                    var writer = provider.GetService<ITestOutputHelper>();

                    writer.WriteLine("A message by event handler");
                };
            });

        var instance = serviceRegister.GetService<IRunnable>();

        if (instance is Scheduler temp)
        {
            Assert.NotNull(temp.Scheduled);
        }
    }

    [Fact]
    public void RegisterDerivedServices_Test()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.RegisterDerivedServices(typeof(IMyService), Assembly.GetExecutingAssembly());

        // asserts
        Assert.NotNull(serviceRegister.GetService<MyService>());
        Assert.NotNull(serviceRegister.GetService<MyServiceV2>());
        Assert.NotNull(serviceRegister.GetService<MyServiceV3>());
    }

    [Fact]
    public void RegisterDerivedServices_GenericTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.RegisterDerivedServices(typeof(IGenericContract<>), Assembly.GetExecutingAssembly());

        // asserts
        Assert.NotNull(serviceRegister.GetService<GenericContractInt>());
        Assert.NotNull(serviceRegister.GetService<GenericContractString>());
        Assert.NotNull(serviceRegister.GetService<GenericContract<string>>());
        Assert.NotNull(serviceRegister.GetService<GenericContract<StringBuilder>>());
    }

    [Fact]
    public void RegisterMany_FromAssembly()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.RegisterMany<IMyService>(this.GetType().Assembly);

        var services = serviceRegister.GetServices<IMyService>().ToList();

        // asserts
        Assert.NotNull(services);
        Assert.True(services.Any());
    }

    [Theory]
    [InlineData(LifetimeScope.Transient)]
    [InlineData(LifetimeScope.Scoped)]
    [InlineData(LifetimeScope.Singleton)]
    public void RegisterMany_FromAssembly_ByRoot(LifetimeScope lifetime)
    {
        var serviceRegister = this.GetProvider(lifetime);

        // acts
        serviceRegister.RegisterMany<IMyService>(this.GetType().Assembly)
            .Register<IRootServiceMany, RootServiceMany>();

        using (lifetime == LifetimeScope.Scoped ? serviceRegister.BeginScope() : null)
        {
            var instance = serviceRegister.GetService<IRootServiceMany>();

            // asserts
            Assert.NotNull(instance);
            Assert.NotEmpty(instance.Services);
        }
    }

    [Theory]
    [InlineData(LifetimeScope.Transient)]
    [InlineData(LifetimeScope.Scoped)]
    [InlineData(LifetimeScope.Singleton)]
    public void RegisterManyCollection_FromAssembly_ByRoot(LifetimeScope lifetime)
    {
        var serviceRegister = this.GetProvider(lifetime);

        // acts
        serviceRegister.RegisterMany<IMyService>(this.GetType().Assembly)
            .Register<IRootServiceMany, RootServiceManyV2>();

        using (lifetime == LifetimeScope.Scoped ? serviceRegister.BeginScope() : null)
        {
            var instance = serviceRegister.GetService<IRootServiceMany>();

            // asserts
            Assert.NotNull(instance);
            Assert.NotEmpty(instance.Services);
        }
    }

    [Fact]
    public void RegisterMany_FromAssemblyWithSingleton()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.RegisterMany<IMyService>([this.GetType().Assembly], LifetimeScope.Singleton);

        var manySvc01 = serviceRegister.GetServices<IMyService>().ToList();
        var manySvc02 = serviceRegister.GetServices<IMyService>().ToList();

        // asserts
        Assert.NotNull(manySvc01);
        Assert.NotEmpty(manySvc01);
        Assert.NotEmpty(manySvc02);

        foreach (var svc in manySvc01)
        {
            Assert.Contains(svc, manySvc02);
        }
    }

    [Fact]
    public void RegisterMany_FromAssemblyUsingServiceType()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.RegisterMany(typeof(IMyService), this.GetType().Assembly);

        var services = serviceRegister.GetServices<IMyService>().ToList();

        // asserts
        Assert.NotNull(services);
        Assert.NotEmpty(services);
    }

    [Fact]
    public void RegisterMany_FromAssemblyUsingServiceTypeWithSingleton()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.RegisterMany(typeof(IMyService), [this.GetType().Assembly], LifetimeScope.Singleton);

        var srvs1 = serviceRegister.GetServices<IMyService>().ToList();
        var srvs2 = serviceRegister.GetServices<IMyService>().ToList();

        // asserts
        Assert.NotNull(srvs1);
        Assert.NotEmpty(srvs1);
        Assert.NotEmpty(srvs2);

        foreach (var srvs in srvs1)
        {
            Assert.Contains(srvs, srvs2);
        }
    }

    [Fact]
    public void RegisterMany_FromServiceImplementationList()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.RegisterMany<IMyService>(new MyService(), new MyServiceV2());
        var services = serviceRegister.GetServices<IMyService>().ToList();

        // asserts
        Assert.NotNull(services);
        Assert.NotEmpty(services);
    }

    [Fact]
    public void RegisterMany_FromServiceTypeList()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.RegisterMany<IMyService>(typeof(MyService), typeof(MyServiceV2));
        var services = serviceRegister.GetServices<IMyService>().ToList();

        // asserts
        Assert.NotNull(services);
        Assert.NotEmpty(services);
    }

    [Fact]
    public void RegisterManyLazy_OpenCloseGenTest()
    {
        var serviceRegister = this.GetProvider();
        
        serviceRegister.RegisterManyLazy(typeof(IMyServiceGen<decimal>),
        [
            LifetimeScope.Singleton.CreateRegistration(typeof(MyServiceGen<decimal>)),
            LifetimeScope.Transient.CreateRegistration(typeof(MyServiceGenV2<decimal>))
        ]);

        serviceRegister.RegisterManyLazy(typeof(IMyServiceGen<>),
        [
            LifetimeScope.Transient.CreateRegistration(typeof(MyServiceGenV2<>)),
            LifetimeScope.Singleton.CreateRegistration(typeof(MyServiceGen<>))
        ]);
        
        // asserts
        var svc1 = serviceRegister.GetServices<IMyServiceGen<int>>()
            .ToList();

        Assert.NotNull(svc1);

        var svc2 = serviceRegister.GetServices<IMyServiceGen<string>>()
            .ToList();

        Assert.NotNull(svc2);

        var svc3 = serviceRegister.GetServices<IMyServiceGen<decimal>>()
            .ToList();

        Assert.NotNull(svc3);
    }

    [Fact]
    public void RegisterResolver_Test()
    {
        var serviceRegister = this.GetProvider();

        serviceRegister.RegisterResolver<IRootService, RootService>()
            .Register<IMyService, MyService>();

        serviceRegister.RegisterResolver<IOtherRootService, OtherRootService>()
            .Register<IMyService, MyServiceV2>();

        var rootSvc = serviceRegister.GetService<IRootService>();
        var otherRootSvc = serviceRegister.GetService<IOtherRootService>();

        Assert.IsType<MyService>(rootSvc.Service);
        Assert.IsType<MyServiceV2>(otherRootSvc.Service);
    }

    [Fact]
    public void RegisterResolver_SingletonTest()
    {
        var serviceRegister = this.GetProvider();

        serviceRegister.RegisterResolver<IRootService, RootService>(LifetimeScope.Singleton)
            .Register<IMyService, MyService>(LifetimeScope.Singleton);

        serviceRegister.RegisterResolver<IOtherRootService, OtherRootService>(LifetimeScope.Singleton)
            .Register<IMyService, MyServiceV2>(LifetimeScope.Singleton);

        var rootSvc = serviceRegister.GetService<IRootService>();
        var otherRootSvc = serviceRegister.GetService<IOtherRootService>();

        Assert.IsType<MyService>(rootSvc.Service);
        Assert.IsType<MyServiceV2>(otherRootSvc.Service);
        Assert.Same(rootSvc, serviceRegister.GetService<IRootService>());
        Assert.Same(otherRootSvc, serviceRegister.GetService<IOtherRootService>());
    }

    [Fact]
    public void RegisterResolver_SingletonV2Test()
    {
        var serviceRegister = this.GetProvider();

        serviceRegister.Register<IMyService, MyServiceV3>(LifetimeScope.Singleton);

        serviceRegister.RegisterResolver<IRootService, RootService>(LifetimeScope.Singleton)
            .Register<IMyService, MyService>(LifetimeScope.Singleton);

        serviceRegister.RegisterResolver<IOtherRootService, OtherRootService>(LifetimeScope.Singleton)
            .Register<IMyService, MyServiceV2>(LifetimeScope.Singleton);

        var myService = serviceRegister.GetService<IMyService>();
        var rootSvc = serviceRegister.GetService<IRootService>();
        var otherRootSvc = serviceRegister.GetService<IOtherRootService>();

        Assert.IsType<MyServiceV3>(myService);
        Assert.IsType<MyService>(rootSvc.Service);
        Assert.IsType<MyServiceV2>(otherRootSvc.Service);
        Assert.Same(rootSvc, serviceRegister.GetService<IRootService>());
        Assert.Same(otherRootSvc, serviceRegister.GetService<IOtherRootService>());
    }

    [Fact]
    public void RegisterResolver_SingletonV3Test()
    {
        var serviceRegister = this.GetProvider();

        serviceRegister.Register<IMyService, MyServiceV3>(LifetimeScope.Singleton);

        serviceRegister.RegisterResolver<IRootService, RootService>(LifetimeScope.Singleton);
            
        var myService = serviceRegister.GetService<IMyService>();
        var rootSvc = serviceRegister.GetService<IRootService>();
        
        Assert.IsType<MyServiceV3>(myService);
        Assert.Same(rootSvc, serviceRegister.GetService<IRootService>());
    }

    [Fact]
    public void RegisterResolver_ScopeTest()
    {
        var serviceRegister = this.GetProvider();

        serviceRegister.Register<IMyService, MyServiceV3>(LifetimeScope.Scoped);

        serviceRegister.RegisterResolver<IRootService, RootService>(LifetimeScope.Scoped);

        using (serviceRegister.BeginScope())
        {
            var myService = serviceRegister.GetService<IMyService>();
            var rootSvc = serviceRegister.GetService<IRootService>();

            Assert.IsType<MyServiceV3>(myService);
            Assert.Same(rootSvc, serviceRegister.GetService<IRootService>());
        }
    }

    [Fact]
    public void RegisterResolver_ScopeV2Test()
    {
        var serviceRegister = this.GetProvider();

        serviceRegister.RegisterResolver<IRootService, RootService>(LifetimeScope.Scoped)
            .RegisterResolver<IMyService, MyServiceV3>(LifetimeScope.Scoped);
        
        using (serviceRegister.BeginScope())
        {
            var rootSvc = serviceRegister.GetService<IRootService>();

            Assert.IsType<MyServiceV3>(rootSvc.Service);
            Assert.Same(rootSvc, serviceRegister.GetService<IRootService>());
        }
    }

    [Fact]
    public void RegisterResolver_SingletonGenTest()
    {
        var serviceRegister = this.GetProvider(LifetimeScope.Singleton);

        serviceRegister.RegisterResolver(typeof(RootServiceGen<>))
            .Register(typeof(IMyServiceGen<>), typeof(MyServiceGen<>));

        serviceRegister.RegisterResolver(typeof(RootServiceGenV2<>))
            .Register(typeof(IMyServiceGen<>), typeof(MyServiceGenDisposable<>));

        var svcRoot01 = serviceRegister.GetService<RootServiceGen<int>>();
        var svcRoot01_01 = serviceRegister.GetService<RootServiceGen<int>>();

        var svcRoot02 = serviceRegister.GetService<RootServiceGenV2<int>>();

        Assert.Same(svcRoot01, svcRoot01_01);

        Assert.IsType<MyServiceGen<int>>(svcRoot01.Service);
        Assert.IsType<MyServiceGenDisposable<int>>(svcRoot02.Service);
    }

    [Fact]
    public void RegisterResolver_ScopedGenTest()
    {
        var serviceRegister = this.GetProvider(LifetimeScope.Scoped);

        serviceRegister.RegisterResolver(typeof(RootServiceGen<>))
            .Register(typeof(IMyServiceGen<>), typeof(MyServiceGen<>));

        serviceRegister.RegisterResolver(typeof(RootServiceGenV2<>))
            .Register(typeof(IMyServiceGen<>), typeof(MyServiceGenDisposable<>));


        RootServiceGen<int> svcRoot01;
        RootServiceGenV2<int> svcRoot02, svcRoot02_01;

        using (serviceRegister.BeginScope())
        {
            svcRoot01 = serviceRegister.GetService<RootServiceGen<int>>();
            svcRoot02 = serviceRegister.GetService<RootServiceGenV2<int>>();
            svcRoot02_01 = serviceRegister.GetService<RootServiceGenV2<int>>();

            Assert.IsType<MyServiceGen<int>>(svcRoot01.Service);
            Assert.IsType<MyServiceGenDisposable<int>>(svcRoot02.Service);

            Assert.Equal(svcRoot02, svcRoot02_01);
            Assert.Same(svcRoot02, svcRoot02_01);
        }

        Assert.Equal(1, ((MyServiceGenDisposable<int>)svcRoot02.Service).DisposeCounter);
        Assert.Equal(1, ((MyServiceGenDisposable<int>)svcRoot02_01.Service).DisposeCounter);
    }

    [Fact]
    public void RegisterGeneric_Test()
    {
        var serviceRegister = this.GetProvider();

        serviceRegister.RegisterGeneric(typeof(IGenericContract<>), [Assembly.GetExecutingAssembly()])
            .RegisterGeneric(typeof(IMyServiceGen<>), [this.GetType().Assembly]);

        // asserts
        Assert.NotNull(serviceRegister.GetService<IGenericContract<string>>());
        Assert.NotNull(serviceRegister.GetService<IGenericContract<int>>());
        Assert.Throws<ServiceUnavailableException>(() => serviceRegister.GetService<GenericContractInt>());
        Assert.Throws<ServiceUnavailableException>(() => serviceRegister.GetService<GenericContractString>());
        Assert.Throws<ServiceUnavailableException>(() => serviceRegister.GetService<GenericContract<string>>());

        // todo. verify the following types, maybe could be removed quietly, it might be enough the above types.
        Assert.NotNull(serviceRegister.GetService<IMyServiceGen<int>>());
        Assert.NotNull(serviceRegister.GetService<IMyServiceGen<string>>());
        Assert.Throws<ServiceUnavailableException>(() => serviceRegister.GetService<IMyServiceGen<long>>());
    }

    [Fact]
    public void Register_ServiceModuleTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register(new DefaultServiceModule());

        // asserts
        Assert.NotNull(serviceRegister.GetService<IRootService>());
        Assert.NotNull(serviceRegister.GetService<IMyService>());
    }

    [Fact]
    public void Register_DuplicatesTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register<IRootService, RootService>();

        // asserts
        Assert.Throws<ServiceRegistrationException>(() => serviceRegister.Register<IRootService, RootServiceV2>());
    }

    [Fact]
    public void Register_WrongServicesTest()
    {
        var serviceRegister = this.GetProvider();

        // asserts
        Assert.Throws<ServiceRegistrationException>(() => serviceRegister.Register<IRootService>());
    }

    [Fact]
    public void Register_ModulesFromAssemblyTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.RegisterModulesFrom(Assembly.GetExecutingAssembly(), true);

        // asserts
        Assert.NotNull(serviceRegister.GetService<IRootService>());
        Assert.NotNull(serviceRegister.GetService<IMyService>());
    }

    [Fact]
    public void BuildChildProvider_Test()
    {
        var serviceRegister = this.GetProvider();

        // todo
    }

    [Fact]
    public void IsRegistered_Test()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register<IMyService, MyService>()
            .Register(typeof(MyServiceV2), LifetimeScope.Singleton.CreateRegistration(typeof(MyServiceV2)))
            .Register(typeof(IMyServiceGen<>), typeof(MyServiceGen<>))
            .RegisterMany<IRootService>(typeof(RootServiceV2), typeof(RootServiceV3));

        // asserts
        Assert.True(serviceRegister.IsRegistered<IMyService>());
        Assert.True(serviceRegister.IsRegistered<MyServiceV2>());

        // open generic
        Assert.True(serviceRegister.IsRegistered(typeof(IMyServiceGen<>)));
    }

    [Fact]
    public void IsRegistered_LazyTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister
            .RegisterLazy(typeof(IMyService), LifetimeScope.Transient.CreateRegistration(typeof(MyService)))
            .RegisterLazy(typeof(IMyServiceGen<>), LifetimeScope.Transient.CreateRegistration(typeof(MyServiceGen<>)))
            ;

        // asserts
        Assert.True(serviceRegister.IsRegistered<IMyService>());
        Assert.True(serviceRegister.IsRegistered(typeof(IMyServiceGen<>)));
    }

    [Fact]
    public void IsRegistered_ManyTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register<IMyService, MyService>()
            .RegisterMany<IRootService>(typeof(RootServiceV2), typeof(RootServiceV3))
            .Register(typeof(IMyServiceGen<>), typeof(MyServiceGen<>))
            .RegisterMany(typeof(IRootServiceGen<>), [typeof(RootServiceGen<>), typeof(RootServiceGenV2<>)]);

        // asserts
        Assert.True(serviceRegister.IsRegistered<IRootService>(true));
        Assert.True(serviceRegister.IsRegistered(typeof(IRootServiceGen<>),true));
    }

    [Fact]
    public void IsRegistered_ManyLazyTest()
    {
        var serviceRegister = this.GetProvider();

        // acts
        serviceRegister.Register<IMyService, MyService>()
            .RegisterManyLazy<IRootService>([
                LifetimeScope.Transient.CreateRegistration<RootServiceV2>(),
                LifetimeScope.Transient.CreateRegistration<RootServiceV3>()
            ])
            .Register(typeof(IMyServiceGen<>), typeof(MyServiceGen<>))
            .RegisterManyLazy(typeof(IRootServiceGen<>), [
                LifetimeScope.Transient.CreateRegistration(typeof(RootServiceGen<>)),
                LifetimeScope.Transient.CreateRegistration(typeof(RootServiceGenV2<>))
            ]);

        // asserts
        Assert.True(serviceRegister.IsRegistered<IRootService>(true));
        Assert.True(serviceRegister.IsRegistered(typeof(IRootServiceGen<>), true));
    }

    [Fact]
    public void BeginScopeTest()
    {
        var serviceProvider = this.GetProvider(LifetimeScope.Scoped);

        serviceProvider
            .Register<IDisposableService, DisposableService>();

        IDisposableService svc01, svc02;

        using (serviceProvider.BeginScope())
        {
            svc01 = serviceProvider.GetService<IDisposableService>();
            svc02 = serviceProvider.GetService<IDisposableService>();

            Assert.Equal(svc01, svc02);
            Assert.Same(svc01, svc02);
        }

        Assert.Equal(1, svc01.DisposeCounter);
        Assert.Equal(1, svc02.DisposeCounter);
    }

    [Fact]
    public void RegisterWithInnerScopeTest()
    {
        var serviceProvider = this.GetProvider(LifetimeScope.Singleton);

        serviceProvider.Register<IDisposableService, DisposableService>(LifetimeScope.Scoped);

        IDisposableService svc01, svc02, svc01_inner1, svc02_inner1;

        using (serviceProvider.BeginScope())
        {
            svc01 = serviceProvider.GetService<IDisposableService>();
            svc02 = serviceProvider.GetService<IDisposableService>();

            Assert.Same(svc01, svc02);

            using (serviceProvider.BeginScope())
            {
                svc01_inner1 = serviceProvider.GetService<IDisposableService>();
                svc02_inner1 = serviceProvider.GetService<IDisposableService>();

                Assert.Same(svc01_inner1, svc02_inner1);

                // they're registered in scope context, but they're opened in different scopes.
                Assert.NotSame(svc01, svc01_inner1);
                // same 'cause they were registered as a singletons.
                Assert.NotSame(svc02, svc02_inner1);
            }

            Assert.Equal(1, svc01_inner1.DisposeCounter);
            Assert.Equal(1, svc02_inner1.DisposeCounter);
        }

        Assert.Equal(1, svc01.DisposeCounter);
        Assert.Equal(1, svc02.DisposeCounter);
        Assert.Equal(1, svc01_inner1.DisposeCounter);
        Assert.Equal(1, svc02_inner1.DisposeCounter);
    }
}
