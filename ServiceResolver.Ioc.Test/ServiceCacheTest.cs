using System;
using System.Text;
using ServiceResolver.Ioc.Caching;
using Xunit;

namespace ServiceResolver.Ioc.Test;

public class ServiceCacheTest
{
    private readonly IServiceCache serviceCache = new ServiceCache();

    [Fact]
    public void Get_Test()
    {
        // asserts
        Assert.Throws<ArgumentException>(() => this.serviceCache.Get(new CacheKey()));
        Assert.Throws<NullReferenceException>(() => this.serviceCache.Get(new CacheKey{ Type = typeof(string)}));

        // acts
        this.serviceCache.TryAdd(new StringBuilder());
        this.serviceCache.TryAdd(new StringBuilder("ciaone"), "ciaone");

        Assert.NotNull(this.serviceCache.Get(new CacheKey{ Type = typeof(StringBuilder) }));
        Assert.NotNull(this.serviceCache.Get(new CacheKey{ Key = "ciaone", Type = typeof(StringBuilder) }));
    }

    [Fact]
    public void Get_Generic_Test()
    {
        // asserts
        Assert.Throws<NullReferenceException>(() => this.serviceCache.Get<string>());
        Assert.Throws<NullReferenceException>(() => this.serviceCache.Get<string>("any_key"));

        // acts
        this.serviceCache.TryAdd(new StringBuilder());
        this.serviceCache.TryAdd(new StringBuilder("ciaone"), "ciaone");

        Assert.NotNull(this.serviceCache.Get<StringBuilder>(string.Empty));
        Assert.NotNull(this.serviceCache.Get<StringBuilder>("ciaone"));
    }

    [Fact]
    public void TryGet_Test()
    {
        // asserts
        Assert.Throws<ArgumentException>(() => this.serviceCache.TryGet(new CacheKey(), out var _));
        Assert.False(this.serviceCache.TryGet(new CacheKey { Type = typeof(string) }, out var _));

        // acts
        this.serviceCache.TryAdd(new StringBuilder());
        this.serviceCache.TryAdd(new StringBuilder("ciaone"), "ciaone");

        Assert.True(this.serviceCache.TryGet(new CacheKey { Type = typeof(StringBuilder) }, out var obj1));
        Assert.NotNull(obj1);

        Assert.True(this.serviceCache.TryGet(new CacheKey { Key = "ciaone", Type = typeof(StringBuilder) }, out var obj2));
        Assert.NotNull(obj2);
    }

    [Fact]
    public void TryAdd_Test()
    {
        // asserts
        Assert.Throws<ArgumentNullException>(() => this.serviceCache.TryAdd(null));
        Assert.True(this.serviceCache.TryAdd(new StringBuilder()));
        Assert.True(this.serviceCache.TryAdd(new StringBuilder("ciaone"), "ciaone"));

        Assert.True(this.serviceCache.TryGet(new CacheKey { Type = typeof(StringBuilder) }, out var obj1));
        Assert.NotNull(obj1);

        Assert.True(this.serviceCache.TryGet(new CacheKey { Key = "ciaone", Type = typeof(StringBuilder) }, out var obj2));
        Assert.NotNull(obj2);
    }

    [Fact]
    public void Upsert_Test()
    {
        // asserts
        Assert.Throws<ArgumentNullException>(() => this.serviceCache.Upsert(null));

        // acts
        var instance1 = new StringBuilder();
        var instance2 = new StringBuilder("ciaone value");

        this.serviceCache.Upsert(instance1);
        Assert.Same(instance1, this.serviceCache.Get<StringBuilder>());

        this.serviceCache.Upsert(instance2);
        Assert.Same(instance2, this.serviceCache.Get<StringBuilder>());
        Assert.NotSame(instance1, this.serviceCache.Get<StringBuilder>());

        this.serviceCache.Upsert(new StringBuilder("ciaone value 2"), "ciaone");

        Assert.True(this.serviceCache.TryGet(new CacheKey { Type = typeof(StringBuilder) }, out var obj1));
        Assert.NotNull(obj1);

        Assert.True(this.serviceCache.TryGet(new CacheKey { Key = "ciaone", Type = typeof(StringBuilder) }, out var obj2));
        Assert.NotNull(obj2);
    }

    [Fact]
    public void Remove_Test()
    {
        // asserts
        Assert.Throws<ArgumentNullException>(() => this.serviceCache.Remove(null));
        Assert.Throws<ArgumentException>(() => this.serviceCache.Remove(new CacheKey()));
        Assert.False(this.serviceCache.Remove(new CacheKey{ Type = typeof(StringBuilder)}));

        // acts
        this.serviceCache.TryAdd(new StringBuilder());

        // asserts
        Assert.True(this.serviceCache.Remove(new CacheKey { Type = typeof(StringBuilder) }));
    }
}
