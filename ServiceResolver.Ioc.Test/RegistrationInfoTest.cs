using System;
using System.Collections.Generic;
using System.Text;
using ServiceResolver.Ioc.Caching;
using ServiceResolver.Ioc.Extensions;
using ServiceResolver.Ioc.Test.Services;
using Xunit;

namespace ServiceResolver.Ioc.Test;

public class RegistrationInfoTest
{
    [Fact]
    public void CreateRegistrationWithConcreteTypeGenericTest()
    {
        var registration = LifetimeScope.Singleton.CreateRegistration<StringBuilder>();

        Assert.NotNull(registration);
        Assert.NotNull(registration.ConcreteType);
        Assert.Null(registration.Factory);
    }

    [Fact]
    public void CreateRegistrationWithConcreteTypeAsArgumentTest()
    {
        var registration = LifetimeScope.Singleton.CreateRegistration(typeof(StringBuilder));

        Assert.NotNull(registration);
        Assert.NotNull(registration.ConcreteType);
        Assert.Null(registration.Factory);
    }

    [Fact]
    public void CreateRegistrationWithFactoryTest()
    {
        var registration = LifetimeScope.Singleton.CreateRegistration(_ => new StringBuilder("demo"));
        Assert.NotNull(registration);
        Assert.NotNull(registration.Factory);
        Assert.Null(registration.ConcreteType);
    }

    [Theory]
    [InlineData(typeof(string))]
    public void ValidateFromFactoryWithWrongServiceTypeTest(Type serviceType)
    {
        var registration = LifetimeScope.Singleton.CreateRegistration(_ => new MyService());

        Assert.NotNull(registration);

        Assert.Throws<InvalidOperationException>(() => registration.Validate(serviceType));
    }

    [Theory]
    [InlineData(typeof(IMyServiceGen<>), typeof(MyServiceGen<int>))]
    [InlineData(typeof(string), typeof(MyService))]
    public void ValidateFromTypeWithWrongServiceTypeTest(Type serviceType, Type concreteType)
    {
        var registration = LifetimeScope.Singleton.CreateRegistration(concreteType);

        Assert.NotNull(registration);
        Assert.NotNull(registration.ConcreteType);
        Assert.Null(registration.Factory);

        Assert.Throws<InvalidOperationException>(() => registration.Validate(serviceType));
    }

    [Theory]
    [InlineData(typeof(IDisposableService))]
    [InlineData(typeof(IDisposable))]
    public void ValidateFromGenericTypeTest(Type serviceType)
    {
        var registration = LifetimeScope.Singleton.CreateRegistration<DisposableService>();

        Assert.NotNull(registration);

        registration.Validate(serviceType);
    }

    [Theory]
    [InlineData(typeof(IMyServiceGen<>), typeof(MyServiceGen<>))]
    [InlineData(typeof(IMyServiceGen<string>), typeof(MyServiceGen<string>))]
    [InlineData(typeof(IMyServiceGen<>), typeof(MyServiceGenDisposable<>))]
    [InlineData(typeof(IMyServiceGen<long>), typeof(MyServiceGenDisposable<long>))]
    [InlineData(typeof(IMyService), typeof(MyService))]
    [InlineData(typeof(IDisposableService), typeof(DisposableService))]
    [InlineData(typeof(IDisposable), typeof(DisposableService))]
    public void ValidateTest(Type serviceType, Type concreteType)
    {
        var registration = LifetimeScope.Singleton.CreateRegistration(concreteType);

        Assert.NotNull(registration);
        Assert.NotNull(registration.ConcreteType);
        Assert.Null(registration.Factory);

        registration.Validate(serviceType);
    }
}
