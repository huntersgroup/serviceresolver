namespace ServiceResolver.Ioc.Test.Services;

public class OtherRootService(IMyService myService) : IOtherRootService
{
    public IMyService Service { get; } = myService;
}

public interface IOtherRootService
{
    IMyService Service { get; }
}
