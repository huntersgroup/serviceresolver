namespace ServiceResolver.Ioc.Test.Services;

public class MyService : IMyService
{
}

public class MyServiceV2 : IMyService
{
}

public class MyServiceV3 : IMyService
{
}

public interface IMyService
{
}
