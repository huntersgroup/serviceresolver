namespace ServiceResolver.Ioc.Test.Services;

public interface IMyServiceGen<out TValue>
{
    TValue CurrentValue { get; }
}
