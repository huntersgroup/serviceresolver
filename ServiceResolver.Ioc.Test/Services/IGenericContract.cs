using System;

namespace ServiceResolver.Ioc.Test.Services;

public interface IGenericContractBase
{
    void Stop();
}

public interface IGenericContract : IGenericContractBase
{
    void Run();
}

public class GenericContractV1 : IGenericContract
{
    public void Run()
    {
        throw new NotImplementedException();
    }

    public void Stop()
    {
        throw new NotImplementedException();
    }
}

public class GenericContractV2 : IGenericContract
{
    public void Run()
    {
        throw new NotImplementedException();
    }

    public void Stop()
    {
        throw new NotImplementedException();
    }
}

public class GenericContractV3 : GenericContractV2
{
    public new void Stop()
    {
        throw new NotImplementedException();
    }
}

public interface IGenericContractBase<in TArg>
{
    void Stop(TArg arg);
}

public interface IGenericContract<in TArg> : IGenericContractBase<TArg>
{
    void Run(TArg arg);
}

public class GenericContract<TArg> : IGenericContract<TArg>
{
    public void Run(TArg arg)
    {
        throw new NotImplementedException();
    }

    public void Stop(TArg arg)
    {
        throw new NotImplementedException();
    }
}

public class GenericContractInt : IGenericContract<int>
{
    public void Run(int arg)
    {
        throw new NotImplementedException();
    }

    public void Stop(int arg)
    {
        throw new NotImplementedException();
    }
}

public class GenericContractString : IGenericContract<string>
{
    public void Run(string arg)
    {
        throw new NotImplementedException();
    }

    public void Stop(string arg)
    {
        throw new NotImplementedException();
    }
}
