using System;

namespace ServiceResolver.Ioc.Test.Services;

public class DisposableService : IDisposableService
{
    public void Dispose()
    {
        this.DisposeCounter++;
    }

    public int DisposeCounter { get; private set; }
}

public class DisposableServiceV2 : DisposableService
{
}

public class DisposableServiceV3 : DisposableService
{
}

public interface IDisposableService : IDisposable
{
    int DisposeCounter { get; }
}
