namespace ServiceResolver.Ioc.Test.Services;

public class RootServiceGen<TData>(IMyServiceGen<TData> service)
    : IRootServiceGen<TData>
{
    public IMyServiceGen<TData> Service { get => service; }
}

public class RootServiceGenV2<TData>(IMyServiceGen<TData> service)
    : IRootServiceGen<TData>
{
    public IMyServiceGen<TData> Service { get => service; }
}

public interface IRootServiceGen<out TData>
{
    IMyServiceGen<TData> Service { get; }
}
