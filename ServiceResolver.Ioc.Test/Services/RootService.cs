namespace ServiceResolver.Ioc.Test.Services;

public class RootService(IMyService myService) : IRootService
{
    public IMyService Service { get; } = myService;
}

public class RootServiceV2(IMyService myService)
    : RootService(myService)
{
}

public class RootServiceV3(IMyService myService)
    : RootService(myService)
{
}


public interface IRootService
{
    IMyService Service { get; }
}
