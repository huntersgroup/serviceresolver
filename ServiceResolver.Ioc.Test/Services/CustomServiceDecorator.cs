namespace ServiceResolver.Ioc.Test.Services;

public class CustomSvc : ICustomSvc
{
}

public class CustomSvcDecoratorV1(ICustomSvc decoratee) : ICustomSvc, ICustomSvcDecorator
{
    public ICustomSvc Decoratee { get; } = decoratee;
}

public class CustomSvcDecoratorV2(ICustomSvc decoratee) : ICustomSvc, ICustomSvcDecorator
{
    public ICustomSvc Decoratee { get; } = decoratee;
}

public class CustomSvcDecoratorV3(ICustomSvc decoratee) : ICustomSvc, ICustomSvcDecorator
{
    public ICustomSvc Decoratee { get; } = decoratee;
}

public interface ICustomSvcDecorator
{
    ICustomSvc Decoratee { get; }
}

public interface ICustomSvc
{
}
