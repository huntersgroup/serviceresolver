using System;
using System.Threading.Tasks;

namespace ServiceResolver.Ioc.Test.Services;

public class MyServiceGen<TValue> : IMyServiceGen<TValue>
{
    public TValue CurrentValue { get; set; }
}

public class MyServiceGenV2<TValue> : IMyServiceGen<TValue>
{
    public TValue CurrentValue { get; set; }
}

public class MyServiceGenDisposable<TValue> : IMyServiceGen<TValue>, IDisposableService, IAsyncDisposable
{
    public TValue CurrentValue { get; set; }

    public int DisposeCounter { get; private set; }

    public void Dispose()
    {
        this.DisposeAsync().AsTask().GetAwaiter().GetResult();
    }

    public ValueTask DisposeAsync()
    {
        this.DisposeCounter++;
        return ValueTask.CompletedTask;
    }
}

public class MyServiceGenericStringImpl : IMyServiceGen<string>
{
    public string CurrentValue { get; set; }
}

[Obsolete]
class MyServiceGenericDoubleImplObsolete : IMyServiceGen<double>
{
    public double CurrentValue { get; set; }
}

public class MyServiceGenericIntImpl : IMyServiceGen<int>
{
    public int CurrentValue { get; set; }
}
