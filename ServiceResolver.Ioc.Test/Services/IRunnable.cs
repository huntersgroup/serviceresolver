using System;
using Xunit.Abstractions;

namespace ServiceResolver.Ioc.Test.Services;

public interface IRunnable
{
    void Start();

    void Stop();
}

public interface IScheduler
{
    Action<EventArgs> Scheduled { get; set; }
}

public class Scheduler : IRunnable, IScheduler
{
    private readonly ITestOutputHelper output;

    public Scheduler(ITestOutputHelper output)
    {
        this.output = output;
    }

    public Action<EventArgs> Scheduled { get; set; }

    public void Start()
    {
        this.output.WriteLine($"Start execution...");

        for (var i = 0; i < 5; i++)
        {
            this.output.WriteLine($"index: {i}");

            this.Scheduled?.Invoke(EventArgs.Empty);

            //Thread.Sleep(TimeSpan.FromSeconds(1));
        }
    }

    public void Stop()
    {
        this.output.WriteLine($"Stop execution...");
    }
}
