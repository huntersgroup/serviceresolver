using System.Collections.Generic;

namespace ServiceResolver.Ioc.Test.Services;
public class RootServiceMany(IEnumerable<IMyService> services) : IRootServiceMany
{
    public IEnumerable<IMyService> Services { get; } = services;
}

public class RootServiceManyV2(ICollection<IMyService> services) : IRootServiceMany
{
    public IEnumerable<IMyService> Services { get; } = services;
}

public interface IRootServiceMany
{
    IEnumerable<IMyService> Services { get; }
}
