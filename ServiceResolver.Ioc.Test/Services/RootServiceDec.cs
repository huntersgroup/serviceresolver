namespace ServiceResolver.Ioc.Test.Services;
public class RootServiceDec(ICustomSvc service) : IRootServiceDec
{
    public ICustomSvc Service
    {
        get => service;
    }
}

public interface IRootServiceDec
{
    ICustomSvc Service { get; }
}
