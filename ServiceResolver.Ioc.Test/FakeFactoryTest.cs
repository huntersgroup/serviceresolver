using Xunit;

namespace ServiceResolver.Ioc.Test;
public class FakeFactoryTest
{
    [Fact]
    public void Test()
    {
        FactoryBase factory1 = FactoryBase.Current;
        FactoryBase factory2 = FactoryBase.Derived;

        var str1 = factory1.WhoIam();
        var str2 = factory2.WhoIam();

        Assert.NotNull(str1);
        Assert.NotNull(str2);
    }
}

public class FactoryBase
{
    public static readonly FactoryBase Current = new();
    public static readonly FactoryDerived Derived = new();

    public string WhoIam()
    {
        return $"{nameof(FactoryBase)} instance";
    }

    public virtual string WhoIamVirtual()
    {
        return $"{nameof(FactoryBase)} instance";
    }
}

public class FactoryDerived : FactoryBase
{
    public new string WhoIam()
    {
        return $"{nameof(FactoryDerived)} instance";
    }

    public override string WhoIamVirtual()
    {
        return $"{nameof(FactoryDerived)} instance";
    }
}
