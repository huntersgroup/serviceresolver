using DryIoc;
using ServiceResolver.Ioc.DryIoc;
using Xunit.Abstractions;
using ServiceRegisterProvider = ServiceResolver.Ioc.DryIoc.ServiceRegisterProvider;

namespace ServiceResolver.Ioc.Test.Impl.DryIoc;

public class DryIocServiceProviderTest(ITestOutputHelper output) : ServiceProviderBaseTest(output)
{
    protected override ImplementorType Type => ImplementorType.DryIoc;

    protected override IServiceRegisterProvider GetProvider(LifetimeScope? defaultLifeStyle = null)
    {
        return new ServiceRegisterProvider(container =>
        {
            var scope = defaultLifeStyle.HasValue ? defaultLifeStyle.Value.Translate() : Reuse.Transient;

            return container.With(rules => rules.WithDefaultReuse(scope));
        });
    }
}
