using System;
using System.Collections.Generic;
using DryIoc;
using ServiceResolver.Ioc.DryIoc.Extensions;
using ServiceResolver.Ioc.Test.Services;
using Xunit;

namespace ServiceResolver.Ioc.Test.Impl.DryIoc;

public class ContainerTest
{
    [Fact]
    public void RegisterManyTest()
    {
        var container = new Container();

        container.Register<IService, MyServiceA>(ifAlreadyRegistered: IfAlreadyRegistered.AppendNewImplementation);
        container.Register<IService, MyServiceB>(ifAlreadyRegistered: IfAlreadyRegistered.AppendNewImplementation);

        container.Register<IService, MyServiceB>(serviceKey: "unique");

        var unique = container.Resolve<IService>(serviceKey: "unique");
        var many01 = container.ResolveMany<IService>();
        var many02 = container.Resolve<IService[]>();

        Assert.NotNull(many01);
        Assert.NotNull(many02);
    }

    [Fact]
    public void RegisterManyTestV2()
    {
        var container = new Container();

        var serviceType = typeof(IService);

        container.RegisterMany(new List<Type>
        {
            typeof(MyServiceA),
            typeof(MyServiceB)
        }, serviceTypeCondition: type => type == serviceType);

        container.Register<IService, MyServiceA>(serviceKey: serviceType);

        var many = container.ResolveMany<IService>();
        var unique = container.Resolve<IService>(serviceKey: serviceType);

        Assert.NotEmpty(many);
        Assert.NotNull(unique);
    }

    [Fact]
    public void TestWithIdenticalScopes()
    {
        var container = new Container();

        // it works even for Reuse.Singleton
        var scopeA = Reuse.Transient;
        var scopeB = Reuse.Transient;

        container.Register(typeof(IService), typeof(MyServiceA),
            reuse: scopeA
            , setup: Setup.With(condition: request => request.DirectParent.ServiceType == typeof(ServiceConsumerA))
        );

        // the intention is having a default resolution if previous conditions aren't satisfied !!
        container.Register(typeof(IService), typeof(MyServiceB),
            reuse: scopeB
        );

        container.Register(typeof(ServiceConsumerA));
        container.Register(typeof(ServiceConsumerB));

        var consumerA = container.Resolve<ServiceConsumerA>();
        var consumerB = container.Resolve<ServiceConsumerB>();

        Assert.NotNull(consumerA);
        Assert.NotNull(consumerB);

        Assert.Equal(typeof(MyServiceA), consumerA.Service.GetType());
        Assert.Equal(typeof(MyServiceB), consumerB.Service.GetType());
    }

    ////[Fact]
    ////public void TestWithDefaultScopeToSingleton()
    ////{
    ////    // issue opened
    ////    // https://github.com/dadhi/DryIoc/discussions/630

    ////    var container = new Container();

    ////    // it works even for Reuse.Singleton
    ////    var scopeA = Reuse.Transient;
    ////    var scopeB = Reuse.Singleton;

    ////    container.Register(typeof(IService), typeof(MyServiceA),
    ////        reuse: scopeA
    ////        , setup: Setup.With(condition: request => request.DirectParent.Type == typeof(ServiceConsumerA))
    ////    );

    ////    // the intention is having a default resolution if previous conditions aren't satisfied !!
    ////    container.Register(typeof(IService), typeof(MyServiceB),
    ////        reuse: scopeB
    ////    );

    ////    container.Register(typeof(ServiceConsumerA));
    ////    container.Register(typeof(ServiceConsumerB));

    ////    var consumerA = container.Resolve<ServiceConsumerA>();
    ////    var consumerB = container.Resolve<ServiceConsumerB>();

    ////    Assert.NotNull(consumerA);
    ////    Assert.NotNull(consumerB);

    ////    Assert.Equal(typeof(MyServiceA), consumerA.Service.GetType());
    ////    Assert.Equal(typeof(MyServiceB), consumerB.Service.GetType());
    ////}
}

public class ServiceConsumerA(IService service)
{
    public IService Service { get; } = service;
}

public class ServiceConsumerB(IService service)
{
    public IService Service { get; } = service;
}

public class MyServiceA : IService { }

public class MyServiceB : IService { }

public interface IService { }
