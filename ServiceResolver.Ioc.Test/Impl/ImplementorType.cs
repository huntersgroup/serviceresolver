namespace ServiceResolver.Ioc.Test.Impl;

public enum ImplementorType : byte
{
    SimpleInjector,
    DryIoc
}
