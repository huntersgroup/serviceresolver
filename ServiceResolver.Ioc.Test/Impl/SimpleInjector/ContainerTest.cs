using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServiceResolver.Ioc.Extensions;
using ServiceResolver.Ioc.SimpleInjector.Extensions;
using ServiceResolver.Ioc.Test.Services;
using SimpleInjector;
using SimpleInjector.Diagnostics;
using SimpleInjector.Lifestyles;
using Xunit;
using Xunit.Abstractions;

namespace ServiceResolver.Ioc.Test.Impl.SimpleInjector;

public class ContainerTest(ITestOutputHelper output)
{
    public ITestOutputHelper Output { get; } = output;

    [Fact]
    public void VerifyMissingRegistrationTest()
    {
        var container = this.GetContainer();

        container.ResolveUnregisteredType += ContainerOnResolveUnregisteredType;

        var svc01 = container.GetAllInstances<IMyService>();

        Assert.NotNull(svc01);
        Assert.Empty(svc01);

        void ContainerOnResolveUnregisteredType(object sender, UnregisteredTypeEventArgs args)
        {
            if (args.Handled)
            {
                return;
            }

            var unregisteredType = args.UnregisteredServiceType;

            if (unregisteredType.IsGenEnumerable())
            {
                var unregisteredGenericArg = unregisteredType.GetGenericArguments()[0];

                var registration = container.Collection.CreateRegistration(unregisteredGenericArg, Enumerable.Empty<Type>());

                args.Register(registration);
            }
        }
    }

    [Fact]
    public void VerifyRegistrationWithKeyTransientTest()
    {
        var containerKey = this.GetContainer();
        var rootContainer = this.GetContainer();

        rootContainer.Register<IMyService, MyService>(Lifestyle.Singleton);
        rootContainer.Register<IRootService, RootService>();

        containerKey.Register<IRootService, RootServiceV2>();

        containerKey.ResolveUnregisteredType += (sender, args) =>
        {
            if (args.Handled)
            {
                return;
            }

            var unregisteredType = args.UnregisteredServiceType;
            var registration = rootContainer.GetRegistration(unregisteredType, false);

            if (registration is not null)
            {
                args.Register(registration.Registration);
            }
        };


        /*
         * the container should manager the event for unregistered service type
         * it should look dependency on root 
         */

        var svc1 = rootContainer.GetInstance<IRootService>();
        var svc2 = containerKey.GetInstance<IRootService>();

        Assert.NotNull(svc1);
        Assert.NotNull(svc2);
        Assert.Equal(svc1.Service, svc2.Service);
    }

    [Fact]
    public void VerifyRegistrationWithKeyOpenGenericTransientTest()
    {
        var containerKey = this.GetContainer();
        var rootContainer = this.GetContainer();

        rootContainer.Register(typeof(IMyServiceGen<>), typeof(MyServiceGen<>),Lifestyle.Singleton);

        containerKey.ResolveUnregisteredType += (sender, args) =>
        {
            if (args.Handled)
            {
                return;
            }

            var unregisteredType = args.UnregisteredServiceType;
            var registration = rootContainer.GetRegistration(unregisteredType, true);

            if (registration is not null)
            {
                args.Register(registration.Registration);
            }
        };

        /*
         * the container should manager the event for unregistered service type
         * it should look dependency on root
         */

        var svc2 = containerKey.GetInstance<IMyServiceGen<string>>();

        Assert.NotNull(svc2);
    }

    [Fact]
    public void VerifyRegistrationWithKeyScopeTest()
    {
        var containerKey = this.GetContainer();
        var rootContainer = this.GetContainer();

        rootContainer.Register<IMyService, MyService>(Lifestyle.Scoped);
        rootContainer.Register<IRootService, RootService>();

        containerKey.Register<IRootService, RootServiceV2>();
        
        containerKey.ResolveUnregisteredType += (sender, args) =>
        {
            if (args.Handled)
            {
                return;
            }

            var unregisteredType = args.UnregisteredServiceType;
            var registration = rootContainer.GetRegistration(unregisteredType, false);

            if (registration is not null)
            {
                args.Register(registration.Registration);
            }
        };

        /*
         * the container should manager the event for unregistered service type
         * it should look dependency on root
         */

        using (AsyncScopedLifestyle.BeginScope(rootContainer))
        {
            var svc1 = rootContainer.GetInstance<IRootService>();
            var svc2 = containerKey.GetInstance<IRootService>();

            Assert.NotNull(svc1);
            Assert.NotNull(svc2);
            Assert.Equal(svc1.Service, svc2.Service);
        }
    }

    [Fact]
    public void RegistrationMany()
    {
        var container = this.GetContainer();

        var type = typeof(StringBuilder);

        var registration = Lifestyle.Singleton.CreateRegistration(type, () => new StringBuilder("## demo ##"), container);

        container.Collection.Register(type, new List<Registration> { registration });

        container.Verify();
        var all = container.GetAllInstances<StringBuilder>();

        Assert.NotNull(all);
    }

    [Fact]
    public void RegisterMany_Test()
    {
        var container = this.GetContainer();

        var serviceType = typeof(IMyServiceGen<>);
        var list = new List<Type>
        {
            typeof(MyServiceGen<>)
        };

        container.Collection.Register(serviceType, (IEnumerable<Type>)list, Lifestyle.Singleton);

        container.Verify();

        var firstCol = container.GetAllInstances<IMyServiceGen<string>>().ToList();
        var secondCol = container.GetInstance<IEnumerable<IMyServiceGen<string>>>().ToList();

        Assert.NotNull(firstCol);
        Assert.NotNull(secondCol);
    }

    [Fact]
    public void RegistrationMany2()
    {
        var container = this.GetContainer();
        var serviceType = typeof(IMyService);

        var reg1 = Lifestyle.Singleton.CreateRegistration(serviceType, () => new MyService(), container);
        var reg2 = Lifestyle.Singleton.CreateRegistration(serviceType, () => new MyServiceV2(), container);

        container.Collection.Register(serviceType, new List<Registration> { reg1, reg2 });

        container.Verify();

        var all1 = container.GetAllInstances<IMyService>();
        var all2 = container.GetAllInstances<IMyService>();

        Assert.Same(all1, all2);
    }

    [Fact]
    public void RegistrationMany3()
    {
        var container = this.GetContainer();
        var serviceType = typeof(IMyService);

        var reg1 = Lifestyle.Singleton.CreateRegistration(serviceType, () => new MyService(), container);
        var reg2 = Lifestyle.Transient.CreateRegistration(serviceType, () => new MyServiceV2(), container);

        container.Collection.Register(serviceType, new List<Registration> { reg1, reg2 });

        container.Verify();

        var all1 = container.GetAllInstances<IMyService>().ToList();
        var all2 = container.GetAllInstances<IMyService>().ToList();

        //Assert.Same(all1, all2);
        Assert.NotEqual(all1, all2);
        //Assert.True(all1 == all2);
    }

    [Fact]
    public void RegistrationMany4()
    {
        var container = this.GetContainer();
        var serviceType = typeof(IMyService);

        var reg1 = Lifestyle.Singleton.CreateRegistration(serviceType, () => new MyService(), container);
        var reg2 = Lifestyle.Scoped.CreateRegistration(serviceType, () => new MyServiceV2(), container);

        container.Collection.Register(serviceType, new List<Registration> { reg1, reg2 });

        container.Verify();

        using (AsyncScopedLifestyle.BeginScope(container))
        {
            var all1 = container.GetAllInstances<IMyService>().ToList();
            var all2 = container.GetAllInstances<IMyService>().ToList();

            //Assert.Same(all1, all2);
            Assert.Equal(all1, all2);
            //Assert.True(all1 == all2);
        }
    }

    [Fact]
    public void ScopeTest()
    {
        var container = this.GetContainer();

        container.Register(() => new StringBuilder($"id: {Guid.NewGuid():D}"), Lifestyle.Scoped);

        using (AsyncScopedLifestyle.BeginScope(container))
        {
            var sb1 = container.GetInstance<StringBuilder>();
            var sb2 = container.GetInstance<StringBuilder>();

            Assert.Equal(sb1, sb2);

            using (AsyncScopedLifestyle.BeginScope(container))
            {
                var sb3 = container.GetInstance<StringBuilder>();
                var sb4 = container.GetInstance<StringBuilder>();

                Assert.Equal(sb3, sb4);
                Assert.NotEqual(sb1, sb3);
            }
        }
    }

    [Fact]
    public void RegisterCollectionTest()
    {
        var container = new Container();
        var options = container.Options;
        
        options.DefaultLifestyle = Lifestyle.Transient;
        options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

        var serviceType = typeof(IMyService);

        var regs = new List<Registration>
        {
            Lifestyle.Transient.CreateRegistration(typeof(MyService), container),
            Lifestyle.Transient.CreateRegistration(serviceType, () => new MyServiceV2(), container)
        };

        container.ResolveUnregisteredType += (_, args) =>
        {
            if (!args.Handled)
            {
                var reg = container.Collection.CreateRegistration<IMyService>(regs);

                args.Register(reg);
            }
        };

        var svc01 = container.GetAllInstances<IMyService>();
        var svc02 = container.GetAllInstances<IMyService>();

        Assert.NotNull(svc01);
        Assert.NotNull(svc02);
        Assert.Same(svc01, svc02);
    }

    [Fact]
    public void RegisterWithSuppressDisposableTransientWarningTest()
    {
        var container = new Container();
        var options = container.Options;

        options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

        var reg = Lifestyle.Transient.CreateRegistration(typeof(DisposableService), container);
        reg.SuppressDiagnosticWarning(DiagnosticType.DisposableTransientComponent, "external dependency");

        container.AddRegistration(typeof(IDisposableService), reg);

        container.Verify(VerificationOption.VerifyOnly);

        Assert.NotNull(container.GetInstance<IDisposableService>());
    }

    private Container GetContainer()
    {
        var container = new Container();
        container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

        return container;
    }
}
