using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using ServiceResolver.Ioc.Exceptions;
using ServiceResolver.Ioc.Extensions;
using ServiceResolver.Ioc.SimpleInjector;
using ServiceResolver.Ioc.Test.Services;
using SimpleInjector.Lifestyles;
using Xunit;
using Xunit.Abstractions;

namespace ServiceResolver.Ioc.Test.Impl.SimpleInjector;

public class SimpleInjectorServiceProviderTest(ITestOutputHelper output) : ServiceProviderBaseTest(output)
{
    protected override ImplementorType Type => ImplementorType.SimpleInjector;

    [Fact]
    public void EmptyDescriptorTest()
    {
        var serviceRegister = new ServiceRegisterProvider(new ServiceRegisterDescriptor());

        Assert.NotNull(serviceRegister);
    }

    [Fact]
    public void IsRegistered_KeyTest()
    {
        var serviceRegister = this.GetProvider();
        var lifetime = LifetimeScope.Transient;
        const string key = "my-ctx";

        // acts
        serviceRegister
            .Register(typeof(IMyService).AsServiceKey(key), lifetime.CreateRegistration<MyService>())
            .Register(typeof(MyServiceV2).AsServiceKey(key), lifetime.CreateRegistration(typeof(MyServiceV2)))
            .Register(typeof(IMyServiceGen<>).AsServiceKey(key), lifetime.CreateRegistration(typeof(MyServiceGen<>)))
            ;

        // asserts
        Assert.True(serviceRegister.IsRegistered(typeof(IMyService).AsServiceKey(key)));
        Assert.True(serviceRegister.IsRegistered(typeof(MyServiceV2).AsServiceKey(key)));
        Assert.True(serviceRegister.IsRegistered(typeof(IMyServiceGen<>).AsServiceKey(key)));
    }

    [Fact]
    public void IsRegistered_KeyLazyTest()
    {
        var serviceRegister = this.GetProvider();
        var lifetime = LifetimeScope.Transient;
        const string key = "my-ctx";

        // acts
        serviceRegister
            .RegisterLazy(typeof(IMyService).AsServiceKey(key), lifetime.CreateRegistration<MyService>())
            .RegisterLazy(typeof(MyServiceV2).AsServiceKey(key), lifetime.CreateRegistration(typeof(MyServiceV2)))
            .RegisterLazy(typeof(IMyServiceGen<>).AsServiceKey(key), lifetime.CreateRegistration(typeof(MyServiceGen<>)))
            ;

        // asserts
        Assert.True(serviceRegister.IsRegistered(typeof(IMyService).AsServiceKey(key)));
        Assert.True(serviceRegister.IsRegistered(typeof(MyServiceV2).AsServiceKey(key)));
        Assert.True(serviceRegister.IsRegistered(typeof(IMyServiceGen<>).AsServiceKey(key)));
    }

    [Fact]
    public void IsRegistered_KeyManyTest()
    {
        var serviceRegister = this.GetProvider();
        var lifetime = LifetimeScope.Transient;
        const string key = "my-ctx";

        serviceRegister.RegisterMany(new ServiceKey(typeof(IMyService), key),
        [
            lifetime.CreateRegistration<MyService>(),
            lifetime.CreateRegistration<MyServiceV2>()
        ]);

        serviceRegister.RegisterMany(new ServiceKey(typeof(IMyServiceGen<>), key),
        [
            lifetime.CreateRegistration(typeof(MyServiceGen<>)),
            lifetime.CreateRegistration(typeof(MyServiceGenV2<>))
        ]);

        // asserts
        Assert.True(serviceRegister.IsRegistered(typeof(IMyService).AsServiceKey(key), true));
        Assert.True(serviceRegister.IsRegistered(new ServiceKey(typeof(IMyServiceGen<>), key), true));
    }

    [Fact]
    public void IsRegistered_KeyManyLazyTest()
    {
        var serviceRegister = this.GetProvider();
        var lifetime = LifetimeScope.Transient;
        const string key = "my-ctx";

        serviceRegister.RegisterManyLazy(new ServiceKey(typeof(IMyService), key),
        [
            lifetime.CreateRegistration<MyService>(),
            lifetime.CreateRegistration<MyServiceV2>()
        ]);

        serviceRegister.RegisterManyLazy(new ServiceKey(typeof(IMyServiceGen<>), key),
        [
            lifetime.CreateRegistration(typeof(MyServiceGen<>)),
            lifetime.CreateRegistration(typeof(MyServiceGenV2<>))
        ]);

        // asserts
        Assert.True(serviceRegister.IsRegistered(typeof(IMyService).AsServiceKey(key), true));
        Assert.True(serviceRegister.IsRegistered(new ServiceKey(typeof(IMyServiceGen<>), key), true));
        Assert.False(serviceRegister.IsRegistered(new ServiceKey(typeof(IMyServiceGen<string>), key), true));
    }

    [Theory]
    [InlineData(LifetimeScope.Transient)]
    [InlineData(LifetimeScope.Scoped)]
    [InlineData(LifetimeScope.Singleton)]
    public void Register_KeyTest(LifetimeScope lifetime)
    {
        var serviceRegister = this.GetProvider();
        const string key = "my-ctx";

        serviceRegister.Register<IMyService, MyService>(lifetime);

        serviceRegister.Register(typeof(IRootService).AsServiceKey(key), lifetime.CreateRegistration<RootService>());

        switch (lifetime)
        {
            case LifetimeScope.Transient:
            {
                var svc01 = serviceRegister.GetService<IRootService>(key);
                var svc02 = serviceRegister.GetService<IRootService>(key);

                Assert.NotEqual(svc01, svc02);

                break;
            }
            case LifetimeScope.Scoped:
            {
                using (serviceRegister.BeginScope())
                {
                    var svc01 = serviceRegister.GetService<IRootService>(key);
                    var svc02 = serviceRegister.GetService<IRootService>(key);

                    Assert.Same(svc01, svc02);
                    Assert.Same(svc01.Service, svc02.Service);
                }
                break;
            }
            case LifetimeScope.Singleton:
            {
                var svc01 = serviceRegister.GetService<IRootService>(key);
                var svc02 = serviceRegister.GetService<IRootService>(key);

                Assert.Same(svc01, svc02);
                Assert.Same(svc01.Service, svc02.Service);

                break;
            }
            default:
            {
                return;
            }
        }
    }

    [Theory]
    [InlineData(LifetimeScope.Transient)]
    [InlineData(LifetimeScope.Scoped)]
    [InlineData(LifetimeScope.Singleton)]
    public void RegisterLazy_KeyTest(LifetimeScope lifetime)
    {
        var serviceRegister = this.GetProvider();
        const string key = "my-ctx";

        serviceRegister.Register<IMyService, MyService>(lifetime);

        serviceRegister.RegisterLazy(typeof(IRootService).AsServiceKey(key), lifetime.CreateRegistration<RootService>());

        switch (lifetime)
        {
            case LifetimeScope.Transient:
            {
                var svc01 = serviceRegister.GetService<IRootService>(key);
                var svc02 = serviceRegister.GetService<IRootService>(key);

                Assert.NotEqual(svc01, svc02);

                break;
            }
            case LifetimeScope.Scoped:
            {
                using (serviceRegister.BeginScope())
                {
                    var svc01 = serviceRegister.GetService<IRootService>(key);
                    var svc02 = serviceRegister.GetService<IRootService>(key);

                    Assert.Same(svc01, svc02);
                    Assert.Same(svc01.Service, svc02.Service);
                }
                break;
            }
            case LifetimeScope.Singleton:
            {
                var svc01 = serviceRegister.GetService<IRootService>(key);
                var svc02 = serviceRegister.GetService<IRootService>(key);

                Assert.Same(svc01, svc02);
                Assert.Same(svc01.Service, svc02.Service);

                break;
            }
            default:
            {
                return;
            }
        }
    }

    [Theory]
    [InlineData(LifetimeScope.Transient)]
    [InlineData(LifetimeScope.Scoped)]
    [InlineData(LifetimeScope.Singleton)]
    public void RegisterLazy_AllKeyTest(LifetimeScope lifetime)
    {
        var serviceRegister = this.GetProvider();
        const string key = "my-ctx";

        serviceRegister.RegisterLazy<IMyService>(lifetime.CreateRegistration<MyService>());
        serviceRegister.RegisterLazy(typeof(IRootService).AsServiceKey(key), lifetime.CreateRegistration<RootService>());

        switch (lifetime)
        {
            case LifetimeScope.Transient:
            {
                var svc01 = serviceRegister.GetService<IRootService>(key);
                var svc02 = serviceRegister.GetService<IRootService>(key);

                Assert.NotEqual(svc01, svc02);

                break;
            }
            case LifetimeScope.Scoped:
            {
                using (serviceRegister.BeginScope())
                {
                    var svc01 = serviceRegister.GetService<IRootService>(key);
                    var svc02 = serviceRegister.GetService<IRootService>(key);

                    Assert.Same(svc01, svc02);
                    Assert.Same(svc01.Service, svc02.Service);
                }
                break;
            }
            case LifetimeScope.Singleton:
            {
                var svc01 = serviceRegister.GetService<IRootService>(key);
                var svc02 = serviceRegister.GetService<IRootService>(key);

                Assert.Same(svc01, svc02);
                Assert.Same(svc01.Service, svc02.Service);

                break;
            }
            default:
            {
                return;
            }
        }
    }

    [Theory]
    [InlineData(LifetimeScope.Transient)]
    [InlineData(LifetimeScope.Scoped)]
    [InlineData(LifetimeScope.Singleton)]
    public void Register_KeyOpenGenericTest(LifetimeScope lifetime)
    {
        var serviceRegister = this.GetProvider();
        const string key = "my-ctx";

        serviceRegister.Register(new ServiceKey(typeof(IMyServiceGen<>), key), lifetime.CreateRegistration(typeof(MyServiceGen<>)))
            .Register(new ServiceKey(typeof(IRootServiceGen<>), key), lifetime.CreateRegistration(typeof(RootServiceGen<>)));

        switch (lifetime)
        {
            case LifetimeScope.Transient:
            {
                var svc01 = serviceRegister.GetService<IRootServiceGen<string>>(key);
                var svc02 = serviceRegister.GetService<IRootServiceGen<string>>(key);

                Assert.NotNull(svc01);
                Assert.NotNull(svc02);
                Assert.NotEqual(svc01, svc02);
                Assert.NotEqual(svc01.Service, svc02.Service);

                break;
            }
            case LifetimeScope.Scoped:
            {
                using (serviceRegister.BeginScope())
                {
                    var svc01 = serviceRegister.GetService<IRootServiceGen<string>>(key);
                    var svc02 = serviceRegister.GetService<IRootServiceGen<string>>(key);

                    Assert.NotNull(svc01);
                    Assert.NotNull(svc02);
                    Assert.Equal(svc01, svc02);
                    Assert.Same(svc01, svc02);
                    Assert.Equal(svc01.Service, svc02.Service);
                    Assert.Same(svc01.Service, svc02.Service);
                }

                break;
            }
            case LifetimeScope.Singleton:
            {
                var svc01 = serviceRegister.GetService<IRootServiceGen<string>>(key);
                var svc02 = serviceRegister.GetService<IRootServiceGen<string>>(key);

                Assert.NotNull(svc01);
                Assert.NotNull(svc02);
                Assert.Equal(svc01, svc02);
                Assert.Same(svc01, svc02);
                Assert.Same(svc01.Service, svc02.Service);

                break;
            }
            default:
            {
                return;
            }
        }
    }

    [Theory]
    [InlineData(LifetimeScope.Transient)]
    [InlineData(LifetimeScope.Scoped)]
    [InlineData(LifetimeScope.Singleton)]
    public void RegisterLazy_KeyOpenGenericTest(LifetimeScope lifetime)
    {
        var serviceRegister = this.GetProvider();
        const string key = "my-ctx";

        serviceRegister.Register(new ServiceKey(typeof(IMyServiceGen<>), key), lifetime.CreateRegistration(typeof(MyServiceGen<>)))
            .RegisterLazy(new ServiceKey(typeof(IRootServiceGen<>), key), lifetime.CreateRegistration(typeof(RootServiceGen<>)));

        switch (lifetime)
        {
            case LifetimeScope.Transient:
            {
                var svc01 = serviceRegister.GetService<IRootServiceGen<string>>(key);
                var svc02 = serviceRegister.GetService<IRootServiceGen<string>>(key);

                Assert.NotNull(svc01);
                Assert.NotNull(svc02);
                Assert.NotEqual(svc01, svc02);
                Assert.NotEqual(svc01.Service, svc02.Service);

                break;
            }
            case LifetimeScope.Scoped:
            {
                using (serviceRegister.BeginScope())
                {
                    var svc01 = serviceRegister.GetService<IRootServiceGen<string>>(key);
                    var svc02 = serviceRegister.GetService<IRootServiceGen<string>>(key);

                    Assert.NotNull(svc01);
                    Assert.NotNull(svc02);
                    Assert.Equal(svc01, svc02);
                    Assert.Same(svc01, svc02);
                    Assert.Equal(svc01.Service, svc02.Service);
                    Assert.Same(svc01.Service, svc02.Service);
                }

                break;
            }
            case LifetimeScope.Singleton:
            {
                var svc01 = serviceRegister.GetService<IRootServiceGen<string>>(key);
                var svc02 = serviceRegister.GetService<IRootServiceGen<string>>(key);

                Assert.NotNull(svc01);
                Assert.NotNull(svc02);
                Assert.Equal(svc01, svc02);
                Assert.Same(svc01, svc02);
                Assert.Same(svc01.Service, svc02.Service);

                break;
            }
            default:
            {
                return;
            }
        }
    }

    [Theory]
    [InlineData(LifetimeScope.Transient)]
    [InlineData(LifetimeScope.Scoped)]
    [InlineData(LifetimeScope.Singleton)]
    public void RegisterLazy_AllKeyOpenGenericTest(LifetimeScope lifetime)
    {
        var serviceRegister = this.GetProvider();
        const string key = "my-ctx";

        serviceRegister.RegisterLazy(new ServiceKey(typeof(IMyServiceGen<>), key), lifetime.CreateRegistration(typeof(MyServiceGen<>)))
            .RegisterLazy(new ServiceKey(typeof(IRootServiceGen<>), key), lifetime.CreateRegistration(typeof(RootServiceGen<>)));

        switch (lifetime)
        {
            case LifetimeScope.Transient:
            {
                var svc01 = serviceRegister.GetService<IRootServiceGen<string>>(key);
                var svc02 = serviceRegister.GetService<IRootServiceGen<string>>(key);

                Assert.NotNull(svc01);
                Assert.NotNull(svc02);
                Assert.NotEqual(svc01, svc02);
                Assert.NotEqual(svc01.Service, svc02.Service);

                break;
            }
            case LifetimeScope.Scoped:
            {
                using (serviceRegister.BeginScope())
                {
                    var svc01 = serviceRegister.GetService<IRootServiceGen<string>>(key);
                    var svc02 = serviceRegister.GetService<IRootServiceGen<string>>(key);

                    Assert.NotNull(svc01);
                    Assert.NotNull(svc02);
                    Assert.Equal(svc01, svc02);
                    Assert.Same(svc01, svc02);
                    Assert.Equal(svc01.Service, svc02.Service);
                    Assert.Same(svc01.Service, svc02.Service);
                }

                break;
            }
            case LifetimeScope.Singleton:
            {
                var svc01 = serviceRegister.GetService<IRootServiceGen<string>>(key);
                var svc02 = serviceRegister.GetService<IRootServiceGen<string>>(key);

                Assert.NotNull(svc01);
                Assert.NotNull(svc02);
                Assert.Equal(svc01, svc02);
                Assert.Same(svc01, svc02);
                Assert.Same(svc01.Service, svc02.Service);

                break;
            }
            default:
            {
                return;
            }
        }
    }

    [Theory]
    [InlineData(LifetimeScope.Transient)]
    [InlineData(LifetimeScope.Scoped)]
    [InlineData(LifetimeScope.Singleton)]
    public void Register_KeyWithDifferentDependenciesTest(LifetimeScope lifetime)
    {
        var serviceRegister = this.GetProvider();
        const string key = "my-ctx";

        serviceRegister.Register<IMyService, MyService>(lifetime)
            .Register(typeof(IMyService).AsServiceKey(key), lifetime.CreateRegistration<MyServiceV2>());

        serviceRegister.Register(typeof(IRootService).AsServiceKey(), LifetimeScope.Transient.CreateRegistration<RootService>())
            .Register(typeof(IRootService).AsServiceKey(key), LifetimeScope.Transient.CreateRegistration<RootServiceV2>());

        switch (lifetime)
        {
            case LifetimeScope.Transient:
            {
                var rootSvc01 = serviceRegister.GetService<IRootService>();
                var rootSvc01_key = serviceRegister.GetService<IRootService>(key);

                // asserts
                Assert.IsType<RootService>(rootSvc01);
                Assert.IsType<RootServiceV2>(rootSvc01_key);
                Assert.IsType<MyService>(rootSvc01.Service);
                Assert.IsType<MyServiceV2>(rootSvc01_key.Service);
                break;
            }
            case LifetimeScope.Scoped:
            {
                using (serviceRegister.BeginScope())
                {
                    var rootSvc01 = serviceRegister.GetService<IRootService>();
                    var rootSvc01_key = serviceRegister.GetService<IRootService>(key);
                    var rootSvc02_key = serviceRegister.GetService<IRootService>(key);

                    // asserts
                    Assert.IsType<RootService>(rootSvc01);
                    Assert.IsType<RootServiceV2>(rootSvc01_key);
                    Assert.IsType<MyService>(rootSvc01.Service);
                    Assert.IsType<MyServiceV2>(rootSvc01_key.Service);

                    Assert.NotEqual(rootSvc01_key, rootSvc02_key);
                    Assert.Equal(rootSvc01_key.Service, rootSvc02_key.Service);
                    Assert.Same(rootSvc01_key.Service, rootSvc02_key.Service);
                }

                break;
            }
            case LifetimeScope.Singleton:
            {
                var rootSvc01 = serviceRegister.GetService<IRootService>();
                var rootSvc01_key = serviceRegister.GetService<IRootService>(key);

                // asserts
                Assert.IsType<RootService>(rootSvc01);
                Assert.IsType<RootServiceV2>(rootSvc01_key);
                Assert.IsType<MyService>(rootSvc01.Service);
                Assert.IsType<MyServiceV2>(rootSvc01_key.Service);

                break;
            }
            default:
            {
                return;
            }
        }
    }

    [Theory]
    [InlineData(LifetimeScope.Transient)]
    [InlineData(LifetimeScope.Scoped)]
    [InlineData(LifetimeScope.Singleton)]
    public void RegisterLazy_KeyWithDifferentDependenciesTest(LifetimeScope lifetime)
    {
        var serviceRegister = this.GetProvider();
        const string key = "my-ctx";

        serviceRegister.RegisterLazy<IMyService>(lifetime.CreateRegistration<MyService>())
            .RegisterLazy(typeof(IMyService).AsServiceKey(key), lifetime.CreateRegistration<MyServiceV2>());

        serviceRegister.RegisterLazy(typeof(IRootService).AsServiceKey(), LifetimeScope.Transient.CreateRegistration<RootService>())
            .RegisterLazy(typeof(IRootService).AsServiceKey(key), LifetimeScope.Transient.CreateRegistration<RootServiceV2>());

        switch (lifetime)
        {
            case LifetimeScope.Transient:
            {
                var rootSvc01 = serviceRegister.GetService<IRootService>();
                var rootSvc01_key = serviceRegister.GetService<IRootService>(key);

                // asserts
                Assert.IsType<RootService>(rootSvc01);
                Assert.IsType<RootServiceV2>(rootSvc01_key);
                Assert.IsType<MyService>(rootSvc01.Service);
                Assert.IsType<MyServiceV2>(rootSvc01_key.Service);

                break;
            }
            case LifetimeScope.Scoped:
            {
                using (serviceRegister.BeginScope())
                {
                    var rootSvc01 = serviceRegister.GetService<IRootService>();
                    var rootSvc01_key = serviceRegister.GetService<IRootService>(key);
                    var rootSvc02_key = serviceRegister.GetService<IRootService>(key);

                    // asserts
                    Assert.IsType<RootService>(rootSvc01);
                    Assert.IsType<RootServiceV2>(rootSvc01_key);
                    Assert.IsType<MyService>(rootSvc01.Service);
                    Assert.IsType<MyServiceV2>(rootSvc01_key.Service);

                    Assert.NotEqual(rootSvc01_key, rootSvc02_key);
                    Assert.Equal(rootSvc01_key.Service, rootSvc02_key.Service);
                    Assert.Same(rootSvc01_key.Service, rootSvc02_key.Service);
                }

                break;
            }
            case LifetimeScope.Singleton:
            {
                var rootSvc01 = serviceRegister.GetService<IRootService>();
                var rootSvc01_key = serviceRegister.GetService<IRootService>(key);

                // asserts
                Assert.IsType<RootService>(rootSvc01);
                Assert.IsType<RootServiceV2>(rootSvc01_key);
                Assert.IsType<MyService>(rootSvc01.Service);
                Assert.IsType<MyServiceV2>(rootSvc01_key.Service);

                break;
            }
            default:
            {
                return;
            }
        }
    }


    [Fact]
    public void Register_WrongTest()
    {
        var serviceRegister = this.GetProvider();
        const string key = "my-ctx";

        serviceRegister.RegisterLazy(typeof(IMyService).AsServiceKey(key), LifetimeScope.Transient.CreateRegistration<MyService>());

        //asserts
        Assert.Throws<ServiceRegistrationException>(
            () => serviceRegister.RegisterLazy(typeof(IMyService).AsServiceKey(key),
                LifetimeScope.Transient.CreateRegistration<MyService>()));

        Assert.Throws<ServiceRegistrationException>(
            () => serviceRegister.RegisterLazy(typeof(IMyService).AsServiceKey(key),
                LifetimeScope.Transient.CreateRegistration<MyServiceV2>()));

        Assert.Throws<ServiceRegistrationException>(
            () => serviceRegister.Register(typeof(IMyService).AsServiceKey(key),
                LifetimeScope.Transient.CreateRegistration<MyService>()));

        Assert.Throws<ServiceRegistrationException>(
            () => serviceRegister.Register(typeof(IMyService).AsServiceKey(key),
                LifetimeScope.Transient.CreateRegistration<MyServiceV2>()));
    }

    [Fact]
    [Description("Tobe moved into basic test")]
    public void RegisterMany_WrongTest()
    {
        var serviceRegister = this.GetProvider();
        const string key = "my-ctx";

        serviceRegister.RegisterManyLazy(typeof(IMyService).AsServiceKey(key), [LifetimeScope.Transient.CreateRegistration<MyService>()]);

        // asserts
        Assert.Throws<ServiceRegistrationException>(
            () => serviceRegister.RegisterManyLazy(typeof(IMyService).AsServiceKey(key),
                [LifetimeScope.Transient.CreateRegistration<MyService>()]));

        Assert.Throws<ServiceRegistrationException>(
            () => serviceRegister.RegisterManyLazy(typeof(IMyService).AsServiceKey(key),
                [LifetimeScope.Transient.CreateRegistration<MyServiceV2>()]));

        Assert.Throws<ServiceRegistrationException>(
            () => serviceRegister.RegisterMany(typeof(IMyService).AsServiceKey(key),
                [LifetimeScope.Transient.CreateRegistration<MyService>()]));

        Assert.Throws<ServiceRegistrationException>(
            () => serviceRegister.RegisterMany(typeof(IMyService).AsServiceKey(key),
                [LifetimeScope.Transient.CreateRegistration<MyServiceV2>()]));
    }

    [Fact]
    public void RegisterMany_WithInfoTest()
    {
        var serviceRegister = this.GetProvider();

        var serviceType = typeof(IMyService);

        var list = new List<IRegistrationInfo>
        {
            LifetimeScope.Singleton.CreateRegistration(_ => new MyService()),
            LifetimeScope.Singleton.CreateRegistration(_ => new MyServiceV2()),
            LifetimeScope.Singleton.CreateRegistration(_ => new MyServiceV3()),
        };

        serviceRegister.RegisterMany(serviceType, list);

        var svc01 = serviceRegister.GetServices<IMyService>();

        Assert.NotNull(svc01);
    }

    [Fact]
    public void RegisterMany_KeyWithInfoTest()
    {
        var serviceRegister = this.GetProvider();
        const string key = "my-ctx";
        
        var list = new List<IRegistrationInfo>
        {
            LifetimeScope.Singleton.CreateRegistration(_ => new MyService()),
            LifetimeScope.Singleton.CreateRegistration(_ => new MyServiceV2()),
            LifetimeScope.Singleton.CreateRegistration(_ => new MyServiceV3()),
        };

        serviceRegister.RegisterMany(typeof(IMyService).AsServiceKey(key), list);

        var svc01 = serviceRegister.GetServices<IMyService>(key);

        Assert.NotNull(svc01);
    }

    [Fact]
    public void RegisterMany_OpenGenInfoTest()
    {
        var serviceRegister = this.GetProvider();

        var serviceType = typeof(IMyServiceGen<>);

        var list = new List<IRegistrationInfo>
        {
            LifetimeScope.Singleton.CreateRegistration(typeof(MyServiceGen<>))
        };

        serviceRegister.RegisterMany(new ServiceKey(serviceType), list);

        var svc01 = serviceRegister.GetServices<IMyServiceGen<string>>();

        Assert.NotNull(svc01);
    }


    [Fact]
    public void RegisterMany_KeyOpenGenInfoTest()
    {
        var serviceRegister = this.GetProvider();
        const string key = "my-ctx";

        var serviceType = typeof(IMyServiceGen<>);

        var list = new List<IRegistrationInfo>
        {
            LifetimeScope.Transient.CreateRegistration(typeof(MyServiceGen<>))
        };

        serviceRegister.RegisterMany(new ServiceKey(serviceType, key), list);

        var svc01 = serviceRegister.GetServices<IMyServiceGen<string>>(key);
        
        Assert.NotNull(svc01);
    }

    [Fact]
    public void RegisterManyCloseGenInfoTest()
    {
        var serviceRegister = this.GetProvider();

        var serviceType = typeof(IMyServiceGen<string>);
        
        var list = new List<IRegistrationInfo>
        {
            LifetimeScope.Singleton.CreateRegistration(_ => new MyServiceGen<string>())
        };

        serviceRegister.RegisterMany(serviceType, list);

        IGenericServiceProvider serviceProvider = serviceRegister;

        var svc01 = serviceProvider.GetServices<IMyServiceGen<string>>();

        Assert.NotNull(svc01);
    }

    [Fact]
    public void RegisterWithInfoTest()
    {
        var serviceRegister = this.GetProvider();

        serviceRegister.Register(typeof(IMyService), LifetimeScope.Singleton.CreateRegistration(typeof(MyService)));

        var svc = serviceRegister.GetService<IMyService>();

        Assert.NotNull(svc);
    }

    [Fact]
    public void RegisterLazyWithInfoTest()
    {
        var serviceRegister = this.GetProvider();

        serviceRegister.RegisterLazy(typeof(IMyService), LifetimeScope.Singleton.CreateRegistration(typeof(MyService)));
        
        Assert.NotNull(serviceRegister.GetService<IMyService>());
    }

    [Fact]
    public void RegisterLazyWithInfoAndLifetimeMismatchTest()
    {
        var serviceRegister = this.GetProvider();

        // todo: DryIoc doesn't check mismatch between dependencies.
        // https://github.com/dadhi/DryIoc/issues/633

        serviceRegister.RegisterLazy(typeof(IRootService), LifetimeScope.Singleton.CreateRegistration(typeof(RootService)))
            .RegisterLazy(typeof(IMyService), LifetimeScope.Transient.CreateRegistration(_ => new MyService(),
                new SuppressDiagnostic { LifestyleMismatch = true }));

        Assert.NotNull(serviceRegister.GetService<IRootService>());
    }

    [Fact]
    public void RegisterLazyWithInfoAndTransientDisposableTest()
    {
        var serviceRegister = this.GetProvider();

        serviceRegister.RegisterLazy(typeof(IDisposableService), LifetimeScope.Transient.CreateRegistration(typeof(DisposableService),
                new SuppressDiagnostic { DisposableTransientComponent = true }));

        Assert.NotNull(serviceRegister.GetService<IDisposableService>());
    }

    [Fact]
    public void RegisterLazyWithInfoOpenGenericTest()
    {
        var serviceRegister = this.GetProvider();

        serviceRegister.RegisterLazy(typeof(IMyServiceGen<>), LifetimeScope.Transient.CreateRegistration(typeof(MyServiceGen<>)));
        
        var svc01 = serviceRegister.GetService<IMyServiceGen<int>>();
        var svc02 = serviceRegister.GetService<IMyServiceGen<long>>();
        var svc03 = serviceRegister.GetService<IMyServiceGen<string>>();
        var svc04 = serviceRegister.GetService(typeof(IMyServiceGen<decimal>));

        Assert.NotNull(svc01);
        Assert.NotNull(svc02);
        Assert.NotNull(svc03);
        Assert.NotNull(svc04);
    }

    [Fact]
    public void RegisterWithOpenAndNoOpenGenericTest()
    {
        var serviceRegister = this.GetProvider();

        serviceRegister.Register(typeof(IMyServiceGen<>), typeof(MyServiceGenDisposable<>), LifetimeScope.Singleton);

        Assert.Throws<ServiceRegistrationException>(() =>
            serviceRegister.Register(typeof(IMyServiceGen<int>), typeof(MyServiceGenDisposable<int>), LifetimeScope.Singleton));

        IGenericServiceProvider serviceProvider = serviceRegister;

        Assert.NotNull(serviceProvider.GetService<IMyServiceGen<int>>());
        Assert.NotNull(serviceProvider.GetService<IMyServiceGen<string>>());
    }

    [Fact]
    public void RegisterLazyWithOpenAndNoOpenGenericTest()
    {
        var serviceRegister = this.GetProvider();

        serviceRegister.RegisterLazy(typeof(IMyServiceGen<>), LifetimeScope.Singleton.CreateRegistration(typeof(MyServiceGenDisposable<>)));
        serviceRegister.RegisterLazy(typeof(IMyServiceGen<int>), LifetimeScope.Singleton.CreateRegistration(typeof(MyServiceGenDisposable<int>)));
        
        IGenericServiceProvider serviceProvider = serviceRegister;

        Assert.NotNull(serviceProvider.GetService<IMyServiceGen<int>>());
        Assert.NotNull(serviceProvider.GetService<IMyServiceGen<string>>());
    }

    [Fact]
    public void RegisterResolverWithOpenAndNoOpenGenericTest()
    {
        var serviceRegister = this.GetProvider();

        serviceRegister.RegisterResolver(typeof(IMyServiceGen<>), typeof(MyServiceGenDisposable<>), LifetimeScope.Singleton);
        serviceRegister.RegisterResolver(typeof(IMyServiceGen<int>), typeof(MyServiceGen<int>), LifetimeScope.Singleton);

        var svc01 = serviceRegister.GetService<IMyServiceGen<decimal>>();
        var svc02 = serviceRegister.GetService<IMyServiceGen<int>>();

        Assert.NotNull(svc01);
        Assert.True(svc01 is MyServiceGenDisposable<decimal>);

        Assert.NotNull(svc02);
        Assert.True(svc02 is MyServiceGen<int>);
    }

    [Fact]
    public void RegisterManyWithOpenAndNoOpenGenericTest()
    {
        var serviceRegister = this.GetProvider();

        serviceRegister.RegisterMany(typeof(IMyServiceGen<>), [typeof(MyServiceGen<>), typeof(MyServiceGenDisposable<>)], LifetimeScope.Singleton);

        Assert.Throws<ServiceRegistrationException>(() => serviceRegister.RegisterMany(typeof(IMyServiceGen<int>),
            [typeof(MyServiceGen<int>), typeof(MyServiceGenDisposable<int>)], LifetimeScope.Singleton));

        Assert.NotNull(serviceRegister.GetServices<IMyServiceGen<int>>());
        Assert.NotNull(serviceRegister.GetServices<IMyServiceGen<string>>());
    }

    [Fact]
    public void IsRegisteredInfoManyTest()
    {
        var serviceRegister = this.GetProvider();

        var serviceType = typeof(IMyService);

        var list = new List<IRegistrationInfo>
        {
            LifetimeScope.Singleton.CreateRegistration(_ => new MyService()),
            LifetimeScope.Singleton.CreateRegistration(_ => new MyServiceV2())
        };

        serviceRegister.RegisterMany(serviceType, list);

        Assert.True(serviceRegister.IsRegistered(typeof(IMyService), true));
        Assert.False(serviceRegister.IsRegistered(typeof(IEnumerable<IMyService>), true));
        Assert.False(serviceRegister.IsRegistered(typeof(IMyService)));
        Assert.NotNull(serviceRegister.GetServices<IMyService>());
        Assert.NotEmpty(serviceRegister.GetServices<IMyService>());
        Assert.Equal(2, serviceRegister.GetServices<IMyService>().Count());
    }

    [Fact]
    public void IsRegisteredInfoSingleAndManyTest()
    {
        var serviceRegister = this.GetProvider();

        var serviceType = typeof(IMyService);

        var list = new List<IRegistrationInfo>
        {
            LifetimeScope.Singleton.CreateRegistration(_ => new MyService()),
            LifetimeScope.Singleton.CreateRegistration(_ => new MyServiceV2())
        };

        serviceRegister.Register(serviceType, list.Last());
        serviceRegister.RegisterMany(serviceType, list);
        
        Assert.True(serviceRegister.IsRegistered(typeof(IMyService)));
        Assert.True(serviceRegister.IsRegistered(typeof(IMyService), true));
        Assert.NotNull(serviceRegister.GetServices<IMyService>());
        Assert.NotEmpty(serviceRegister.GetServices<IMyService>());
        Assert.Equal(2, serviceRegister.GetServices<IMyService>().Count());

        Assert.Same(serviceRegister.GetService<IMyService>(), serviceRegister.GetService<IMyService>());
    }

    [Fact]
    public void IsRegisteredInfoSingleAndManyLazyTest()
    {
        var serviceRegister = this.GetProvider();

        var serviceType = typeof(IMyService);

        var list = new List<IRegistrationInfo>
        {
            LifetimeScope.Singleton.CreateRegistration(_ => new MyService()),
            LifetimeScope.Singleton.CreateRegistration(_ => new MyServiceV2())
        };

        serviceRegister.RegisterLazy(serviceType, list.Last());
        serviceRegister.RegisterManyLazy(serviceType, list);
        
        Assert.True(serviceRegister.IsRegistered(typeof(IMyService)));
        Assert.True(serviceRegister.IsRegistered(typeof(IMyService), true));
        Assert.NotNull(serviceRegister.GetServices<IMyService>());
        Assert.NotEmpty(serviceRegister.GetServices<IMyService>());
        Assert.Equal(2, serviceRegister.GetServices<IMyService>().Count());

        Assert.Same(serviceRegister.GetService<IMyService>(), serviceRegister.GetService<IMyService>());
    }

    [Fact]
    public void IsRegisteredInfoSingleAndManyTest02()
    {
        var serviceRegister = this.GetProvider();

        var serviceType = typeof(IMyService);

        var list = new List<IRegistrationInfo>
        {
            LifetimeScope.Singleton.CreateRegistration(_ => new MyService()),
            LifetimeScope.Singleton.CreateRegistration(_ => new MyServiceV2())
        };

        // asserts
        Assert.False(serviceRegister.IsRegistered(typeof(IMyService)));

        // acts
        serviceRegister.Register(serviceType, list.Last());

        // asserts
        Assert.True(serviceRegister.IsRegistered(typeof(IMyService)));
        Assert.Throws<ServiceRegistrationException>(() => serviceRegister.Register(serviceType, list.First()));

        // asserts
        Assert.False(serviceRegister.IsRegistered(typeof(IMyService), true));

        // acts
        serviceRegister.RegisterMany(serviceType, list);

        // asserts
        Assert.True(serviceRegister.IsRegistered(typeof(IMyService), true));
        Assert.Throws<ServiceRegistrationException>(() => serviceRegister.RegisterMany(serviceType, list));

        // asserts
        Assert.NotNull(serviceRegister.GetServices<IMyService>());
        Assert.NotEmpty(serviceRegister.GetServices<IMyService>());
        Assert.Equal(2, serviceRegister.GetServices<IMyService>().Count());
        Assert.Same(serviceRegister.GetService<IMyService>(), serviceRegister.GetService<IMyService>());
    }

    [Fact]
    public void RegisterGenericInfoTest()
    {
        var serviceRegister = this.GetProvider();

        serviceRegister.Register(typeof(IMyServiceGen<>), LifetimeScope.Singleton.CreateRegistration(typeof(MyServiceGen<>)));
        
        Assert.NotNull(serviceRegister.GetService<IMyServiceGen<string>>());
    }

    protected override IServiceRegisterProvider GetProvider(LifetimeScope? defaultLifeStyle = null)
    {
        return new ServiceRegisterProvider(new ServiceRegisterDescriptor
        {
            Initializer = container =>
            {
                //container.Options.DefaultScopedLifestyle = new ThreadScopedLifestyle();
                container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
                if (defaultLifeStyle.HasValue)
                {
                    container.Options.DefaultLifestyle = defaultLifeStyle.Value.Translate();
                }
            },
            //ScopeFactory = ThreadScopedLifestyle.BeginScope
            ScopeFactory = AsyncScopedLifestyle.BeginScope
        });
    }
}

