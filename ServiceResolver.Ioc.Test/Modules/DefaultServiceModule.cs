using ServiceResolver.Ioc.Modules;
using ServiceResolver.Ioc.Registers;
using ServiceResolver.Ioc.Test.Services;

namespace ServiceResolver.Ioc.Test.Modules;

public class DefaultServiceModule
    : IServiceModule
{
    public void Initialize(IServiceRegister serviceRegister)
    {
        serviceRegister.Register<IRootService, RootService>()
            .Register<IMyService, MyService>();
    }
}
