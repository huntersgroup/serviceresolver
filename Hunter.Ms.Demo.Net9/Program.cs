using System.Reflection;
using System.Text;
using ServiceResolver.Ioc.AspNetCore.Extensions;
using ServiceResolver.Ioc.Extensions;
using ServiceResolver.Ioc.Options;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Host
    .UseServiceResolver(new SimpleInjectorOptions
    {
        //ConstructorResolutionType = ResolutionType.Hybrid,
        IgnoreMissingRegistrations = true,
        BeforeRegistering = collection =>
        {
            collection.AddOpenApi("v1");
            collection.AddControllers();

            var key = "my-key";
            collection.AddScoped<StringBuilder>(provider => new StringBuilder("Resolution without a key..."));
            collection.AddKeyedScoped<StringBuilder>(key, (_, _) => new StringBuilder($"Resolution with key: {key}"));
        },
        OnRegistering = serviceRegister =>
        {
            serviceRegister
                .AddAspNetCore()
                .AddControllerActivation()
                ;

            serviceRegister.RegisterModulesFrom(Assembly.GetExecutingAssembly());
        },
        AfterBuildingProvider = provider =>
        {
            provider.Verify();
        }
    })
    ;

var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseAuthorization();

app.MapOpenApi();

app.MapControllers();

await app.RunAsync();
