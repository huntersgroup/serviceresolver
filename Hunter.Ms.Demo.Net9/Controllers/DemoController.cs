using System.Text;
using Microsoft.AspNetCore.Mvc;

namespace Hunter.Ms.Demo.Controllers;

[ApiController]
[Route("[controller]")]
public class DemoController : ControllerBase
{
    private static readonly string[] NumbersFirst =
    [
        "one", "two", "three", "four", "five"
    ];

    private static readonly string[] NumbersSecond =
    [
        "one", "two", "three", "four", "five"
    ];

    [HttpGet]
    public IEnumerable<string> Get([FromKeyedServices("my-key")] StringBuilder builder)
    {
        return NumbersFirst;
    }
}
