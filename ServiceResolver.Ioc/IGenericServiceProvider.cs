using System;
using System.Collections.Generic;

namespace ServiceResolver.Ioc;

/// <summary>
/// A common service provider which resolves typed services.
/// </summary>
public interface IGenericServiceProvider
    : IServiceProvider
{
    /// <summary>
    /// Check if serviceType is registered into container
    /// </summary>
    /// <param name="serviceType">type to check</param>
    /// <param name="many">Indicates if there's a many registration for the given type.</param>
    /// <returns>true if registration is found</returns>
    bool IsRegistered(Type serviceType, bool many = false);

    /// <summary>
    ///  Check if TService type is registered into container
    /// </summary>
    /// <typeparam name="TService">>type to check</typeparam>
    /// <param name="many">Indicates if there's a many registration for the given type.</param>
    /// <returns>true if registration is found</returns>
    bool IsRegistered<TService>(bool many = false) where TService : class;

    /// <summary>
    /// Check if serviceType is registered into container
    /// </summary>
    /// <param name="svcKey"></param>
    /// <param name="many"></param>
    /// <returns></returns>
    bool IsRegistered(ServiceKey svcKey, bool many = false);
    
    /// <summary>
    /// Begins a new scope for the underlying container.
    /// </summary>
    /// <returns></returns>
    IScope BeginScope();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serviceType"></param>
    /// <param name="key"></param>
    /// <returns></returns>
    object GetService(Type serviceType, object key = null);

    /// <summary>
    /// Gets the service registered in the underlying container.
    /// </summary>
    /// <typeparam name="TService">The type of the service.</typeparam>
    /// /// <param name="key">The context key used to retrieve the given service.</param>
    /// <returns></returns>
    TService GetService<TService>(object key = null) where TService : class;

    /// <summary>
    /// Gets all services associated to the given TService type registered in the underlying container.
    /// </summary>
    /// <typeparam name="TService">The type of the service.</typeparam>
    /// /// <param name="key">The context key used to retrieve the given service.</param>
    /// <returns></returns>
    IEnumerable<TService> GetServices<TService>(object key = null) where TService : class;
}
