namespace ServiceResolver.Ioc;

/// <summary>
/// 
/// </summary>
public class TypeResolutionOptions
{
    /// <summary>
    /// 
    /// </summary>
    public bool IncludeObsoleted { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public bool IncludeGenericTypeDefinitions { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public bool IncludeReferencedAssemblies { get; set; }
}
