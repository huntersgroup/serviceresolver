using System;
using System.Reflection;
using ServiceResolver.Ioc.Modules;
using ServiceResolver.Ioc.Registers;

namespace ServiceResolver.Ioc;

/// <summary>
/// Represents a generic IOC container which is able to register / resolve services.
/// </summary>
/// <seealso cref="IServiceProvider" />
/// <seealso cref="System" />
public interface IServiceRegisterProvider
    : IServiceRegister, IGenericServiceProvider
{
    /// <summary>
    /// Registers the specified module.
    /// </summary>
    /// <param name="module">The module.</param>
    void Register(IServiceModule module);

    /// <summary>
    /// Registers the modules from the given assembly.
    /// </summary>
    /// <param name="assembly">The assembly.</param>
    /// <param name="includeObsoleted">registered also Obsolete modules </param>
    void RegisterModulesFrom(Assembly assembly, bool includeObsoleted = false);

    /// <summary>
    /// Builds a child service provider inheriting all registrations made by the calling service instance.
    /// </summary>
    /// <returns></returns>
    IServiceRegisterProvider BuildChildServiceProvider();

    /// <summary>
    /// Verifies if all services are registered correctly.
    /// The calling of this method must be after registering all services, because once verified the calling instance, the underlying container will be locked.
    /// </summary>
    void Verify();
}
