using System.Diagnostics;

namespace ServiceResolver.Ioc;

/// <summary>
/// 
/// </summary>
[DebuggerDisplay("disposable-transient: {this.DisposableTransientComponent}, lifeStyle-mismatch: {this.LifestyleMismatch}")]
public class SuppressDiagnostic
{
    /// <summary>
    /// 
    /// </summary>
    public bool DisposableTransientComponent { get; init; } = false;

    /// <summary>
    /// 
    /// </summary>
    public bool LifestyleMismatch { get; init; } = false;
}
