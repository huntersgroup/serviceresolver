using System;
using System.Diagnostics;

namespace ServiceResolver.Ioc;

/// <summary>
/// Represents another way to register services.
/// </summary>
[DebuggerDisplay("scope = {this.Scope}, has-factory: {this.Factory != null}, concreteType = {this.ConcreteType}")]
internal class RegistrationInfo : IRegistrationInfo
{
    /// <summary>
    /// Creates a new instance with relative parameters.
    /// </summary>
    /// <param name="concreteType"></param>
    /// <param name="scope"></param>
    /// <param name="factory"></param>
    internal RegistrationInfo(LifetimeScope scope, Type concreteType, Func<IGenericServiceProvider, object> factory)
    {
        this.Scope = scope;
        this.ConcreteType = concreteType;
        this.Factory = factory;
    }

    /// <inheritdoc />
    public Type ConcreteType { get; }

    /// <inheritdoc />
    public LifetimeScope Scope { get; }

    /// <inheritdoc />
    public Func<IGenericServiceProvider, object> Factory { get; }

    /// <inheritdoc />
    public SuppressDiagnostic SuppressDiagnostic { get; init; } = new();
}

/// <summary>
/// 
/// </summary>
/// <typeparam name="TService"></typeparam>
internal class RegistrationInfo<TService>
    : RegistrationInfo, IRegistrationInfo<TService>
    where TService : class
{
    internal RegistrationInfo(LifetimeScope scope)
        :base(scope, typeof(TService), null)
    {
    }

    internal RegistrationInfo(LifetimeScope scope, Func<IGenericServiceProvider, TService> factory)
        : base(scope, null, factory)
    {
    }
}

/// <summary>
/// 
/// </summary>
/// <typeparam name="TService"></typeparam>
public interface IRegistrationInfo<out TService> : IRegistrationInfo
    where TService : class
{
}

/// <summary>
/// 
/// </summary>
public interface IRegistrationInfo
{
    /// <summary>
    /// Gets the concrete type for this registration.
    /// <para>
    /// This property can be null if <see cref="Factory"/> property is set.
    /// </para>
    /// </summary>
    Type ConcreteType { get; }

    /// <summary>
    /// Gets the scope of this service registration.
    /// <para>
    /// If the value is null, it assumes the default value is set on underlying container.
    /// </para>L
    /// </summary>
    LifetimeScope Scope { get; }

    /// <summary>
    /// Gets the factory of this service.
    /// <para>
    /// This property can be null, in that case <see cref="ConcreteType"/> must be set.
    /// </para>
    /// </summary>
    Func<IGenericServiceProvider, object> Factory { get; }

    /// <summary>
    /// Gets the strategy for suppressing diagnostic warnings for the given registration.
    /// </summary>
    SuppressDiagnostic SuppressDiagnostic { get; }
}
