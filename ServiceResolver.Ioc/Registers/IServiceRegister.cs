using System;
using System.Collections.Generic;
using System.Reflection;
using ServiceResolver.Ioc.Caching;

namespace ServiceResolver.Ioc.Registers;

/// <summary>
/// Represents a generic way for registering services.
/// </summary>
public interface IServiceRegister
{
    /// <summary>
    /// Gets the local cache used to store temporally items (not registered into the underlying DI engine).
    /// </summary>
    IServiceCache Cache { get; }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="svcKey"></param>
    /// <param name="registrationInfo"></param>
    /// <returns></returns>
    IServiceRegister Register(ServiceKey svcKey, IRegistrationInfo registrationInfo);

    /// <summary>
    /// Register the specified <see cref="IRegistrationInfo"/> instance.
    /// </summary>
    /// <param name="serviceType"></param>
    /// <param name="registrationInfo"></param>
    /// <returns></returns>
    IServiceRegister Register(Type serviceType, IRegistrationInfo registrationInfo);

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    /// <param name="registrationInfo"></param>
    /// <returns></returns>
    IServiceRegister Register<TService>(IRegistrationInfo<TService> registrationInfo)
        where TService : class;

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    /// <typeparam name="TImplementor"></typeparam>
    /// <param name="registrationInfo"></param>
    /// <returns></returns>
    IServiceRegister Register<TService, TImplementor>(IRegistrationInfo<TImplementor> registrationInfo)
        where TService : class where TImplementor : class, TService;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="svcKey"></param>
    /// <param name="registrationInfo"></param>
    /// <returns></returns>
    IServiceRegister RegisterLazy(ServiceKey svcKey, IRegistrationInfo registrationInfo);

    /// <summary>
    /// Register the specified <see cref="IRegistrationInfo"/> instance in lazy mode.
    /// <para>
    /// The lazy mode means the given registration is registered whenever the specified serviceType is requested.
    /// </para>
    /// </summary>
    /// <param name="serviceType"></param>
    /// <param name="registrationInfo"></param>
    /// <returns></returns>
    IServiceRegister RegisterLazy(Type serviceType, IRegistrationInfo registrationInfo);

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    /// <param name="registrationInfo"></param>
    /// <returns></returns>
    IServiceRegister RegisterLazy<TService>(IRegistrationInfo<TService> registrationInfo)
        where TService : class;

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    /// <typeparam name="TImplementor"></typeparam>
    /// <param name="registrationInfo"></param>
    /// <returns></returns>
    IServiceRegister RegisterLazy<TService, TImplementor>(IRegistrationInfo<TImplementor> registrationInfo)
        where TService : class where TImplementor : class, TService;

    /// <summary>
    /// Registers the specified service type, It could be used by open generic registrations.
    /// </summary>
    /// <param name="serviceType">Type of the service.</param>
    /// <param name="implementorType">Type of the implementor.</param>
    /// <param name="scope">The scope.</param>
    /// <returns></returns>
    IServiceRegister Register(Type serviceType, Type implementorType, LifetimeScope? scope = null);

    /// <summary>
    /// Registers the specified service type, with the specified factory.
    /// </summary>
    /// <param name="serviceType"></param>
    /// <param name="serviceFactory"></param>
    /// <param name="scope"></param>
    /// <returns></returns>
    IServiceRegister Register(Type serviceType, Func<object> serviceFactory, LifetimeScope? scope = null);

    /// <summary>
    /// Registers the specified service type, with the specified factory.
    /// </summary>
    /// <param name="serviceType"></param>
    /// <param name="serviceCreator"></param>
    /// <param name="scope"></param>
    /// <returns></returns>
    IServiceRegister Register(Type serviceType, Func<IGenericServiceProvider, object> serviceCreator,
        LifetimeScope? scope = null);
    
    /// <summary>
    /// Registers the indicated service type using the specified scope.
    /// </summary>
    /// <typeparam name="TService">The type of the service.</typeparam>
    /// <typeparam name="TImplementor">The type of the service implementor.</typeparam>
    /// <param name="scope">The lifetime scope for the given service.</param>
    /// <returns>Returns the calling instance.</returns>
    IServiceRegister Register<TService, TImplementor>(LifetimeScope? scope = null)
        where TService : class where TImplementor : class, TService;

    /// <summary>
    /// Registers the indicated service type using the specified scope.
    /// </summary>
    /// <typeparam name="TConcreteService">The type of the concrete service.</typeparam>
    /// <param name="scope">The lifetime scope for the given service.</param>
    /// <returns>Returns the calling instance.</returns>
    IServiceRegister Register<TConcreteService>(LifetimeScope? scope = null)
        where TConcreteService : class;

    /// <summary>
    /// Registers the specified implementor type, It could be used by open generic registrations.
    /// </summary>
    /// <param name="implementorType">Type of the implementor.</param>
    /// <param name="scope">The scope.</param>
    /// <returns></returns>
    IServiceRegister Register(Type implementorType, LifetimeScope? scope = null);

    /// <summary>
    /// Registers the indicated service type using the specified service creator and lifetime scope.
    /// </summary>
    /// <typeparam name="TService">The type of the service.</typeparam>
    /// <param name="serviceCreator">The service creator.</param>
    /// <param name="scope">The lifetime scope for the given service.</param>
    /// <returns>Returns the calling instance.</returns>
    IServiceRegister Register<TService>(Func<TService> serviceCreator, LifetimeScope? scope = null)
        where TService : class;

    /// <summary>
    /// Registers the indicated service type using the specified service creator and lifetime scope.
    /// </summary>
    /// <typeparam name="TService">The type of the service.</typeparam>
    /// <param name="serviceCreator">The service creator.</param>
    /// <param name="scope">The lifetime scope for the given service.</param>
    /// <returns>>Returns the calling instance.</returns>
    IServiceRegister Register<TService>(Func<IGenericServiceProvider, TService> serviceCreator, LifetimeScope? scope = null)
        where TService : class;

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    /// <typeparam name="TDecorator"></typeparam>
    /// <param name="scope"></param>
    /// <returns></returns>
    public IServiceRegister RegisterDecorator<TService, TDecorator>(LifetimeScope? scope = null)
        where TService : class
        where TDecorator : class, TService;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serviceType"></param>
    /// <param name="decoratorType"></param>
    /// <param name="scope"></param>
    /// <returns></returns>
    IServiceRegister RegisterDecorator(Type serviceType, Type decoratorType, LifetimeScope? scope = null);

    /// <summary>
    /// Registers a conditional service, with a custom strategy using conditions for service resolutions.
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    /// <param name="conditionalRegister"></param>
    /// <returns></returns>
    IServiceRegister RegisterConditional<TService>(Action<IConditionalRegister<TService>> conditionalRegister)
        where TService : class;

    /// <summary>
    /// Registers a conditional service, with a custom strategy using conditions for service resolutions.
    /// </summary>
    /// <param name="serviceType"></param>
    /// <param name="conditionalRegister"></param>
    /// <returns></returns>
    IServiceRegister RegisterConditional(Type serviceType, Action<IConditionalRegister> conditionalRegister);

    /// <summary>
    /// Register initializer for the indicated service type using the specified Action.
    /// </summary>
    /// <typeparam name="TService">The type of the service.</typeparam>
    /// <param name="serviceInitialize">The service initializer Action.</param>
    /// <returns>>Returns the calling instance.</returns>
    IServiceRegister RegisterInitializer<TService>(Action<IGenericServiceProvider, TService> serviceInitialize)
        where TService : class;

    /// <summary>
    /// Register initializer for the indicated service type using the specified Action.
    /// </summary>
    /// <typeparam name="TService">The type of the service.</typeparam>
    /// <param name="serviceInitialize">The service initializer.</param>
    /// <returns>>Returns the calling instance.</returns>
    IServiceRegister RegisterInitializer<TService>(Action<TService> serviceInitialize)
        where TService : class;

    /// <summary>
    /// Registers all concrete type implements TServiceBase from the given assembly (and referenced Assemblies from assembly) 
    ///  </summary>
    /// <typeparam name="TServiceBase">The base type</typeparam>
    /// <param name="assembly">assembly</param>
    /// <param name="includeObsoleted">register obsolete classes</param>
    /// <param name="scope">registration scope</param>
    /// <returns>Returns the calling instance.</returns>
    IServiceRegister RegisterDerivedServices<TServiceBase>(Assembly assembly, bool includeObsoleted = false, LifetimeScope? scope = null)
        where TServiceBase : class;

    /// <summary>
    /// Registers all concrete type implements | derives the given serviceBase parameter from the given assembly (and referenced Assemblies from assembly)
    /// </summary>
    /// <param name="serviceBase"></param>
    /// <param name="assembly"></param>
    /// <param name="includeObsoleted"></param>
    /// <param name="scope"></param>
    /// <returns></returns>
    IServiceRegister RegisterDerivedServices(Type serviceBase, Assembly assembly, bool includeObsoleted = false,
        LifetimeScope? scope = null);

    /// <summary>
    /// Registers the many service implementors to the specified service type.
    /// </summary>
    /// <typeparam name="TService">The type of the service.</typeparam>
    /// <param name="implementorTypes">The implementor types.</param>
    /// <returns>Returns the calling instance.</returns>
    IServiceRegister RegisterMany<TService>(params Type[] implementorTypes)
        where TService : class;

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    /// <param name="implementorTypes"></param>
    /// <param name="scope"></param>
    /// <returns></returns>
    IServiceRegister RegisterMany<TService>(IEnumerable<Type> implementorTypes, LifetimeScope? scope = null)
        where TService : class;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serviceType"></param>
    /// <param name="implementorTypes"></param>
    /// <param name="scope"></param>
    /// <returns></returns>
    IServiceRegister RegisterMany(Type serviceType, IEnumerable<Type> implementorTypes, LifetimeScope? scope = null);

    /// <summary>
    /// Registers many service implementors to the specified service type.
    /// </summary>
    /// <typeparam name="TService">The type of the service.</typeparam>
    /// <param name="singletons">The singletons.</param>
    /// <returns></returns>
    IServiceRegister RegisterMany<TService>(params TService[] singletons)
        where TService : class;

    /// <summary>
    /// Registers many services implementors related to specified service type.
    /// </summary>
    /// <typeparam name="TService">The type of the service.</typeparam>
    /// <param name="assemblies">The assemblies used to find service implementors.</param>
    /// <returns></returns>
    IServiceRegister RegisterMany<TService>(params Assembly[] assemblies)
        where TService : class;

    /// <summary>
    /// Registers many services implementors related to specified service type, using a lifetime scope.
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    /// <param name="assemblies"></param>
    /// <param name="scope"></param>
    /// <returns></returns>
    IServiceRegister RegisterMany<TService>(IEnumerable<Assembly> assemblies, LifetimeScope? scope = null)
        where TService : class;

    /// <summary>
    /// Registers many services implementors related to specified service type.
    /// </summary>
    /// <param name="serviceType">The type of the service.</param>
    /// <param name="assemblies">The assemblies used to find service implementors.</param>
    /// <returns></returns>
    IServiceRegister RegisterMany(Type serviceType, params Assembly[] assemblies);

    /// <summary>
    /// Registers many services implementors related to specified service type, using a lifetime scope.
    /// </summary>
    /// <param name="serviceType"></param>
    /// <param name="assemblies"></param>
    /// <param name="scope"></param>
    /// <returns></returns>
    IServiceRegister RegisterMany(Type serviceType, IEnumerable<Assembly> assemblies, LifetimeScope? scope = null);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="svcKey"></param>
    /// <param name="assemblies"></param>
    /// <param name="scope"></param>
    /// <returns></returns>
    IServiceRegister RegisterMany(ServiceKey svcKey, IEnumerable<Assembly> assemblies, LifetimeScope? scope = null);

    /// <summary>
    /// Registers many services using the given <see cref="IRegistrationInfo"/> instance.
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    /// <param name="registrationInfos"></param>
    /// <returns></returns>
    IServiceRegister RegisterMany<TService>(IEnumerable<IRegistrationInfo> registrationInfos)
        where TService : class;

    /// <summary>
    /// Registers many services using the given <see cref="IRegistrationInfo{TService}"/> instance.
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    /// <param name="registrationInfos"></param>
    /// <returns></returns>
    IServiceRegister RegisterMany<TService>(IEnumerable<IRegistrationInfo<TService>> registrationInfos)
        where TService : class;

    /// <summary>
    /// Registers many services using the given <see cref="IEnumerable{RegistrationInfo}"/> instance.
    /// </summary>
    /// <param name="serviceType"></param>
    /// <param name="registrationInfos"></param>
    /// <returns></returns>
    IServiceRegister RegisterMany(Type serviceType, IEnumerable<IRegistrationInfo> registrationInfos);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="svcKey"></param>
    /// <param name="registrationInfos"></param>
    /// <returns></returns>
    IServiceRegister RegisterMany(ServiceKey svcKey, IEnumerable<IRegistrationInfo> registrationInfos);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="svcKey"></param>
    /// <param name="registrationInfos"></param>
    /// <returns></returns>
    IServiceRegister RegisterManyLazy(ServiceKey svcKey, IEnumerable<IRegistrationInfo> registrationInfos);

    /// <summary>
    /// Registers many services using the given <see cref="IEnumerable{IRegistrationInfo}"/> instance in a lazy way.
    /// </summary>
    /// <param name="serviceType"></param>
    /// <param name="registrationInfos"></param>
    /// <returns></returns>
    IServiceRegister RegisterManyLazy(Type serviceType, IEnumerable<IRegistrationInfo> registrationInfos);

    /// <summary>
    /// Registers many services using the given <see cref="IEnumerable{IRegistrationInfo}"/> instance in a lazy way.
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    /// <param name="registrationInfos"></param>
    /// <returns></returns>
    IServiceRegister RegisterManyLazy<TService>(IEnumerable<IRegistrationInfo<TService>> registrationInfos)
        where TService : class;

    /// <summary>
    /// Registers a resolver instance, which builds up a new resolution context, able to register other isolated services resolvable only by the resolver.
    /// </summary>
    /// <param name="serviceType">Type of the service.</param>
    /// <param name="implementorType">Type of the implementor.</param>
    /// <param name="scope">The lifetime scope.</param>
    /// <returns>Returns a new instance of IServiceRegister which register new services resolvable by the resolver associated.</returns>
    IServiceRegister RegisterResolver(Type serviceType, Type implementorType, LifetimeScope? scope = null);

    /// <summary>
    /// Registers a resolver instance, which builds up a new resolution context, able to register other isolated services resolvable only by the resolver.
    /// </summary>
    /// <typeparam name="TService">The type of the service to resolve.</typeparam>
    /// <typeparam name="TImplementor">The type of the implementor.</typeparam>
    /// <param name="scope">The lifetime scope.</param>
    /// <returns>Returns a new instance of IServiceRegister which register new services resolvable by the resolver associated.</returns>
    IServiceRegister RegisterResolver<TService, TImplementor>(LifetimeScope? scope = null)
        where TService : class
        where TImplementor : class, TService;

    /// <summary>
    /// Registers a resolver instance, which builds up a new resolution context, able to register other isolated services resolvable only by the resolver.
    /// </summary>
    /// <typeparam name="TService">The type of the service.</typeparam>
    /// <param name="scope">The lifetime scope.</param>
    /// <returns>Returns a new instance of IServiceRegister which register new services resolvable by the resolver associated.</returns>
    IServiceRegister RegisterResolver<TService>(LifetimeScope? scope = null)
        where TService : class;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="concreteType"></param>
    /// <param name="scope"></param>
    /// <returns></returns>
    IServiceRegister RegisterResolver(Type concreteType, LifetimeScope? scope = null);

    /// <summary>
    /// Registers a resolver instance, which builds up a new resolution context, able to register other isolated services resolvable only by the resolver.
    /// </summary>
    /// <typeparam name="TService">The type of the service.</typeparam>
    /// <param name="serviceCreator">The service creator.</param>
    /// <param name="scope">The lifetime scope.</param>
    /// <returns>Returns a new instance of IServiceRegister which register new services resolvable by the resolver associated.</returns>
    IServiceRegister RegisterResolver<TService>(Func<TService> serviceCreator, LifetimeScope? scope = null)
        where TService : class;

    /// <summary>
    /// Registers a resolver instance, which builds up a new resolution context, able to register other isolated services resolvable only by the resolver.
    /// </summary>
    /// <typeparam name="TService">The type of the service.</typeparam>
    /// <param name="serviceCreator">The service creator.</param>
    /// <param name="scope">The lifetime scope.</param>
    /// <returns>Returns a new instance of IServiceRegister which register new services resolvable by the resolver associated.</returns>
    IServiceRegister RegisterResolver<TService>(Func<IGenericServiceProvider, TService> serviceCreator,
        LifetimeScope? scope = null)
        where TService : class;

    /// <summary>
    /// Registers all concrete non-abstract services which implements the given generic type.
    /// </summary>
    /// <param name="genericServiceType">Type of the generic service.</param>
    /// <param name="assemblies">The assemblies.</param>
    /// <param name="scope"></param>
    /// <returns></returns>
    IServiceRegister RegisterGeneric(Type genericServiceType, IEnumerable<Assembly> assemblies, LifetimeScope? scope = null);
}
