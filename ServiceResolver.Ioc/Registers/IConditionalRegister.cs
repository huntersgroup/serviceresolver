using System;

namespace ServiceResolver.Ioc.Registers;

/// <summary>
/// Represents a generic way for resolving a specific service using custom conditions for specific resolutions.
/// </summary>
/// <typeparam name="TService"></typeparam>
public interface IConditionalRegister<in TService>
    where TService : class
{
    /// <summary>
    /// Register a service using the given conditional resolution.
    /// </summary>
    /// <typeparam name="TImplementor">Type to resolve related to service type registration</typeparam>
    /// <param name="condition">Expression to evaluate.</param>
    /// <param name="scope">The lifetime scope.</param>
    /// <returns></returns>
    IConditionalRegister<TService> Register<TImplementor>(Func<ConsumerService, bool> condition, LifetimeScope? scope = null)
        where TImplementor : class, TService;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="condition"></param>
    /// <param name="serviceFactory"></param>
    /// <param name="scope"></param>
    /// <returns></returns>
    [Obsolete("Use the other overload which these args: [serviceFactory, condition and scope]. This method will be removed on next major version.")]
    IConditionalRegister<TService> Register(Func<ConsumerService, bool> condition, Func<IGenericServiceProvider, TService> serviceFactory, LifetimeScope? scope = null);

    /// <summary>
    /// Register a service using the given conditional resolution, using the specified factory resolution.
    /// </summary>
    /// <param name="condition">Expression to evaluate.</param>
    /// <param name="serviceFactory">The function used to create the service.</param>
    /// <param name="scope">The lifetime scope.</param>
    /// <returns></returns>
    IConditionalRegister<TService> Register(Func<IGenericServiceProvider, TService> serviceFactory, Func<ConsumerService, bool> condition, LifetimeScope? scope = null);

    /// <summary>
    /// Register a default resolution for the given conditional resolution.
    /// <para>
    /// This method is used to register a fallback conditional registration, so it will be used when the other conditions weren't satisfied. 
    /// </para>
    /// </summary>
    /// <typeparam name="TImplementor">Type to resolve related to service type registration</typeparam>
    /// <param name="scope">The lifetime scope.</param>
    void Default<TImplementor>(LifetimeScope? scope = null)
        where TImplementor : class, TService;

    /// <summary>
    /// Register a default resolution for the given conditional resolution, using the specified factory resolution.
    /// </summary>
    /// <param name="serviceFactory"></param>
    /// <param name="scope">The lifetime scope.</param>
    void Default(Func<IGenericServiceProvider, TService> serviceFactory, LifetimeScope? scope = null);
}

/// <summary>
/// Represents a way to resolve services using custom conditions, for specific concrete type resolutions.
/// </summary>
public interface IConditionalRegister
{
    /// <summary>
    /// Gets the service type to register with conditions.
    /// </summary>
    Type ServiceType { get; }

    /// <summary>
    /// Register a service type which implements the current <see cref="ServiceType"/>.
    /// </summary>
    /// <param name="implementorType"></param>
    /// <param name="condition"></param>
    /// <param name="scope"></param>
    /// <returns></returns>
    IConditionalRegister Register(Type implementorType, Func<ConsumerService, bool> condition, LifetimeScope? scope = null);

    /// <summary>
    /// Register a service factory which returns an instance that implements the current <see cref="ServiceType"/>.
    /// </summary>
    /// <param name="condition"></param>
    /// <param name="serviceFactory"></param>
    /// <param name="scope"></param>
    /// <returns></returns>
    IConditionalRegister Register(Func<IGenericServiceProvider, object> serviceFactory, Func<ConsumerService, bool> condition, LifetimeScope? scope = null);

    /// <summary>
    /// Register a fallback condition when there aren't satisfied previous conditions.
    /// </summary>
    /// <param name="implementorType"></param>
    /// <param name="scope"></param>
    void Default(Type implementorType, LifetimeScope? scope = null);

    /// <summary>
    /// Register a fallback condition when there aren't satisfied previous conditions
    /// </summary>
    /// <param name="serviceFactory"></param>
    /// <param name="scope"></param>
    void Default(Func<IGenericServiceProvider, object> serviceFactory, LifetimeScope? scope = null);
}
