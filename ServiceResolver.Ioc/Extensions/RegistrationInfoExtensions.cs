using System;
using System.Collections.Generic;
using System.Linq;

namespace ServiceResolver.Ioc.Extensions;

/// <summary>
/// 
/// </summary>
public static class RegistrationInfoExtensions
{
    /// <summary>
    /// Validate the compability between the given registration with the given service type.
    /// </summary>
    /// <param name="registration"></param>
    /// <param name="serviceType"></param>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="InvalidOperationException"></exception>
    public static void Validate(this IRegistrationInfo registration, Type serviceType)
    {
        if (registration is null)
        {
            throw new ArgumentNullException(nameof(registration));
        }

        if (serviceType is null)
        {
            throw new ArgumentNullException(nameof(serviceType));
        }

        if (!(registration.ConcreteType is null ^ registration.Factory is null))
        {
            throw new ArgumentException(
                $"The given register is wrong, just one of its properties ({nameof(registration.ConcreteType)}, {nameof(registration.Factory)}) must be set.");
        }

        if (serviceType.IsGenericTypeDefinition)
        {
            if (registration.ConcreteType == null)
            {
                throw new InvalidOperationException(
                    $"The given {nameof(serviceType)} is open-generic, so the given registration.{nameof(registration.ConcreteType)} must be set.");
            }

            if (!registration.ConcreteType.IsGenericTypeDefinition)
            {
                throw new InvalidOperationException(
                    $"The given {nameof(serviceType)} is open-generic, so the given registration.{nameof(registration.ConcreteType)} must be open-generic too.");
            }
        }
        else
        {
            if (registration.ConcreteType != null)
            {
                if (registration.ConcreteType.IsGenericTypeDefinition)
                {
                    throw new InvalidOperationException(
                        $"The given {nameof(serviceType)} isn't open-generic, so the given registration.{nameof(registration.ConcreteType)} mustn't be open-generic.");
                }

                if (!serviceType.IsAssignableBy(registration.ConcreteType))
                {
                    throw new InvalidOperationException(
                        $"The given {nameof(serviceType)} isn't assignable by the given registration.{nameof(registration.ConcreteType)}.");
                }
            }

            // there's no way to deduce the return type with factories not compiled by consumer routines
            // so, an unsafe control is done, only in the cases of different object types.
            if (registration.Factory != null && typeof(object) != registration.Factory.Method.ReturnType && !serviceType.IsAssignableBy(registration.Factory.Method.ReturnType))
            {
                throw new InvalidOperationException(
                    $"The given {nameof(serviceType)} is not compatible with the return type of the given registration.Factory function, return type: {registration.Factory.Method.ReturnType}"
                );
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="registrations"></param>
    /// <param name="serviceType"></param>
    /// <exception cref="InvalidOperationException"></exception>
    public static void Validate(this IEnumerable<IRegistrationInfo> registrations, Type serviceType)
    {
        var index = 0;

        try
        {
            foreach (var registrationInfo in registrations)
            {
                registrationInfo.Validate(serviceType);
                index++;
            }
        }
        catch (Exception e)
        {
            throw new InvalidOperationException(
                $"The given service registration collection contains an error at the index {index}, see inner exception for details.", e);
        }
    }
}
