using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using IDisposable = System.IDisposable;

namespace ServiceResolver.Ioc.Extensions;

/// <summary>
/// 
/// </summary>
public static class ReflectionExtensions
{
    /// <summary>
    /// Gets the derived types from.
    /// </summary>
    /// <param name="assembly">The assembly.</param>
    /// <param name="baseType">Type of the base.</param>
    /// <param name="options"></param>
    /// <returns></returns>
    /// <exception cref="System.ArgumentNullException">assembly;The given assembly to use for loading service is null.</exception>
    public static IEnumerable<Type> GetDerivedTypesFrom(this Assembly assembly, Type baseType, TypeResolutionOptions options = null)
    {
        if (assembly == null)
        {
            throw new ArgumentNullException(nameof(assembly), "The given assembly to use for loading service is null.");
        }

        options ??= new TypeResolutionOptions();

        var assemblies = options.IncludeReferencedAssemblies
            ? assembly.GetReferencedAssemblies()
                .Select(Assembly.Load)
                .ToList()
            : [];

        assemblies.Add(assembly);
            
        var derivedTypes =
            assemblies.AsParallel()
                .SelectMany(
                    currentAss =>
                        baseType.GetImplementedTypes(currentAss, options)
                )
                .ToList();

        return derivedTypes;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="baseType"></param>
    /// <param name="currentAss"></param>
    /// <param name="options"></param>
    /// <returns></returns>
    public static IEnumerable<Type> GetImplementedTypes(this Type baseType, Assembly currentAss, TypeResolutionOptions options)
    {
        return currentAss.GetTypes()
            .Where(baseType.IsAssignableBy)
            .Where(type => type.IsConcreteClass(options.IncludeGenericTypeDefinitions))
            .Where(type => options.IncludeObsoleted || !type.IsObsolete());
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="assembly"></param>
    /// <param name="baseType"></param>
    /// <param name="options"></param>
    /// <returns></returns>
    /// <exception cref="InvalidOperationException"></exception>
    public static IEnumerable<ServiceResolution> GetResolutionFromGenericImplementations(this Assembly assembly, Type baseType, TypeResolutionOptions options = null)
    {
        if (!baseType.IsGenericType)
        {
            throw new InvalidOperationException($"The given service type must be generic, type: {baseType.FullName}");
        }

        if (options == null)
        {
            options = new TypeResolutionOptions();
        }
        else
        {
            options.IncludeGenericTypeDefinitions = false;
        }

        var types = assembly.GetDerivedTypesFrom(baseType, options);
        var resolutions = new List<ServiceResolution>();

        foreach (var type in types)
        {
            var interfacesTypes = type.GetInterfaces();
            if (interfacesTypes.Any())
            {
                var genArgs = interfacesTypes
                    .First()
                    .GetGenericArguments();

                resolutions.Add(new ServiceResolution(baseType.MakeGenericType(genArgs), type));
            }
        }

        return resolutions;
    }

    /// <summary>
    /// Indicates if the given type is a concrete class, so not an interface or abstract.
    /// </summary>
    /// <param name="type"></param>
    /// <param name="includeGenericTypeDefinitions"></param>
    /// <returns></returns>
    public static bool IsConcreteClass(this Type type, bool includeGenericTypeDefinitions = false)
    {
        return type.IsClass && !type.IsInterface && !type.IsAbstract && (includeGenericTypeDefinitions || !type.IsGenericTypeDefinition);
    }

    /// <summary>
    /// Determines whether this instance is obsolete.
    /// </summary>
    /// <param name="obj">The object.</param>
    /// <returns></returns>
    public static bool IsObsolete(this object obj)
    {
        return obj.GetType().IsObsolete();
    }

    /// <summary>
    /// Determines whether this instance is obsolete.
    /// </summary>
    /// <param name="type">The type.</param>
    /// <returns></returns>
    public static bool IsObsolete(this Type type)
    {
        return type.IsDecorated<ObsoleteAttribute>();
    }

    /// <summary>
    /// Determines whether this instance type is decorated with the TAttribute attribute.
    /// </summary>
    /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
    /// <param name="obj">The object.</param>
    /// <returns></returns>
    public static bool IsDecorated<TAttribute>(this object obj)
        where TAttribute : Attribute
    {
        return obj.GetType().IsDecorated<TAttribute>();
    }

    /// <summary>
    /// Determines whether this instance type is decorated with the TAttribute attribute.
    /// </summary>
    /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
    /// <param name="type">The type.</param>
    /// <returns></returns>
    public static bool IsDecorated<TAttribute>(this Type type)
        where TAttribute : Attribute
    {
        return type.GetCustomAttributes().Any(attr => attr.GetType() == typeof(TAttribute));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="method"></param>
    /// <param name="attributes"></param>
    /// <returns></returns>
    public static bool HasAnyParamsDecorated(this MethodBase method, params Type[] attributes)
    {
        if (method is null)
        {
            return false;
        }

        var parameters = method.GetParameters();

        foreach (var parameter in parameters)
        {
            var assert = parameter.CustomAttributes.Any(data => attributes.Contains(data.AttributeType));
            if (assert)
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="type"></param>
    /// <param name="attributes"></param>
    /// <param name="includeNonPublic"></param>
    /// <returns></returns>
    public static bool HasCtorWithAnyParamsDecorated(this Type type, Type[] attributes, bool includeNonPublic = false)
    {
        if (type is null)
        {
            return false;
        }

        var flags = BindingFlags.Instance | BindingFlags.Public;

        if (includeNonPublic)
        {
            flags |= BindingFlags.NonPublic;
        }

        return type.GetConstructors(flags).Any(ctor => ctor.HasAnyParamsDecorated(attributes));
    }

    /// <summary>
    /// Tries to dispose the given instance, It's depends on if implements IDisposable interface.
    /// </summary>
    /// <param name="instance"></param>
    public static void TryToDispose(this object instance)
    {
        (instance as IDisposable)?.Dispose();
    }

    /// <summary>
    /// Gets true if the baseType is assignable by concreteType, considering open generic types.
    /// </summary>
    /// <param name="baseType"></param>
    /// <param name="concreteType"></param>
    /// <returns></returns>
    public static bool IsAssignableBy(this Type baseType, Type concreteType)
    {
        if (baseType == null || concreteType == null)
        {
            return false;
        }

        if (baseType.IsAssignableFrom(concreteType))
        {
            return true;
        }

        var interfaceTypes = concreteType.GetInterfaces()
            .Select(type => type.GetGenTypeDefinitionOrItSelf())
            .ToList();

        var baseTypeGen = baseType.GetGenTypeDefinitionOrItSelf();

        foreach (var interfaceType in interfaceTypes)
        {
            if (interfaceType == baseTypeGen)
            {
                return true;
            }
        }

        if (concreteType.GetGenTypeDefinitionOrItSelf() == baseTypeGen)
        {
            return true;
        }

        var bType = concreteType.BaseType;

        return bType != null && baseTypeGen.IsAssignableBy(bType);
    }

    /// <summary>
    /// Gets true if the given instance implements <see cref="IEnumerable{T}"/> contract.
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public static bool IsEnumerable(this Type type)
    {
        return typeof(IEnumerable<>).IsAssignableBy(type);
    }

    /// <summary>
    /// Gets true if the given instance implements <see cref="IEnumerable{T}"/> contract, but the given type is generic too.
    ///
    /// <para>
    /// This extension method is similar to <see cref="IsEnumerable"/>, but it excludes not generic-types (e.g. <see cref="string"/> type).
    /// </para>
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public static bool IsGenEnumerable(this Type type)
    {
        return type.IsGenericType && type.IsEnumerable();
    }

    /// <summary>
    /// Tries to get the generic type of the given <see cref="IEnumerable{T}"/> type.
    /// <para>
    /// Consider it works only with classes or interfaces which implements | inherits by <see cref="IEnumerable{T}"/> contract.
    /// </para>
    /// </summary>
    /// <param name="type"></param>
    /// <param name="genericType"></param>
    /// <returns></returns>
    public static bool TryGetEnumerableGenType(this Type type, out Type genericType)
    {
        if (type.IsGenEnumerable())
        {
            var genDefEnum = typeof(IEnumerable<>);

            var enumType = type.GetGenTypeDefinitionOrItSelf() == genDefEnum
                ? type
                : type.GetInterfaces().First(t => t.GetGenTypeDefinitionOrItSelf() == genDefEnum);

            genericType = enumType.GetGenericArguments()[0];
            return true;
        }

        genericType = null;

        return false;
    }

    /// <summary>
    /// Compares the given two types if they're equals, or they're equivalent in the case of open/close generic types.
    /// </summary>
    /// <param name="sxType"></param>
    /// <param name="rxType"></param>
    /// <returns></returns>
    public static bool IsCompatible(this Type sxType, Type rxType)
    {
        return sxType == rxType || sxType.GetGenTypeDefinitionOrItSelf() == rxType.GetGenTypeDefinitionOrItSelf();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="type"></param>
    /// <param name="key"></param>
    /// <returns></returns>
    public static ServiceKey AsServiceKey(this Type type, object key = null)
    {
        return new ServiceKey(type, key);
    }

    /// <summary>
    /// Get the generic type definition in the case of generics, otherwise it returns itself.
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    private static Type GetGenTypeDefinitionOrItSelf(this Type type)
    {
        return type.IsGenericType ? type.GetGenericTypeDefinition() : type;
    }
}
