using System;

namespace ServiceResolver.Ioc.Extensions;

/// <summary>
/// 
/// </summary>
public static class LifetimeScopeExtensions
{
    /// <summary>
    /// Creates a new <see cref="IRegistrationInfo"/> instance using the given scope parameter, and using the given generic type.
    /// </summary>
    /// <typeparam name="TConcreteType"></typeparam>
    /// <param name="scope"></param>
    /// <param name="diagnostic"></param>
    /// <returns></returns>
    public static IRegistrationInfo<TConcreteType> CreateRegistration<TConcreteType>(this LifetimeScope scope, SuppressDiagnostic diagnostic = null)
        where TConcreteType : class
    {
        return new RegistrationInfo<TConcreteType>(scope)
        {
            SuppressDiagnostic = diagnostic ?? new SuppressDiagnostic()
        };
    }

    /// <summary>
    /// Creates a new <see cref="IRegistrationInfo"/> instance using the given scope parameter, and using the given concrete type.
    /// </summary>
    /// <param name="scope"></param>
    /// <param name="concreteType"></param>
    /// <param name="diagnostic"></param>
    /// <returns></returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static IRegistrationInfo CreateRegistration(this LifetimeScope scope, Type concreteType, SuppressDiagnostic diagnostic = null)
    {
        if (concreteType is null)
        {
            throw new ArgumentNullException(nameof(concreteType), "The given parameter cannot be null to build a RegistrationInfo instance.");
        }

        return new RegistrationInfo(scope, concreteType, null)
        {
            SuppressDiagnostic = diagnostic ?? new SuppressDiagnostic()
        };
    }

    /// <summary>
    /// Creates a new <see cref="IRegistrationInfo{TService}"/> instance using the given scope parameter, and using the given factory service resolver.
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    /// <param name="scope"></param>
    /// <param name="factory"></param>
    /// <param name="diagnostic"></param>
    /// <returns></returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static IRegistrationInfo<TService> CreateRegistration<TService>(this LifetimeScope scope, Func<IGenericServiceProvider, TService> factory, SuppressDiagnostic diagnostic = null)
        where TService : class
    {
        if (factory is null)
        {
            throw new ArgumentNullException(nameof(factory), "The given parameter cannot be null to build a RegistrationInfo instance.");
        }

        return new RegistrationInfo<TService>(scope, factory)
        {
            SuppressDiagnostic = diagnostic ?? new SuppressDiagnostic()
        };
    }
}
