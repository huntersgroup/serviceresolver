using System;
using ServiceResolver.Ioc.Exceptions;

namespace ServiceResolver.Ioc.Extensions;

/// <summary>
/// 
/// </summary>
public static class GenericServiceProviderExtensions
{
    private const string SingleRegistrationErrorMessage = "The given service type cannot be registered more than once.";
    private const string ManyRegistrationErrorMessage = "The given service collection type cannot be registered more than once.";

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serviceProvider"></param>
    /// <param name="svcKey"></param>
    /// <param name="many"></param>
    /// <exception cref="ServiceRegistrationException"></exception>
    public static void ThrowExceptionIfRegistered(this IGenericServiceProvider serviceProvider, ServiceKey svcKey, bool many = false)
    {
        if (!serviceProvider.IsRegistered(svcKey, many))
        {
            return;
        }

        var message = many ? ManyRegistrationErrorMessage : SingleRegistrationErrorMessage;

        throw new ServiceRegistrationException(svcKey.Type, message);
    }
}
