namespace ServiceResolver.Ioc;

/// <summary>
/// Indicates the lifetime scope for services resolved by IOC container.
/// </summary>
public enum LifetimeScope
{
    /// <summary>
    /// Indicates that a service is created whenever It's resolved.
    /// </summary>
    Transient = 0,

    /// <summary>
    /// Indicates that a service is reused in the same thread.
    /// </summary>
    Scoped = 2,

    /// <summary>
    /// Indicates that an unique service will be used whenever It's resolved.
    /// </summary>
    Singleton = 5
}
