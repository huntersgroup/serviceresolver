**ServiceResolver.Ioc** is an abstraction about a standard way to register and resolve services, using a custom implementation, or encapsulating an underlying DI engine.

for details, see the [documentation](https://bitbucket.org/huntersgroup/serviceresolver/wiki/Home).
