namespace ServiceResolver.Ioc;

/// <summary>
/// 
/// </summary>
public class SingleRegistration : ServiceRegistration
{
    /// <summary>
    /// 
    /// </summary>
    public IRegistrationInfo Registration { get; init; }

    ///<inheritdoc />
    public override bool Equals(object obj)
    {
        if (obj is SingleRegistration reg)
        {
            return this.ServiceKey == reg.ServiceKey;
        }

        return false;
    }

    ///<inheritdoc />
    public override int GetHashCode()
    {
        return this.ServiceKey.GetHashCode();
    }
}
