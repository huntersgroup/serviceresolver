using System;

namespace ServiceResolver.Ioc.Exceptions;

/// <summary>
/// Represents a generic exception whenever a service isn't registered into underlying service provider.
/// </summary>
/// <seealso cref="System.Exception" />
public class ServiceUnavailableException
    : Exception
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ServiceUnavailableException"/> class.
    /// </summary>
    /// <param name="serviceType">Type of the service.</param>
    /// <param name="message">The message.</param>
    public ServiceUnavailableException(Type serviceType, string message)
        : base(message)
    {
        ServiceType = serviceType;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ServiceUnavailableException"/> class.
    /// </summary>
    /// <param name="serviceType">Type of the service.</param>
    /// <param name="message">The message.</param>
    /// <param name="innerException">The inner exception.</param>
    public ServiceUnavailableException(Type serviceType, string message, Exception innerException)
        : base(message, innerException)
    {
        ServiceType = serviceType;
    }

    /// <summary>
    /// Gets the type of the service.
    /// </summary>
    /// <value>
    /// The type of the service.
    /// </value>
    public Type ServiceType { get; }
}
