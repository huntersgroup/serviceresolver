using System;

namespace ServiceResolver.Ioc.Exceptions;

/// <summary>
/// Represents a generic exception thrown by service provider.
/// </summary>
/// <seealso cref="System.Exception" />
public class ServiceRegistrationException : Exception
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ServiceRegistrationException"/> class.
    /// </summary>
    /// <param name="serviceType">Type of the service.</param>
    /// <param name="message">The message.</param>
    public ServiceRegistrationException(Type serviceType, string message)
        : base(message)
    {
        this.ServiceType = serviceType;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ServiceRegistrationException"/> class.
    /// </summary>
    /// <param name="serviceType">Type of the service.</param>
    /// <param name="message">The message.</param>
    /// <param name="innerException">The inner exception.</param>
    public ServiceRegistrationException(Type serviceType, string message, Exception innerException)
        : base(message, innerException)
    {
        this.ServiceType = serviceType;
    }

    /// <summary>
    /// Gets the type of the service.
    /// </summary>
    /// <value>
    /// The type of the service.
    /// </value>
    public Type ServiceType { get; }
}
