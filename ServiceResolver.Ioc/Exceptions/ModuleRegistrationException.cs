using System;

namespace ServiceResolver.Ioc.Exceptions;

/// <summary>
/// Represents an exception whenever occurs an error during module registration.
/// </summary>
public class ModuleRegistrationException : Exception
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ModuleRegistrationException"/> class.
    /// </summary>
    /// <param name="moduleType">Type of the module.</param>
    /// <param name="message">The message.</param>
    public ModuleRegistrationException(Type moduleType, string message)
        : base(message)
    {
        ModuleType = moduleType;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ModuleRegistrationException"/> class.
    /// </summary>
    /// <param name="moduleType">Type of the module.</param>
    /// <param name="message">The message.</param>
    /// <param name="innerException">The inner exception.</param>
    public ModuleRegistrationException(Type moduleType, string message, Exception innerException)
        : base(message, innerException)
    {
        ModuleType = moduleType;
    }

    /// <summary>
    /// Gets the type of the module.
    /// </summary>
    /// <value>
    /// The type of the module.
    /// </value>
    public Type ModuleType { get; }
}
