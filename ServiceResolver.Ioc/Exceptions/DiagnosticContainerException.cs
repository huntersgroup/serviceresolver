using System;
using System.Collections.Generic;
using System.Linq;

namespace ServiceResolver.Ioc.Exceptions;

/// <summary>
/// Represents an exception whenever a service resolver has some dependencies not registered correctly.
/// </summary>
/// <seealso cref="System.Exception" />
public class DiagnosticContainerException : Exception
{
    /// <summary>
    /// Initializes a new instance of the <see cref="DiagnosticContainerException"/> class.
    /// </summary>
    /// <param name="message">The message.</param>
    public DiagnosticContainerException(string message)
        : base(message)
    {
        this.InnerExceptions = Enumerable.Empty<Exception>();
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="DiagnosticContainerException"/> class.
    /// </summary>
    /// <param name="message">The message.</param>
    /// <param name="innerException">The inner exception.</param>
    public DiagnosticContainerException(string message, Exception innerException)
        : base(message, innerException)
    {
        this.InnerExceptions = Enumerable.Empty<Exception>();
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="DiagnosticContainerException"/> class.
    /// </summary>
    /// <param name="message">The message.</param>
    /// <param name="innerExceptions"></param>
    public DiagnosticContainerException(string message, IEnumerable<Exception> innerExceptions)
        : base(message)
    {
        this.InnerExceptions = new List<Exception>(innerExceptions);
    }

    /// <summary>
    /// Inner exceptions that describe internal errors whenever service providers verify service registrations.
    /// </summary>
    public IEnumerable<Exception> InnerExceptions { get; }
}
