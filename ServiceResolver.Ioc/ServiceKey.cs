using System;

namespace ServiceResolver.Ioc;

/// <summary>
/// Represents a way to search services registered into the underlying container.
/// </summary>
/// <param name="Type">Gets the service type reference for this instance.</param>
/// <param name="Key">Gets the context key of this service type.</param>
public record ServiceKey(Type Type, object Key = null);
