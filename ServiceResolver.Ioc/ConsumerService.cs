using System;

namespace ServiceResolver.Ioc;

/// <summary>
/// Represents information about service consumer in a service resolution context.
/// </summary>
public class ConsumerService
{
    /// <summary>
    /// Creates a new instance for <see cref="ConsumerService"/>.
    /// </summary>
    /// <param name="consumerType"></param>
    public ConsumerService(Type consumerType)
    {
        this.ConsumerType = consumerType;
    }

    /// <summary>
    /// Gets the implementation type of the consumer of the component that should be resolved / created.
    /// </summary>
    public Type ConsumerType { get; }
}
