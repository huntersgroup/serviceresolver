using System;

namespace ServiceResolver.Ioc.Caching;

/// <summary>
/// Represents an item stored into the cache.
/// </summary>
public class CacheItem
{
    /// <summary>
    /// Gets or sets the key related to this item.
    /// </summary>
    public CacheKey Key { get; init; }

    /// <summary>
    /// Gets or sets the item to cache.
    /// </summary>
    public object Item { get; init; }
    
    /// <summary>
    /// Calls a dispose method (if it implements <see cref="IDisposable"/> or <see cref="IAsyncDisposable"/>).
    /// </summary>
    public bool Dispose { get; init; }

    ///<inheritdoc />
    public override bool Equals(object obj)
    {
        if (obj is CacheItem item)
        {
            return this.Key.Equals(item.Key);
        }

        return false;
    }

    ///<inheritdoc />
    public override int GetHashCode()
    {
        return this.Key.GetHashCode();
    }
}

/// <summary>
/// 
/// </summary>
public record CacheKey
{
    /// <summary>
    /// 
    /// </summary>
    public string Key { get; init; } = string.Empty;

    /// <summary>
    /// 
    /// </summary>
    public Type Type { get; init; }
}
