namespace ServiceResolver.Ioc.Caching;

/// <summary>
/// Represents a generic way for caching services, available during service registration. 
/// </summary>
public interface IServiceCache
{
    /// <summary>
    /// Gets the item related to the given key.
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    object Get(CacheKey key);

    /// <summary>
    /// Try to get an item related to the given key.
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    /// <param name="key"></param>
    /// <returns></returns>
    TData Get<TData>(string key = null);

    /// <summary>
    /// Try to get an item related to the given key.
    /// </summary>
    /// <param name="key"></param>
    /// <param name="item"></param>
    /// <returns></returns>
    bool TryGet(CacheKey key, out object item);

    /// <summary>
    /// Try to add a new item if no item exists with the same key.
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    /// <param name="item"></param>
    /// <param name="key"></param>
    /// <returns></returns>
    bool TryAdd<TData>(TData item, string key = null);

    /// <summary>
    /// Try to add the given item if no one exists with the same key.
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    bool TryAdd(CacheItem item);

    /// <summary>
    /// Adds or inserts a new item, it depends on the existence of another item with the same key.
    /// </summary>
    /// <param name="item"></param>
    /// <param name="key"></param>
    /// <returns></returns>
    void Upsert<TData>(TData item, string key = null);

    /// <summary>
    /// Adds or inserts a new item, it depends on the existence of another item with the same key.
    /// </summary>
    /// <param name="item"></param>
    void Upsert(CacheItem item);

    /// <summary>
    /// Removes an item if exits one with the given key.
    /// </summary>
    /// <param name="key"></param>
    /// <returns>returns true if the deletion was possible, otherwise false.</returns>
    bool Remove(CacheKey key);
}
