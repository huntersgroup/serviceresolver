using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace ServiceResolver.Ioc.Caching;

/// <summary>
/// Represents a local cache to store temporally items without the need to register into underlying DI engine.
/// </summary>
public class ServiceCache : IServiceCache, IDisposable, IAsyncDisposable
{
    private readonly ConcurrentDictionary<CacheKey, CacheItem> items = new();
    private bool disposed;

    ///<inheritdoc />
    public object Get(CacheKey key)
    {
        ThrowIfInvalid(key);

        if (this.TryGet(key, out var item))
        {
            return item;
        }

        throw new NullReferenceException($"No item with the relative key: {key}");
    }

    ///<inheritdoc />
    public TData Get<TData>(string key = null)
    {
        var cacheKey = new CacheKey
        {
            Key = key ?? string.Empty,
            Type = typeof(TData)
        };

        if (this.TryGet(cacheKey, out var item))
        {
            return (TData)item;
        }

        throw new NullReferenceException($"No item with the relative key: {key}");
    }

    ///<inheritdoc />
    public bool TryGet(CacheKey key, out object item)
    {
        ThrowIfInvalid(key);

        if (this.items.TryGetValue(key, out var value))
        {
            item = value.Item;
            return true;
        }

        item = default;
        return false;
    }

    ///<inheritdoc />
    public bool TryAdd<TData>(TData item, string key = null)
    {
        var cacheItem = new CacheItem
        {
            Key = new CacheKey { Key = key ?? string.Empty, Type = typeof(TData) },
            Item = item
        };

        return this.TryAdd(cacheItem);
    }

    ///<inheritdoc />
    public bool TryAdd(CacheItem item)
    {
        ThrowIfNullReference(item);
        ThrowIfInvalid(item.Key);

        return this.items.TryAdd(item.Key, item);
    }

    ///<inheritdoc />
    public void Upsert<TData>(TData item, string key = null)
    {
        ThrowIfNullReference(item);

        var cacheItem = new CacheItem
        {
            Key = new CacheKey { Key = key ?? string.Empty, Type = typeof(TData) },
            Item = item
        };

        this.Upsert(cacheItem);
    }

    ///<inheritdoc />
    public void Upsert(CacheItem item)
    {
        ThrowIfNullReference(item);
        ThrowIfInvalid(item.Key);

        if (!this.items.TryAdd(item.Key, item))
        {
            this.items[item.Key] = item;
        }
    }

    ///<inheritdoc />
    public bool Remove(CacheKey key)
    {
        ThrowIfInvalid(key);

        return this.items.TryRemove(key, out _);
    }

    ///<inheritdoc />
    public void Dispose() => this.DisposeAsync().AsTask().GetAwaiter().GetResult();

    ///<inheritdoc />
    public async ValueTask DisposeAsync()
    {
        if (this.disposed)
        {
            return;
        }

        this.disposed = true;

        foreach (var item in this.items.Values)
        {
            var instance = item.Item;

            if (!item.Dispose)
            {
                continue;
            }

            try
            {
                switch (instance)
                {
                    case IAsyncDisposable asyncDisp:
                        await asyncDisp.DisposeAsync();
                        break;
                    case IDisposable disp:
                        disp.Dispose();
                        break;
                }
            }
            catch
            {
                // nothing to do here !!
            }
        }

        this.items.Clear();
    }

    private static void ThrowIfInvalid(CacheKey key)
    {
        if (key is null)
        {
            throw new ArgumentNullException(nameof(key));
        }

        if (key.Key is null)
        {
            throw new ArgumentException($"The {nameof(CacheKey)}.{nameof(key.Key)} cannot be null.");
        }

        if (key.Type is null)
        {
            throw new ArgumentException($"The {nameof(CacheKey)}.{nameof(key.Type)} cannot be null.");
        }
    }

    private static void ThrowIfNullReference(object instance)
    {
        if (instance is null)
        {
            throw new ArgumentNullException(nameof(instance));
        }
    }
}
