using System;
using System.Diagnostics;

namespace ServiceResolver.Ioc;

/// <summary>
/// 
/// </summary>
[DebuggerDisplay("key: {this.ServiceKey.Key}, type: {this.ServiceKey.Type}, is-generic: {this.ServiceKey.Type.IsGenericType}, is-open-generic: {this.ServiceKey.Type.IsGenericTypeDefinition}")]
public class ServiceRegistration
{
    /// <summary>
    /// 
    /// </summary>
    [Obsolete("Use the property ServiceKey that includes ServiceType reference.")]
    public Type ServiceType
    {
        get => this.ServiceKey.Type;
        init
        {
            this.ServiceKey = new ServiceKey(value);
        }
    }

    /// <summary>
    /// Gets the key of this instance.
    /// </summary>
    public ServiceKey ServiceKey { get; init; }
}
