using ServiceResolver.Ioc.Registers;

namespace ServiceResolver.Ioc.Modules;

/// <summary>
/// 
/// </summary>
public interface IServiceModule
{
    /// <summary>
    /// Initializes the specified main container.
    /// </summary>
    /// <param name="serviceRegister">The main container.</param>
    void Initialize(IServiceRegister serviceRegister);
}
