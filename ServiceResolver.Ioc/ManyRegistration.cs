using System.Collections.Generic;

namespace ServiceResolver.Ioc;

/// <summary>
/// 
/// </summary>
public class ManyRegistration : ServiceRegistration
{
    /// <summary>
    /// 
    /// </summary>
    public IEnumerable<IRegistrationInfo> Registrations { get; init; }

    ///<inheritdoc />
    public override bool Equals(object obj)
    {
        if (obj is ManyRegistration reg)
        {
            return this.ServiceKey == reg.ServiceKey;
        }

        return false;
    }

    ///<inheritdoc />
    public override int GetHashCode()
    {
        return this.ServiceKey.GetHashCode();
    }
}
