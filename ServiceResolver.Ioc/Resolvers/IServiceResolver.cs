using System;

namespace ServiceResolver.Ioc.Resolvers;

/// <summary>
/// Represents a generic way for resolving a specific service.
/// </summary>
public interface IServiceResolver
{
    /// <summary>
    /// Gets the type of the service registered.
    /// </summary>
    /// <value>
    /// The type of the service.
    /// </value>
    Type ServiceType { get; }

    /// <summary>
    /// Gets the service.
    /// </summary>
    /// <returns></returns>
    object GetService(Type compatibleType = null);
}

/// <summary>
/// Represents a generic way for resolving a registered service.
/// </summary>
/// <typeparam name="TCService">The type of the service.</typeparam>
public interface IServiceResolver<out TCService>
    : IServiceResolver
{
    /// <summary>
    /// Resolves the registered service.
    /// </summary>
    /// <returns></returns>
    TCService Resolve();
}
