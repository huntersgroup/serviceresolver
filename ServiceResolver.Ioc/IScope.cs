using System;

namespace ServiceResolver.Ioc;

/// <summary>
/// Represents a specific scope for lifetime instances.
/// </summary>
public interface IScope : IDisposable
{
    /// <summary>
    /// Gets an identifier for this instance.
    /// </summary>
    string Id { get; }

    /// <summary>
    /// Gets a boolean value if this instance was disposed.
    /// </summary>
    bool IsDisposed { get; }
}
