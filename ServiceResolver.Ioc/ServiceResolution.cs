using System;
using System.Diagnostics;

namespace ServiceResolver.Ioc;

/// <summary>
/// Represents a relationship between a service component with one of its implementation type.
/// </summary>
[DebuggerDisplay("service: {this.Service}, implementation: {this.Implementation}")]
public class ServiceResolution
{
    /// <summary>
    /// Service type
    /// </summary>
    public Type Service { get; }

    /// <summary>
    /// Implementation type
    /// </summary>
    public Type Implementation { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="ServiceResolution"/> class.
    /// </summary>
    /// <param name="service">The service.</param>
    /// <param name="implementation">The implementation.</param>
    public ServiceResolution(Type service, Type implementation)
    {
        this.Service = service;
        this.Implementation = implementation;
    }
}
