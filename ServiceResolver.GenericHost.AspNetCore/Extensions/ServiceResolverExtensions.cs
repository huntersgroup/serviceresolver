using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.DependencyInjection;
using ServiceResolver.Ioc.AspNetCore.Hosting;
using ServiceResolver.Ioc.AspNetCore.Http;
using ServiceResolver.Ioc.AspNetCore.Mvc;
using ServiceResolver.Ioc.Extensions;
using ServiceResolver.Ioc.Registers;

namespace ServiceResolver.Ioc.AspNetCore.Extensions;

/// <summary>
/// 
/// </summary>
public static class ServiceResolverExtensions
{
    /// <summary>
    /// Adds basic registrations into <see cref="IServiceRegister"/> instance for ASP.NET Core.
    /// </summary>
    /// <typeparam name="TStartupFilter">An <see cref="IStartupFilter"/> custom implementation.</typeparam>
    /// <param name="serviceRegister">The <see cref="IServiceRegister"/> instance to use for this integration.</param>
    /// <returns></returns>
    public static IServiceRegister AddAspNetCore<TStartupFilter>(this IServiceRegister serviceRegister)
        where TStartupFilter : class, IStartupFilter
    {
        var startupScopeReg = LifetimeScope.Singleton.CreateRegistration<TStartupFilter>();

        // retrieve the IServiceCollection instance, previously added into local cache.
        var svcCollection = serviceRegister.Cache.Get<IServiceCollection>();

        if (svcCollection.TryGetDescriptors<IStartupFilter>(out var services))
        {
            var registrations = services
                .Select(descriptor => descriptor.MakeRegistration(() => new SuppressDiagnostic
                {
                    DisposableTransientComponent = true,
                    LifestyleMismatch = true
                }))
                .ToList();

            registrations.Add(startupScopeReg);

            serviceRegister.RegisterMany(typeof(IStartupFilter), registrations);
        }
        else
        {
            serviceRegister.RegisterMany(typeof(IStartupFilter), [
                startupScopeReg
            ]);
        }

        serviceRegister
            //.Register<IStartupFilter, TStartupFilter>()
            .Register<IHttpContextAccessor, HttpContextAccessor>(LifetimeScope.Singleton);

        return serviceRegister;
    }

    /// <summary>
    /// Adds basic registrations into <see cref="IServiceRegister"/> instance for ASP.NET Core, using a custom <see cref="IStartupFilter"/> implementation.
    /// </summary>
    /// <param name="serviceRegister">The <see cref="IServiceRegister"/> instance to use for this integration.</param>
    /// <returns></returns>
    public static IServiceRegister AddAspNetCore(this IServiceRegister serviceRegister)
    {
        // A new decorator for IHttpContextFactory instance, used to manipulate HttpContext instances before return call.

        return serviceRegister.RegisterDecorator<IHttpContextFactory, HttpContextFactoryDecorator>(LifetimeScope.Singleton)
            .AddAspNetCore<RequestScopeStartupFilter>();
    }

    /// <summary>
    /// Register all application's controllers in <see cref="IServiceRegister"/> instance, permitting <see cref="IServiceRegister"/> instance to create those controllers, using the given <see cref="LifetimeScope"/> instance.
    /// </summary>
    /// <typeparam name="TControllerActivator">An <see cref="IControllerActivator"/> custom implementation.</typeparam>
    /// <param name="serviceRegister">The <see cref="IServiceRegister"/> instance.</param>
    /// <param name="controllerScope">The <see cref="LifetimeScope"/> instance used to register each controller.</param>
    /// <returns></returns>
    public static IServiceRegister AddControllerActivation<TControllerActivator>(this IServiceRegister serviceRegister, LifetimeScope controllerScope = LifetimeScope.Transient)
        where TControllerActivator : class, IControllerActivator
    {
        // retrieve the IServiceCollection instance, previously added into local cache.
        var svcCollection = serviceRegister.Cache.Get<IServiceCollection>();

        var applicationPartManager = svcCollection.GetApplicationPartManager(nameof(AddControllerActivation));

        serviceRegister.RegisterMvcControllers(applicationPartManager, controllerScope)
            .Register<IControllerActivator, TControllerActivator>(LifetimeScope.Singleton)
            ;

        return serviceRegister;
    }

    /// <summary>
    /// Register all application's controllers in <see cref="IServiceRegister"/> instance, permitting <see cref="IServiceRegister"/> instance to create those controllers, using the given <see cref="LifetimeScope"/> instance.
    /// </summary>
    /// <param name="serviceRegister">The <see cref="IServiceRegister"/> instance.</param>
    /// <param name="controllerScope">The <see cref="LifetimeScope"/> instance used to register each controller.</param>
    /// <returns></returns>
    public static IServiceRegister AddControllerActivation(this IServiceRegister serviceRegister, LifetimeScope controllerScope = LifetimeScope.Transient)
    {
        return serviceRegister.AddControllerActivation<ServiceResolverControllerActivator>(controllerScope);
    }

    /// <summary>
    /// Gets the <see cref="ApplicationPartManager"/> instance, to use for controllers registration.
    /// </summary>
    /// <param name="services"></param>
    /// <param name="methodName"></param>
    /// <returns></returns>
    /// <exception cref="InvalidOperationException"></exception>
    private static ApplicationPartManager GetApplicationPartManager(
        this IServiceCollection services,
        string methodName)
    {
        if (!services.TryGetSingleton<ApplicationPartManager>(out var instance))
        {
            throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture,
                "A registration for the {0} is missing from the ASP.NET Core configuration system. This is most likely caused by a missing call to services.AddMvcCore() or services.AddMvc() as part of the ConfigureServices(IServiceCollection) method of the Startup class. A call to one of those methods will ensure the registration of the {1}.",
                typeof(ApplicationPartManager).FullName, nameof(ApplicationPartManager)));
        }

        return instance ?? throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture,
            "Although a registration for {0} exists in the ASP.NET Core configuration system, the registration is not added as an existing instance. This makes it impossible for service resolver's {1} method to get this instance from ASP.NET Core's IServiceCollection. This is most likely because {2} was overridden by you or a third-party library. Make sure that you use the AddSingleton overload that takes in an existing instance—i.e. call services.AddSingleton<{2}>(new {2}()).",
            typeof(ApplicationPartManager).FullName, methodName, nameof(ApplicationPartManager)));
    }

    /// <summary>
    /// Register all controllers.
    /// </summary>
    /// <param name="serviceRegister"></param>
    /// <param name="manager"></param>
    /// <param name="lifetime"></param>
    /// <returns></returns>
    private static IServiceRegister RegisterMvcControllers(
        this IServiceRegister serviceRegister,
        ApplicationPartManager manager,
        LifetimeScope lifetime)
    {
        var feature = new ControllerFeature();
        manager.PopulateFeature(feature);
        var types = feature.Controllers.Select(typeInfo => typeInfo.AsType());
        serviceRegister.RegisterControllerTypes(types, lifetime);

        return serviceRegister;
    }

    /// <summary>
    /// Register all controllers using the given <see cref="LifetimeScope"/> instance.
    /// </summary>
    /// <param name="serviceRegister"></param>
    /// <param name="types"></param>
    /// <param name="lifetime"></param>
    private static void RegisterControllerTypes(
        this IServiceRegister serviceRegister,
        IEnumerable<Type> types,
        LifetimeScope lifetime)
    {
        foreach (var type in types.ToArray())
        {
            var diagnostics = new SuppressDiagnostic
            {
                DisposableTransientComponent = lifetime == LifetimeScope.Transient && type.ShouldSuppressDisposingControllers()
            };

            //IRegistrationInfo registration;

            //if (type.TryGetServiceKeysFromCtorParams(out var keys))
            //{
            //    registration = lifetime.CreateRegistration(serviceProvider =>
            //    {
            //        var parameters = keys.Select(key => serviceProvider.GetService(key.Type, key.Key)).ToArray();
            //        return ActivatorUtilities.CreateInstance(serviceProvider.GetCurrentProvider(), type, parameters);

            //    }, diagnostics);
            //}
            //else
            //{
            //    registration = lifetime.CreateRegistration(type, diagnostics);
            //}

            var registration = lifetime.CreateRegistration(type, diagnostics);
            serviceRegister.Register(type, registration);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    private static bool ShouldSuppressDisposingControllers(this Type type)
    {
        var controllerTypeOrNull = type.GetBaseControllerTypeOrNull();
        return controllerTypeOrNull != null && type.GetProtectedDisposeMethod()?.DeclaringType == controllerTypeOrNull;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="controllerType"></param>
    /// <returns></returns>
    private static Type GetBaseControllerTypeOrNull(this Type controllerType)
    {
        const string mvcControllerName = "Microsoft.AspNetCore.Mvc.Controller";

        for (var baseType = controllerType.BaseType; baseType != null; baseType = baseType.BaseType)
        {
            if (baseType.FullName == mvcControllerName)
            {
                return baseType;
            }
        }

        return null;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="controllerType"></param>
    /// <returns></returns>
    private static MethodInfo GetProtectedDisposeMethod(this Type controllerType)
    {
        const string disposeMethodName = "Dispose";

        foreach (var method in controllerType.GetMethods(BindingFlags.Instance | BindingFlags.NonPublic))
        {
            if (!method.IsPrivate && !method.IsPublic && method.ReturnType == typeof(void) && method.Name == disposeMethodName && method.GetParameters().Length == 1 && method.GetParameters()[0].ParameterType == typeof(bool))
            {
                return method;
            }
        }

        return null;
    }

    /// <summary>
    /// Gets all service keys found in the <see cref="FromKeyedServicesAttribute"/> attribute parameter, present in the constructor.
    /// </summary>
    /// <param name="controllerType"></param>
    /// <param name="svcKeys"></param>
    /// <returns></returns>
    /// <exception cref="InvalidOperationException"></exception>
    private static bool TryGetServiceKeysFromCtorParams(this Type controllerType, out List<ServiceKey> svcKeys)
    {
        var ctors = controllerType.GetConstructors();

        svcKeys = null;

        if (ctors.Length > 1)
        {
            throw new InvalidOperationException($"The given controller ('{controllerType}') has many constructors, please define only one.");
        }

        var ctor = ctors[0];

        var parameters = ctor.GetParameters();

        foreach (var parameter in parameters)
        {
            var attribute = parameter.GetCustomAttribute<FromKeyedServicesAttribute>();

            if (attribute is null)
            {
                continue;
            }

            svcKeys ??= [];

            svcKeys.Add(new ServiceKey(parameter.ParameterType, attribute.Key));
        }

        return svcKeys?.Count > 0;
    }
}
