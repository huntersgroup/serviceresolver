using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.DependencyInjection;

namespace ServiceResolver.Ioc.AspNetCore.Mvc;

/// <summary>
/// Represents a custom implementation of <see cref="IControllerActivator"/>, enabling <see cref="IServiceProvider"/> instance create controller instances.
/// </summary>
public class ServiceResolverControllerActivator : IControllerActivator
{
    private readonly IServiceProvider serviceProvider;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serviceProvider"></param>
    public ServiceResolverControllerActivator(IServiceProvider serviceProvider)
    {
        this.serviceProvider = serviceProvider;
    }

    ///<inheritdoc />
    public object Create(ControllerContext context)
    {
        var controllerType = context.ActionDescriptor.ControllerTypeInfo.AsType();

        return this.serviceProvider.GetRequiredService(controllerType);
    }

    ///<inheritdoc />
    public void Release(ControllerContext context, object controller)
    {
        // no action is required here !!!
    }
}
