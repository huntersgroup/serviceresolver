# About this library

**ServiceResolver.GenericHost.AspNetCore** is a library for AspNetCore applications, which uses ServiceResolver.Ioc library.

Take this example defining a new **IServiceRegisterProvider** implementation (or other third party IServiceRegisterProvider implementation).

```csharp
class CustomServiceRegisterProvider : IServiceRegisterProvider
{
    public CustomServiceRegisterProvider(CustomServiceRegisterDescriptor descriptor)
    {
        // coding for setup.
    }

    // your custom implementation ..
}
```

## Configuration for AspNet applications (NET6 or higher)

### WebApplicationBuilder configuration

```csharp
var builder = WebApplication.CreateBuilder(args);

builder.Host
    // custom extension method to use for integration.
    .UseServiceResolver(new ServiceRegisterOptions<IServiceRegisterProvider>
    {
        // a factory used to create your service register provider
        OnCreating = collection =>
        {
            return new CustomServiceRegisterProvider(new CustomServiceRegisterDescriptor());
        },
        // (optional)
        // an action used to prepare (if needed) your IServiceCollection.
        BeforeRegistering = collection =>
        {
            collection.AddControllers();
        },
        // an action used to register your services.
        OnRegistering = provider =>
        {
            // example:
            serviceRegister
                .AddAspNetCore()
                .AddControllerActivation();
        },
        // (optional)
        // an action used to executes custom action at the end of all previous steps.
        AfterBuildingProvider = provider =>
        {
            provider.Verify();
        }
    });

var app = builder.Build();

// Configure the HTTP request pipeline.
app.UseAuthorization();
app.MapControllers();
await app.RunAsync();
```

### WebApplicationBuilder configuration with a function factory

```csharp
var builder = WebApplication.CreateBuilder(args);

builder.Host
    // custom extension method to use for integration.
    .UseServiceResolver(context => new ServiceRegisterOptions<IServiceRegisterProvider>
    {
        // a factory used to create your service register provider
        OnCreating = collection =>
        {
            return new CustomServiceRegisterProvider(new CustomServiceRegisterDescriptor());
        },
        // (optional)
        // an action used to prepare (if needed) your IServiceCollection.
        BeforeRegistering = collection =>
        {
            collection.AddControllers();
        },
        // an action used to register your services.
        OnRegistering = provider =>
        {
            // example:
            serviceRegister
                .AddAspNetCore()
                .AddControllerActivation();
        },
        // (optional)
        // an action used to executes custom action at the end of all previous steps.
        AfterBuildingProvider = provider =>
        {
            provider.Verify();
        }
    });

var app = builder.Build();

// Configure the HTTP request pipeline.
app.UseAuthorization();
app.MapControllers();
await app.RunAsync();
```
