using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;

namespace ServiceResolver.Ioc.AspNetCore.Http;

/// <summary>
/// Represents a wrapper for the default <see cref="IHttpContextFactory"/> instance, which uses the given <see cref="IServiceProvider"/> parameter to use internally to <see cref="HttpContext"/> instances.
/// </summary>
/// <param name="decoree"></param>
/// <param name="serviceProvider"></param>
internal class HttpContextFactoryDecorator(IHttpContextFactory decoree, IServiceProvider serviceProvider) : IHttpContextFactory
{
    ///<inheritdoc />
    public HttpContext Create(IFeatureCollection featureCollection)
    {
        var instance = decoree.Create(featureCollection);
        instance.RequestServices = serviceProvider;
        return instance;
    }

    ///<inheritdoc />
    public void Dispose(HttpContext httpContext)
    {
        decoree.Dispose(httpContext);
    }
}
