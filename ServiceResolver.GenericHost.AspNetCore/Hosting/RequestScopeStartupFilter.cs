using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace ServiceResolver.Ioc.AspNetCore.Hosting;

/// <summary>
/// Represents a custom implementation of <see cref="IStartupFilter"/> contract, enabling open / close <see cref="AsyncServiceScope"/> instances.
/// </summary>
public class RequestScopeStartupFilter : IStartupFilter
{
    private readonly IServiceProvider serviceProvider;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serviceProvider"></param>
    public RequestScopeStartupFilter(IServiceProvider serviceProvider)
    {
        this.serviceProvider = serviceProvider;
    }

    ///<inheritdoc />
    public Action<IApplicationBuilder> Configure(Action<IApplicationBuilder> next) => builder =>
    {
        this.ConfigureRequestScoping(builder);
        next(builder);
    };

    /// <summary>
    /// 
    /// </summary>
    /// <param name="builder"></param>
    private void ConfigureRequestScoping(IApplicationBuilder builder) => builder.Use(async (httpContext, next) =>
    {
        var scope = this.serviceProvider.CreateAsyncScope();

        try
        {
            await next();
        }
        finally
        {
            await scope.DisposeAsync();
        }

        scope = default;
    });
}
