using System.Reflection;
using ServiceResolver.Ioc.AspNetCore.Extensions;
using ServiceResolver.Ioc.Extensions;
using ServiceResolver.Ioc.Options;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Host
    .UseServiceResolver(new SimpleInjectorOptions
    {
        //ConstructorResolutionType = ResolutionType.Hybrid,
        IgnoreMissingRegistrations = true,
        BeforeRegistering = collection =>
        {
            collection.AddControllers();
        },
        OnRegistering = serviceRegister =>
        {
            serviceRegister
                .AddAspNetCore()
                .AddControllerActivation()
                ;

            serviceRegister.RegisterModulesFrom(Assembly.GetExecutingAssembly());
        },
        AfterBuildingProvider = provider =>
        {
            provider.Verify();
        }
    })
    ;

var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseAuthorization();

app.MapControllers();

await app.RunAsync();
