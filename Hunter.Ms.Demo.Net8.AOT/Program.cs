using System.Reflection;
using System.Text.Json.Serialization;
using Hunter.Ms.Demo.Models;
using Hunter.Ms.Demo.Routing;
using Hunter.Ms.Demo.Services;
using ServiceResolver.Ioc;
using ServiceResolver.Ioc.Extensions;
using ServiceResolver.Ioc.Options;

var builder = WebApplication.CreateSlimBuilder(args);

builder.Host
    .UseServiceResolver(context => new SimpleInjectorOptions
    {
        BeforeRegistering = collection =>
        {
            collection.AddAuthorization()
                .ConfigureHttpJsonOptions(options =>
                {
                    options.SerializerOptions.TypeInfoResolverChain.Add(AppWeatherJsonSerializerContext.Default);
                });
        },
        OnRegistering = serviceRegister =>
        {
            serviceRegister.Register(typeof(IMyCustomLogger<>), typeof(MyCustomLogger<>), LifetimeScope.Singleton);

            serviceRegister.RegisterModulesFrom(Assembly.GetExecutingAssembly());
        },
        AfterBuildingProvider = provider =>
        {
            provider.Verify();
        }
    })
    ;

var app = builder.Build();

app.UseAuthorization();

app.MapWeatherForecast();

await app.RunAsync();


[JsonSerializable(typeof(WeatherForecast[]))]
internal partial class AppWeatherJsonSerializerContext : JsonSerializerContext;
