using Hunter.Ms.Demo.Models;
using Hunter.Ms.Demo.Services;
using Microsoft.AspNetCore.Mvc;

namespace Hunter.Ms.Demo.Routing;

internal static class WeatherForecastMapGroup
{
    internal static WebApplication MapWeatherForecast(this WebApplication app)
    {
        var Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        var weatherGroup = app.MapGroup("/weatherforecast");

        weatherGroup.MapGet("/", ([FromServices] IMyCustomLogger<WeatherForecast> logger) =>
        {
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
                {
                    Date = DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
                    TemperatureC = Random.Shared.Next(-20, 55),
                    Summary = Summaries[Random.Shared.Next(Summaries.Length)]
                })
                .ToArray();
        });

        return app;
    }
}
