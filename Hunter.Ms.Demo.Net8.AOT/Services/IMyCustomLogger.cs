namespace Hunter.Ms.Demo.Services;

public interface IMyCustomLogger<out TService>
    where TService : class
{
    ILogger<TService> Logger { get; }
}
