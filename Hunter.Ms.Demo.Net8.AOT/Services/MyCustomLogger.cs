namespace Hunter.Ms.Demo.Services;

internal class MyCustomLogger<TService>(ILogger<TService> logger)
    : IMyCustomLogger<TService>
    where TService : class
{
    public ILogger<TService> Logger { get => logger; }
}
