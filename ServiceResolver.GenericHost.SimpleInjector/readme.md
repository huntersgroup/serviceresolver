# About this library

**ServiceResolver.GenericHost.SimpleInjector** is a library for .NET generic host, which uses **ServiceResolver.Ioc.SimpleInjector** library.

## Configuration for console applications (Net Core, Net 6 or higher)

### IHostBuilder configuration

```csharp
var hostBuilder = Host.CreateDefaultBuilder(args)
    // custom extension method to use for integration.
    .UseServiceResolver(new SimpleInjectorOptions
    {
        // (optional)
        BeforeRegistering = collection =>
        {
            // your IServiceCollection registrations or integrations if needed
        },
        // an action uses to register your services.
        OnRegistering = provider =>
        {
            // example:
            provider.Register<StringBuilder>(() => new StringBuilder("hello world.."));
        },
        // (optional)
        AfterBuildingProvider = provider =>
        {
            // an action uses to executes custom action at the end of all previous steps.
            provider.Verify();
        }
    });

var host = hostBuilder.Build();

await host.RunAsync();
```

### IHostBuilder configuration with a function factory

```csharp
var hostBuilder = Host.CreateDefaultBuilder(args)
    // custom extension method to use for integration.
    .UseServiceResolver(context => new SimpleInjectorOptions
    {
        // (optional)
        BeforeRegistering = collection =>
        {
            // your IServiceCollection registrations or integrations if needed
        },
        // an action uses to register your services.
        OnRegistering = provider =>
        {
            // example:
            provider.Register<StringBuilder>(() => new StringBuilder("hello world.."));
        },
        // (optional)
        AfterBuildingProvider = provider =>
        {
            // an action uses to executes custom action at the end of all previous steps.
            provider.Verify();
        }
    });

var host = hostBuilder.Build();

await host.RunAsync();
```

## Configuration for AspNet applications (NET6 or higher)

For the given examples, you need to install the [ServiceResolver.GenericHost.AspNetCore](https://www.nuget.org/packages/ServiceResolver.GenericHost.AspNetCore) nuget package.

### WebApplicationBuilder configuration

```csharp
var builder = WebApplication.CreateBuilder(args);

builder.Host
    .UseServiceResolver(new SimpleInjectorOptions
    {
        //ConstructorResolutionType = ResolutionType.Hybrid,
        IgnoreMissingRegistrations = true,
        BeforeRegistering = collection =>
        {
            collection.AddControllers();
        },
        OnRegistering = serviceRegister =>
        {
            // minimal configuration for aspnet
            serviceRegister
                .AddAspNetCore()
                .AddControllerActivation()
                ;
        },
        AfterBuildingProvider = provider =>
        {
            provider.Verify();
        }
    })
    ;

var app = builder.Build();

// Configure the HTTP request pipeline.
app.UseAuthorization();
app.MapControllers();
await app.RunAsync();
```

### WebApplicationBuilder configuration with a function factory

```csharp
var builder = WebApplication.CreateBuilder(args);

builder.Host
    .UseServiceResolver(context => new SimpleInjectorOptions
    {
        //ConstructorResolutionType = ResolutionType.Hybrid,
        IgnoreMissingRegistrations = true,
        BeforeRegistering = collection =>
        {
            collection.AddControllers();
        },
        OnRegistering = serviceRegister =>
        {
            // minimal configuration for aspnet
            serviceRegister
                .AddAspNetCore()
                .AddControllerActivation()
                ;
        },
        AfterBuildingProvider = provider =>
        {
            provider.Verify();
        }
    })
    ;

var app = builder.Build();

// Configure the HTTP request pipeline.
app.UseAuthorization();
app.MapControllers();
await app.RunAsync();
```
