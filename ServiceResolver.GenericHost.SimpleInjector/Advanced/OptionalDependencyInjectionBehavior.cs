using System;
using SimpleInjector;
using SimpleInjector.Advanced;

namespace ServiceResolver.Ioc.Advanced;

/// <summary>
/// Represents a hybrid behavior to inject dependencies.
/// </summary>
/// <param name="container"></param>
/// <param name="decoree"></param>
/// <param name="hybrid">if true so instances retrieve the underlying <see cref="IConstructorResolutionBehavior"/> instance injecting some action if implements <see cref="IHybridResolutionBehavior"/></param>
public class OptionalDependencyInjectionBehavior(Container container, IDependencyInjectionBehavior decoree, bool hybrid = false)
    : IDependencyInjectionBehavior
{
    private readonly Lazy<Func<Type, bool>> optionalParameterEnabled = new(() =>
    {
        if (hybrid && container.Options.ConstructorResolutionBehavior is IHybridResolutionBehavior ctorBeh)
        {
            return type => ctorBeh.ContainsType(type);
        }

        // always activated.
        return _ => true;

    });

    ///<inheritdoc />
    public bool VerifyDependency(InjectionConsumerInfo dependency, out string errorMessage)
    {
        var ret = decoree.VerifyDependency(dependency, out errorMessage);
        return ret;
    }

    ///<inheritdoc />
    public InstanceProducer GetInstanceProducer(InjectionConsumerInfo dependency, bool throwOnFailure)
    {
        var parameter = dependency.Target.Parameter;

        try
        {
            return decoree.GetInstanceProducer(dependency, throwOnFailure);
        }
        catch (Exception e)
        {
            var activated = this.optionalParameterEnabled.Value(dependency.ImplementationType);

            if (activated && parameter is { IsOptional: true })
            {
                var instanceProducer = parameter.BuildDefaultProducer(container);

                return instanceProducer;
            }

            throw new InvalidOperationException(
                "It was a problem on creating the instance producer, see inner exception for details.", e);
        }
    }
}
