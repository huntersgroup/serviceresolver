using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using SimpleInjector.Advanced;

namespace ServiceResolver.Ioc.Advanced;

/// <summary>
/// Represents a hybrid way to resolve service constructors behaviour, using the main resolution behavior by all registrations present in <see cref="IServiceCollection"/> instance.
/// </summary>
internal class HybridConstructorResolutionBehavior : IConstructorResolutionBehavior, IHybridResolutionBehavior
{
    private readonly IConstructorResolutionBehavior mainCtorResolutionBehavior;
    private readonly IConstructorResolutionBehavior defaultCtorResolutionBehavior;
    private readonly IServiceCollection collection;
    private List<Type> types;

    /// <summary>
    /// Creates a new <see cref="HybridConstructorResolutionBehavior"/> instance.
    /// </summary>
    /// <param name="mainCtorResolutionBehavior">The main resolution behaviour used for all service registrations present into <see cref="collection"/> parameter.</param>
    /// <param name="defaultCtorResolutionBehavior">The fallback resolution behaviour used for all rest service registrations, not present into <see cref="collection"/> parameter.</param>
    /// <param name="collection">All registrations used by <see cref="mainCtorResolutionBehavior"/> parameter.</param>
    /// <exception cref="ArgumentNullException"></exception>
    public HybridConstructorResolutionBehavior(IConstructorResolutionBehavior mainCtorResolutionBehavior,
        IConstructorResolutionBehavior defaultCtorResolutionBehavior,
        IServiceCollection collection)
    {
        this.mainCtorResolutionBehavior = mainCtorResolutionBehavior ?? throw new ArgumentNullException(nameof(mainCtorResolutionBehavior));
        this.defaultCtorResolutionBehavior = defaultCtorResolutionBehavior ?? throw new ArgumentNullException(nameof(defaultCtorResolutionBehavior));
        this.collection = collection;
    }

    ///<inheritdoc />
    public ConstructorInfo TryGetConstructor(Type implementationType, out string errorMessage)
    {
        this.types ??= this.collection
            .Select(descriptor => descriptor.ImplementationType).Where(type => type is not null)
            .Distinct()
            .ToList();

        return this.ContainsType(implementationType)
            ? this.mainCtorResolutionBehavior.TryGetConstructor(implementationType, out errorMessage)
            : this.defaultCtorResolutionBehavior.TryGetConstructor(implementationType, out errorMessage);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="implementationType"></param>
    /// <returns></returns>
    public bool ContainsType(Type implementationType)
    {
        // if it's generic, it tries to verify if exists in the current types field.
        return this.types.Contains(implementationType) ||
               (implementationType.IsGenericType && this.types.Contains(implementationType.GetGenericTypeDefinition()));
    }
}
