using System;
using ServiceResolver.Ioc.Options;
using SimpleInjector;
using SimpleInjector.Advanced;

namespace ServiceResolver.Ioc.Advanced;

/// <summary>
/// Represents a hybrid behavior to inject dependencies.
/// </summary>
/// <param name="container">The container which belongs this instance.</param>
/// <param name="decoree">The original or default <see cref="IDependencyInjectionBehavior"/> instance.</param>
/// <param name="options">The options for this instance.</param>
public class HybridDependencyInjectionBehavior(Container container, IDependencyInjectionBehavior decoree, InjectionBehaviorOptions options)
    : IDependencyInjectionBehavior
{
    private readonly Lazy<Func<Type, bool>> optionalParameterEnabled = new(() =>
    {
        switch (options.OptionalParameters)
        {
            case ResolutionType.LessRestrictive:
            {
                return _ => true;
            }
            case ResolutionType.Hybrid:
            {
                if (container.Options.ConstructorResolutionBehavior is IHybridResolutionBehavior ctorBeh)
                {
                    return type => ctorBeh.ContainsType(type);
                }

                return _ => false;
            }
            default:
            {
                return _ => false;
            }
        }
    });

    private readonly Lazy<Func<Type, bool>> missingManyRegistrationEnabled = new(() =>
    {
        switch (options.MissingManyRegistrations)
        {
            case ResolutionType.LessRestrictive:
            {
                return _ => true;
            }
            case ResolutionType.Hybrid:
            {
                if (container.Options.ConstructorResolutionBehavior is IHybridResolutionBehavior ctorBeh)
                {
                    return type => ctorBeh.ContainsType(type);
                }

                return _ => false;
            }
            default:
            {
                return _ => false;
            }
        }
    });

    ///<inheritdoc />
    public bool VerifyDependency(InjectionConsumerInfo dependency, out string errorMessage)
    {
        var verified = decoree.VerifyDependency(dependency, out errorMessage);
        return verified;
    }

    ///<inheritdoc />
    public InstanceProducer GetInstanceProducer(InjectionConsumerInfo dependency, bool throwOnFailure)
    {
        var parameter = dependency.Target.Parameter;

        try
        {
            return decoree.GetInstanceProducer(dependency, throwOnFailure)
                   ?? this.GetEmptyCollectionProducer(dependency)
                   ;
        }
        catch (Exception e)
        {
            var activated = this.optionalParameterEnabled.Value(dependency.ImplementationType);

            if (activated && parameter is { IsOptional: true })
            {
                var instanceProducer = parameter.BuildDefaultProducer(container);

                return instanceProducer;
            }

            return this.GetEmptyCollectionProducer(dependency)
                   ?? throw new InvalidOperationException(
                       "It was a problem on creating the instance producer, see inner exception for details.", e);

            //throw new InvalidOperationException(
            //    "It was a problem on creating the instance producer, see inner exception for details.", e);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="dependency"></param>
    /// <returns></returns>
    private InstanceProducer GetEmptyCollectionProducer(InjectionConsumerInfo dependency)
    {
        var parameter = dependency.Target.Parameter;

        var activated = this.missingManyRegistrationEnabled.Value(dependency.ImplementationType);

        if (container.IsLocked && activated && parameter.TryBuildEmptyEnumerableProducer(container, out var producer))
        {
            return producer;
        }

        return null;
    }
}
