using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using ServiceResolver.Ioc.Extensions;
using SimpleInjector;

namespace ServiceResolver.Ioc.Advanced;

/// <summary>
/// 
/// </summary>
public static class ReflectionExtensions
{
    private static readonly Dictionary<Type, object> DefaultValues = [];
    private static readonly Dictionary<Type, object> EmptyCollections = [];

    /// <summary>
    /// 
    /// </summary>
    /// <param name="info"></param>
    /// <returns></returns>
    public static object GetDefaultValue(this ParameterInfo info)
    {
        return info.HasDefaultValue ? info.DefaultValue : info.ParameterType.GetDefaultValue();
    }

    /// <summary>
    /// Builds a new producer, using a default value related to the given <see cref="ParameterInfo.ParameterType"/> value.
    /// </summary>
    /// <param name="info">The parameter info.</param>
    /// <param name="container">The container instance.</param>
    /// <returns></returns>
    public static InstanceProducer BuildDefaultProducer(this ParameterInfo info, Container container)
    {
        var expression = Expression.Constant(info.GetDefaultValue(), info.ParameterType);

        return InstanceProducer.FromExpression(info.ParameterType, expression, container);
    }

    /// <summary>
    /// Gets the default value for the given <see cref="Type"/> instance.
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public static object GetDefaultValue(this Type type)
    {
        if (DefaultValues.TryGetValue(type, out var val))
        {
            return val;
        }

        var expression = Expression.Lambda<Func<object>>(Expression.Default(type));
        val = expression.Compile().Invoke();

        DefaultValues.Add(type, val);

        return val;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="type"></param>
    /// <param name="enumerable"></param>
    /// <returns></returns>
    public static bool TryGetEmptyEnumerable(this Type type, out object enumerable)
    {
        // todo: try to replace with type.AsEmptyCollection() extension instead.

        if (EmptyCollections.TryGetValue(type, out enumerable))
        {
            return true;
        }

        if (!type.IsGenEnumerable())
        {
            return false;
        }

        //var openGenericMethod = typeof(Enumerable).GetMethod("Empty");
        //var closeGenericMethod = openGenericMethod?.MakeGenericMethod(type.GetGenericArguments());
        //enumerable = closeGenericMethod?.Invoke(obj: null, null);

        enumerable = type.GetGenericArguments()[0].AsEmptyCollection();

        if (enumerable == null)
        {
            return false;
        }

        EmptyCollections.Add(type, enumerable);
        return true;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="info"></param>
    /// <param name="container"></param>
    /// <param name="producer"></param>
    /// <returns></returns>
    public static bool TryBuildEmptyEnumerableProducer(this ParameterInfo info, Container container, out InstanceProducer producer)
    {
        var parameterType = info.ParameterType;

        if (parameterType.TryGetEmptyEnumerable(out var enumerable))
        {
            if (enumerable != null)
            {
                var expression = Expression.Constant(enumerable, parameterType);
                producer = InstanceProducer.FromExpression(parameterType, expression, container);

                return true;
            }
        }

        producer = null;

        return false;
    }
}
