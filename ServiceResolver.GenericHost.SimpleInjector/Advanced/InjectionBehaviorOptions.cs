using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using ServiceResolver.Ioc.Options;
using SimpleInjector;

namespace ServiceResolver.Ioc.Advanced;

/// <summary>
/// Represents a custom features used to configure <see cref="ContainerOptions.DependencyInjectionBehavior"/> instance.
/// </summary>
public class InjectionBehaviorOptions
{
    /// <summary>
    /// Gets or sets the dependency injection behaviour to adopt whenever a missing dependency is an optional parameter.
    /// <code>
    /// examples:
    ///     <see cref="ResolutionType.Standard"/> uses default injection behavior (no default parameter value is resolved).
    ///     <see cref="ResolutionType.LessRestrictive"/> resolves all optional parameter, registering with default value.
    ///     <see cref="ResolutionType.Hybrid"/> is like <see cref="ResolutionType.LessRestrictive"/> strategy, but only for <see cref="IServiceCollection"/> registrations.
    /// </code>
    /// <para>
    /// The default resolution is <see cref="ResolutionType.Hybrid"/>.
    /// </para>
    /// </summary>
    public ResolutionType OptionalParameters { get; init; } = ResolutionType.Hybrid;

    /// <summary>
    /// Gets or sets the dependency injection behavior to adopt whenever a <see cref="IEnumerable{T}"/> type could be resolved, whenever they aren't registered.
    /// <code>
    /// examples:
    ///     <see cref="ResolutionType.Standard"/> uses default injection behavior (no empty enumerable is resolved).
    ///     <see cref="ResolutionType.LessRestrictive"/> computes all default resolutions, registering empty <see cref="IEnumerable{T}"/> instances.
    ///     <see cref="ResolutionType.Hybrid"/> is like <see cref="ResolutionType.LessRestrictive"/> strategy, but only for <see cref="IServiceCollection"/> registrations.
    /// </code>
    /// <para>
    /// The default resolution is <see cref="ResolutionType.Hybrid"/>.
    /// </para>
    /// </summary>
    public ResolutionType MissingManyRegistrations { get; init; } = ResolutionType.Hybrid;
}
