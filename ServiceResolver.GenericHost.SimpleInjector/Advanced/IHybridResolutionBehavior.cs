using System;

namespace ServiceResolver.Ioc.Advanced;

/// <summary>
/// 
/// </summary>
public interface IHybridResolutionBehavior
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="implementationType"></param>
    /// <returns></returns>
    bool ContainsType(Type implementationType);
}
