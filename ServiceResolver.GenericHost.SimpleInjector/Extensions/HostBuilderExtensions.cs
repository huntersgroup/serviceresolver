using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ServiceResolver.Ioc.SimpleInjector;
using ServiceResolver.Ioc.SimpleInjector.Advanced;
using ServiceResolver.Ioc.Advanced;
using ServiceResolver.Ioc.Options;
using ServiceResolver.Ioc.SimpleInjector.Extensions;
using SimpleInjector;

namespace ServiceResolver.Ioc.Extensions;

/// <summary>
/// 
/// </summary>
public static class HostBuilderExtensions
{
    /// <summary>
    /// Represents an extension method to use for building a custom <see cref="IServiceProvider"/> instance, using the given <see cref="SimpleInjectorOptions"/> instance.
    /// <para>
    /// This is a convenient way to set up a <see cref="IServiceRegisterProvider"/> instance, which underlying implementation is for SimpleInjector.
    /// </para>
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="options"></param>
    /// <returns></returns>
    public static IHostBuilder UseServiceResolver(this IHostBuilder builder, SimpleInjectorOptions options)
    {
        builder.UseServiceResolver(options.AsRegisterOptions());

        return builder;
    }

    /// <summary>
    /// Represents an extension method to use for building a custom <see cref="IServiceProvider"/> instance, using the given <see cref="SimpleInjectorOptions"/> instance.
    /// <para>
    /// This is a convenient way to set up a <see cref="IServiceRegisterProvider"/> instance, which underlying implementation is for SimpleInjector.
    /// </para>
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="optionsFactory"></param>
    /// <returns></returns>
    public static IHostBuilder UseServiceResolver(this IHostBuilder builder, Func<HostBuilderContext, SimpleInjectorOptions> optionsFactory)
    {
        builder.UseServiceResolver(context => optionsFactory(context).AsRegisterOptions());

        return builder;
    }

    /// <summary>
    /// Converts the given <see cref="SimpleInjectorOptions"/> instance in a <see cref="ServiceRegisterOptions{IServiceRegisterProvider}"/> instance.
    /// </summary>
    /// <param name="options"></param>
    /// <returns></returns>
    private static ServiceRegisterOptions<IServiceRegisterProvider> AsRegisterOptions(this SimpleInjectorOptions options)
    {
        return new ServiceRegisterOptions<IServiceRegisterProvider>
        {
            AutoRegistration = options.AutoRegistration,
            IgnoreMissingRegistrations = options.IgnoreMissingRegistrations,
            SuppressDiagnosticType = options.SuppressDiagnosticType,
            OnCreating = collection => collection.OnServiceRegisterCreation(options),
            BeforeRegistering = collection =>
            {
                // redo the action in order to add custom actions
                options.BeforeRegistering?.Invoke(collection);
            },
            OnRegistering = provider =>
            {
                if (options.HostSettings is not null && options.HostSettings.Enabled)
                {
                    provider.RegisterHost(options.HostSettings);
                }

                options.OnRegistering?.Invoke(provider);
            },
            AfterBuildingProvider = provider =>
            {
                // redo the action in order to add custom actions
                options.AfterBuildingProvider?.Invoke(provider);
            }
        };
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="collection"></param>
    /// <param name="options"></param>
    /// <returns></returns>
    private static IServiceRegisterProvider OnServiceRegisterCreation(this IServiceCollection collection, SimpleInjectorOptions options)
    {
        var serviceResolver = new ServiceRegisterProvider(new ServiceRegisterDescriptor
        {
            Initializer = container =>
            {
                var containerOpt = container.Options;

                containerOpt.UseFullyQualifiedTypeNames = true;

                if (options.ScopedLifestyle is not null)
                {
                    containerOpt.DefaultScopedLifestyle = options.ScopedLifestyle;
                }

                if (options.Lifestyle is not null)
                {
                    containerOpt.DefaultLifestyle = options.Lifestyle;
                }

                if (options.SuppressDiagnosticType == ResolutionType.LessRestrictive)
                {
                    containerOpt.EnableAutoVerification = false;
                    containerOpt.SuppressLifestyleMismatchVerification = true;
                }

                containerOpt.ConstructorResolutionBehavior = options.ConstructorResolutionType switch
                {
                    ResolutionType.LessRestrictive => new BestConstructorResolutionBehavior(container),
                    ResolutionType.Hybrid => new HybridConstructorResolutionBehavior(
                        new BestConstructorResolutionBehavior(container), containerOpt.ConstructorResolutionBehavior,
                        collection),
                    _ => containerOpt.ConstructorResolutionBehavior
                };

                // default DependencyInjectionBehavior instance.
                var depInjectionBehaviorDef = containerOpt.DependencyInjectionBehavior;

                containerOpt.DependencyInjectionBehavior =
                    new HybridDependencyInjectionBehavior(container,
                        depInjectionBehaviorDef,
                        options.InjectionBehaviorOptions);

                if (options.IgnoreMissingRegistrations)
                {
                    container.ResolveUnregisteredType += (_, args) => OnResolveUnregisteredCollectionTypes(container, args);
                }

                // the last action on option is executed here.
                // it could override the current container setup previously done.
                options.ContainerInit?.Invoke(container);
            }
        });

        return serviceResolver;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="container"></param>
    /// <param name="args"></param>
    private static void OnResolveUnregisteredCollectionTypes(Container container, UnregisteredTypeEventArgs args)
    {
        if (args.Handled)
        {
            return;
        }

        var unregisteredType = args.UnregisteredServiceType;

        if (unregisteredType.IsGenEnumerable())
        {
            var unregisteredGenericArg = unregisteredType.GetGenericArguments()[0];

            var registration = container.Collection.CreateRegistration(unregisteredGenericArg, Enumerable.Empty<Type>());

            args.Register(registration);
        }
    }
}
