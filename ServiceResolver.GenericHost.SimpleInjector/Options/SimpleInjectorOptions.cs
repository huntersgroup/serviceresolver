using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ServiceResolver.Ioc.Advanced;
using SimpleInjector.Lifestyles;
using SimpleInjector;
using ServiceResolver.Ioc.Hosting;
using ServiceResolver.Ioc.SimpleInjector.Advanced;

namespace ServiceResolver.Ioc.Options;

/// <summary>
/// Represents the current options to build a new <see cref="IServiceProviderFactory{TContainerBuilder}"/>
/// </summary>
public class SimpleInjectorOptions : IServiceRegisterOptions<IServiceRegisterProvider>
{
    /// <summary>
    /// Gets or sets the boolean value indicating if the <see cref="IServiceProvider"/> instance will be registered by itself.
    /// </summary>
    public bool AutoRegistration { get; init; } = true;

    ///<inheritdoc />
    public bool IgnoreMissingRegistrations { get; init; } = true;

    /// <summary>
    /// Gets or sets the boolean value indicating if a custom <see cref="IHost"/> must be used, instead of the standard on
    /// </summary>
    public HostSettings HostSettings { get; init; } = new();

    /// <summary>
    /// Gets or sets the constructor resolution behavior to adopt whenever a service type should be built.
    /// <example>
    /// <code>
    /// examples:
    /// 
    ///     <see cref="ResolutionType.Standard"/> uses default constructor behavior (unique public constructor).
    ///     <see cref="ResolutionType.LessRestrictive"/> uses <see cref="BestConstructorResolutionBehavior"/> strategy.
    ///     <see cref="ResolutionType.Hybrid"/> uses <see cref="HybridConstructorResolutionBehavior"/> strategy.
    /// </code>
    /// </example>
    /// <para>
    /// The default resolution is <see cref="ResolutionType.Hybrid"/>.
    /// </para>
    /// </summary>
    public ResolutionType ConstructorResolutionType { get; init; } = ResolutionType.Hybrid;

    /// <summary>
    /// Gets or sets the strategy which diagnostics are suppressed.
    /// <code>
    /// examples:
    ///     <see cref="ResolutionType.Standard"/> uses default behavior (all diagnostics are enabled).
    ///     <see cref="ResolutionType.LessRestrictive"/> disables all diagnostics.
    ///     <see cref="ResolutionType.Hybrid"/> is like <see cref="ResolutionType.LessRestrictive"/> strategy, but only for <see cref="IServiceCollection"/> registrations.
    /// </code>
    /// <para>
    /// The default resolution is <see cref="ResolutionType.Hybrid"/>.
    /// </para>
    /// </summary>
    public ResolutionType SuppressDiagnosticType { get; init; } = ResolutionType.Hybrid;

    /// <summary>
    /// Gets or sets the custom options for dependencies injection resolution.
    /// </summary>
    public InjectionBehaviorOptions InjectionBehaviorOptions { get; init; } = new();

    /// <summary>
    /// Gets or sets the default LifeStyle.
    /// </summary>
    public Lifestyle Lifestyle { get; init; }

    /// <summary>
    /// Gets or sets the default ScopedLifeStyle, used whenever services are resolved inside to scopes.
    /// </summary>
    public ScopedLifestyle ScopedLifestyle { get; init; } = new AsyncScopedLifestyle();

    /// <summary>
    /// Gets or sets the custom action used to initialize the underlying container.
    /// <para>
    /// This action could override completely previous setup done by this instance (like <see cref="ConstructorResolutionType"/>, <see cref="Lifestyle"/> and <see cref="ScopedLifestyle"/> parameters.)
    /// </para>
    /// </summary>
    public Action<Container> ContainerInit { get; init; }

    /// <summary>
    /// A custom action used to prepare or change <see cref="IServiceCollection"/> instance before <see cref="OnRegistering"/> action.
    /// <para>
    /// After execution of this action, the <see cref="IServiceCollection"/> instance becomes read-only.
    /// </para>
    /// </summary>
    public Action<IServiceCollection> BeforeRegistering { get; init; }

    /// <summary>
    /// Gets or sets the custom action used to register all services into <see cref="IServiceRegisterProvider"/> instance.
    /// <para>
    /// On this step, the <see cref="IServiceCollection"/> parameter is read-only, if it's needed to add / remove / change service registrations
    /// it can be done using the <see cref="BeforeRegistering"/> action.
    /// </para>
    /// </summary>
    public Action<IServiceRegisterProvider> OnRegistering { get; init; }

    /// <summary>
    /// Gets or sets the custom action used to execute at the end of building the <see cref="IServiceProvider"/> instance.
    /// </summary>
    public Action<IServiceRegisterProvider> AfterBuildingProvider { get; init; }
}
